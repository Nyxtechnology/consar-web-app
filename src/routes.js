import {generateAsyncRouteComponent as ar} from './helpers/rrv4Helpers.js'

const home = ar({name: 'Home', loader: () => import('./containers/Home/' /* webpackChunkName: "Home" */)})

const jovenes = ar({name: 'Jovenes', loader: () => import('./containers/Jovenes/' /* webpackChunkName: "Jovenes" */)})
const adultos = ar({name: 'Adultos', loader: () => import('./containers/Adultos/' /* webpackChunkName: "Adultos" */)})
const mayores =  ar({name: 'Mayores', loader: () => import('./containers/Mayores/' /* webpackChunkName: "Mayores" */)})
const mapeo = ar({name: 'Mapeo', loader: () => import('./containers/Itinerario/' /* webpackChunkName: "Mapeo" */)})
const referencias = ar({name: 'Referencias', loader: () => import('./containers/Referencias/' /* webpackChunkName: "Referencias" */)})
const afore = ar({name: 'Afore', loader: () => import('./containers/Afore/' /* webpackChunkName: "Afore" */)})
const guia = ar({name: 'Guia', loader: () => import('./containers/Guia/' /* webpackChunkName: "Guia" */)})
const calculadora = ar({name: 'Calculadora', loader: () => import('./containers/Calculadora/' /* webpackChunkName: "Calculadora" */)})
const mobile = ar({name: 'Mobile', loader: () => import('./containers/Mobile/' /* webpackChunkName: "Mobile" */)})
const mobileFaq = ar({name: 'MobileFaq', loader: () => import('./containers/Faq/' /* webpackChunkName: "MobileFaq" */)})
const mobileContact = ar({name: 'MobileContact', loader: () => import('./containers/Mobile/Contact' /* webpackChunkName: "MobileContact" */)})

const proyectos = ar({name: 'Proyectos', loader: () => import('./containers/Destinos/' /* webpackChunkName: "Proyectos" */)})
const proyectosCasa = ar({name: 'proyectosCasa', loader: () => import('./containers/Destinos/Casa' /* webpackChunkName: "proyectosCasa" */)})
const proyectosNegocio = ar({name: 'proyectosNegocio', loader: () => import('./containers/Destinos/Negocio' /* webpackChunkName: "proyectosNegocio" */)})
const proyectosAuto = ar({name: 'proyectosAuto', loader: () => import('./containers/Destinos/Auto' /* webpackChunkName: "proyectosAuto" */)})
const proyectosIndepedencia = ar({name: 'proyectosIndepedencia', loader: () => import('./containers/Destinos/Independencia' /* webpackChunkName: "proyectosIndepedencia" */)})
const proyectosLaboral = ar({name: 'proyectosLaboral', loader: () => import('./containers/Destinos/Laboral' /* webpackChunkName: "proyectosLaboral" */)})
const proyectosPension = ar({name: 'proyectosPension', loader: () => import('./containers/Destinos/Pension' /* webpackChunkName: "proyectosPension" */)})
const proyectosSalud = ar({name: 'proyectosPersonal', loader: () => import('./containers/Destinos/Salud' /* webpackChunkName: "proyectosSalud" */)})
const proyectosPersonal = ar({name: 'proyectosPersonal', loader: () => import('./containers/Destinos/Personal' /* webpackChunkName: "proyectosPersonal" */)})
const proyectosEstudios = ar({name: 'proyectosEstudios', loader: () => import('./containers/Destinos/Estudios' /* webpackChunkName: "proyectosEstudios" */)})
const proyectosPareja = ar({name: 'proyectosPareja', loader: () => import('./containers/Destinos/Pareja' /* webpackChunkName: "proyectosPareja" */)})
const proyectosFamilia = ar({name: 'proyectosFamilia', loader: () => import('./containers/Destinos/Familia' /* webpackChunkName: "proyectosFamilia" */)})
const proyectosViaje = ar({name: 'proyectosViaje', loader: () => import('./containers/Destinos/Viaje' /* webpackChunkName: "proyectosViaje" */)})
const proyectosPasatiempo = ar({name: 'proyectosPasatiempo', loader: () => import('./containers/Destinos/Pasatiempo' /* webpackChunkName: "proyectosPasatiempo" */)})
const proyectosLegado = ar({name: 'proyectosLegado', loader: () => import('./containers/Destinos/Legado' /* webpackChunkName: "proyectosLegado" */)})
const proyectosImpulsar = ar({name: 'proyectosImpulsar', loader: () => import('./containers/Destinos/Impulsar' /* webpackChunkName: "proyectosImpulsar" */)})
const proyectosOtro = ar({name: 'proyectosOtro', loader: () => import('./containers/Destinos/Otro' /* webpackChunkName: "proyectosOtro" */)})

const segmentRoute = arr => arr.reduce((a, b) => ([
  ...a,
  {...b, path: '/jovenes' + b.path},
  {...b, path: '/mayores' + b.path},
  {...b, path: '/adultos' + b.path}
]), [])

const routes = [
  {path: '/', exact: true, component: home},
  {path: '/jovenes', exact: true, component: jovenes},
  {path: '/adultos', exact: true, component: adultos},
  {path: '/mayores', exact: true, component: mayores},
  {path: '/app', exact: true, component: mobile},
  {path: '/app/faq', exact: true, component: mobileFaq},
  {path: '/app/contacto', exact: true, component: mobileContact},
  ...segmentRoute([
    {path: '/mapeo', exact: true, component: mapeo},
    {path: '/referencias', exact: true, component: referencias},
    {path: '/referencias/:name', exact: true, component: referencias},
    {path: '/afore', exact: true, component: afore},
    {path: '/guia', exact: true, component: guia},
    {path: '/calculadora', exact: true, component: calculadora},
    {path: '/proyectos', exact: true, component: proyectos},
    {path: '/proyectos/casa', exact: true, component: proyectosCasa},
    {path: '/proyectos/negocio', exact: true, component: proyectosNegocio},
    {path: '/proyectos/auto', exact: true, component: proyectosAuto},
    {path: '/proyectos/independencia', exact: true, component: proyectosIndepedencia},
    {path: '/proyectos/laboral', exact: true, component: proyectosLaboral},
    {path: '/proyectos/pension', exact: true, component: proyectosPension},
    {path: '/proyectos/salud', exact: true, component: proyectosSalud},
    {path: '/proyectos/personal', exact: true, component: proyectosPersonal},
    {path: '/proyectos/estudios', exact: true, component: proyectosEstudios},
    {path: '/proyectos/pareja', exact: true, component: proyectosPareja},
    {path: '/proyectos/familia', exact: true, component: proyectosFamilia},
    {path: '/proyectos/viaje', exact: true, component: proyectosViaje},
    {path: '/proyectos/pasatiempo', exact: true, component: proyectosPasatiempo},
    {path: '/proyectos/legado', exact: true, component: proyectosLegado},
    {path: '/proyectos/impulsar', exact: true, component: proyectosImpulsar},
    {path: '/proyectos/otro', exact: true, component: proyectosOtro},
  ]),
]

export default routes
