import {getReferences} from '../helpers/services'

const referencesLoadSuccess = data => ({type: 'referencesLoadSuccess', data})
export const referencesLoad = () => (dispatch, getState) => {
  if (getState().references.list.length) return
  return getReferences()
  .then(e => dispatch(referencesLoadSuccess(e.data)))
}

const referenceLoadSuccess = data => ({type: 'referenceLoadSuccess', data})
export const referenceLoad = (id) => (dispatch, getState) => {
  return getReferences(id)
  .then(e => dispatch(referenceLoadSuccess(e.data)))
}
