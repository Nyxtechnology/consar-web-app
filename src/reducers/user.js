import {SET_USER, DELETE_USER} from '../constants/actionTypes'

const initialState = {
  email: '',
  exp: null,
  municipality_id: null,
  username: '',
  is_pro: false,
  profilePic: '',
  name: '',
  alpha: false
}

export default function user (state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      return {...state, ...action.user}
    case DELETE_USER:
      return initialState
    default:
      return state
  }
}
