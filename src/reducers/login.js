const initialState = {
  process: null,
  err: null,
  email: '',
  reemail: '',
  firstname: '',
  lastname: '',
  password: '',
  repassword: '',
  file: {}
}

export default function login (state = initialState, action) {
  switch (action.type) {
    case 'LOGIN_CHANGE':
      return {...state, ...action.data, err: null}
    case 'LOGIN_RESET':
      return initialState
    case 'LOGIN_ATTEMPT':
      return {...state, process: true, err: null}
    case 'LOGIN_SUCCESS':
      return initialState
    case 'LOGIN_ERROR':
      return {
        ...state,
        process: null,
        err: action.error,
        credential: action.error.code === 'auth/account-exists-with-different-credential' ? action.error.credential : null
      }
    default:
      return state
  }
}
