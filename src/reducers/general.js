const initialState = {
  process: null,
  modal: '',
  value: null,
  step: 0,
  h: 0,
  w: 0,
  videos: [true, true, true]
}

export default function general (state = initialState, action) {
  switch (action.type) {
    case 'GENERAL_CHANGE':
      return {...state, ...action.state}
    case 'SETUSER':
      return {...state, process: false, user: action.user}
    case 'setModal':
      return {...state, ...action.data}
    case 'closeAll':
      return {...state, modal: null, value: null}
    case 'GENERAL_RESET':
      return initialState
    case 'setDimensions':
      return {...state, h: action.h, w: action.w}
    case 'closeVideo':
      return {...state, videos: state.videos.map((v, i) => i === action.data ? false : v)}
    case 'setStep':
      return {...state, step: action.data.step, startTime: action.data.startTime}
    default:
      return state
  }
}
