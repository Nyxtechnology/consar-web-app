import compute from '../helpers/calculator'

const init = {
  period: 20,
  frequency: 52,
  initialInput: 1000,
  periodicalInput: 100,
  interest: 3
}
const initialState = {
  ...init,
  view: 0,
  totalSaving: 0,
  totalInterest: 0,
  totalCapital: 0,
  history: [],
  ...compute(init)
}

export default function calculator (state = initialState, action) {
  switch (action.type) {
    case 'calculatorChange':
      return {...state, ...action.data, ...compute({...state, ...action.data})}
    case 'setView':
      return {...state, view: action.data}
    default:
      return state
  }
}
