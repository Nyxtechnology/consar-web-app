const initialState = {
  process: null,
  list: [],
  topic: null
}

export default function references (state = initialState, action) {
  switch (action.type) {
    case 'referencesLoadSuccess':
      return {...state, list: action.data}
    case 'referenceLoadSuccess':
      return {...state, topic: action.data}
    default:
      return state
  }
}
