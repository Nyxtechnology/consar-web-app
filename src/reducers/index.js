import { combineReducers } from 'redux'
import general from './general'
import user from './user'
import login from './login'
import references from './references'
import calculator from './calculator'

const rootReducer = combineReducers({
  general,
  user,
  login,
  references,
  calculator
})

export default rootReducer
