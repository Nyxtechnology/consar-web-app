import i18n from 'i18next'
import {reactI18nextModule} from 'react-i18next'

i18n
  .use(reactI18nextModule)
  .init({
    lng: 'es',
    debug: true,
    interpolation: {escapeValue: false},
    react: {wait: true},
    resources: {
      es: {
        translation: {

        }
      },
      en: {
        translation: {

        }
      },
    }
  })


export default i18n
