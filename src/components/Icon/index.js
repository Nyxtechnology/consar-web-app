import React from 'react'

const Icon = (props) => (
  <img
    onClick={props.onClick}
    className={props.className}
    src={'/static/img/' + props.icon}
    style={{
      position: 'absolute',
      ...props.style,
    }}
  />
)

export default Icon
