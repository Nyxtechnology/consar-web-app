import React from 'react'

const ConsarNav = props => (
  <nav className='navbar navbar-inverse' style={{width: '100%', background: 'black'}}>
    <div className='container'>
      <div className='navbar-header'>
        <button type='button' className='navbar-toggle collapsed' data-toggle='collapse' data-target='#subenlaces'>Submenú</button>
      </div>
      <div className='collapse navbar-collapse' id='subenlaces'>
        <ul className='nav navbar-nav navbar-right'>
          <li><a href='http://www.gob.mx/consar/archivo/articulos' data-section='.news.section'>Blog</a></li>
          <li><a href='http://www.gob.mx/consar/archivo/multimedia' data-section='.multimedia'>Álbum de fotos</a></li>
          <li><a href='http://www.gob.mx/consar/archivo/prensa' data-section='.press'>Prensa</a></li>
          <li><a href='https://www.gob.mx/consar/archivo/agenda' data-section='.schedule'>Agenda</a></li>
          <li><a href='http://www.gob.mx/consar/archivo/acciones_y_programas' data-section='.programs'>Acciones y Programas</a></li>
          <li><a href='http://www.gob.mx/consar/archivo/documentos' data-section='.documents'>Documentos</a></li>
          <li><a href='http://www.consar.gob.mx/gobmx/Transparencia/transparencia.aspx' data-section='.tran.section'>Transparencia</a></li>
          <li><a href='http://www.gob.mx/consar#contacto' data-section='.tran.section'>Contacto</a></li>
        </ul>
      </div>
    </div>
  </nav>
)

export default ConsarNav
