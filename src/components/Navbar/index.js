import React from 'react'
import style from './style.css'

const Navbar = (props) => (
  <div className={style.navbar}>
    <div
      onClick={() => props.setModal({modal: props.general.modal === 'leftnav' ? null : 'leftnav'})}
      style={{color: props.menuColor}}>
      <div><i className='fa fa-bars' /></div>
      <div>Menú</div>
    </div>
  </div>
)

export default Navbar
