import React from 'react'
import style from './style.css'
import debounce from 'lodash/debounce'
import window from 'global/window'

const sizes = {
  'normal': 295,
  'small': 240,
  icon: 150,
}

class Masonry extends React.Component {
  state = { cols: 1 }
  order = (e) => {
    const { colsSize } = this.props
    this.setState({cols: parseInt(this.refs.masonry.offsetWidth / sizes[colsSize])})
  }
  componentDidMount = () => {
    this.order(this)
    this.debounced = debounce(() => this.order(this), 100)
    window.addEventListener('resize', this.debounced)
  }
  componentWillReceiveProps = (nextProps) => {
    this.order(this)
  }
  componentWillUnmount = () => {
    window.removeEventListener('resize', this.debounced)
  }
  render = () => {
    const { children, menu, menuPosition, process, icon, errCode, emptyEl } = this.props
    const errE = (
      <div className={style.empty}>
        <i className={`fa fa-times`} />
        <h3>
          { errCode === 400 ? 'Datos invalidos o incorrectos'
            : errCode === 500 ? 'Problemas con el servidor'
            : errCode === 'Network Error' ? 'Problemas de conexión'
            : errCode ? 'Se ha producido un error inesperado'
            : null
          }
        </h3>
      </div>
    )
    const processE = <div className={style.process}><i className='fa fa-circle-o-notch fa-spin' /> Buscando...</div>
    const realCols = this.state.cols
    const num = menu && (menuPosition === 'col-left' || menuPosition === 'col-right')
      ? (this.state.cols - 1) || 1
      : this.state.cols || 1
    let cols = children.reduce((a, b, i) => {
      a[i % num] = [...a[i % num] || [], b]
      return a
    }, [])

    while (cols.length < num) cols.push([])
    if (!process && menu && menuPosition === 'left') cols[0].unshift(menu)
    else if (!process && menu && menuPosition === 'right') cols[cols.length - 1].unshift(menu)
    else if (!process && menu && menuPosition === 'col-left') realCols === 1 ? cols[0].unshift(menu) : cols.unshift(menu)
    else if (!process && menu && menuPosition === 'col-right') realCols === 1 ? cols[0].unshift(menu) : cols.push(menu)
    else if (!process && menu) cols[cols.length - 1].unshift(menu)

    return (
      <div className={style.masonry} ref='masonry'>
        { !process && !errCode && children.length ? cols.map((col, i) => (
          <div key={i} className={style.col} >
            {col}
          </div>
        ))
        : <div className={menu ? style.nochields : style.nochieldsnomenu}>
          {menu && menu}
          { process && <div className={style.process}>{processE}</div>}
          { !process && errCode && <div className={style.process}>{errE}</div>}
          { !process && !errCode && !children.length && <div className={style.process}>{emptyEl(icon)}</div>}
        </div>
        }
      </div>
    )
  }
}

Masonry.defaultProps = {
  colsSize: 'normal',
  icon: 'gift',
  emptyEl: (icon) => <div className={style.empty}><i className={`fa fa-${icon}`} /><h3>Nada por aqui</h3></div>
}

export default Masonry
