export default (section = 'adultos') => [
  {
    img: 'mapeo',
    name: 'Mapa de proyectos',
    link: `/${section}/mapeo`,
    desc: `Descubre y selecciona los proyectos clave que vivirás en la aventura de tu vida.`
  },
  {
    img: 'proyectos',
    name: 'Proyectos',
    link: `/${section}/proyectos`,
    desc: `Visualiza a detalle los grandes proyectos de tu vida y lo que necesitas para alcanzarlos.`
  },
  {
    img: 'afore',
    name: 'Cuenta AFORE',
    link: `/${section}/afore`,
    desc: `Conoce cómo funciona la Cuenta AFORE y saca el mayor provecho de tu ahorro para el retiro.`
  },
  {
    img: 'movil',
    name: 'AforeMóvil',
    link: '/app',
    desc: 'Aplicación móvil que te permite tomar el control de tu Cuenta AFORE y construir el futuro que deseas.'
  },
  {
    img: 'guia',
    name: 'Guías Previsionales',
    link: `/${section}/guia`,
    desc: `Prepárate para anticipar las etapas y los sucesos de tu vida que requerirán un soporte  económico.`
  },
  {
    img: 'referencias',
    name: 'Referencias',
    link: `/${section}/referencias`,
    desc: `Toma nota de documentos y casos de interés sobre finanzas y cultura previsional.`
  },
  {
    img: 'cursos',
    name: 'Cursos',
    link: `/${section}/referencias/Cursos-de-educacion-financiera-SyayMQYBf`,
    desc: `Prepárate, aprende y certifícate sobre el funcionamiento del Sistema de Ahorro para el Retiro y otros temas.`
  },
  {
    img: 'calculadora',
    name: 'Calculadoras',
    link: `/${section}/calculadora`,
    desc: `Haz cuentas y obtén los resultados que más le sumen a tu ahorro para el retiro.`
  },
  {
    img: 'blog',
    name: 'Blog',
    href: 'http://www.mifuturoconsentido.gob.mx/wordpress2/',
    desc: 'Haz cuentas y obtén los resultados que más le sumen a tu ahorro para el retiro.'
  }
]
