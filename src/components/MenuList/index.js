import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'
import defaultData from './data'

const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1

const External = (data) => (
  <a className={style.item} href={data.href} target='_blank'>
    <img src={`/static/img/menu/${data.img}${data.type || 0}.svg`} />
    <div className={style.itemText}>
      <h3>{data.name}</h3>
      <p>{data.desc}</p>
    </div>
    <div><button>Ir</button></div>
  </a>
)
const Interal = (data) => (
  <Link className={style.item} to={data.link}>
    <img src={`/static/img/menu/${data.img}${data.type || 0}.svg`} />
    <div className={style.itemText}>
      <h3>{data.name}</h3>
      <p>{data.desc}</p>
    </div>
    <div><button>Ir</button></div>
  </Link>
)

const Menulist = ({type, section, data = defaultData, active, className, activeClassName}) => (
  <div className={applyClass(active, className, activeClassName)} id='menulist'>
    <h2>¿Qué te gustaría saber?</h2>
    <div>
      {data(['jovenes', 'adultos', 'mayores'][type]).map((d, i) => d.href
        ? <External {...{...d, type}} key={i} />
        : <Interal {...{...d, type}} key={i} />
      )}
    </div>
  </div>
)

export default Menulist
