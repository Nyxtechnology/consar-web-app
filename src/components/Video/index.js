import React from 'react'
import style from './style.css'

const Video = (props) => !props.step || !props.url ? null : (
  <div className={style.container}>
    <div><i className='fa fa-times' onClick={props.toggle} /></div>
    <div>
      <div>
        <iframe
          width='853' height='480'
          src={'https://www.youtube.com/embed/' + props.url}
          frameBorder='0'
          allowFullScreen />
      </div>
    </div>
  </div>
)

export default Video
