import React from 'react'
import moment from 'moment'
import style from './style.css'
import get from 'lodash/fp/get'

class Calendar extends React.Component {
  state = {date: moment()}
  componentDidMount = () =>
    get('props.date.value', this) && this.setState({date: moment(this.props.date.value, 'YYYY-MM-DD')})
  componentWillReceiveProps = next =>
    get('date.value', next) &&
    next.date.value !== get('props.date.value', this) &&
    this.setState({date: moment(next.date.value, 'YYYY-MM-DD')})
  changeYear = (e) => {
    const date = this.state.date
    date.year(parseInt(e.target.value))
    this.setState({ date })
  }
  selectDay = (day) => {
    const date = this.state.date
    date.date(day)
    this.setState({ date })
  }
  nextYear = () => {
    const date = this.state.date
    date.year(date.year() + 1)
    date.date(1)
    this.setState({ date })
  }
  backYear = () => {
    const date = this.state.date
    date.year(date.year() - 1)
    date.date(1)
    this.setState({ date })
  }
  nextMonth = () => {
    const date = this.state.date
    date.month(date.month() + 1)
    date.date(1)
    this.setState({ date })
  }
  backMonth = () => {
    const date = this.state.date
    date.month(date.month() - 1)
    date.date(1)
    this.setState({ date })
  }
  selectDate = () => {
    const obj = {id: this.props.date.id, value: this.state.date.format('YYYY-MM-DD')}
    if (this.props.onChange) this.props.onChange({...obj, target: obj})
    if (this.props.toggle) return this.props.toggle()
  }
  render = () => {
    const { date } = this.state
    const year = date.year()
    const month = date.month()
    const day = date.date()
    const leap = date.isLeapYear()
    const days = []
    const monthMap = {
      0: { name: 'Enero', days: 31 },
      1: { name: 'Febrero', days: leap ? 29 : 28 },
      2: { name: 'Marzo', days: 31 },
      3: { name: 'Abril', days: 30 },
      4: { name: 'Mayo', days: 31 },
      5: { name: 'Junio', days: 30 },
      6: { name: 'Julio', days: 31 },
      7: { name: 'Agosto', days: 31 },
      8: { name: 'September', days: 30 },
      9: { name: 'Octubre', days: 31 },
      10: { name: 'Noviembre', days: 30 },
      11: { name: 'Diciembre', days: 31 }
    }

    for (let x = 1; x <= monthMap[month].days; x++) {
      days.push(<div className={day === x ? style.daySelected : ''} onClick={() => this.selectDay(x)} key={x}>{x}</div>)
    }

    if (!this.props.date || !this.props.date.id) return null

    return (
      <div className={style.container}>
        <div className={style.monthContainer}>
          <div>
            <i className='fa fa-caret-left' onClick={() => this.backMonth()} />
            <div>{monthMap[month].name}</div>
            <i className='fa fa-caret-right' onClick={() => this.nextMonth()} />
          </div>
          <div>
            <i className='fa fa-caret-left' onClick={() => this.backYear()} />
            <input value={year} onChange={this.changeYear} />
            <i className='fa fa-caret-right' onClick={() => this.nextYear()} />
          </div>
        </div>
        <div className={style.daysContainer}>
          {days}
        </div>
        <div className={style.btnsContainer}>
          <button onClick={this.props.toggle}>Cerrar</button>
          <button onClick={this.selectDate}>Aceptar</button>
        </div>
      </div>
    )
  }
}

export default Calendar
