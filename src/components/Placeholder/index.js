import React from 'react'
import style from './style.css'

const Placeholder = props => (
  <div className={style.Placeholder}>
    {props.toggle ? <div><i className='fa fa-times' onClick={() => props.toggle(null)} /></div> : null}
    <div>
      Próximamente
    </div>
  </div>
)

export default Placeholder
