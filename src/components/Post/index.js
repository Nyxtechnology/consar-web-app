import React from 'react'
import style from './style.css'

const Post = (props) => (
  <div className={style.post}>
    <a href={props.link} target='_blank'>
      <img src={props.img} />
      <div>
        <h3>{props.title}</h3>
      </div>
    </a>
  </div>
)

const Blog = (props) => {
  const buttonBg = ['#ED6C5C', '#52D2FA', '#88C227'][props.type]
  return (
    <div className={style.blog}>
      <img src='/static/img/blog.png' />
      <h2>Blog “Mi futuro con $entido”</h2>
      <h3>Fortalece tus conocimientos económico-financieros con el apoyo de notas e infografías.</h3>
      <div>{props.posts.map((p, i) => <Post key={i} {...p} />)}</div>
      <div><a style={{background: buttonBg}} target='_blank' href={props.more}>Ver más</a></div>
    </div>
  )
}

export default Blog
