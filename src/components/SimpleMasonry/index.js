import React from 'react'
import style from './style.css'

const SimpleMasonry = (props) => {
  const num = parseInt(props.width / 100) || 1
  let cols = props.children.reduce((a, b, i) => {
    a[i % num] = [...a[i % num] || [], b]
    return a
  }, [])
  .map((col, i) => (<div key={i} className={style.col} >{col}</div>))

  return (<div className={style.masonry}>{cols}</div>)
}

export default SimpleMasonry
