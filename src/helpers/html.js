import serialize from 'serialize-javascript'

const prefix = `/static/${process.env.target || 'development'}/`
const main = prefix + 'main.js'

const productionHeaderDependencies = `
`
const productionDependencies = `
  <script type='application/javascript' src='${prefix}vendors.js'></script>
`

export default (props) => {
  const {root, initialState, routerContext, splits} = props || {}
  const head = {
    title: 'La Aventura de mi Vida',
    ogtitle: 'La Aventura de mi Vida',
    description: 'Imagina tu vida como una aventura en la que llevarás a cabo algunos proyectos especialmente importantes. Aprende aquí a manejar tu dinero para que disfrutes la realización de esos objetivos.',
    ogdescription: 'Imagina tu vida como una aventura en la que llevarás a cabo algunos proyectos especialmente importantes. Aprende aquí a manejar tu dinero para que disfrutes la realización de esos objetivos.',
    keywords: [],
    ...props.head
  }
  const metas = `
    <title>${head.title || ''}</title>
    <meta name='description' content='${head.description || ''}' />
    <meta property='og:title' content='${head.ogtitle || ''}' />
    <meta property='og:description' content='${head.ogdescription || ''}' />
    <meta property='og:url' content='${head.ogurl || ''}' />
    <meta property='og:type' content='${head.ogtype || 'website'}' />
    <meta property='og:image' content='${head.ogimage || '/static/img/opgimg.png'}' />
    <meta name='keywords' content='${head.keywords}'>
  `

  return `
    <html>
      <head>
        <link rel='shortcut icon' type='image/png' href='/static/favicon.ico'/>
        <meta charSet='utf-8'/>
        <meta httpEquiv='x-ua-compatible' content='ie=edge'/>
        <meta name='viewport' content='width=device-width, initial-scale=1'/>
        ${metas}
        ${splits.length ? `<link rel='stylesheet' href='/static/production/${splits[0]}.css' />` : ''}
        ${process.env.NODE_ENV === 'production' ? productionHeaderDependencies : ''}
        <script src="https://use.fontawesome.com/ca75a64bea.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
        <link async href="https://framework-gb.cdn.gob.mx/assets/styles/main.css" rel="stylesheet" />
      </head>
      <body>
        <div id='root'>${root || ''}</div>
        <script type='text/javascript'>
          window.initialState = ${serialize(initialState)}
          window.splitPoints = ${routerContext ? JSON.stringify(routerContext.splitPoints) : null}
          window.splits = ${JSON.stringify(splits)}
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        ${process.env.NODE_ENV === 'production' ? productionDependencies : ''}
        <script type='application/javascript' src='${main}'></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114131060-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-115402338-1');
        </script>
        <script async src='https://framework-gb.cdn.gob.mx/gobmx.js'></script>
      </body>
    </html>
  `
}
