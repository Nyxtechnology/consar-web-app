const gcd = (x, y) => {
  x = Math.abs(x)
  y = Math.abs(y)
  while (y) {
    let t = y
    y = x % y
    x = t
  }
  return x
}

const lcm = (x, y) => Math.abs((x * y) / gcd(x, y))

const getR = (i, fr) => {
  const mcm = lcm(1, fr)
  return ((mcm / 1 * 1) + (mcm / fr * i)) / mcm
}

const defaultParams = {
  period: 0,
  frequency: 0,
  initialInput: 0,
  periodicalInput: 0,
  interest: 0
}

const compute = ({period, frequency, initialInput, periodicalInput, interest} = defaultParams) => {
  let history = []
  const it = period * frequency
  const r = getR(parseFloat(interest) / 100, frequency)
  const totalSaving = parseFloat(periodicalInput) * it + parseFloat(initialInput)
  let totalCapital = 0
  let cfa = parseFloat(initialInput)

  for (let i = 1; i <= it; i++) {
    totalCapital = parseFloat(periodicalInput) + cfa * r
    cfa = totalCapital
    history.push({
      initialInput,
      periodicalInput: periodicalInput * i,
      totalInterest: totalCapital - (parseFloat(periodicalInput) * i + parseFloat(initialInput)),
      totalCapital,
      current: i
    })
  }

  return {totalSaving, totalInterest: totalCapital - totalSaving, totalCapital, history, period}
}

export default compute
