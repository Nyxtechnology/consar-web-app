import moment from 'moment'
import api from './api'
import form from './form'

const aurl = 'http://189.202.226.159:3009/'
export const getReferences = (slug = '') => api(aurl + 'api/references/' + slug)
