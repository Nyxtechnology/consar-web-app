
export default {
  keywords: [
    'Consar',
    'Aventura de mi vida',
    'Afore',
    'plan de ahorro',
    'como ahorrar dinero',
    'Planear mi vida',
    'tips para ahorrar',
    'consejos para ahorrar',
    'Comprar casa',
    'Comprar auto',
    'Iniciar mi negocio',
    'Libertad financiera',
    'Obtener primer empleo',
    'Preparar mi jubilación',
    'Asegurar mi fondo de emergencias',
    'Alcanzar mis metas profesionales',
    'Estudiar en la universidad',
    'Vivir en pareja',
    'Formar una familia',
    'Planificar viaje',
    'Consolidar mi pasatiempo',
    'Dejar una herencia',
    'Crecer mi negocio',
    'Mi gran proyecto'
  ]
}
