import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as generalActions from '../../actions/general'
import style from './style.css'
import Icon from '../../components/Icon/'
import ConsarNav from '../../components/ConsarNav/'
import random from 'lodash/random'
import seo from './seo'

const cloudCfg = () => ({
  className: style.cloud,
  icon: 'ch' + random(1, 7) + '.svg',
  speed: random(1, 5),
  style: {
    transform: random(0, 1) ? 'scaleX(-1)' : '',
    width: random(2, 4) + '%',
    top: random(0, 20) + '%',
    left: random(0, 100) + '%'
  }
})
const shineCfg = () => ({
  className: style.shime,
  icon: 'icon' + random(1, 4) + '.svg',
  speed: random(1, 5),
  style: {
    position: 'absolute',
    left: random(0, 100) + '%',
    top: random(0, 100) + '%',
    color: '#FFF',
    background: 'none',
    width: random(10, 30) + 'px'
  }
})
class Home extends React.Component {
  static seo = seo
  state = {clouds: [], shines: [], sch: 'auto'}
  componentWillReceiveProps = next => this.setState({sch: this.refs.header.clientHeight})
  componentDidMount = () => this.setState({
    clouds: [...Array(parseInt(10)).keys()].map((i) => <Icon key={i} {...cloudCfg()} />),
    shines: [...Array(parseInt(20)).keys()].map((i) => <Icon key={i} {...shineCfg()} />),
    sch: this.refs.header.clientHeight
  })
  render = () => {
    const w = this.props.general.w
    const h = this.props.general.h ? this.props.general.h - this.state.sch : this.state.sch
    return (
      <div className={style.container}>
        <ConsarNav />
        <div ref='header' className={style.header} style={{position: 'relative'}}>
          {this.state.shines}
          <h2>Ahorro y futuro</h2>
          <h1>La aventura de mi vida</h1>
          <h3>¿Qué es lo que te mueve? ¿Cuáles son los grandes objetivos que deseas alcanzar durante tu vida?</h3>
          <h3>Selecciona tu rango de edad, ubica tus proyectos y traza el mapa de la aventura de tu vida.</h3>
        </div>
        <div className={style.squaresContainer} style={{height: w < 600 ? 'auto' : h < 500 ? 500 : h}}>
          <div
            className={style.squaresbackground}>
            <div className={style.cb1827} />
            <div className={style.cb2836} />
            <div className={style.cb3745} />
          </div>
          {this.props.general.w > 600 ? this.state.clouds : null}
          <div className={style.squares}>
            <div className={style.c1827} onClick={() => this.props.history.push('/jovenes')}>
              <a><div className={style.textbox}><h3>18-27</h3><small> Años</small></div></a>
            </div>
            <div className={style.c2836} onClick={() => this.props.history.push('/adultos')}>
              <a><div className={style.textbox}><h3>28-36</h3><small> Años</small></div></a>
            </div>
            <div className={style.c3745} onClick={() => this.props.history.push('/mayores')}>
              <a><div className={style.textbox}><h3>37-45</h3><small> Años</small></div></a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default connect(({general}) => ({general}), (dispatch) =>
  bindActionCreators(generalActions, dispatch))(Home)
