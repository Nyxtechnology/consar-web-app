
export default {
  title: '¿Tienes entre 37 y 45 años? Disfruta la aventura de tu vida | CONSAR',
  description: '¿Ya tienes un plan para gozar tu jubilación, dejar una herencia o asegurar tu fondo de emergencia? Sigue los consejos de la CONSAR para alcanzar tus metas.',
  ogtitle: '¿Tienes entre 37 y 45 años? Disfruta la aventura de tu vida | CONSAR',
  ogdescription: '¿Ya tienes un plan para gozar tu jubilación, dejar una herencia o asegurar tu fondo de emergencia? Sigue los consejos de la CONSAR para alcanzar tus metas.',
  keywords: [
    'Consar',
    'Aventura de mi vida',
    'Afore',
    'plan de ahorro',
    'como ahorrar dinero',
    'Planear mi vida',
    'tips para ahorrar',
    'consejos para ahorrar',
    'Comprar casa',
    'Comprar auto',
    'Iniciar mi negocio',
    'Libertad financiera',
    'Preparar mi jubilación',
    'Asegurar mi fondo de emergencias',
    'Planificar viaje ',
    'Consolidar mi pasatiempo',
    'Construir una herencia',
    'Crecer mi negocio',
    'Mi gran proyecto'
  ]
}
