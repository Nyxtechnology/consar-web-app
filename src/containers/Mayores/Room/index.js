import React from 'react'
import style from './style.css'

const Room = (props) => {
  const isNotStep1 = props.step !== 1
  const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1
  return (
    <div className={applyClass(isNotStep1, style.room, style.roomHidden)}>
      <div className={applyClass(isNotStep1, style.circle, style.circleHidden)} />
      <img
        className={applyClass(isNotStep1, style.years, style.yearsHidden)}
        src='/static/img/mayores/cartel.svg' />
      <img
        className={applyClass(isNotStep1, style.window, style.windowHidden)}
        src='/static/img/mayores/window.svg' />
      <img
        onClick={props.start}
        className={applyClass(isNotStep1, style.menu, style.menuHidden)}
        src='/static/img/mayores/menu.svg' />
      <img
        className={applyClass(isNotStep1, style.chair, style.chairHidden)}
        src='/static/img/mayores/chairs.svg' />
      <img
        className={applyClass(isNotStep1, style.dude, style.dudeHidden)}
        src='/static/img/c3745.png' />
      <img
        className={applyClass(isNotStep1, style.scanner, style.scannerHidden)}
        src='/static/img/mayores/scanner.svg' />
      <img
        className={applyClass(isNotStep1, style.bag, style.bagHidden)}
        src='/static/img/mayores/bag.svg' />
      <div
        className={applyClass(isNotStep1, style.text, style.textHidden)} >
        <h1>La Aventura de mi Vida</h1>
        <h2>
          Con el tiempo vas acumulando conocimiento y experiencia, haciéndote más
          estratégico. ¿Qué estilo de vida vienes construyendo y cuáles son los
          destinos que aún te falta conquistar?
        </h2>
      </div>
    </div>
  )
}

export default Room
