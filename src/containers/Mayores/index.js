import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as generalActions from '../../actions/general'
import style from './style.css'
import Video from '../../components/Video/'
import Room from './Room/'
import Blog from './Blog'
import Menu from '../../components/MenuList/'
import Layout from '../Layout/'
import scrollTo from '../../helpers/scrollTo'
import seo from './seo'

class Mayores extends React.Component {
  static seo = seo
  state = {step: 0, process: false}
  componentDidMount = () => {
    if (!this.props.general.videos[2]) {
      window.scrollTo(0, 0)
      return this.setState({step: 1})
    }
  }
  next = () => {
    this.props.closeVideo(2)
    this.setState({step: this.state.step + 1})
  }
  start = () => scrollTo(this.refs.container, document.getElementById('menulist').offsetTop - 50, 300)
  componentWillReceiveProps = next => {
    if (next.match.url !== this.props.match.url) {
      return window.scrollTo(0, 0)
    }
  }
  render = () => (
    <Layout {...this.props}>
      <div className={style.container} ref='container'>
        <div>
          <Video
            toggle={this.state.process ? null : this.next}
            step={this.props.general.videos[2]}
            url='KzLhYBBDUIk' />
          <div className={style.acontainer}>
            <Room {...{next: this.next, step: this.state.step, start: this.start}} />
          </div>
          <Menu {...{type: 2, active: this.state.step === 0, className: style.menulist, activeClassName: style.menulistHidden}} />
          {this.state.step ? <img className={style.footer} src='/static/img/mayores/rocas1.svg' /> : null}
          {this.state.step ? <Blog /> : null}
        </div>
      </div>
    </Layout>
  )
}

export default connect(({general}) => ({general}), (dispatch) =>
  bindActionCreators(generalActions, dispatch))(Mayores)
