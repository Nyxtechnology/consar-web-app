import React from 'react'
import style from './style.css'
import Post from '../../../components/Post/'

const posts = [
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=212',
    img: '/static/img/posts/verde/1.png',
    title: 'Opciones para pensionarse',
    desc: ''
  },
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=301',
    img: '/static/img/posts/verde/2.png',
    title: '¿Qué significa ser trabajador formal?',
    desc: ''
  },
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=313',
    img: '/static/img/posts/verde/3.png',
    title: '¿Cómo funciona la recaudación de las aportaciones en el SAR?',
    desc: ''
  }
]

const Blog = (props) => (
  <div className={style.blog}>
    <Post {...{type: 2, posts, more: 'http://www.mifuturoconsentido.gob.mx/wordpress2/'}} />
  </div>
)

export default Blog
