import React from 'react'
import style from './style.css'
import data from './data'
import Nav from './Nav'
import ConsarNav from '../../components/ConsarNav/'

const Faq = props => (
  <div>
    <ConsarNav />
    <div className={style.faq}>
      <Nav {...props} />
      <div>
        <h1>Preguntas frecuentes</h1>
        {data.map((p, i) => (
          <div key={i} id={i}>
            <h2>{p.title}</h2>
            {p.content}
          </div>
        ))}
      </div>
    </div>
  </div>
)

export default Faq
