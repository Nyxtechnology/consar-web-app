import React from 'react'

export default [
  {
    title: '¿Cómo puedo acceder a todos los servicios que me ofrece mi AforeMóvil?',
    content: <p>
      Tienes que identificarte y autenticarte a través de AforeMóvil. Si la AFORE en la que te encuentras
      registrado no participa en la aplicación, podrás acceder, pero con servicios limitados. Actualmente las
      AFORE participantes son Inbursa, Invercap, PensionISSSTE, Profuturo GNP, Sura y XXI Banorte; próximamente
      más AFORE se sumarán.
    </p>
  },
  {
    title: '¿Qué pasa con mi cuenta en caso de que pierda o roben mi teléfono inteligente?',
    content: <p>
      Tu cuenta y tus datos están seguros, ya que para acceder, la aplicación solicitará tu contraseña
      o una fotografía de tu rostro para estar seguros que eres tú. En cuanto tengas un nuevo teléfono descarga
      la aplicación e ingresa nuevamente tú información. Recuerda que AforeMóvil protege tus datos con los más
      altos estándares de seguridad.
    </p>
  },
  {
    title: '¿Qué pasa si cambio de número o aparato telefónico?',
    content: <p>
      Descarga nuevamente la aplicación AforeMóvil, la cual te pedirá tu CURP y una fotografía para
      validar tu identidad y asociar tu nuevo número o aparato telefónico a la app. Es muy simple y es gratis.
    </p>
  },
  {
    title: '¿Qué pasa si cambio mi dirección de correo electrónico?',
    content: <p>
      Descarga nuevamente la aplicación AforeMóvil y captura tu nuevo correo electrónico; posteriormente
      recibirás un código de seguridad vía SMS, el cual debes ingresar junto con tu contraseña de inicio de
      sesión para activar la app.
    </p>
  },
  {
    title: '¿Cómo puedo recuperar mi contraseña de acceso?',
    content: <p>
      En caso de que olvides tu contraseña, al momento de iniciar sesión debes seleccionar la opción
      de “Recuperar Contraseña”, la app te solicitará información para identificarte y en algunos casos
      una foto de tu rostro para estar seguros que eres tú. Asimismo, la app te enviará un código SMS y
      te pedirá que captures una nueva contraseña.
    </p>
  },
  {
    title: '¿Cuándo vas a poder traspasarte desde la APP?',
    content: <p>
      Nos encontramos diseñando los mecanismos para que próximamente los
      usuarios inicien su solicitud de traspasos a través de AforeMóvil.
    </p>
  },
  {
    title: '¿Cuáles son los servicios y beneficios de descargar AforeMóvil?',
    content: <div>
      <p>
        Con el simple hecho de descargar AforeMóvil e ingresando algunos datos podrás localizar
        la AFORE en la que se encuentra tu cuenta individual. En caso de no contar con una Afore, es
        decir, si eres un trabajador independiente o nunca has cotizado en la formalidad podrás
        registrarte en la AFORE de tu elección.
      </p>
      <p>
        Asimismo, una vez que te identifiques con la información requerida podrás acceder a servicios como:
      </p>
      <ul>
        <li>Consulta de saldo en línea</li>
        <li>Solicitud de envío de Estado de Cuenta electrónico</li>
        <li>Registrar a tus hijos menores de edad a una Afore (cuenta para toda la vida)</li>
        <li>Hacer ahorro voluntario en línea con cargo a tarjetas bancarias</li>
        <li>Domiciliar tu ahorro voluntario (cargos recurrentes y automáticos)</li>
        <li>Localizar establecimientos para hacer aportaciones voluntarias en efectivo</li>
        <li>Recibir notificaciones de tu AFORE (buzón)</li>
        <li>Realizar estimaciones previsionales con Calculadoras de Ahorro</li>
      </ul>
    </div>
  },
  {
    title: '¿Qué otros servicios se podrán agregar en un futuro?',
    content: <div>
      <p>
        La aplicación AforeMóvil se irá renovando, actualizando y mejorando en el tiempo, es
        por eso que nos encontramos diseñando nuevas herramientas y servicios que estarán
        disponibles próximamente, tales como:
      </p>
      <ul>
        <li>Perfil avanzado para usuarios que se hayan identificado presencialmente con su AFORE (con expediente electrónico y biométricos)</li>
        <li>Inicio del proceso de Traspasos de Cuentas</li>
        <li>Esquema de citas con un Agente Promotor o en sucursales de la AFORE </li>
        <li>Solicitud de pago de parcialidades de retiros por desempleo</li>
        <li>Actualización de datos (domicilio, teléfono, beneficiarios, correo electrónico)</li>
        <li>Consulta de Movimientos en la cuenta</li>
        <li>Consulta de saldos y movimientos para niños registrados en AforeMóvil</li>
        <li>Calculadoras especializadas</li>
        <li>Programas de lealtad</li>
        <li>Localización de sucursales de Afore</li>
      </ul>
    </div>
  },
  {
    title: '¿Por qué no tiene folio el estado de cuenta que se emite en la APP?',
    content: <div>
      <p>
        El estado de cuenta que se emiten a través AforeMóvil no tiene folio ya que la app
        no es una herramienta para traspasarte, es una herramienta que proporciona al usuario
        información de su cuenta individual y servicios para incrementar sus ahorros.
      </p>
      <p>
        Los trabajadores en todo momento pueden solicitar a su AFORE un estado de cuenta con
        folio a través de los canales que estas dispongan.
      </p>
    </div>
  },
  {
    title: '¿Si estoy asignado a una AFORE el proceso de registro es el mismo que el de un independiente?',
    content: <div>
      <p>
        El proceso de identificación y autenticación para tu registro móvil es el mismo, la única
        diferencia es el monto de aportaciones voluntarias que tendrás que realizar para concluir tu solicitud de registro.
      </p>
      <p>
        Si eres un trabajador asignado deberás aportar al menos $200.00
      </p>
      <p>
        Si eres un trabajador independiente deberás aportar al menos $50.00
      </p>
    </div>
  },
  {
    title: '¿Cuál es el beneficio/impacto que se proyecta con esta App? ¿En qué momento se podrán conocer los resultados?',
    content: <div>
      <p>
        AforeMóvil tendrá beneficios sin precedentes en el sistema de pensiones mexicano al ser una herramienta
        que permitirá a toda la población conectarse con las AFORE sin importar su edad, ubicación o estatus
        laboral y de una forma sencilla y útil. Asimismo, sin ningún costo para los usuarios, éstos podrán disponer
        de servicios remotos que les permitirá tener el control de su cuenta de pensión en su teléfono móvil e inclusive
        realizar aportaciones voluntarias en línea para incrementar sus ahorros.
      </p>
      <p>
        La app tendrá un impacto a corto y largo plazo en la forma en cómo los mexicanos, sobre todo los
        jóvenes, comprenden el ahorro previsional y la importancia del ahorro para el retiro.
      </p>
      <p>
        Esperamos contar con millones de usuarios en mediano plazo, sobre la marcha se informará sobre el uso de la app.
      </p>
    </div>
  },
  {
    title: '¿Si tengo dos CURP puedo tener dos cuentas de AFORE Móvil?',
    content: <div>
      <p>
        No, los usuarios solo podrán descargar y activar la aplicación móvil con una CURP, que
        es con la que se identificarán en la app y con la que se vincularán con su cuenta individual en la AFORE.
      </p>
      <p>
        En caso de que cuentes con dos o más CURPs deberás corregir tu situación con las instituciones
        competentes (en este caso RENAPO) para evitar problemas futuros, ¡regularízate!
      </p>
    </div>
  },
  {
    title: '¿Si vivo en el extranjero puedo usar AFORE Móvil?',
    content: <p>
      Sí, los mexicanos que viven en el extranjero pueden descargar y usar AforeMóvil. Actualmente
      nos encontramos diseñando y desarrollando los mecanismos para que también puedan realizar aportaciones
      voluntarias desde su país de residencia.
    </p>
  },
  {
    title: '¿Pueden acceder a esta aplicación los menores de edad con cuenta individual registrada en AFORE?',
    content: <p>
    No, actualmente solo mayores de edad pueden usar AforeMóvil y éstos pueden registrar a sus hijos menores de
    edad para que puedan acceder al sistema de ahorro para el retiro e iniciar sus ahorros. Actualmente nos
    encontramos diseñando y desarrollando una app para niños que estará disponible próximamente.
    </p>
  },
  {
    title: '¿Cuál es el mínimo de requerimientos que debe contener el equipo móvil para poder descargar esta aplicación?',
    content: <p>
      Se requiere un teléfono inteligente con sistema operativo iOS a partir de la versión 9.0 o Android
      a partir de la versión 4.3 y cámara fotográfica.
    </p>
  },
  {
    title: '¿Aproximadamente cada cuánto tiempo se actualizará la aplicación?',
    content: <div>
      <p>
        Dependerá de las mejoras y nuevas funcionalidades que se vayan desarrollando, siempre
        buscando una mejor experiencia para los usuarios.
      </p>
      <p>
        Dependerá de las mejoras y nuevas funcionalidades que se vayan desarrollando, siempre
        buscando una mejor experiencia para los usuarios.
      </p>
    </div>
  },
  {
    title: '¿Cómo se garantiza la seguridad de la información del usuario?',
    content: <p>
      AforeMóvil solicita información específica y realiza una identificación biométrica
      del rostro de los usuarios para generar una firma electrónica única, con lo que se protege
      y asegura el acceso a la información. Asimismo, toda la información generada e ingresada en
      la app es transmitida y almacenada en la Base de Datos Nacional del SAR que cuenta con los más
      altos estándares de seguridad de la información y se encuentra regulada y supervisada por la
      Comisión Nacional del Sistema de Ahorro para el Retiro (CONSAR).
    </p>
  },
  {
    title: '¿Tendrá la Aplicación interacción con el usuario o solo será informativa?',
    content: <p>
      AforeMóvil, además de brindar información, es completamente interactiva con los usuarios, ya
      que les permite realizar ahorro voluntario en línea, solicitar estados de cuenta, ubicar
      establecimientos para realizar aportaciones voluntarias, realizar cálculos pensionarios a través
      de calculadoras e inclusive recibir y enviar mensajes directamente con su AFORE utilizando
      un buzón de notificaciones.
    </p>
  },
  {
    title: '¿Está diseñada la APP para realizar trámites?',
    content: <p>
      Sí, todos los usuarios que no tienen o no han elegido una AFORE podrán solicitar su registro
      móvil, adicionalmente los usuarios pueden solicitar su saldo, copia de su estado de cuenta, realizar
      aportaciones voluntarias y registrar a sus hijos menores de edad. Posteriormente se liberarán
      actualizaciones que incluirán otros servicios y trámites.
    </p>
  },
  {
    title: '¿Cuándo funcionará el programa de lealtad (puntos) y con quién se llevará a cabo?',
    content: <p>
      Los programas de lealtad permitirán a los usuarios contar con beneficios con empresas u
      organizaciones asociadas, por ejemplo, que puedan canjear puntos por ahorro voluntario. Actualmente
      nos encontramos diseñando los mecanismos para que esto pueda llevarse a cabo y será liberado próximamente.
    </p>
  },
  {
    title: '¿En algún momento la App contará con un mecanismo de enlace con un ejecutivo de mi AFORE para establecer comunicación desde y a través de la App?',
    content: <p>
      Sí, actualmente AforeMóvil brinda información sobre los medios de contacto de su AFORE y estamos diseñando
      los mecanismos para que los usuarios puedan ponerse ubicar las sucursales o enlazarse directamente con su AFORE.
    </p>
  },
  {
    title: '¿Mi AFORE ofrece el servicio de AforeMóvil?',
    content: <div>
      <p>
        Actualmente seis AFORE tienen el servicio de AforeMóvil, por lo que es muy probable que tú estés
        en alguna de ellas. De lo contrario, la AforeMóvil te notificará que tu AFORE no ofrece el
        servicio, te permitirá acceder con servicios limitados y te mostrará las AFORE que sí cuenten con el
        servicio por si deseas ponerte en contacto con ellas.
      </p>
      <p>
        Actualmente las AFORE participantes son Inbursa, Invercap, PensionISSSTE, Profuturo GNP, Sura y XXI Banorte; próximamente más AFORE se sumarán.
      </p>
    </div>
  },
  {
    title: '¿Por qué la App no contempla a todas las AFORES?',
    content: <p>
      La regulación emitida por la Comisión Nacional del Sistema de Ahorro para el Retiro (CONSAR) es flexible
      en cuanto a la libertad de las AFORE para participar y usar la aplicación móvil, es decir, no existe una
      obligación y dependerá de la voluntad, visión y estrategia de cada AFORE.
    </p>
  },
  {
    title: '¿Cuándo se van a sumar el resto de las AFORES?',
    content: <div>
      <p>
        La regulación emitida por la Comisión Nacional del Sistema de Ahorro para el Retiro (CONSAR) es flexible
        en cuanto a la libertad de las AFORE para participar y usar la aplicación móvil, por lo que dependerá de
        la voluntad, visión y estrategia de cada AFORE.
      </p>
      <p>
        Si bien actualmente las AFORE participantes son Inbursa, Invercap, PensionISSSTE, Profuturo GNP, Sura y
        XXI Banorte, sabemos que hay interés de las AFORE restantes para sumarse próximamente a esta iniciativa.
      </p>
    </div>
  },
  {
    title: '¿Qué diferencia tiene AFORE Móvil con respecto a las App de las propias administradoras?',
    content: <div>
      <p>
        AforeMóvil es una aplicación diseñada para que cualquier mexicano pueda acceder al sistema de
        ahorro para el retiro sin importar su estatus laboral, ubicación, edad o la AFORE donde se encuentre
        su cuenta individual, de tal forma que los usuarios cuenten con servicios mínimos independientemente
        de la AFORE de su elección, de manera remota y segura. AforeMóvil se ira renovando y mejorando para
        que todos los usuarios puedan acceder a los mismos servicios, ya que es una aplicación universal y centralizada.
      </p>
      <p>
        Los usuarios que usen AforeMóvil tendrán la libertad de utilizar las herramientas y
        aplicaciones que su AFORE tenga disponible.
      </p>
    </div>
  },
  {
    title: '¿Qué pasa si la cuenta individual no tiene registrada la CURP? ¿Se puede acceder a la APP AFORE Móvil? y ¿se pueden realizar Ahorro Voluntario?',
    content: <p>
      Sí, la app permitirá a los usuarios registrarse y utilizar AforeMóvil. Se realizarán procedimientos
      para identificar y unificar las cuentas que originalmente ya se encontraban en la base de datos, así
      como herramientas para que los usuarios ingresen mayor información para identificar sus cuentas.
    </p>
  },
  {
    title: '¿Qué sucede con las nuevas aportaciones voluntarias que realice desde la App si soy una persona que ya me encuentro realizando ahorro voluntario de manera frecuente? ¿en qué SIEFORE se invertirán dichos recursos? ¿podré hacerlas deducibles o no deducibles independientemente de lo que haya decidido con las aportaciones anteriores?',
    content: <p>
      AforeMóvil es un nuevo canal para realizar aportaciones voluntarias en línea y con cargo a tarjetas
      bancarias, ya sea por única vez o con cargos recurrentes y automáticos (domiciliación). Los usuarios
      pueden realizar aportaciones voluntarias por otros canales como las redes comerciales o directamente
      con su AFORE, y las domiciliaciones que en su caso ya tiene programadas se mantendrán vigentes.
      La app permite elegir si el ahorro voluntario será o no deducible de impuestos para cada aportación
      realizada e independientemente al resto de las aportaciones realizadas; asimismo, serán invertidas en
      la SIEFORE Básica que corresponde a la edad del ahorrador o a la SIEFORE Adicional que la AFORE tenga disponible.
    </p>
  },
  {
    title: '¿Sabes cuánto puede aumentar tu pensión si realizas ahorro voluntario?',
    content: <div>
      <p>
        Si destinas un poco de tus ingresos a este tipo de ahorro los beneficios a largo plazo en cuestión de
        cantidad pueden ser grandes. El siguiente ejemplo te muestra cómo crece el ahorro:
      </p>
      <p>
        Un trabajador que empieza a cotizar hoy, que tiene un sueldo mensual de 10 mil 515 pesos, 25 años de
        edad y que planea retirarse a los 65 años, recibiría una pensión de 3 mil 514 pesos sin Ahorro Voluntario, y
        con Ahorro Voluntario recibiría 10 mil 515 pesos.
      </p>
      <img src='/static/img/mobile/image1.png' />
    </div>
  }
]
