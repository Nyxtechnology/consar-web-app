import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'

const Nav = props => {
  return (
    <nav className={style.nav} style={{zIndex: 99}}>
      <img src='/static/img/mobile/logo.png' />
      <div>
        <Link to='/app' className={props.step === 1 ? style.navselected : null} onClick={() => props.setStep(1)}>Inicio</Link>
        <a className={style.green}>Contacto</a>
      </div>
    </nav>
  )
}

export default Nav
