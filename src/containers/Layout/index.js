import React from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import style from './style.css'
import * as generalActions from '../../actions/general'
import Navbar from '../../components/Navbar/'
import Leftnav from '../../components/Leftnav/'
import ConsarNav from '../../components/ConsarNav/'

const Layout = (props) => (
  <div className={style.layoutContainer} onClick={props.general.modal ? props.closeAll : null}>
    <ConsarNav />
    <Navbar {...props} />
    <Leftnav {...props} />
    <div className={style.contentWrapper}>{props.children}</div>
  </div>
)

export default connect(({general}) => ({general}), (dispatch) =>
  bindActionCreators(generalActions, dispatch))(Layout)
