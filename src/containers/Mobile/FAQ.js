import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'
const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1

export default class FAQ extends React.Component {
  render = () => {
    const cond = this.props.step !== 4
    return (
      <div className={applyClass(cond, style.faq, style.faqHidden)} id='faq'>
        <h2>Preguntas frecuentes</h2>
        <div>
          <div>
            <h3>¿Cómo puedo acceder a todos los servicios que me ofrece mi AforeMóvil?</h3>
            <p>
              Tienes que identificarte y autenticarte a través de AforeMóvil. Si la AFORE en la que te encuentras
              registrado no participa en la aplicación, podrás acceder, pero con servicios limitados. Actualmente
              las AFORE participantes son Inbursa, Invercap, PensionISSSTE, Profuturo GNP, Sura y XXI Banorte; próximamente
              más AFORE se sumarán.
            </p>

            <h3>¿Qué pasa con mi cuenta en caso de que pierda o roben mi teléfono inteligente?</h3>
            <p>
              Tu cuenta y tus datos están seguros, ya que para acceder, la aplicación solicitará tu contraseña o una
              fotografía de tu rostro para estar seguros que eres tú. En cuanto tengas un nuevo teléfono descarga la
              aplicación e ingresa nuevamente tú información. Recuerda que AforeMóvil protege tus datos con los más
              altos estándares de seguridad.
            </p>

            <h3>¿Qué pasa si cambio de número o aparato telefónico?</h3>
            <p>
              Descarga nuevamente la aplicación AforeMóvil, la cual te pedirá tu CURP y una fotografía para validar
              tu identidad y asociar tu nuevo número o aparato telefónico a la app. Es muy simple y es gratis.
            </p>
            <Link to='/faq'>Ver más</Link>
          </div>
          <div><img src='/static/img/mobile/phones.png' /></div>
        </div>
      </div>
    )
  }
}
