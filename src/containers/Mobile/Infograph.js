import React from 'react'
import style from './style.css'
const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1

const data = ['0.png', '1.png', '2.png', '3.png', '4.jpg', '5.jpg', '6.jpg']
export default class Infograph extends React.Component {
  render = () => {
    const cond = this.props.step !== 2
    return (
      <div className={applyClass(cond, style.info, style.infoHidden)} id='infografia'>
        <h2>Infografías</h2>
        <div className={applyClass(cond, style.info1, style.info1Hidden)}>
          {data.map((n, i) => (
            <a key={i} target='_blank' href={`/static/img/mobile/infos/${n}`}>
              <img src={`/static/img/mobile/infos/${n}`} />
            </a>
          ))}
        </div>
      </div>
    )
  }
}
