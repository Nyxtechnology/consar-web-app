import React from 'react'
import style from './style.css'
const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1

export default class Final extends React.Component {
  render = () => {
    const cond = this.props.step !== 6
    return (
      <div className={applyClass(cond, style.final, style.finalHidden)}>
        <h2>¡Descárgala ya!</h2>
        <div>
          <a className={style.store} target='_blank'
            href='https://itunes.apple.com/mx/app/aforem%C3%B3vil/id1240001519?l=en&mt=8'>
            <img src='/static/img/mobile/appStore.svg' />
            <div>
              <h3>Descarga gratis en</h3>
              <h2>APP STORE</h2>
            </div>
          </a>
          <a className={style.store} style={{background: '#29abe2'}} target='_blank'
            href='https://play.google.com/store/apps/details?id=mx.com.procesar.aforemovil'>
            <img src='/static/img/mobile/playStore.svg' />
            <div>
              <h3>Descarga gratis en</h3>
              <h2>GOOGLE PLAY</h2>
            </div>
          </a>
        </div>
        <img src='/static/img/mobile/phones.png' />
      </div>
    )
  }
}
