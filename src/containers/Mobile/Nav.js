import React from 'react'
import Social from './Social'
import style from './style.css'
import ConsarNav from '../../components/ConsarNav/'

const Nav = props => {
  return (
    <div className={style.navContainer}>
      <ConsarNav />
      <nav className={style.nav}>
        <div>
          <img src='/static/img/mobile/logo.png' />
          <Social />
        </div>
        <div>
          <a className={props.view === 'intro' ? style.navselected : null} onClick={() => props.select('intro')}>Inicio</a>
          <a className={props.view === 'infografia' ? style.navselected : null} onClick={() => props.select('infografia')}>Infografías</a>
          <a className={props.view === 'videos' ? style.navselected : null} onClick={() => props.select('videos')}>Videos</a>
          <a className={props.view === 'faq' ? style.navselected : null} onClick={() => props.select('faq')}>FAQ</a>
          <a className={style.green + ' ' + (props.view === 'contacto' ? style.navselected : null)} onClick={() => props.select('contacto')}>Contacto</a>
        </div>
      </nav>
    </div>
  )
}

export default Nav
