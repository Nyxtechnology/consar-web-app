import React from 'react'
import style from './style.css'
import Nav from './Nav'
import Intro from './Intro'
import Infograph from './Infograph'
import Videos from './Videos'
import FAQ from './FAQ'
import Contact from './Contact'
import Final from './Final'
import document from 'global/document'

export default class Mobile extends React.Component {
  state = {scroll: 0, view: 'intro'}
  componentDidMount = () => {
    window.scrollTo(0, 0)
    document.addEventListener('scroll', this.setScroll)
  }
  componentWillUnmount = () => document.removeEventListener('scroll', this.setScroll)
  setScroll = e => this.setState({scroll: document.body.scrollTop})
  select = view => {
    document.body.scrollTo(0, document.getElementById(view).offsetTop - 100)
    this.setView(view)
  }
  setView = view => this.setState({view})
  render = () => (
    <div className={style.mobile}>
      <Nav {...this.state} select={this.select} />
      <Intro {...this.state} setView={this.setView} />
      <Infograph {...this.state} setView={this.setView} />
      <Videos {...this.state} setView={this.setView} />
      <FAQ {...this.state} setView={this.setView} />
      <Contact {...this.state} setView={this.setView} />
      <Final {...this.state} setView={this.setView} />
    </div>
  )
}
