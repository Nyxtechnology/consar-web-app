import React from 'react'
import style from './style.css'
const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1

export default class Contact extends React.Component {
  render = () => {
    const cond = this.props.step !== 5
    return (
      <div className={applyClass(cond, style.info, style.infoHidden)} id='contacto'>
        <h2>Contacto</h2>
        <div className={applyClass(cond, style.info1, style.info1Hidden)}>
          <div className={style.contact}>
            <h3>Cuéntanos qué opinas de la app o resuelve tus dudas en: </h3>
            <a href='mailto:aforemovil@consar.gob.mx'>aforemovil@consar.gob.mx</a>
          </div>
        </div>
      </div>
    )
  }
}
