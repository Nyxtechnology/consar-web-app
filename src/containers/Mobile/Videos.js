import React from 'react'
import style from './style.css'
const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1

export default class Videos extends React.Component {
  render = () => {
    const cond = this.props.step !== 3
    const videos = ['DohdtbWIxc8', 'bYIFR71FO4E', 'ERDuJhlDSn0', 'sYvM2ejFB9M', '9UH-eiqvCI0', 'jzG5iJkgJGg']
    return (
      <div className={applyClass(cond, style.videos, style.videosHidden)} id='videos'>
        <h2>Videos</h2>
        <div className={applyClass(cond, style.videos1, style.videos1Hidden)}>
          {videos.map((v, i) => (
            <div className={style.vcontainer}>
              <iframe
                width='560'
                height='315' src={'https://www.youtube.com/embed/' + v} frameBorder='0' allowFullScreen />
            </div>
          ))}
        </div>
      </div>
    )
  }
}
