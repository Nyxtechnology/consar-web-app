import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'

const Social = () => (
  <div className={style.social}>
    <Link
      to='https://www.facebook.com/ConsarMexico/'
      target='_blank'
      className={style.fb}><i className='fa fa-facebook' /> Facebook</Link>
    <Link
      to='https://twitter.com/consar_mx?lang=es'
      target='_blank'
      className={style.tt}><i className='fa fa-twitter' /> Twitter</Link>
  </div>
)

export default Social
