import React from 'react'
import style from './style.css'
import Post from '../../../components/Post/'

const posts = [
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=214',
    img: '/static/img/posts/azul/1.png',
    title: '¿Qué diferencia hay entre régimen anterior y régimen de cuentas individuales del SAR?',
    desc: ''
  },
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=210',
    img: '/static/img/posts/azul/2.png',
    title: '¿De qué factores dependerá el monto de mi pensión?',
    desc: ''
  },
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=303',
    img: '/static/img/posts/azul/3.png',
    title: 'Altas y bajas: ¿Cómo impactan en mi cuenta AFORE?',
    desc: ''
  }
]

const Blog = (props) => (
  <div className={style.blog}>
    <Post {...{type: 1, posts, more: 'http://www.mifuturoconsentido.gob.mx/wordpress2/'}} />
  </div>
)

export default Blog
