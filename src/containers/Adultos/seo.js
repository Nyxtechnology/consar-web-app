
export default {
  title: '¿Tienes entre 28 y 36 años? Construye la aventura de tu vida | CONSAR',
  description: 'La CONSAR te ayuda en planear y alcanzar los grandes objetivos de tu vida: Formar una familia, Comprar una casa, lograr metas profesionales, emprender etc ',
  ogtitle: '¿Tienes entre 28 y 36 años? Construye la aventura de tu vida | CONSAR',
  ogdescription: 'La CONSAR te ayuda en planear y alcanzar los grandes objetivos de tu vida: Formar una familia, Comprar una casa, lograr metas profesionales, emprender etc ',
  keywords: [
    'Consar',
    'Aventura de mi vida',
    'Afore',
    'Objetivos de tu vida',
    'Dinero en casa',
    'Metodos para ahorrar dinero',
    'Planear futuro',
    'Formar una familia',
    'Comprar casa',
    'Alcanzar metas profesionales',
    'Emprender',
    'Asegurar mi fondo de emergencias',
    'Crecer mi negocio'
  ]
}
