import React from 'react'
import style from './style.css'

export default class Book extends React.Component {
  render = () => {
    return (
      <div className={style.bookContainer}>
        <div className={style.book + ' ' + (this.props.hover.name ? style.bookOpen : '')} id='book'>
          <ul className={style.hardcover_front}>
            <li>
              <img src='/static/img/adulto/cover.jpg' alt='' width='100%' height='100%' />
              <span className={style.ribbon + ' ' + style.bestseller}>Nº1</span>
            </li>
            <li />
          </ul>
          <ul className={style.page}>
            <li />
            <li>
              {this.props.hover.img ? <img src={'/static/img/menu/' + this.props.hover.img + '1.svg'} /> : null}
              <h4>{this.props.hover.name}</h4>
              <p>{this.props.hover.desc}</p>
            </li>
            <li />
            <li />
            <li />
          </ul>
          <ul className={style.hardcover_back}>
            <li />
            <li />
          </ul>
          <ul className={style.book_spine}>
            <li />
            <li />
          </ul>
        </div>
      </div>
    )
  }
}
