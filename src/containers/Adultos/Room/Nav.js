import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'
import data from '../../../components/MenuList/data'

const Nav = props => (
  <nav className={style.nav} id='menus'>
    {data().filter(i => i.name !== 'Blog').map((a, i) => (
      <div onMouseEnter={e => props.setHover(a)} onMouseLeave={e => props.setHover({})}>
        {a.href
          ? <a href={a.href} target='_blank'>
            <img src={'/static/img/adulto/icono' + a.img + '.svg'} />
            <div>{a.name}</div>
          </a>
          : <Link to={a.link}>
            <img src={'/static/img/adulto/icono' + a.img + '.svg'} />
            <div>{a.name}</div>
          </Link>
        }
      </div>
    ))}
  </nav>
)

export default Nav
