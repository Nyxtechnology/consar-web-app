import React from 'react'
import style from './style.css'

const Room = (props) => {
  const isNotStep1 = props.step !== 1
  const isNotStep2 = props.step !== 2
  const opacity = isNotStep1 ? 0 : 1
  const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1
  const items = [
    <svg
      className={applyClass(isNotStep1, style.circle1, style.circle1Hidden)}>
      <circle cx='50%' cy='50%' r='50%' />
    </svg>,
    <svg
      className={applyClass(isNotStep1, style.circle2, style.circle2Hidden)}>
      <circle cx='50%' cy='50%' r='50%' />
    </svg>,
    <svg
      className={applyClass(isNotStep1, style.circle3, style.circle3Hidden)}>
      <circle cx='50%' cy='50%' r='50%' />
    </svg>,
    <img
      className={applyClass(isNotStep1, style.years, style.yearsHidden)}
      src='/static/img/adulto/years.svg' />,
    <img
      className={applyClass(isNotStep1, style.window, style.windowHidden)}
      src='/static/img/adulto/window.svg' />,
    <img
      className={applyClass(isNotStep1, style.books, style.booksHidden)}
      src='/static/img/adulto/books.svg' />,
    <img
      className={applyClass(isNotStep1, style.clock, style.clockHidden)}
      src='/static/img/adulto/clock.svg' />,
    <img
      onClick={props.start}
      className={applyClass(isNotStep1, style.poster, style.posterHidden)}
      src='/static/img/adulto/poster.svg' />,
    <img
      className={applyClass(isNotStep1, style.bookcase, style.bookcaseHidden)}
      src='/static/img/adulto/bookcase.svg' />,
    <img
      className={applyClass(isNotStep1, style.desk, style.deskHidden)}
      src='/static/img/adulto/desk.svg' />,
    <img
      className={applyClass(isNotStep1, style.chair, style.chairHidden)}
      src='/static/img/adulto/chair.svg' />,
    <img
      className={applyClass(isNotStep1, style.dude, style.dudeHidden)}
      src='/static/img/c2836.png' />,
    <div
      className={applyClass(isNotStep1, style.text1, style.text1Hidden)}
      style={{transform: isNotStep1 ? 'translateY(200%)' : null, opacity}} >
      <h1>La Aventura de mi Vida</h1>
      <h2>
        Vives un momento de crecimiento personal y profesional. ¿Quieres probar
        nuevos retos y conseguir más triunfos? Aprovecha el empuje y disfruta
        rebasando tus propias fronteras.
      </h2>
    </div>
  ]

  return (
    <div className={style.room}>
      {items}
    </div>
  )
}

export default Room
