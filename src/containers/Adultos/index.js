import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as generalActions from '../../actions/general'
import style from './style.css'
import Video from '../../components/Video/'
import Room from './Room/'
import Book from './Room/Book'
import Nav from './Room/Nav'
import Blog from './Blog'
import Menu from '../../components/MenuList/'
import Layout from '../Layout/'
import seo from './seo'
import window from 'global/window'

class Adultos extends React.Component {
  static seo = seo
  state = {step: 0, process: false, hover: {}}
  componentDidMount = () => {
    if (!this.props.general.videos[1]) this.setState({step: 1})
    window.scrollTo(0, 0)
  }
  next = () => {
    this.props.closeVideo(1)
    this.setState({step: this.state.step + 1})
  }
  start = () => window.scrollTo(0, document.getElementById('menus').offsetTop + 180)
  setHover = hover => this.setState({hover})
  render = () => (
    <Layout {...this.props}>
      <div className={style.container} ref='container'>
        <div>
          <Video
            toggle={this.state.process ? null : this.next}
            step={this.props.general.videos[1]}
            url='lYZHmzrYwjo' />
          <Room {...{next: this.next, step: this.state.step, start: this.start}} />
          <div className={style.bookContainer}>
            <Nav {...{setHover: this.setHover}} />
            <Book {...{hover: this.state.hover}} />
            <Menu {...{type: 1, active: true, className: style.menulist}} />
          </div>
          {this.state.step ? <img className={style.footer} src='/static/img/adulto/footer.svg' /> : null}
          {this.state.step ? <Blog /> : null}
        </div>
      </div>
    </Layout>
  )
}

export default connect(({general}) => ({general}), (dispatch) =>
  bindActionCreators(generalActions, dispatch))(Adultos)
