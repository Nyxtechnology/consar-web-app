import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Negocio = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/emprender mi negocio.png' />
        <h1 style={{color}}>Emprender mi negocio</h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Diseña tu Modelo y Plan de Negocios. Puedes realizarlo tú mismo o con el apoyo de algún especialista.</p></li></strong>
                <strong><li><p>Determina la fuente de financiamiento de tu negocio.</p></li></strong>
                <strong><li><p>Realiza la constitución jurídica de tu negocio, ya sea como persona física o moral, e infórmate de las obligaciones fiscales necesarias.</p></li></strong>
              </ol>
              <img src='/static/img/projects/negocio1.jpg' />

              <p>
                Thomas renunció a una vida normal cuando decidió traer sus ideas a la realidad. Tras varios fracasos, sus amigos, sus padres y sus antiguos colegas le recomendaron volver a las oficinas de gobierno. Thomas respondió: “No he fracasado, simplemente he encontrado 10,000 maneras que no funcionan.” Gracias a eso, Thomas Alva Edison creó la bombilla eléctrica, entre cientos de inventos que revolucionaron nuestras vidas.
              </p>
              <p>
                ¿Te interesa emprender? ¿Eres del tipo de personas que dentro de 20 años estarás más decepcionado por las cosas que no hiciste que por las que hiciste? ¡Entonces suelta amarras y navega lejos de puertos seguros!
              </p>
              <p>
                Emprender es combinar elementos de una manera original para crear algo nuevo. Es idear un producto o servicio y volverlo rentable, es decir, lograr que tus ganancias sean mayores a tus costos.
              </p>
              <p>
                Al ser un emprendedor, buscas reunir a las personas correctas y los recursos adecuados para hacer posible tu idea, para generar un producto innovador o para producir uno que ya existe con valor agregado o distinción particular.
              </p>
            </div>
            <img src='/static/img/projects/negocio2.jpg' />
          </div>
              <h2 style={{color}}>Los beneficios de emprender tu negocio:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Alcanza un mejor nivel y satisfacción de vida: </strong>
                  <p>Cuando eres emprendedor puedes aspirar al nivel de ganancias que te propongas. Eres responsable de los sueldos de tus trabajadores y de su bienestar. Si eres buen líder, también dirigirás la filosofía de trabajo de tu equipo y el nivel de calidad de tus productos y servicios. Emprendiendo puedes generar un estilo de vida donde te sientas satisfecho con el tiempo y el esfuerzo que inviertas en tu día a día. Muy probablemente, tu negocio se convierta en tu causa personal, misma que compartas y retroalimentes con tu equipo y la veas realizada en la satisfacción de tus clientes.</p>
                </li>
                <li>
                  <strong style={{color}}>Dejar un legado a generaciones futuras: </strong>
                  <p>Al ser emprendedor también adquieres habilidades y experiencias que pueden favorecer a las generaciones venideras; la posibilidad de heredar inmuebles, bienes materiales o negocios brinda una ventaja a hijos y seres queridos.</p>
                </li>
                <li>
                  <strong style={{color}}>Generar empatía e impacto: </strong>
                  <p>Implica que tu proyecto o negocio genere inspiración y motivación, así como interés en quienes están involucrados en el proyecto, además de valor para las personas externas, como los clientes.</p>
                </li>
                <li>
                  <strong style={{color}}>Autonomía laboral: </strong>
                  <p>Decidir sobre tu idea representa la libertad que tendrás para definir las mejores prácticas y tener un ambiente laboral comprometido, gustoso y eficiente. Ser tu propio jefe te permite decidir cómo administrar tu trabajo, elegir los tiempos que consideres adecuados para aprovechar de mejor manera tus capacidades, recursos humanos y materiales.</p>
                </li>
                <li>
                  <strong style={{color}}>Independencia financiera: </strong>
                  <p>  Como empresario te enfocas en los ingresos, la rentabilidad y las utilidades, generados a partir de un proyecto propio y con base en tu esfuerzo y capacidad.
                    Por supuesto, el desarrollar una plataforma financiera sana y sustentable en tu negocio no tiene que ver con aislarse de otras fuentes de capital; la independencia financiera no te impide solicitar préstamos o alianzas para capitalizarte hacia nuevos proyectos. La capacidad de cumplir con responsabilidad a los compromisos y acuerdos que establezcas con tus socios va de la mano con tu capacidad de ser estratega y asertivo en las finanzas.</p>
                </li>
                <li>
                  <strong style={{color}}>Fuente de ahorro o de ingresos para el futuro: </strong>
                  <p>Al tener tu propio negocio no sólo aspirarías a tener el ingreso necesario para cubrir tus gastos a corto y mediano plazo, sino que podrías destinar una parte de tus ingresos para gastos futuros, como para el retiro. Podrías realizar aportaciones voluntarias a tu Cuenta AFORE, o que tu negocio se convierta en una fuente de ingresos adicionales o una actividad importante para el momento de tu jubilación.</p>
                </li>
              </ul>
              </div>
              <img src='/static/img/projects/negocio3.png' />

            </div>
              <h2 style={{color}}>Las implicaciones de emprender mi negocio:</h2>
            <div className={style.textContainer}>
              <div>
              <p>¿Estás listo para emprender? Toma en cuenta lo siguiente:</p>
              <ul>
                <li>
                  <strong style={{color}}>Valores profesionales y de vida: </strong>
                  <p>
                    Cuando encontramos la manera de incorporar valores como la tenacidad, la perseverancia y la disciplina a nuestra vida diaria y profesional, tenemos una mejor preparación para afrontar retos y adversidades. Es claro que deberás sacrificar ciertas cosas para conseguir que tu proyecto salga a flote o sea exitoso, por lo que la adopción de estos valores te será de mucha ayuda.
                  </p>
                </li>
                <li>
                  <strong style={{color}}>Curvas de aprendizaje: </strong>
                  <p>
                    Uno de los factores que hace que el crecimiento de los emprendedores sea mayor, es el desarrollo de su curva de aprendizaje. Aprovecha al máximo todo el proceso por medio del cual adquieras experiencia sobre cada área de tu negocio.
                  </p>
                </li>
                <li>
                  <strong style={{color}}>Conocimientos administrativos, contables y financieros: </strong>
                  <p>
                    Sin duda habrás escuchado el dicho “el que quiera tienda, que la atienda”. Es importante que adquieras y desarrolles conocimientos administrativos, financieros y contables, ya que éstos son fundamentales para la operación y crecimiento de tu empresa. Diversas instituciones te ofrecen cursos básicos a buen precio e incluso en línea.
                  </p>
                  <ul>
                    <li>
                      <strong style={{color}}>Administración: </strong>
                      <p>
                        Te ayudará a controlar los recursos que tienes disponibles de manera eficiente a fin de maximizar el rendimiento o utilidad de tu empresa, también te apoyará para determinar si tu equipo está haciendo un buen trabajo, así como a detectar, comprender y resolver ciertos retos que se presenten en el camino.
                      </p>
                    </li>
                    <li>
                      <strong style={{color}}>Contabilidad: </strong>
                      <p>
                        Conocer los principios básicos te ayudará a analizar estados de situación financiera, que reflejan el estado de “salud” de tu negocio. Se ocupa del registro de las entradas y salidas de los recursos de tu empresa.
                      </p>
                    </li>
                    <li>
                      <strong style={{color}}>Finanzas: </strong>
                      <p>
                        Abarca varios ámbitos de tu empresa. La primera se refiere al análisis de la situación financiera. Al combinar la información contable y los procesos administrativos podrás tomar decisiones informadas sobre puntos estratégicos que te ayudarán a corregir o impulsar tu negocio. Por ejemplo, áreas de oportunidad donde puedes ser más ahorrativo o donde tu producción debe crecer porque hay demanda. Además, un análisis financiero te ayudará a realizar proyecciones sobre tu negocio, dando respuesta a las siguientes preguntas:
                      </p>
                      <ul>
                        <li><p>¿Cuánto voy a vender?</p></li>
                        <li><p>¿Cuáles son mis gastos y costos?</p></li>
                        <li><p>¿Cuál es la utilidad?</p></li>
                        <li><p>¿Cómo puedo maximizar mi utilidad?</p></li>
                        <li><p>¿Cómo puedo minimizar los costos y gastos?</p></li>
                        <li><p>¿Cuál será el crecimiento de mi empresa en el mediano y largo plazo?</p></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <strong style={{color}}>Idea de negocio: </strong>
                  <p>
                    Se trata de desarrollar un producto o servicio para ser vendido, procurando que sea innovador o que se distinga por tener un valor adicional con respecto a productos similares en el mercado.
                  </p>
                </li>
                <li>
                  <strong style={{color}}>Modelo de negocios: </strong>
                  <p>
                    Es una representación de la organización en la empresa: ¿Qué vende? ¿Cómo vende? ¿Cómo se organiza? Habitualmente, cada uno de los conceptos relacionados con la forma en que tu organización generará ingresos se establece de manera puntual, así como el portafolio de productos o servicios que tu empresa ofrecerá.
                  </p>
                </li>
                <li>
                  <strong style={{color}}>Plan de negocios: </strong>
                  <p>
                    Es uno de los aspectos fundamentales que cualquier empresa debe realizar. En él se elabora una descripción minuciosa de la empresa, así como la planeación a nivel organizacional, administrativa, de marketing y de finanzas, identificando oportunidades, fortalezas, debilidades y amenazas. Se le conoce en inglés como business plan. Entre algunos de los puntos que incluye, están:
                  </p>
                  <ul>
                    <li>
                      <p>Objetivos estratégicos. Definir objetivos a corto, mediano y largo plazo.</p>
                    </li>
                    <li>
                      <p>
                        Modelo de producción. Cómo vas a elaborar tu producto, desde comprar la materia prima hasta la presentación del producto final. Es importante detallar todo el proceso minuciosamente.
                      </p>
                    </li>
                    <li>
                      <p>
                        Costos de producción y de administración. Acorde al modelo de producción, es importante anotar todos los costos, clasificados en costos fijos (como la renta, luz, gas) y costos variables (los insumos necesarios para realizar una cantidad determinada de tu producto, los sueldos de los trabajadores y los gastos colaterales, como la publicidad).
                      </p>
                    </li>
                    <li>
                      <p>
                        Estructura organizacional. ¿Cuántas personas vas a necesitar y con qué roles?
                      </p>
                    </li>
                    <li>
                      <p>
                        Planeación financiera. A qué rubros de la empresa reinvertirás, en qué orden y bajo qué objetivos.
                      </p>
                    </li>
                  </ul>
                </li>
                <li>
                  <strong style={{color}}>Estudio de mercado: </strong>
                  <p>
                    El estudio de mercado te proporciona información sobre las condiciones de competencia en el sector que te interesa, como el número de competidores, la participación de mercado, los niveles de oferta y demanda, entre otros. Usualmente se busca que el estudio lo realice una empresa especializada en este tipo de investigaciones. Existen incubadoras, agencias de marketing o las ahora llamadas empresas de consultoría que ofrecen servicios a precio accesible, así como programas gubernamentales destinados a apoyar el emprendimiento.
                  </p>
                </li>
                <li>
                  <strong style={{color}}>Constitución de una empresa: </strong>
                  <p>
                    Uno de los trámites más importantes para una empresa es constituirse jurídicamente, por lo que se debe evaluar si se trata de una persona física o moral. Una vez definido esto, deberás realizar el trámite pertinente para que la empresa sea registrada de manera oficial ante la ley.
                  </p>
                </li>
                <li>
                  <strong style={{color}}>Capital de inversión: </strong>
                  <p>
                    El capital de inversión se refiere a la cantidad de dinero inicial necesaria para comenzar a desempeñar una actividad económica con fines de lucro. Las principales vías de adquisición de capital son:
                  </p>
                  <ul>
                    <li><p>A través del capital de los propios fundadores de la empresa.</p></li>
                    <li><p>Apoyos o subvenciones gubernamentales. Actualmente existen instituciones gubernamentales dedicadas al apoyo de emprendedores, como el Instituto Nacional del Emprendedor (INADEM).</p></li>
                    <li><p>Préstamos o financiamientos de bancos.</p></li>
                    <li><p>Inversionistas externos, a quienes se debe presentar el plan de negocios.</p> </li>
                    <li><p>Plataformas de recaudación (crowdfunding).</p></li>
                  </ul>
                </li>
              </ul>
            </div>
            </div>
              <h2 style={{color}}>Requisitos que deben cumplir tú y tu empresa para solicitar y obtener un crédito.</h2>
            <div className={style.textContainer}>
              <div>
              <ul>
                <li>
                  <strong style={{color}}>Financiamiento: </strong>
                  <ul>
                    <li>
                      <strong style={{color}}>Edad: </strong>
                      <p>
                        Cada institución tiene requisitos particulares, pero en la mayoría de los casos la edad mínima del candidato es de 25 a 30 años.
                      </p>
                    </li>
                    <li>
                      <strong style={{color}}>Antigüedad de la empresa: </strong>
                      <p>
                        Para ser candidato a un crédito es necesario que la empresa tenga al menos entre dos y tres años de operación. Con ello se busca garantizar el haber superado el despegue de la empresa. En caso de ser persona moral, es necesario que tengas dos años de actividad y si se trata de una persona física con actividad empresarial, tres años de operación ininterrumpida en el giro o actividad.
                      </p>
                    </li>
                    <li>
                      <strong style={{color}}>Flujo de Efectivo: </strong>
                      <p>
                        Te solicitarán estados financieros y estados de cuenta bancarios de los últimos dos o tres años previos a la solicitud del crédito. Algunos bancos exigen ventas mínimas anuales, con montos de alrededor de 50,000 pesos mensuales.
                      </p>
                    </li>
                    <li>
                      <strong style={{color}}>Tener un buen historial crediticio: </strong>
                      <p>
                        La puntualidad en el pago de otros créditos puede ser determinante para que tu solicitud sea rechazada o aceptada. Todas las personas pueden obtener su Reporte de Crédito Especial de manera gratuita en el Buró de Crédito en su sitio web: <a style={{color}} target='_blank' href='www.burodecredito.com.mx'>www.burodecredito.com.mx</a>.
                      </p>
                    </li>
                    <li>
                      <strong style={{color}}>Garantías: </strong>
                      <p>
                        En la mayoría de los casos se solicitan garantías de pago, como tener un aval. Si el crédito que pides es muy alto o de largo plazo, lo más probable es que te pidan una garantía hipotecaria.
                      </p>
                    </li>
                  </ul>
                </li>
                <li>
                  <strong style={{color}}>Requerimientos Fiscales: </strong>
                  <p>
                    Al abrir un negocio, debes cumplir ciertas obligaciones fiscales:
                  </p>
                  <ul>
                    <li><p>Obtener el Registro Federal de Contribuyentes (RFC).</p></li>
                    <li><p>Solicitar tu Firma Electrónica (FIEL).</p></li>
                    <li><p>Tramitar el Certificado de Sello Digital (CSD).</p></li>
                    <li><p>Buscar un software que te permita generar, enviar y almacenar facturas electrónicas.</p></li>
                    <li>
                      <p>El SAT obliga a todos los contribuyentes a emitir Comprobantes Fiscales Digitales por Internet (CFDI), así como recibos de nómina electrónicos a todos los trabajadores a los que la empresa pague sueldos, salarios o ingresos asimilados.</p>
                    </li>
                  </ul>
                  <p>Con todo lo anterior podrás y deberás presentar declaraciones mensuales de:</p>
                  <ul>
                    <li><p>Impuesto Sobre la Renta (ISR).</p></li>
                    <li><p>Solicitar tu Firma Electrónica (FIEL).</p></li>
                    <li><p>Impuesto al Valor Agregado (IVA).</p></li>
                    <li><p>Retenciones de IVA e ISR.</p></li>
                    <li><p>Información de Operaciones con Terceros (DIOT).</p></li>
                    <li><p>Impuestos locales.</p></li>
                  </ul>
                </li>
                <p>Presentar las declaraciones anuales de IVA, ISR y la Informativa Múltiple (DIM).</p>
              </ul>
              <p>Cumplir tus obligaciones fiscales ante el SAT te permite mantener la salud financiera de tu empresa. </p>
              <blockquote>“Ser un emprendedor es vivir unos pocos años de tu vida como nadie quiere, de tal forma que puedes disfrutar del resto de tu vida como nadie puede.”
                <footer>— Anonimo</footer>
              </blockquote>
              <h3 style={{color}}>Ligas de interés</h3>
              <ul>
                <li><a target='_blank' style={{color}} href='http://www.creditojoven.gob.mx/portalcj/content/index.html'>Crédito joven:</a></li>
                <li><a target='_blank' style={{color}} href='https://www.inadem.gob.mx/'>INADEM - Instituto Nacional del Emprendedor</a></li>
                <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvcapitalemprendedor.html'>CONDUSEF – Capital emprendedo</a></li>
                <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvinversion.html'>CONDUSEF – Cuadernos y videos: Inversión</a></li>
                <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvcredito.html'>CONDUSEF – Cuadernos y videos: Crédito</a></li>
                <li><a target='_blank' style={{color}} href='http://www.condusef.gob.mx/gbmx/?p=comercio-electronico'>CONDUSEF – Comercio electrónico</a></li>
              </ul>
            </div>
          </div>
      </div>
    </Layout>
  )
}

Negocio.seo = {
  title: 'Emprender mi negocio | CONSAR ',
  description: '¿Qué sería de la vida si no tuviéramos el valor de intentar cosas nuevas? Me decidí a apostarle todo a mis ideas.',
  ogtitle: 'Emprender mi negocio | CONSAR',
  ogdescription: '¿Qué sería de la vida si no tuviéramos el valor de intentar cosas nuevas? Me decidí a apostarle todo a mis ideas.',
  keywords: [
    'consar',
    'Emprender mi negocio'
  ]
}

export default Negocio
