import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Familia = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/formar una familia.png' />
        <h1 style={{color}}>Formar mi propia familia</h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Planifica tu familia: decide cuándo y cuántos hijos tendrás. Concluye deudas y dispón de ahorros que ayuden a solventar el embarazo, el parto y los cuidados post parto.</p></li></strong>
                <strong><li><p>En la etapa prenatal realiza estudios clínicos a la mamá y el futuro bebé, prevé el acondicionamiento físico del nuevo integrante y quién apoyará en sus cuidados, así como los gastos hospitalarios de ambos, derivados del nacimiento.</p></li></strong>
                <strong><li><p>Prevé los gastos derivados del crecimiento de tu familia en términos de: un espacio adecuado, servicios básicos, educación, salud, desarrollo integral, apoyo doméstico/guardería, entre otros.</p></li></strong>
                <strong><li><p>Comienza a formar un patrimonio para tus hijos desde el momento en que nacen: abrir una cuenta AFORE para menores es una excelente opción para prevenir las etapas futuras de la vida. Con igual y hasta mayor importancia, no descuides tu propio ahorro para el retiro y el de tu pareja.</p></li></strong>
              </ol>
              <img src='/static/img/projects/familia1.jpg' />

              <p>Más allá de permanecer en gustosa convivencia y cohabitación con tu familia, o establecerte por tu cuenta para vivir con tu pareja, el proyecto de formar tu propia familia es probablemente uno de los más importantes a nivel de reafirmación personal.</p>
              <p>Implica, por encima de todo, tu capacidad de dar: desde dar vida hasta dar y participar en todo lo que alimente y desarrolle a las personas involucradas en este gran proyecto, empezando honestamente por ti, tu pareja, tus hijas o hijos y también, aunque en un principio no lo perezca, a tus familiares y los de tu pareja.</p>
              <p>Formar tu propia familia implica responsabilidad, y es que independientemente de que sea un proyecto planeado o que surja por circunstancias inesperadas (entiéndase claramente “quedar embarazados” sin preverlo, porque sucede y sucede mucho), asumir esta responsabilidad y decidir disfrutarla durante un importante periodo de tu vida es sin duda un preciado acto de compromiso y convicción.</p>
              <p>Los hijos son un detonador y potenciador natural de acciones. Fácilmente se convierten el motor de gran parte de lo que harás y planearás como madre o padre. Por lo mismo, la experiencia te advierte para que no descuides y relegues tu individualidad, tus intereses y tus sueños, así como los de tu pareja. En la medida de tus posibilidades, busca siempre un buen equilibrio atendiendo un máximo de frentes. Verás que son muchas etapas, siendo las primeras las de atender con toda prioridad a tus pequeños y, dicho sea de paso, ¡atesorando cualquier oportunidad para dormir! A medida que crezca o crezcan, podrás recuperar aspectos de tu vida personal y de tu pareja que quedaron momentáneamente relegados.</p>
              <p>Existen un sinfín de aspectos qué explorar en este vasto proyecto de vida, mismos que no cabrían en esta ficha, pero mencionaremos algunos de los más relevantes. Además, formar tu propia familia será una prueba permanente, pues cada vez que creas entender cómo funciona uno u otro aspecto, te sorprenderás descubriendo que ya cambió todo, de nuevo. ¡Qué mejor prueba para reinventarse y crecer todos los días!</p>
            </div>
            <img src='/static/img/projects/familia2.jpg' />

          </div>
          <h2 style={{color}}>Los beneficios de formar mi propia familia:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Desarrollo personal</strong>
                    <p>Probablemente, uno de los mayores beneficios radica en la enorme cantidad de circunstancias que tarde o temprano provocan y alimentan tu crecimiento personal en todos los ámbitos, y solo depende de ti verlas, medirlas, aprovecharlas y valorarlas.</p>
                    <p>Por ejemplo, en un principio enfrentas el desapego familiar. Dejas el espacio creado por tus padres para crear el tuyo propio. Te descubres y experimentas, definiendo poco a poco tu propio mundo, mismo que en un momento dado es invadido y conquistado por tu pareja, pero no te preocupes, es muy probable que tú hayas hecho lo mismo con el suyo, y si no, efectivamente, preocúpate.</p>
                    <p>Suponiendo que no hay vencedor ni vencido, descubres entonces un universo de oportunidades, de entre las cuales formar tu propia familia sin duda terminará por hacerse presente, y para ese entonces ¡ni siquiera estarás a la mitad del camino!</p>
                    <p>Será tu turno de volver a empezar, desde el otro lado de la moneda, y no solo porque como padre o madre debas ser el proveedor de todo lo necesario para tu propia familia, sino porque volverás a enfrentar, por ejemplo, el desapego, pero ahora para trabajarlo con tus propios hijos, por el bien de todos, y así sucesivamente en un sinfín de circunstancias.</p>
                    <p>Todas estas oportunidades de ser y compartir, de hacer equipo, de hacer familia, son inagotables. Al cabo de muchos años, y solo hasta entonces, quizá puedas contestarte la pregunta: ¿Quién dio y enseñó más a quién, yo a mis hijos, o mis hijos a mí? El proyecto es descubrirse y volverse a cubrir, con más vida.</p>
                </li>
                <li>
                  <strong style={{color}}>Descendencia y trascendencia</strong>
                  <p>Además del llamado instintivo y emocional de tener hijos, que a su vez te podrían dar nietos y demás generaciones venideras, crear tu propia familia responde a un sentido de evolución, donde tú y tu pareja son a su vez parte de árboles genealógicos. ¿Conoces la historia de tus antepasados? ¿Cuántas generaciones atrás? Trazar tu árbol genealógico y el de tu pareja puede resultar una tarea muy interesante y enriquecedora.</p>
                  <p>Con respecto a la descendencia, hay quienes se lo toman con sencillez y sin prestar demasiada atención a las características de la pareja con quien tendrán hijos, pero también hay quienes se lo toman muy a pecho, estableciendo y cuidando el linaje, es decir, dando gran importancia a las variables sociales, culturales y religiosas, entre otras, que influyen en la trascendencia de las creencias y tradiciones familiares de generación en generación. Una vez más, es cosa de cada quien.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/familia3.jpg' />

          </div>
          <h2 style={{color}}>Las implicaciones de formar mi propia familia:</h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Compromiso y responsabilidad</strong>
                  <p>Formar una familia implica compromiso y responsabilidad de, y para, cada uno de sus integrantes. Antes de formar tu propia familia, tú y tu pareja deberán analizar si están listos desde el punto de vista mental, emocional y financiero ya que las decisiones que tomen afectarán a todos los integrantes de su núcleo familiar. </p>
                  <p>Se sobreentiende que la responsabilidad primordial es de los padres o jefes de familia, quienes deben poner todos los recursos y la dedicación para asegurar el bienestar de sus hijos, pero además, también puede tratarse del apoyo a sus propios padres u otros familiares. Y como se dijo anteriormente, tarde o temprano es un trabajo en equipo.</p>
                  <p>De alguna manera, tu responsabilidad como padre o madre es ilimitada, ¿o quién sería capaz de delimitar lo que se necesita para la formación de un hijo o hija? Y con ello no se trata de darlo todo a toda costa, sino de aspirar a dar calidad antes que cantidad. ¿Qué padres no desearían ver superados por sus hijos, sus temores, sus carencias o sus limitaciones? Y sin embargo… es una tarea para la cual no hay un manual infalible; solo queda reinventarse a cada paso.</p>
                  <p>Por ejemplo, para la educación de los hijos, entre la influencia de la educación formal en la escuela, la influencia de los padres y del hogar, y la influencia de los amigos y del medio social, sabemos que es una compleja combinación entre todas, donde cada parte juega un papel decisivo, pero ¿qué tanto puedes intervenir e influir en ello desde tu rol de padre o madre? La respuesta se reelabora constantemente, y ello es parte del compromiso adquirido.</p>
                </li>
                <li>
                  <strong style={{color}}>Planificación familiar</strong>
                  <p>Planificar tu familia es decidir cuándo y cuántos hijos tendrás. Significa ante todo tu elección de encauzar las circunstancias a diferencia de estar a la expectativa de lo que te toque; significa que tomes el control, organices tus recursos, elijas y aproveches los momentos y sus oportunidades. </p>
                  <p>Con respecto a preparar la llegada de un nuevo miembro en tu familia:</p>
                  <ul>
                    <li><p>En su caso, concluye toda deuda pendiente.</p></li>
                    <li><p>Date el tiempo necesario para disponer de ahorros a fin de solventar el embarazo, el parto, y los cuidados post parto. Disponer de tus ahorros te permite evitar recurrir a un crédito que te cargue intereses y presiones por más tiempo.</p></li>
                    <li><p>Prepara un fondo de ahorros para imprevistos, como una baja en tus ingresos durante ese periodo o cualquier complicación. Pensar en “lo peor” no significa desearlo, sino prevenirse para, en caso dado, enfrentarlo mejor y superarlo.</p></li>
                    <li><p>Valora tus recursos en general para la mayor estabilidad posible en tu actividad o trabajo, en la organización del hogar y en el seguimiento de pendientes y compromisos.</p></li>
                  </ul>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Prepararse. La etapa prenatal</strong>
                  <ul>
                    <li><p>Realizar estudios que aseguren la salud de la madre y del futuro bebé, durante el embarazo.</p></li>
                    <li><p>En su momento, prever el acondicionamiento físico que necesitará el bebé, desde el acomodo y los accesorios para que duerma, para cambiarlo y para bañarlo, así como la definición de roles que desempeñarán los padres.</p></li>
                    <li><p>Apalabrar el posible apoyo experimentado de familiares.</p></li>
                    <li><p>Investigar el lugar que desean o más les conviene para que nazca el bebé, cuantificar y prever los recursos para cubrir los gastos de diferentes aspectos, como el parto o la cesárea, la estancia, exámenes médicos diversos, honorarios médicos, etc.</p></li>
                    <li><p>Estar informados sobre el parto o la cesárea, así como cuáles son las implicaciones para la mamá y para el bebé.</p> </li>
                    <li><p>Investigar los beneficios de contratar un seguro que cubra los gastos del parto.</p></li>
                    <li><p>Informarse sobre los estudios de laboratorio para el recién nacido y el seguimiento pediátrico, así como el seguimiento ginecológico post parto de la madre.</p></li>
                  </ul>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Lo que podría añadirse al presupuesto familiar</strong>
                  <p>Determina si tu situación financiera te permite asumir los gastos derivados de crecer el número de integrantes de tu familia.
                  </p>
                  <ul>
                    <li>
                      <strong style={{color}}>Servicios básicos:</strong>
                      <p>Podría aumentar sensiblemente el consumo de energía eléctrica, agua, gas, etc.</p>
                    </li>
                    <li>
                      <strong style={{color}}>Salud</strong>
                      <p>La capacidad de procurar atención médica ante situaciones relacionadas con la salud de los integrantes de la familia, particularmente a temprana edad de los hijos, pero también, para ser realistas y ver el cuadro completo, prevé los posibles cuidados de salud durante tu vejes y la de tu pareja.</p>
                    </li>
                    <li>
                      <strong style={{color}}>Educación:</strong>
                      <p>Estos gastos pueden ser muy variables. Como sea, hay muchos esquemas de becas que puedes comenzar a pagar con antelación para cubrir, por ejemplo, los gastos por concepto de colegiaturas de escuela y de universidad.</p>
                    </li>
                    <li>
                      <strong style={{color}}>Desarrollo integral:</strong>
                      <p>Estos gastos también pueden ser muy variables, entendidos como las cuotas, los honorarios o la adquisición de artículos para realizar deportes, aprender idiomas o a tocar algún instrumento musical, entre muchos más.</p>
                    </li>
                    <li>
                      <strong style={{color}}>Apoyo doméstico, familiar y guardería:</strong>
                      <p>Cuando vivas el momento de la maternidad/paternidad, resultará indispensable contar con apoyo doméstico ya sea por parte de un familiar o de alguien contratado ex profeso para este fin. Prevé la implicación de los honorarios para cubrir estos servicios dentro de tu presupuesto familiar.</p>
                    </li>
                    <li>
                      <strong style={{color}}>Infancia y finanzas:</strong>
                      <p>En su momento, que puede ser a muy temprana edad de los hijos, podrás compartir diversas actividades de educación financiera en familia, mismas que serán enseñanzas formativas para toda la vida. Acciones tan sencillas como descubrir una moneda o billete debajo la almohada a cambio de un diente de leche caído, o dar “el domingo” o la mesada como pago por cierta participación en los quehaceres del hogar, o ahorrar en una alcancía o en una cuenta bancaria para un objetivo específico, son pequeños procesos que permiten hablar del dinero y de cómo funciona la economía.</p>
                      <p>Como padres, existe un impulso natural de reunir algún tipo de patrimonio para tus hijos, que les ayude a futuro, en alguna situación especial. Por ejemplo, ya es posible abrir una cuenta AFORE para tus hijos desde que nacen. Si bien no se piensa en ahorrar para el retiro de un bebé, considera que ahorrar de manera voluntaria en una cuenta AFORE a nombre de tus hijos, desde sus primeros años de vida, tiene la gran ventaja de aprovechar el efecto acumulativo del interés compuesto, es decir, ganar rendimientos que se reinvierten y te dan aún más rendimientos de manera exponencial a lo largo de años y años. El ahorro también puede ser usado en el mediano plazo, por ejemplo, para la educación de tus hijos.</p>
                      <p>¿Te interesa este tema? Consulta la siguiente nota y visualiza el interés compuesto en sus gráficas:</p>
                      <a target='_blank' style={{color}} href='https://www.gob.mx/consar/articulos/cuenta-afore-para-ninos-quien-educa-a-quien?idiom=es'>Ir a la nota</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='http://www.imss.gob.mx/derechoH'>IMSS – Instituto Mexicano del Seguro Social</a></li>
              <li><a target='_blank' style={{color}} href='https://www.gob.mx/difnacional'>DIF – Sistema Nacional para el Desarrollo Integral de la Familia. Portal.</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_tenerunhijo.html'>CONDUSEF – Eventos en tu vida: Tener un hijo</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/guias.html'>CONDUSEF – Guía familiar de educación financiera</a></li>
              <li><a target='_blank' style={{color}} href='https://www.unicef.org/es'>UNICEF - Fondo de las Naciones Unidas para la Infancia</a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Familia.seo = {
  title: 'Formar mi propia familia | CONSAR ',
  description: 'Si lidiar con uno mismo no es cualquier cosa, hacerlo en familia, ¡es sensacional!',
  ogtitle: 'Formar mi propia familia | CONSAR',
  ogdescription: 'Si lidiar con uno mismo no es cualquier cosa, hacerlo en familia, ¡es sensacional!',
  keywords: [
    'consar',
    'Formar mi propia familia'
  ]
}

export default Familia
