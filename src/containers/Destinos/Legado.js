import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Legado = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/legado famiiar.png' />
        <h1 style={{color}}>Construir una herencia </h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Construir una herencia consiste en crear un patrimonio a lo largo de tu vida con el objetivo de que más adelante, quien tú decidas se beneficie de él.
                </p></li></strong>
                <strong><li><p>Considerando tu esfuerzo y dedicación en la construcción de tu legado, piensa en preparar lo mejor posible a quien lo recibirá, para que minimices la posibilidad de que el bien sea desmantelado, malvendido y despilfarrado.
                </p></li></strong>
                <strong><li><p>Haz tu testamento oportunamente. Basta con que poseas algún patrimonio para que lo puedas establecer y con ello te asegures de que tus recursos pasen a manos de quien tú quieras, en caso de que fallezcas.</p></li></strong>
                <strong><li><p>En cualquier momento puedes actualizar tu testamento, anulando la versión anterior.</p></li></strong>
                <strong><li><p>Aprovecha la campaña “Septiembre, mes del testamento” a través de la cual los Notarios del país otorgan asesoría gratuita sobre la forma de elaborar tu testamento, además de que reducen los costos de sus honorarios hasta en un 50%.</p></li></strong>
              </ol>
              <img src='/static/img/projects/herencia1.jpg' />

              <p>¿Has pensado en la diferencia que existe entre construir tu patrimonio “hasta donde y como puedas” para legarlo algún día, y construir una herencia guiándote por un propósito específico que trascienda?</p>
              <p>Es diferente y es una cuestión de enfoques: En el primer caso, la creación de tu patrimonio no depende directamente de ti y le cedes al azar la oportunidad de construirlo, o no. En el segundo caso, te ubicas como el responsable de lo que construirás y por lo mismo, obtienes una causa que te sirve como motor y empuje para avanzar; estableces una visión que seguir.</p>
              <p>Construir una herencia es un tema muy amplio y sin duda de largo plazo. En el sentido más obvio, puede consistir en acumular bienes para dejarlos a una o varias personas a nivel familiar o empresarial, sin embargo, también puede ser un legado de objetos con un valor sentimental y/o moral, o un concepto dentro de un marco ideológico particular, es decir, un bien que se acompaña de reglas, principios o siguiendo una determinada visión, entre otros.</p>
              <p>Hay dos puntos de vista sobre este proyecto, que son igualmente interesantes y útiles: Por un lado está la construcción de la herencia, que es el trabajo que le dedicarás a la idea de tu proyecto y los resultados que lograrás durante tu vida para darle valor a ese bien, y/o, por otro lado, tratándose de una herencia, también presupone que en cierto momento planees ceder la estafeta y sobre todo cómo imaginas que lograrás esa transición. </p>
              <p>Sin duda, ello depende de lo que hayas construido, pues no es lo mismo heredar cierta cantidad de dinero, o un negocio que implica personal e instalaciones, o un gran terreno en un lugar maravilloso pero en litigio debido a que nunca atendiste correctamente tus obligaciones prediales o lo que corresponda.</p>
              <p>De alguna manera, si adoptas el proyecto de construir una herencia, probablemente le estés dando forma y valor a la trascendencia de muchas de las acciones que te hayas propuesto realizar en toda la aventura de tu vida.</p>
            </div>
            <img src='/static/img/projects/herencia2.jpg' />

          </div>
          <h2 style={{color}}>Los beneficios de construir una herencia</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Patrimonio familiar</strong>
                  <p>Lo más común es pensar en dejarle a los hijos o familiares directos, o cercanos, una herencia que les facilite aunque sea un poco el camino que uno mismo recorrió pero con menos apoyo. Por ejemplo, si puedes heredar una vivienda a tus hijos, ellos podrán concentrar sus recursos y esfuerzos en buscar mayor capacitación para sus actividades, o pagar los estudios de sus propios hijos, o invertir en negocios, todo ello para aspirar a una mejor calidad de vida, de una generación a otra. </p>
                </li>
                <li>
                  <strong style={{color}}>Legado de valores y conocimiento</strong>
                  <p>Además de los bienes materiales, tu legado se acompaña de tu forma de pensar, o dicho de otra manera, de lo que para ti es la fórmula para darle seguimiento al bien que legarás. Por supuesto, esto es mucho más complejo que simplemente ceder un recurso material, y por lo mismo, puede darte mucha satisfacción ver en tus herederos el potencial para que adopten y a su vez desarrollen el proyecto que desarrollaste. De alguna manera, formarlos para ello se convierte en parte de lo que les heredarás.</p>
                </li>
                <li>
                  <strong style={{color}}>Sentimiento de trascendencia</strong>
                  <p>Este proyecto también va ligado a posibles anhelos que pudieras tener, como el sentido de trascendencia. Más allá de reunir bienes y recursos para beneficio de otros en el futuro, la idea de construir un proyecto de largo plazo puede ir ligado a la intención de que el mismo perdure, trascienda y siga su desarrollo, aunque ya no estés o participes en él.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/herencia3.jpg' />

          </div>
          <h2 style={{color}}>Las implicaciones de construir una herencia</h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Cantidad y calidad de lo construido</strong>
                  <p>Construir una herencia implica cantidad, procurando que el bien sea lo más grande y valioso posible, pero de igual forma implica calidad, en el entendido de que la herencia pueda trascender y no se convierta en una carga o problema para quienes la reciban.</p>
                  <p>Puede que como constructor de la herencia no te preocupe el devenir de la misma cuando ya no estés, aunque por lo general, a todos nos molesta ver desperdiciado lo que nos costó sangre, sudor y lágrimas.</p>
                  <p>Como se mencionó anteriormente, el bien por heredar puede ser algo concreto que no requiera mayores explicaciones o experiencia para ser poseído o utilizado, como un monto económico, un predio o una vivienda, entre otros, pero también podría ser algo mucho más complejo, donde los herederos deban tener conocimientos específicos y probada experiencia, como para un negocio o bienes difíciles de tener u operar.
                  En ambos casos, como constructor de la herencia y con el objetivo de no verla desmantelada, malvendida y despilfarrada, tienes que conocer y preparar a tus herederos. Si se trata de dinero, debes “asegurarte” de que se invierta y se le dé un buen uso; si se trata de un negocio, tendrás que dedicar parte de tu proceso de construcción de la herencia para preparar a los herederos, de manera a que estén interesados, motivados y confiados en sí mismos para encaminar el seguimiento.</p>
                  <p>En este caso, recuerda que el éxito para la trascendencia de tu proyecto/herencia depende de tu capacidad para enseñar, es decir, para delegar a otros las tareas importantes, darles espacio para participar opinando y actuando, para que se equivoquen y aprendan y, a fin de cuentas, para que hagan propio el proyecto.</p>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Los herederos: saber valorar</strong>
                  <p>Bien dice el dicho: “Lo que fácil viene, fácil se va”. Parafraseando el punto anterior, una parte fundamental de construir una herencia y que sea bien aprovechada es lograr que los herederos la valoren lo mejor posible.</p>
                  <p>Tradicionalmente, primero se piensa en los hijos o los familiares directos como herederos naturales, pero ello no garantiza que sea tu única o la mejor elección. La valoración de los herederos es importante y depende de cada quien, solo cerciórate de que lo que construyas como herencia sea recibido por quien lo sepa valorar.</p>
                </li>
                <li>
                  <strong style={{color}}>El testamento</strong>
                  <p>Hacer tu testamento oportunamente evita que heredes problemas a otros.</p>
                  <p>El testamento es un instrumento legal establecido y registrado ante un Notario público, en el que se manifiesta tu voluntad sobre el destino que deseas para tus bienes y derechos cuando fallezcas. Esto evita incertidumbre y conflictos, familiares y en general, sobre la determinación de quién tiene derecho a qué.</p>
                  <p>En el supuesto de que no dejaras un testamento, la ley establece a tus herederos y las proporciones. Esta situación puede provocar que tus familiares enfrenten gastos elevados y surjan conflictos entre personas que consideren tener derecho a recibir tus bienes. Ello puede derivar en tener que tramitar un juicio sucesorio ante el Juzgado de lo Familiar para determinar el reconocimiento de herederos, que en muchas ocasiones puede no coincidir con la voluntad de la persona que falleció y que no formuló su testamento.</p>
                  <p>La Secretaría de Gobernación emprende una campaña anual llamada “Septiembre, mes del testamento”, para promover el ejercicio del testamento y contribuir a una cultura de previsión, de certeza y seguridad jurídica en el derecho a heredar. Para ello, los Notarios del país participan otorgando asesoría gratuita a los interesados sobre la forma de elaborar su testamento, además de que amplían sus horarios de labores y reducen los costos de sus honorarios hasta en un 50%.</p>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvahorro.html'>CONDUSEF – Cuadernos y videos: Ahorro
              </a></li>
              <li><a target='_blank' style={{color}} href='https://www.gob.mx/testamento'>Mes del testamento
              </a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvinversion.html'>CONDUSEF – Cuadernos y videos: Inversión
              </a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Legado.seo = {
  title: 'Construir una herencia | CONSAR ',
  description: 'Dejar un legado con la sabiduría de las experiencias, las cicatrices y los trofeos, no tiene precio.',
  ogtitle: 'Construir una herencia | CONSAR',
  ogdescription: 'Dejar un legado con la sabiduría de las experiencias, las cicatrices y los trofeos, no tiene precio.',
  keywords: [
    'consar',
    'Construir una herencia'
  ]
}

export default Legado
