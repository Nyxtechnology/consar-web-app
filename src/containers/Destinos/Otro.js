import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Otro = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/otro mi gran objetivo es.png' />
        <h1 style={{color}}>Mi gran objetivo es…</h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <p>¿Cuál dirías que es tu razón de ser en la vida, o tus razones? Muchas personas no lo saben, mientras que otras ya han encontrado la respuesta y te pueden platicar cuál consideran que es su papel, su misión u objetivo primordial que da sentido a sus vidas.</p>
              <p>En “La Aventura de mi Vida” te proponemos algunos “objetivos de vida” expresados como proyectos, que pueden ser bastante comunes, aunque existen muchos más. Si tienes otro proyecto específico y propio que no está dentro de las opciones que te presentamos aquí, defínelo y persíguelo.</p>
              <p>¿Sabes si es una idea, un sueño, un objetivo o un proyecto? De tu capacidad para describirlo depende tu posibilidad de dimensionarlo y, a su vez, de emprender lo necesario para realizarlo. ¿Por dónde empezar? ¿Cómo hacer de tus convicciones una realidad? </p>
              <p>Empieza por definir tu proyecto:</p>
              <ol>
                <strong><li><p>Consideraciones generales. Describe primero de qué se trata, las características que a tu juicio son las esenciales y como si se lo contaras a alguien. Evita que tu proyecto se quede en lo abstracto. Si es necesario, apóyate en ejemplos y comparaciones. En un principio, mientras más “aterrizado” lo tengas más sencillo será desarrollarlo.</p></li></strong>
                <strong><li><p>Beneficios. Describe sus ventajas: qué te da, qué logras, por qué es un proyecto tan importante y cómo te mueve.</p></li></strong>
                <strong><li><p>Requisitos. Qué necesitas para llevarlo a cabo.</p></li></strong>
                <strong><li><p>Implicaciones. Qué debes cambiar o adecuar de ti mismo y del contexto general para poder llevarlo a cabo.</p></li></strong>
              </ol>
              <img src='/static/img/projects/otro1.jpg' />
              <p>Asegúrate de que sea lo que quieres:</p>
              <p>La mejor forma de comprobar a qué grado te mueve tu proyecto, es haciéndolo. Si lograste describir los cuatro puntos anteriores, pasarás a la acción, empezando por las implicaciones, es decir, adecuando lo que sea necesario para que tu proyecto se encamine. Si realmente te mueve, verás que lo que otros ven como barreras o problemas para ti son retos por resolver. De tu convicción depende que todas las experiencias alrededor de tu proyecto te resulten constructivas y útiles. Después de todo, “querer es poder”.</p>
              <p>Procura que en un principio, la idea de tu proyecto sea sencilla. A medida de que lo desarrolles podrás ampliar el concepto y su grado de detalle. Lo importante es que su posible grado de complejidad o crecimiento no te rebase y pierdas la capacidad de organizar su realización.</p>
              <p>Por lo mismo, al inicio, busca que tu proyecto sea:</p>
              <ul>
                <li>
                  <strong style={{color}}>Específico</strong>
                  <p>Que no se preste a confusión. Debes poder describirlo fácilmente; a mayor grado de explicaciones, mayor complejidad y trabajo para visualizarlo mentalmente y realizarlo.</p>
                </li>
                <li><strong style={{color}}>Viable</strong>
                  <p>Sé ambicioso, definitivamente, pero con los pies sobre la tierra. Analizar la viabilidad de tu proyecto te sirve para medir tu potencial y saber si concibes, dentro de ese mismo sueño o proyecto, la posibilidad de llegar a él.</p>
                  <p>Considera que si es demasiado ambicioso para ti, si es incongruente con tu convicción o potencial, probablemente te desmotives con los primeros tropiezos. Y no hay problema: redimensiona o cambia tu proyecto. En cambio, si te das cuenta que los tropiezos en realidad no lo son, ya vas por buen camino. Cuando tu proyecto es viable, desarrollas aptitudes, habilidades, destrezas y capacidades diversas para conseguirlo.</p>
                </li>
                <li>
                  <strong style={{color}}>Medible</strong>
                  <p>Es importante que puedas medir lo que haces y lo que obtienes, para saber si vas bien o mal. Mide las consecuencias y los resultados de tus acciones con indicadores, es decir, valores –generalmente numéricos- que te permiten cuantificar lo hecho. Por ejemplo, que el número de quejas en un buzón de sugerencias sea menor a 5 al mes, puede ser un indicador de determinado desempeño para un área de atención al público.</p>
                  <p>Recuerda que para alcanzar el objetivo de tu proyecto puedes establecer tantas metas como te sea necesario, y cada una de ellas con su o sus respectivos indicadores. El seguimiento te será más claro.</p>
                </li>
              </ul>
              <p>Finalmente, organiza tu agenda, tu presupuesto y lo que veas que es necesario para realizar tu gran objetivo. Revisa los proyectos de La Aventura de mi Vida que tengan alguna similitud con el tuyo, particularmente el rubro de acciones financieras clave. Ello podrá darte ideas y serte de utilidad al estructurar la parte financiera de tu propio objetivo. </p>
            </div>
          </div>
      </div>
    </Layout>
  )
}
Otro.seo = {
  title: 'Otro: Mi gran objetivo es… | CONSAR ',
  description: 'Soñar no cuesta nada. Bajar un sueño a la realidad cuesta todavía menos si lo hago por convicción.',
  ogtitle: 'Otro: Mi gran objetivo es… | CONSAR',
  ogdescription: 'Soñar no cuesta nada. Bajar un sueño a la realidad cuesta todavía menos si lo hago por convicción.',
  keywords: [
    'consar',
    'Otro: Mi gran objetivo es…'
  ]
}

export default Otro
