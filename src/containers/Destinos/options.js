export default {
  'casa': {
    name: 'casa',
    title: 'Adquirir mi vivienda',
    descption: `Busco un espacio de fuertes cimientos, para que  en él construya mi hogar con sueños y logros.`,
    video: 'yv2DZoJTMZk',
  },
  'negocio': {
    name: 'negocio',
    title: 'Emprender mi negocio',
    descption: `¿Qué sería de la vida si no tuviéramos el valor de intentar cosas
    nuevas? Me decidí a apostarle todo a mis ideas.`,
    video: 'KbXko_Z54iE',
  },
  'auto': {
    name: 'auto',
    title: 'Comprar un auto',
    descption: `No hay nada como llevar las riendas de mi vida, aunque para fines prácticos, ¡mejor me pongo al volante!`,
    video: '3dCFhjmKi5I',
  },
  'independencia': {
    name: 'independencia',
    title: 'Independizarme financieramente',
    descption: `Trazar la ruta que deseo, con las personas que elijo y empacando lo que
    necesito, todo sin depender de algo ni de alguien.`,
    video: 's52cTl_7mb0',
  },
  'laboral': {
    name: 'laboral',
    title: 'Obtener el empleo que quiero',
    descption: `Es llegar a donde siempre quise, convencido de que es lo que merezco y por lo que he trabajado.`,
    video: 'er6vmKvz4p0',
  },
  'pension': {
    name: 'pension',
    title: 'Preparar mi jubilación',
    descption: `La oportunidad de reinventarme, tomando lo mejor de mí y lo que falte por venir.`,
    video: '0LnXQ6317uo',
  },
  'salud': {
    name: 'salud',
    title: 'Asegurar mi fondo para emergencias',
    descption: `¿Qué atender primero: lo urgente o lo importante? En todos los casos, prever me salva del apuro.`,
    video: 'bLJhkLPUjLI',
  },
  'personal': {
    name: 'personal',
    title: 'Alcanzar mi meta profesional',
    descption: `Mi vida se define por las oportunidades, incluyendo las que he
    perdido. ¿De qué está hecho mi currículum y qué le falta?`,
    video: 'NKfy3LX-Sb4',
  },
  'estudios': {
    name: 'estudios',
    title: 'Realizar estudios superiores',
    descption: `Así como la mejor forma de ganar es preparándose, la mayor técnica
    del aprendizaje es compartir el conocimiento.`,
    video: '50GqzL0CzRQ',
  },
  'pareja': {
    name: 'pareja',
    title: 'Vivir en pareja',
    descption: `Cuando hay buena química se disfrutan todos los experimentos. ¿Y si  compartimos fórmulas?`,
    video: 'DOiTNX-8CAQ',
  },
  'familia': {
    name: 'familia',
    title: 'Formar mi propia familia',
    descption: `Si lidiar con uno mismo no es cualquier cosa, hacerlo en familia, ¡es sensacional!`,
    video: 'LOcHBjByEeU',
  },
  'viaje': {
    name: 'viaje',
    title: 'Lograr un viaje especial',
    descption: `Por más que me cuenten en 4, 5 o 6D… nunca habrá nada como vivirlo en carne propia.`,
    video: 'FxtI7qfg-Y4',
  },
  'pasatiempo': {
    name: 'pasatiempo',
    title: 'Consolidar mi pasatiempo',
    descption: `Si ya no quiero sentir que el tiempo pasa, mejor dejo que mi pasatiempo lo acapare.`,
    video: 'T9SyHW-W2dA',
  },
  'legado': {
    name: 'legado',
    title: 'Construir una herencia',
    descption: `Dejar un legado con la sabiduría de las experiencias, las cicatrices y los trofeos, no tiene precio.`,
    video: 'YFDV7PVYngg',
  },
  'impulsar': {
    name: 'impulsar',
    title: 'Crecer mi negocio',
    descption: `Lo importante no solo es llegar a la cima, sino saberse mantener en
    ella. ¿Cuál es el siguiente reto para mi negocio? `,
    video: 'EpNqTkF8XfQ',
  },
  'otro': {
    name: 'otro',
    title: 'Otro: Mi gran objetivo es…',
    descption: `Soñar no cuesta nada. Bajar un sueño a la realidad cuesta todavía menos si lo hago por convicción.`,
    video: 'alagGO1J7Gk',
  }
}
