import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Viaje = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>(
      <div className={style.item}>
        <img src='/static/img/projects/banners/realizar un viaje especial.png' />
        <h1 style={{color}}>Lograr un viaje especial</h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Haz un plan financiero del viaje acorde a tus posibilidades. Calcula y presupuesta tus gastos por concepto, lo más detalladamente posible:
                  <ul>
                    <li><p>Transporte</p></li>
                    <li><p>Hospedaje</p></li>
                    <li><p>Alimentos y bebidas</p></li>
                    <li><p>Actividades y diversiones</p></li>
                    <li><p>Otros</p></li>
                  </ul>
                </p></li></strong>
                <strong><li><p>Considera los costos de aspectos previos como:
                  <ul>
                    <li><p>Pasaporte y/o visa</p></li>
                    <li><p>Comisiones por cambio de moneda</p></li>
                    <li><p>Vacunas</p></li>
                    <li><p>Ropa, accesorios y productos para el viaje</p></li>
                  </ul>
                </p></li></strong>
                <strong><li><p>Procura ahorrar hasta completar el total de tu presupuesto de viaje. Sin duda tomará tiempo y esfuerzo, pero es mucho mejor a que pagues con crédito y después padezcas las presiones de los pagos con intereses acumulados a tu deuda.</p></li></strong>
                <strong><li><p>En la medida de tus posibilidades y mediante procesos seguros, paga anticipadamente lo que puedas de tu viaje. Ello te permite acceder a tarifas especiales y a descuentos.</p></li></strong>
                <strong><li><p>Asegúrate de estar al día en tus pagos para no generar intereses o morosidad mientras viajas; cerciórate de que las cuentas bancarias que vayas a utilizar tengan fondos suficientes y por lo mismo, que tus posibles cargos domiciliados a estas cuentas no afecten los saldos previstos para el viaje. Da aviso a tu banco de que usarás tus tarjetas de crédito y/o débito en el país al que viajarás, para que reconozca las transacciones y no las bloquee; confirma que tengan validez en el lugar al que vas.</p></li></strong>
                <strong><li><p>Apégate al plan que trazaste para no gastar más de lo previsto. Ese es el propósito de una buena y oportuna planeación.</p></li></strong>
                <strong><li><p>No utilices la tarjeta de crédito si no tienes la certeza de poder pagarla rigurosamente en los plazos establecidos.</p></li></strong>
              </ol>
              <img src='/static/img/projects/viaje1.jpg' />

              <p>Hoy en día, viajar está prácticamente al alcance de todos gracias a una importante gama de rutas y medios para desplazarse de un lado a otro de nuestra región, país, continente o del planeta. Viajar implica trasladarte fuera de tu territorio, fuera de lo que acostumbras, siguiendo una ruta o trayectoria específica y permanecer en ello durante cierto tiempo, por ocio, negocios, u otros fines.</p>
              <p>Este proyecto se presta para crear una fuerte expectativa personal porque, como su nombre lo indica, sugiere que realices un viaje motivado por una razón o un propósito especialmente relevante para ti, o quizá, un desplazamiento con un mayor grado de complejidad en su organización y desarrollo. El hecho de que sea sobresaliente presupone una idea importante que lo alimenta y por lo mismo, cierto trabajo de planeación y preparativos.</p>
              <p>Lograr un viaje especial significa que cumplas en persona con el propósito que te hayas establecido, por el simple hecho de llevarlo a cabo o bien por obtener algo específico del recorrido o de la experiencia.</p>
              <p>Sin importar lo ambicioso que resulte “lo especial” del viaje, la riqueza y satisfacción de viajar deberían hacer de éste un proyecto recurrente a lo largo de la aventura de tu vida.</p>
            </div>
            <img src='/static/img/projects/viaje2.jpg' />

          </div>
          <h2 style={{color}}>Los beneficios de lograr un viaje especial:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <p>Más allá de la satisfacción de desarrollar este proyecto y culminar el o los objetivos que te hayas fijado, algunos beneficios son:</p>
              <ul>
                <li>
                  <strong style={{color}}>Viajar ilustra</strong>
                  <p>Con la tecnología actual tenemos acceso casi ilimitado a todo lo que hay y sucede en el mundo, pero nada de ello se compara con la posibilidad de vivir las experiencias en persona. A pesar de poseer información, lo cual por supuesto es útil y deseable, el conocimiento más valioso que puedas obtener lo adquieres de la práctica, del uso de todos tus sentidos, de tu mente y de tu cuerpo en lugares y momentos específicos.</p>
                  <p>Una de las mayores ventajas de viajar es la acumulación de experiencias que son fuente de aprendizaje, conocimiento y placer, debido a la exposición a ambientes nuevos o diferentes en los que se entrelazan aspectos muy diversos, como los naturales y culturales.</p>
                </li>
                <li>
                  <strong style={{color}}>Ampliar la perspectiva</strong>
                  <p>Al salir del contexto que consideras habitual, incrementas tu atención y percepción como una reacción natural de alerta. Por lo mismo, e idealmente con mayor predisposición por tratarse de un viaje especial para ti, estar en un lugar ajeno a tu entorno habitual puede llegar a ampliar tu visión de las cosas y de la vida en general, por ejemplo, modificando algunas de tus prioridades o redimensionando tus valores, entre muchas otras posibilidades. No se trata de convertirte en otra persona, pero algo de introspección nunca hace daño.</p>
                  <p>De igual forma, y tan solo como uno de tantos ejemplos, enfrentar códigos de comunicación diferentes a los que normalmente utilizas para interactuar con otras personas es una excelente oportunidad para probar tu capacidad de observación, análisis, improvisación e ingenio.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/viaje3.jpg' />

          </div>
          <h2 style={{color}}>Las implicaciones de lograr un viaje especial:</h2>
          <div className={style.textContainer}>
            <div>
              <p>Piensa en lo que necesitas organizar antes, durante y al finalizar el viaje.</p>
              <ul>
                <li>
                  <strong style={{color}}>Itinerario:</strong>
                  <ul>
                  <br/>
                    <li>
                      <p><b>Objetivos específicos del viaje.</b> Establece tus objetivos, como visitar personas y vivir las experiencias propias de los destinos por recorrer, realizar o conseguir determinadas cosas, etc.</p>
                    </li>
                    <li>
                      <p><b>Ruta y destinos.</b> ¿Irás directamente a un solo lugar o el viaje implica cierto recorrido con dos o más destinos? Y aunque fueras a un solo sitio, ¿qué hay que ver o hacer en los alrededores?
                      Dentro del itinerario mismo, deja de vez en cuando algo de espacio para improvisar y disfrutar lo que las circunstancias te ofrezcan. Valora dejarte sorprender por lo imprevisto, por la forma de vivir de la gente y por los detalles.</p>
                    </li>
                    <li>
                      <p><b>Duración.</b> ¿Cuánto tiempo tomará el viaje y como distribuirás ese tiempo entre los diferentes destinos y objetivos? Determina lo prioritario y el tiempo que requiere. Si te sobra de tiempo, ello no implica que debas programarlo todo y pierdas la libertad y el placer de vagar o divagar.</p>
                    </li>
                  </ul>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Presupuesto</strong>
                  <p>Desglosa los costos de la manera más precisa posible. En su defecto, siempre será preferible que te quedes un poco sobrado a que te quedes corto en los montos. </p>
                  <p>Establece el tope presupuestal del viaje. Este monto depende sobre todo de lo que definas en los puntos del itinerario, y de variables dentro del presupuesto, como el nivel de calidad que quieras particularmente en los servicios de alojamiento, transporte, alimentación, etc. Haz una estimación del monto total que costará el viaje, mismo que podrás afinar a medida que confirmes el itinerario con mayor precisión.</p>
                  <p>Quizás no puedas rebasar cierta cantidad de dinero. De ser así, deberás controlar tus gastos con redoblada disciplina. Establece tu límite por debajo del tope que realmente dispones; ello te dará un pequeño margen en caso de cualquier sobregasto o sobrecosto.</p>
                  <p>Por el contrario, también puede ser que valga la pena redistribuir o subir el monto de lo que te piensas gastar, añadiendo algunas actividades extra con tal de aprovechar lo de por sí ya invertido en “ir hasta allá”.</p>
                  <ul>
                    <li>
                      <b>Transporte.</b>
                      <p>Este es un tema fundamental y en ocasiones engorroso, porque implica decidir tajantemente los destinos, las rutas, los medios de transporte, los días, los horarios, las tarifas y las condiciones que mejor te convengan. Implica que te tomes el tiempo para investigar y decidir con cuidado.</p>
                      <p>Recuerda que si deseas hacer ajustes en tu itinerario durante el viaje, por ejemplo prolongando o recortando la estadía en algún lugar o cancelando algo, es probable que pagues cargos o penalizaciones sobre lo ya adquirido, o que en su momento ya no encuentres boletos o espacio.</p>
                      <p>Si en tu recorrido debes trasbordar, es decir, moverte de un medio de trasporte a otro con tiempos programados, recuerda prever el tiempo suficiente para esos desplazamientos y con un margen para cualquier retraso.</p>
                      <p>Para desplazarte en ciudades o regionalmente, busca opciones promocionales o de paquete. Algún Travelpass, tarjeta o carnet para el transporte público puede resultar más económico que pagar pasajes a granel.</p>
                      <p>Para tus gastos de transporte es difícil establecer un monto promedio diario. Debes presupuestar este tema tomando en cuenta las características de cada desplazamiento.</p>
                    </li>
                    <li>
                      <b>Hospedaje.</b>
                      <p>Al igual que para el transporte, suele ser preferible que investigues y hagas reservaciones. Ello puede ser importante dependiendo de la temporada y de la posible saturación de gente en los destinos que elijas. Como sea, investigar oportunamente te permite ubicar y reservar el alojamiento acorde a tu gusto, necesidades y presupuesto.</p>
                      <p>Toma en cuenta qué tanto sacarás provecho ocupando los lugares donde te hospedes, o si la mayor parte del tiempo estarás fuera y solo llegarás a descansar. Dicho de otra forma, quizás no necesites pagar una habitación con terraza y vista al mar, si pasas todo el día en la playa. Este tipo de detalles te pueden generar ahorros importantes.</p>
                      <p>Establece un monto promedio diario para el hospedaje.</p>
                    </li>
                    <li>
                      <b>Alimentación.</b>
                      <p>Este tema es muy amplio y normalmente lo podrás resolver al momento en cada sitio, aunque también en esto, un poco de investigación te dirá qué lugares deberían ser tus paradas obligadas para probar la mejor cocina local o la más elogiada, de acuerdo a las opiniones que compartan los lugareños u otros viajeros. </p>
                      <p>Si te quedas en casa de amigos o familiares, sería normal que correspondieras con algún recurso por el alojamiento, la alimentación y las facilidades que te den.</p>
                      <p>Establece un monto promedio diario para bebidas y alimentos. No olvides las bebidas que necesitarás fuera de los alimentos.</p>
                    </li>
                    <li>
                      <b>Actividades diversas.</b>
                      <p>Al igual que para el tema del transporte, deberás presupuestar específicamente todas las actividades de las que puedas investigar el costo, como por ejemplo, las entradas a museos, parques de diversiones, reservas naturales y demás, cuya información esté en internet u otro medio a tu alcance. Para cubrir los gastos de todas las experiencias de las cuales desconozcas el costo, podrías prever un fondo limitado del que vayas tomando “hasta donde alcance”.</p>
                    </li>
                  </ul>
                  <p>Transporte, hospedaje, alimentación y actividades diversas deberían ser los tópicos más importantes para elaborar tu presupuesto, aunque cada viaje tiene sus propias características. Si hay otras, desagrégalas e inclúyelas en tu presupuesto para que sea más preciso y sobre todo útil.</p>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Apoyo de una agencia de viajes:</strong>
                  <p>Si no estás seguro de controlar y organizar todos los aspectos que implica tu viaje especial, siempre puedes contratar el servicio de un agente o de una agencia de viajes. Toma en cuenta que ello tiene un costo y que de todas formas deberás proporcionarles toda la información de lo que deseas hacer en tu viaje; ellos básicamente se encargarán de investigar y ofrecerte las diferentes opciones para que elijas.</p>
                </li>
                <li>
                  <strong style={{color}}>Paquetes:</strong>
                  <p>Quizá un paquete tipo “all inclusive” (todo incluido), con tus boletos para viajar, el hospedaje, los alimentos y hasta algunas actividades específicas, cubra adecuadamente lo que deseas para tu viaje especial. Un viaje pre-organizado de este tipo puede ser ideal para ir a la segura, sabiendo que todo está previsto y que habrá quien responda por cualquier contratiempo. Te ahorras gran parte de la planeación y la tramitología, aunque ello también tiene su precio. </p>
                  <p>Si te gusta improvisar sobre la marcha es probable que no te convenga comprar un paquete, puesto que tu libertad para moverte fuera de lo paneado será restringida, y te implicará gastos adicionales.</p>
                  <p>Otra opción puede ser combinar parte de tu itinerario con momentos pre-organizados tipo paquete, y en otros momentos cubrir tus necesidades con mayor flexibilidad. Ejemplo de ello son los cruceros, que tienen una ruta definida, lo incluyen básicamente todo y también tocan puerto para hacer escalas en lugares específicos en los que puedes descender del barco y organizar tus actividades como mejor te parezca.</p>
                </li>
                <li>
                  <strong style={{color}}>Trámites previos al viaje:</strong>
                  <ul>
                    <br/>
                    <li>
                      <b>Pasaporte o visa (si viajas al extranjero).</b> <p>Revisa cuándo expiran tus documentos; no sea que ya tengas todo listo y alguno ya haya expirado. Renovar tu pasaporte o visa puede tomar bastante tiempo.</p>
                    </li>

                    <li>
                      <b>Tipo de cambio (si viajas al extranjero).</b> <p>Calcula cuidadosamente el tipo de cambio del peso a la moneda propia de tu destino de viaje. De preferencia, tómate el tiempo de ver qué Banco o Casa de Cambio te ofrece la mejor tasa de cambio.</p>
                    </li>

                    <li>
                      <b>Vacunas y Sanidad (si viajas al extranjero).</b> <p>Antes de viajar toma en cuenta las vacunas recomendadas o exigidas por las autoridades, que debas aplicarte con el fin de no contraer enfermedades locales. Las restricciones también pueden ser para que no transportes ciertos productos y alimentos. Estas medidas no son exclusivas para viajes internacionales, puede haber casos a nivel nacional.</p>
                    </li>
                  </ul>
                </li>
                <li>
                  <strong style={{color}}>Aspectos de previsión:</strong>
                  <ul>
                    <br/>
                    <li>
                      <b>Clima.</b> <p>Nunca está de más conocer las condiciones climatológicas de tu destino para, por ejemplo, llevar la ropa o accesorios adecuados o que existan las condiciones propicias para desarrollar tus actividades.</p>
                    </li>

                    <li>
                      <b>Usos y costumbres.</b> <p>Infórmate sobre los usos y costumbres del lugar que visites. Normalmente, lo importante y a destacar suele ser parte de la información turística básica, presentada en rubros tipo precauciones, consejos útiles o avisos de interés.</p>
                    </li>

                    <li>
                      <b>Idiomas.</b> <p>Hoy en día, varias aplicaciones para el celular te pueden sacar de apuros con traducciones simultáneas, siempre y cuando tengas conectividad. Anota los mensajes más básicos, como “Dónde están los sanitarios”, “Necesito llegar a esta dirección” o “Cuánto cuesta esto”, etc.</p>
                    </li>

                    <li>
                      <b>Contratiempos y emergencias.</b> <p>Al viajar, lleva un pequeño kit para apuros o malestares menores tipo indigestión, resfriado, una cortada o dolor muscular, etc. Si puedes, dispón de recursos de respaldo en caso de un accidente, como quizá una tarjeta de crédito únicamente para dicho fin, y procura tener algún contacto en la zona o región al que puedas llamar en busca de apoyo.</p>
                      <p>Para mayor seguridad y tranquilidad, también está la opción de disponer de un seguro de viaje, con los términos y condiciones que sean los más adecuados para tu proyecto.</p>
                      <p>Tener los datos de las representaciones mexicanas más cercanas a tu destino puede ser de utilidad. Los consulados mexicanos en otros países suelen tener páginas de internet con las recomendaciones pertinentes a las características de cada país. Consúltalas antes de viajar.</p>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='https://guiadelviajero.sre.gob.mx/'>SER - Secretaría de Relaciones Exteriores. Guía del viajero.
              </a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Viaje.seo = {
  title: 'Lograr un viaje especial | CONSAR ',
  description: 'Por más que me cuenten en 4, 5 o 6D… nunca habrá nada como vivirlo en carne propia.',
  ogtitle: 'Lograr un viaje especial | CONSAR',
  ogdescription: 'Por más que me cuenten en 4, 5 o 6D… nunca habrá nada como vivirlo en carne propia.',
  keywords: [
    'consar',
    'Lograr un viaje especial'
  ]
}

export default Viaje
