
export default {
  title: 'Catálogo de proyectos para la Aventura de tu vida | CONSAR ',
  description: 'Visualiza a detalle los grandes proyectos de tu vida y lo que necesitas para alcanzarlos.',
  ogtitle: 'Catálogo de proyectos para la Aventura de tu vida | CONSAR',
  ogdescription: 'Visualiza a detalle los grandes proyectos de tu vida y lo que necesitas para alcanzarlos.',
  keywords: [
    'Pensión IMSS',
    'Aventura de mi vida'
  ]
}
