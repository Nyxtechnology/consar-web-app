import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Independencia = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <div className={style.item}>
      <img src='/static/img/projects/banners/independencia financiera.png' />
      <h1 style={{color}}>Independizarme financieramente</h1>
        <div className={style.textContainer}>
          <h4 style={{color}}>Pasos a seguir</h4>
          <div>
            <ol>
              <strong><li><p>Para ser independiente financieramente necesitarás un trabajo o actividad remunerada que te provea los recursos para tu manutención.</p></li></strong>
              <strong><li><p>Elige un plan de vida de acuerdo a tus proyectos y aspiraciones.</p></li></strong>
              <strong><li><p>Establece un plan de ingresos y egresos periódico.</p></li></strong>
              <strong><li><p>Idealmente, establece una meta de ahorro, asígnale un objetivo específico, cúmplela cabalmente y después gasta lo que te quede.</p></li></strong>
              <strong><li><p>Analiza las opciones que hay en el mercado para invertir tu ahorro y pon tu dinero a trabajar. Haz Ahorro Voluntario en tu Cuenta AFORE, no solo te permite acceder a altos rendimientos sino que puedes disponer de tu ahorro en períodos cortos o largos, de acuerdo a tu plan de vida.</p></li></strong>
            </ol>
            <img src='/static/img/projects/Independizarme1.jpg' />

            <p>
              Lograr tu independencia financiera o económica es como aprender a andar en bicicleta. En un principio, tus padres u otras personas te mueven sobre tres, cuatro o más ruedas. Dependes de ellos y gozas de mucha seguridad. Después, poco a poco vas descubriendo tus aptitudes y pones a prueba tu equilibrio, apoyado por ruedas laterales en tu bicicleta. Observas a otros más avanzados que te inspiran, recibes consejos e información. Tus temores van abriendo paso a las experiencias, al interés y a la diversión. Finalmente, a base de practicar todo lo que te sea necesario, mides y desarrollas tus capacidades. Quizá te conviertas en un ciclista intrépido y veloz, o quizá decidas especializarte en el ciclismo de montaña o de carreras, o simplemente divertirte sobre un monociclo.
            </p>
            <p>
              Desarrollar tu autonomía financiera es muy similar. Es el proceso que implica dejar alguna seguridad económica proporcionada por alguien más, para obtenerla tú mismo y de ahí en adelante seguir siendo autosuficiente en lo económico. Se trata de que adquieras experiencia y mayores capacidades para improvisar y ajustarte a los imprevistos.
            </p>
            <p>
              Por lo mismo, este proceso consiste en elegir tus propias reglas sobre la forma como deseas vivir, con la calidad de vida que corresponda a tus proyectos y aspiraciones.
            </p>
            <p>
              Es un acto de madurez que va más allá de solo salirte de la casa de tus padres o tutores; también es no depender de créditos para poder vivir, es aprender a valorar y no abaratar tu trabajo y tu potencial, es buscar constantemente nuevas formas de generar ingresos, es perseguir las oportunidades para lograr la vida que te propones.
            </p>
          </div>
          <img src='/static/img/projects/independizarme2.jpg' />

          </div>
            <h2 style={{color}}>Los beneficios de independizarte financieramente: </h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
            <ul>
              <li>
                <strong style={{color}}>Libertad para la toma de decisiones: </strong>
                <p>
                  Disponer de recursos económicos es un punto de partida hacia cualquier otro proyecto de vida. Al generar tus ingresos deberás ser capaz de decidir cómo emplearlos y en qué.
                </p>
                <p>
                  Cuando juntas elementos como la experiencia y los recursos adecuados, puedes ser capaz de realizar cualquier tipo de sueño, sin restricciones: desde un viaje, hasta un negocio o formar una familia.
                </p>
              </li>
              <li>
                <strong style={{color}}>Responsabilidad: </strong>
                <p>
                  Lograr tu autonomía financiera implica mucha responsabilidad, misma que se ve reflejada en el control y equilibrio que tengas de tus ingresos y egresos. Ésta se va cultivando y fortaleciendo al grado de convertirse en una virtud sumamente útil para mejorar tu desempeño y crecimiento económico.
                </p>
              </li>
              <li>
                <strong style={{color}}>Estabilidad y seguridad: </strong>
                <p>
                  Al alcanzar la independencia financiera puedes establecer un plan de gastos que te permita disponer de tus recursos, brindándote estabilidad y seguridad en el corto, mediano y largo plazo.
                </p>
                <p>
                  Recuerda que dentro de esa estabilidad siempre podrás destinar una parte a la inversión y al ahorro previsional, como el ahorro para el retiro, que no deja de ser una forma de invertir.
                </p>
              </li>
              <li>
                <strong style={{color}}>Calidad de vida: </strong>
                <p>
                  Este concepto toma en cuenta el punto de vista de la autorrealización de cada persona, cada quien elige cuánto necesita y cuánto es suficiente para llevar una vida de acuerdo a sus gustos y principios. Tú, ¿qué tipo de vida quieres tener? Y al respecto ¿tus acciones son congruentes para alcanzar ese tipo de vida? Porque esa es la diferencia entre soñar y lograr.
                </p>
              </li>
              <li>
                <strong style={{color}}>Movilidad social: </strong>
                <p>
                  Ser independiente en lo financiero significa no depender de los recursos de otros, como entre generaciones familiares, donde los apoyos son de padres a hijos y/o abuelos, pero también de hijos a padres y/o abuelos.
                </p>
                <p>
                  Otra forma de decirlo, es que busques liberar a quienes te apoyan económicamente, para que ellos mismos puedan aprovechar sus recursos. En este sentido, por ejemplo, el que los padres mayores no dependan de los recursos que les dan sus hijos, permite que ese dinero los hijos lo inviertan en su propia calidad de vida, o favoreciendo el desarrollo y las condiciones de vida de sus propios descendientes.
                </p>
                <p>
                  Desde esta perspectiva, independizarse financieramente es una importante manera de otorgar estabilidad financiera a quienes fungirían como nuestros proveedores
                </p>
              </li>
            </ul>
          </div>
          <img src='/static/img/projects/Independizarme3.jpg' />

          </div>
            <h2 style={{color}}>Las implicaciones de independizarse financieramente:</h2>
          <div className={style.textContainer}>
            <p>
              ¿Qué actitud debes tomar para construir este proyecto? Ejemplo: Ser consciente y franco con tus necesidades y gustos, además de organizar tus recursos para hacerlo posible.
            </p>
            <ul>
              <li>
                <strong style={{color}}>Tu fuente de ingresos: </strong>
                <p>
                  La independencia financiera requiere de ingresos que te permitan cubrir por lo menos tus gastos básicos o al nivel que te establezcas responsablemente, es decir, al nivel que te permita acumular dinero en lugar de endeudarte.
                </p>
                <p>
                  Necesitas llevar a cabo una actividad remunerada en una empresa o de manera independiente para generar los ingresos necesarios para tu planeación financiera o presupuesto personal, y con ello poder satisfacer completamente tus necesidades.
                </p>
              </li>
              <li>
                <strong style={{color}}>Planeación: </strong>
                <p>
                  Debes realizar un plan detallado de tus ingresos y gastos mensuales, empezando por cubrir tus necesidades básicas como: vivienda, comida, transporte, agua y energía eléctrica, entre otros. Después vienen más conceptos, sin duda priorizando aquéllos que representan inversiones de salud, económicas, formación personal, relaciones públicas/sociales, capacitación y especialización, entre varias más.
                </p>
              </li>
              <li>
                <strong style={{color}}>Ahorro e inversión: </strong>
                <p>
                  Practicar el hábito del ahorro requiere de una economía del hogar que te permita invertir el ahorro como una asignación programada de una parte de tus ingresos, a diferencia y en lugar de que sientas que sacrificas a disgusto algún remanente de tu gasto. Es una parte esencial y estratégica de tus finanzas personales, pues te da la posibilidad de disponer de dinero para invertir y con ello ganar rendimientos.
                </p>
                <p>
                  Toda inversión conlleva un nivel de riesgo. Hay inversiones con riesgos muy bajos (como los bonos gubernamentales, CETES) o con riesgos mayores (como acciones en los mercados financieros).
                </p>
              </li>
            </ul>
          </div>
          <div>
        </div>
        <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li>
                <a target='_blank' style={{color}} href='https://www.gob.mx/consar/articulos/un-presupuesto-para-el-futuro-la-clave-del-exito-para-un-buen-retiro?idiom=es'>
                  Formato descargable para presupuesto, dentro de la nota del Blog de CONSAR: “Cómo entender tu ahorro para el futuro”.
                </a>
              </li>
              <li>
                <a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_independizarse.html'>
                  CONDUSEF – Eventos en tu vida: Independizarse
                </a>
              </li>
              <li>
                <a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/presupuesto.html'>
                  CONDUSEF – Haz más con tu dinero: Presupuesto
                </a>
              </li>
              <li>
                <a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/consejos.html'>
                  CONDUSEF – Consejos para tu bolsillo
                </a>
              </li>
              <li>
                <a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/conduguias.html'>
                  CONDUSEF – Conduguías y folletos
                </a>
              </li>
            </ul>
          </div>
    </div>
  )
}
Independencia.seo = {
  title: 'Independizarme financieramente | CONSAR ',
  description: 'Trazar la ruta que deseo, con las personas que elijo y empacando lo que necesito, todo sin depender de algo ni de alguien.',
  ogtitle: 'Independizarme financieramente | CONSAR',
  ogdescription: 'Trazar la ruta que deseo, con las personas que elijo y empacando lo que necesito, todo sin depender de algo ni de alguien.',
  keywords: [
    'consar',
    'Independizarme financieramente'
  ]
}

export default Independencia
