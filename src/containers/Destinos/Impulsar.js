import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Impulsar = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/impulsar negocio.png' />
        <h1 style={{color}}>Crecer mi negocio </h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>La idea de crecer tu negocio debe surgir de la oportunidad, de tu capacidad de medir el potencial de tu negocio y de la existencia de necesidades en el mercado para que te amplíes o transformes.
                </p></li></strong>
                <strong><li><p>Puede que generar mayores utilidades no sea el fin último de crecer tu empresa, pero sin duda es necesario. Tenlo claro en el radar.
                </p></li></strong>
                <strong><li><p>Crecer tu negocio debe responder a un objetivo concreto en el marco de emprender y seguir invirtiendo.</p></li></strong>
                <strong><li><p>Nunca dejes de ver lo que hace tu competencia.</p></li></strong>
                <strong><li><p>Elabora una estrategia que responda a la suma organizada de acciones y recursos.</p></li></strong>
                <strong><li><p>Metas e indicadores van de la mano para evaluar oportunamente si la estrategia está cumpliendo con los objetivos y las expectativas.</p></li></strong>
                <strong><li><p>Investiga tus posibles vías de financiamiento y de obtención de recursos para la transformación de tu negocio.</p></li></strong>
              </ol>
              <img src='/static/img/projects/crecernegocio1.jpg' />

              <p>Emprender y poner un negocio requiere importantes habilidades, como el valor de arriesgarte y la convicción para no desanimarte con los primeros problemas o fracasos. Mucho se aprende de la prueba y del error, o dicho de otra forma, mucho se aprende sobre la marcha, al medir y valorar cada decisión tomada.</p>
              <p>Plantearse un proyecto sobre crecer tu negocio, cuando ya has recorrido un buen tramo de experiencias, no es cualquier cosa. De entrada, la intención de crecer tu negocio puede responder a un gran número de razones, desde observar una creciente demanda por parte de tus clientes, hasta considerar la posibilidad de abastecer nuevos segmentos del mercado o de exportar, pasando por alguna alianza estratégica o tu incursión en productos y servicios innovadores, entre muchas razones más.</p>
              <p>Lo importante es que la idea de crecer tu negocio debe surgir de la oportunidad, de tu capacidad de medir el potencial de tu negocio y de la existencia de necesidades en el mercado para que te amplíes o transformes. Este último punto es importante porque la idea de crecer no tiene que ver únicamente con vender más, sino que también puede responder a una oportunidad para especializarse, de responder a mayores exigencias de calidad, o quizá de innovar con nuevos productos y servicios.</p>
              <p>Aquí encontrarás algunas ideas y puntos de vista para inspirarte y ayudarte. De ti depende convertir este proyecto en el impulso para que des un paso importante con tu negocio, o mejor aún, para que adoptes este proyecto con visión de largo plazo y desarrolles tu capacidad para adaptarte permanentemente a las posibilidades y oportunidades.</p>
            </div>
            <img src='/static/img/projects/crecernegocio2.jpg' />

          </div>
          <h2 style={{color}}>Los beneficios de crecer mi negocio</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Desarrollo personal</strong>
                  <p>“Incrementar” y diversificar tus actividades empresariales, en cantidad o calidad, con todos los procesos y las decisiones que ello pueda necesitar, es evolucionar, y si va combinado con tu placer por lo que haces en tu negocio, entonces no hay mejor fórmula para seguir creciendo. </p>
                </li>
                <li>
                  <strong style={{color}}>Utilidades</strong>
                  <p>Las utilidades quizás no sean el objetivo principal de cualquier empresa, pero definitivamente, son indispensables para subsistir. Si crecer tu negocio te permite incrementar los ingresos, ciertamente hay un valioso e indiscutible beneficio.</p>
                </li>
                <li>
                  <strong style={{color}}>Diversificar</strong>
                  <p>De la mano con el beneficio del desarrollo personal, vale la pena destacar el beneficio de crecer y que ello te permita diversificar tus experiencias. Alianzas estratégicas suelen dar paso a ello y, para bien o para mal mientras no te lleven a la quiebra, siempre serán constructivas.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/crecernegocio3.jpg' />

          </div>
          <h2 style={{color}}>Las implicaciones de crecer mi negocio</h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>¿Para qué crecer?</strong>
                    <p>Cualquier empresario con experiencia reconocerá que crecer por crecer no tiene sentido ni beneficio. De hecho, crecer por crecer puede sonar a una vieja fórmula empresarial, rebasada y probablemente contraproducente hoy en día. Ejemplo veloz: Para qué rentar una oficina si Internet lo resuelve.</p>
                    <p>Crecer tu negocio responde a un objetivo preciso, tuyo, y son las condiciones generales del entorno a tu negocio y de tu visión las que le dan sentido. Por lo mismo, como empresario aprendes a observar, a buscar y aprovechar las oportunidades pero también a transformar lo necesario para crear oportunidades, y de la misma manera aprendes a esperar, a ser prudente y tener paciencia, a dejar que el ambiente exterior sea más favorable mientras cuidas y pules aspectos internos.</p>
                    <p>Crecer tu negocio es entonces continuidad del mismo principio que te empujó a iniciar tu negocio: emprender e invertir. Es mantenerte activo en la transformación y evolución del mercado, pero también de tus inquietudes y motivaciones personales.</p>
                  <p>  Es más, el simple hecho de mantenerse con utilidades requiere de la capacidad de adaptarse a los cambios y exigencias del entorno. Lo dice el dicho: “Lo importante no es llegar, sino mantenerse”. Visto de esta manera, crecer es también no quedarse atrás.</p>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Observación e investigación</strong>
                  <p>Nunca dejes de ver lo que hace tu competencia, lo que hacen otros emprendedores aunque parezca que no se relacionan de cerca con tu negocio, lo que sucede en el entorno inmediato a tu negocio, pero también en el contexto general que influye en tu negocio, y por encima de todo, nunca dejes de escuchar y observar a tus clientes, y posibles clientes.</p>
                </li>
                <li>
                  <strong style={{color}}>Autoevaluación previa al crecimiento</strong>
                  <p>Valora objetivamente no solo el potencial sino la capacidad de crecimiento de tu negocio, en equipo. Involucra a tu personal y mide tus recursos en general considerando el tiempo necesario que dicho crecimiento pudiera tomar. Se trata de un proceso y como tal, evalúa todos los aspectos que estén a tu alcance.</p>
                </li>
                <li>
                  <strong style={{color}}>Estrategia</strong>
                  <p>Con base en tu experiencia, elabora una estrategia, es decir, en qué se traduce y se aterriza el crecimiento del negocio: qué cambiará, qué funciones se modificarán y en qué medida los responsables de llevar a cabo los cambios los entienden, los comparten y los pueden desarrollar. La estrategia es la suma organizada de acciones y recursos. Por supuesto, establece en equipo los objetivos, las metas y los procesos.</p>
                </li>
                <li>
                  <strong style={{color}}>Implementación y medición</strong>
                  <p>Sin entrar en detalles sobre cómo se llevará a cabo el “crecimiento” del negocio, el hecho de elaborar una estrategia debe permitirte medir el avance, siempre antes de que se convierta en un bloqueo. Metas e indicadores van de la mano para evaluar oportunamente si la estrategia está cumpliendo con los objetivos y las expectativas.</p>
                </li>
                <li>
                  <strong style={{color}}>Inversión</strong>
                  <p>Crecer es invertir en la transformación. Tu capacidad y la de tu equipo para cuantificar la inversión general a fin de impulsar el negocio, es parte fundamental del diagnóstico inicial, pero de igual forma del seguimiento.</p>
                  <ul>
                    <li>
                      <p>Recursos humanos
                      Cerciórate de que tu personal esté capacitado para ello, y si no es el caso o hay áreas desprotegidas, estable la forma para obtener las especializaciones pertinentes, interna o externamente, dentro del rango de inversión que te lo permita y durante el tiempo previsto.</p>
                    </li>
                    <li>
                      <p>Recursos económicos y materiales
                      Nuevamente con base en tu experiencia o con la debida investigación que sea lo más detallada posible, considera tus posibles vías de financiamiento y de obtención de recursos para la operación/producción:</p>
                      <ul>
                        <li><p>A través del capital de los propios integrantes de la empresa</p></li>
                        <li><p>Apoyos o subvenciones gubernamentales. Actualmente existen instituciones gubernamentales dedicadas al apoyo de emprendedores, como el Instituto Nacional del Emprendedor, INADEM</p></li>
                        <li><p>Préstamos o financiamientos de bancos comerciales</p></li>
                        <li><p>Inversionistas externos, a quienes presentarás el plan de negocios </p></li>
                        <li><p>Plataformas de crowdfunding</p></li>
                        <li><p>Proveedores y acceso a insumos diversos según el negocio</p></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <strong style={{color}}>Economías de escala y cadenas productivas</strong>
                  <p>A través del uso eficiente del capital humano y de trabajo, puedes lograr lo que se conoce como economía de escala, la cual implica que el costo de producción disminuye en la medida en la que la producción aumenta.</p>
                </li>
                <li>
                  <strong style={{color}}>Reubicación</strong>
                  <p>Evalúa la ubicación de tu negocio con respecto a la plaza, el mercado y la cobertura que implicará el crecimiento de actividades. Por complejo que parezca, este punto puede representar importantes beneficios de contabilidad dentro del plan de negocio.</p>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='https://www.inadem.gob.mx/'>Instituto Nacional del Emprendedor, INADEM
              </a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvcredito.html'>CONDUSEF – Cuadernos y videos: Crédito
              </a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Impulsar.seo = {
  title: 'Crecer mi negocio | CONSAR ',
  description: 'Lo importante no solo es llegar a la cima, sino saberse mantener en ella. ¿Cuál es el siguiente reto para mi negocio?',
  ogtitle: 'Crecer mi negocio | CONSAR',
  ogdescription: 'Lo importante no solo es llegar a la cima, sino saberse mantener en ella. ¿Cuál es el siguiente reto para mi negocio?',
  keywords: [
    'consar',
    'Crecer mi negocio'
  ]
}

export default Impulsar
