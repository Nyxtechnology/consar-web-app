import React from 'react'
import style from './style.css'
import options from './options'
import Layout from '../Layout/'
import ConsarNav from '../../components/ConsarNav/'
import seo from './seo'
import window from 'global/window'
import {Link, Switch, Route} from 'react-router-dom'
import Principal from './Principal'

export default class Destinos extends React.Component {
  static seo = seo
  componentDidMount = () => window.scrollTo(0, 0)
  componentWillReceiveProps = next => {
    if (next.match.url !== this.props.match.url) {
      return window.scrollTo(0, 0)
    }
  }
  render = () => {
    const url = this.props.location.pathname.match(/jovenes|adultos|mayores/)
    const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
    const color = ['#925152', '#002453', '#47774C'][section]
    return <Principal {...this.props} />
  }
}
