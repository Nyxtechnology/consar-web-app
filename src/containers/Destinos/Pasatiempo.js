import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Pasatiempo = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/complementar la vida a traves de un pasqatiempo.png' />
        <h1 style={{color}}>Consolidar mi pasatiempo</h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Define tu pasatiempo y no permitas que la rutina lo apague. Si sucedió con uno, intenta otro que verdaderamente te mueva.
                </p></li></strong>
                <strong><li><p>Identifica los costos que representa hacerlo y elabora un presupuesto básico que te permita conocer y controlar los gastos del mismo, desde la fase inicial hasta todas las subsecuentes que pueda implicar.
                </p></li></strong>
                <strong><li><p>La especialización y el crecimiento personal que logres con tu pasatiempo te permitirá valorar si podrías en un momento dado, como durante tu retiro, dedicarte de lleno a él e incluso generar tus ingresos económicos.</p></li></strong>
              </ol>
              <img src='/static/img/projects/pasatiempo1.jpg' />

              <p>Un pasatiempo es una actividad que realizamos generalmente durante nuestro tiempo libre y es una manera de ocuparnos en algo que nos da placer. Se convierte en un hábito cuando la realizamos con cierta periodicidad, a manera de una afición o hobby por cierto tiempo o para la vida.</p>
              <p>¿Practicar un deporte, tomar fotografías o hacer artesanías, son pasatiempos? Sí, cuando es por gusto y distracción. Si lo haces de manera profesional, es decir, percibiendo una remuneración económica a cambio, el enfoque puede ser diferente aunque la línea entre una actividad laboral y un pasatiempo puede ser muy delgada. Y esa es la idea de este proyecto, dentro de la aventura de tu vida: un pasatiempo que en cierto momento se pueda convertir en tu actividad más interesante e importante.</p>
              <p>Con el paso de los años, mientras adquieres experiencia, puede que te des cuenta de que tienes facilidad, un gusto especial y quizá hasta un don particular para realizar alguna actividad. Con frecuencia, no prevés un buen pasatiempo, simplemente lo descubres. Por ejemplo, pongamos el caso de una persona que trabaja en la construcción, y que cada que tiene la oportunidad le gusta cocinar, al grado de que sabe bastante de cocina, improvisa y “tiene buen sazón”, lo que sus familiares y amigos gustosos le reconocen. Un día, quizá al jubilarse, esta persona decide abrir una pequeña fonda y así consigue combinar su gusto por cocinar, aplicar toda la experiencia que ha ido desarrollando y al mismo tiempo obtener ingresos junto con la satisfacción de sus comensales. Y como este caso, hay miles.</p>
              <p>¿Cuál es tu pasatiempo favorito y cómo lo desarrollarías para sacarle el mayor provecho?</p>
            </div>
            <img src='/static/img/projects/pasatiempo2.jpg' />

          </div>
          <h2 style={{color}}>Los beneficios de consolidar mi pasatiempo:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Cambiar la rutina y reducir el estrés</strong>
                  <p>Generalmente, todos estamos expuestos a situaciones estresantes del trabajo o por responsabilidades económicas y familiares, entre otras. Un pasatiempo puede ser la mejor forma de distraerse, relajarse y hasta resolver algunos aspectos del estrés cotidiano.</p>
                </li>
                <li>
                  <strong style={{color}}>Fuente de placer y realización</strong>
                  <p>Dependiendo de tus gustos, elegirás el pasatiempo que te agrade y cuando lo lleves a cabo será una fuente de placer y felicidad. Un hobby suele desligarse de compromisos y por lo mismo lo realizas con libertad, creatividad, a tu tiempo y poniendo o modificando las reglas como mejor te parezca. El interés y la pasión por tu pasatiempo te pueden convertir en un verdadero especialista en la materia. Podrías fortalecer tu sentido de realización personal al compartir tu experiencia, el acervo creado y tu innovación.</p>
                </li>
                <li>
                  <strong style={{color}}>Potenciar y experimentar tus habilidades</strong>
                  <p>La mayoría de los pasatiempos toman tiempo, te exigen concentración así como que desarrolles y practiques técnicas, por lo que te beneficias de muchas maneras, como al desarrollar de tu paciencia, tu concentración, tus sentidos y tu capacidad cognitiva en general.</p>
                </li>
                <li>
                  <strong style={{color}}>Redimensionar tu pasatiempo como actividad principal</strong>
                  <p>La constancia y especialización con que desarrolles tu pasatiempo te permitirán perfilarlo como una alternativa de actividad lucrativa o no, a la que puedas dedicarte en determinados periodos de tu vida o, por ejemplo, durante tu retiro.</p>
                </li>
                <li>
                  <strong style={{color}}>Proyección y objetivos</strong>
                  <p>Además de proporcionarte satisfacción, la realización periódica de un pasatiempo casi siempre implica la evolución de tus metas u objetivos. Un pescador querrá probar nuevos sitios, mejorar sus técnicas, atrapar nuevas especies o los ejemplares más grandes o de mayor reto; lo mismo sucede al coleccionar algo, pintar, practicar un deporte, viajar o leer. Consciente o inconscientemente aspiramos a más, mejor, nuevo y diferente. ¿Qué tan definido o qué tan vasto puede llegar a ser tu pasatiempo?</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/pasatiempo3.jpg' />

          </div>
          <h2 style={{color}}>Las implicaciones de consolidar mi pasatiempo</h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Tiempo y recursos invertidos</strong>
                  <p>La gran ventaja de un pasatiempo es que tu propio interés es el que te guía hacia lo que necesitas, como por ejemplo, dónde obtener más información o dónde practicar, o bien qué importancia le das y cómo dosificas tu hobby en tu vida. Bien dice el dicho que el interés mueve montañas. </p>
                  <p>Por lo mismo, tus limitaciones directas posiblemente solo sean de tiempo (aunque parezca contradictorio) y recursos. Tiempo, porque la dinámica de lo cotidiano puede dejar pocos espacios para realizar tu pasatiempo, y lo que no se atiende, deja de ser.
                  En cuanto a los recursos económicos u otros, es recomendable que antes de invertir en un pasatiempo identifiques los costos que representa de manera integral o “a la larga”.</p>
                  <p>Si realmente te mueve, sin duda encontrarás la forma de irlo consolidando. Por supuesto, elaborar un presupuesto de lo elemental puede ser de gran utilidad.</p>
                  <ul>
                  <br/>
                    <li>
                      <strong style={{color}}>Inversión inicial.</strong> <p>Determina cuál es el monto que debes invertir en el equipo o el material requerido para tu pasatiempo.</p>
                    </li>
                    <li>
                      <strong style={{color}}>Mantenimiento de equipo y gastos recurrentes.</strong> <p>Quizá sea necesario que periódicamente des mantenimiento al equipo que uses o tengas que comprar insumos de manera recurrente para realizar tu pasatiempo, por lo que conviene que conozcas lo mejor posible los costos que ello representa.</p>
                    </li>
                    <li>
                      <strong style={{color}}>Consolidarlo.</strong> <p>Determina qué es lo que rodea o lo que podría rodear tu pasatiempo, para expandirte. Puede ser que tu pasatiempo no dé para tanto, que sea una distracción esporádica y nada más. Pero también puede ser que ese deporte que iniciaste simplemente para estar en forma te lleve a competencias importantes, o que ese producto que has ido perfeccionando ya requiera de una patente nacional e internacional, o que tu pasión por determinado tema te haya hecho un verdadero experto, gustoso de guiar o asesorar a otros.
                      Como toda inversión inteligente y atendida a lo largo del tiempo, invertir en tu pasatiempo, con tu tiempo y tus recursos, te puede traer de vuelta mucho más. Ciertamente, este enfoque es una puerta de entrada para tu capacidad de emprender.</p>
                    </li>
                  </ul>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Comunícalo</strong>
                  <p>Compartir información y experiencias con personas afines al tipo o tema de tu pasatiempo acelera por mucho el aprendizaje que puedes obtener y le da una dimensión mucho más amplia. Internet y las redes sociales presentan un sinfín de posibilidades para ello.</p>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvahorro.html'>CONDUSEF – Cuadernos y videos: Ahorro
              </a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvpresupuesto.html'>CONDUSEF – Cuadernos y videos: Presupuesto
              </a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/presupuesto.html'>CONDUSEF – Haz más con tu dinero: Presupuesto
              </a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Pasatiempo.seo = {
  title: 'Consolidar mi pasatiempo | CONSAR ',
  description: 'Si ya no quiero sentir que el tiempo pasa, mejor dejo que mi pasatiempo lo acapare.',
  ogtitle: 'Consolidar mi pasatiempo | CONSAR',
  ogdescription: 'Si ya no quiero sentir que el tiempo pasa, mejor dejo que mi pasatiempo lo acapare.',
  keywords: [
    'consar',
    'Consolidar mi pasatiempo'
  ]
}

export default Pasatiempo
