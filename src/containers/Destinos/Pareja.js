import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Pareja = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/vivir en pareja.png' />
        <h1 style={{color}}>Vivir en pareja</h1>
        <div className={style.textContainer}>
          <h4 style={{color}}>Pasos a seguir</h4>
          <div>
            <ol>
            <strong><li><p>Una vez que has elegido vivir en pareja considera los ingresos y egresos que te permitan cubrir las necesidades en común y también en lo individual</p></li></strong>

            <strong><li><p>Organicen las responsabilidades administrativas comunes con un presupuesto, por básico que sea, para vigilar y equilibrar sus gastos, su ahorro y su capacidad para invertir.</p></li></strong>

            <strong><li><p>Al buscar su propia vivienda, tomen en cuenta su capacidad financiera a corto y mediano plazo, para resolver si iniciarán rentando, comprando o construyendo paulatinamente un lugar para establecerse de manera definitiva.</p></li></strong>

            <strong><li><p>Consideren que el ahorro acumulado en la Subcuenta de Vivienda de su Cuenta AFORE les permitirá acceder a un crédito hipotecario.</p></li></strong>
            <strong><li><p>Deberán decidir si casarse o vivir en unión libre. En ambos casos se adquieren responsabilidades legales y, dependiendo el régimen por el cual se hayan unido (bienes separados o mancomunados), hay derechos y obligaciones correspondientes.</p></li></strong>
            </ol>
            <img src='/static/img/projects/pareja1.jpg' />

            <p>Vivir en pareja es el paso lógico para satisfacer el deseo de compartir y convivir con otra persona que amas, que te gusta o te interesa. Es un proyecto de integración, de complemento, de dar y recibir en un acuerdo que aspira al desarrollo de ambas personas, con el paso del tiempo; es adquirir la madurez necesaria para crear, descubrir y disfrutar las coincidencias, así como tolerar y aprender de las diferencias.</p>
            <p>A nivel individual, implica tu predisposición y capacidad para ceder, es una prueba de reafirmación y al mismo tiempo de adecuación de tus costumbres, valores y creencias. Por ejemplo, tu espacio vital se modifica al igual que el orden de tus prioridades; compartes tus recursos y ya no eres el centro de atención ni de decisión. </p>
            <p>Convivir con tu pareja implica establecer roles y responsabilidades, readecuar las rutinas, negociar con un alto grado de posibilidad de vulnerar y quizás sacrificar algunas de tus zonas de confort, aunque, de igual forma, se abre un sinfín de oportunidades para aprender y crecer.</p>
            <p> Y no cabe duda de que la construcción de una relación en pareja es una de las mayores inversiones que puedas realizar en tu vida. Pones tu tiempo, tu energía, tu espacio y tus recursos; puedes entregar mucho, intensamente, coqueteando con el riesgo de ganar en abundancia o de perder cuando menos lo esperes; puedes hacerlo con cierta inseguridad, como con predisposición a que no durará y será de corto plazo, o al contrario, con toda la motivación y actitud de aspirar a algo perdurable y enriquecedor en el largo plazo.</p>
            <p>Como sea, vivir en pareja e invertir en tu relación es un proyecto para “ganar-ganar” si haces de cada momento una experiencia constructiva para ambos.</p>
          </div>
          <img src='/static/img/projects/pareja2.jpg' />

        </div>
        <h2 style={{color}}>Los beneficios de vivir en pareja</h2>
        <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
          <div>
            <ul>
              <li>
                <strong style={{color}}>La unión hace la fuerza</strong>
                  <p>Uno de los mayores beneficios de vivir en pareja es hacer equipo. La convivencia les permite organizar sus vidas de manera a crear y realizar proyectos en común o individuales, pero con el apoyo del otro. Saberse respaldado por un ser querido es un poderoso incentivo. Por supuesto, ello necesita alimentar lazos sólidos de comprensión, respeto y confianza.</p>
              </li>
              <li>
                <strong style={{color}}>Estabilidad emocional</strong>
                <p>Establecer una relación con la intención de que sea duradera implica mucho compromiso emocional y un acuerdo de principios entre ambas personas. Normalmente se da por hecho, pero dialogar y expresar los enfoques de cada quien sobre temas como la fidelidad, los celos, la autoridad, el ocio, la convivencia con las familias y los amigos, el trabajo o los sueños propios de cada uno, entre muchos más, es un requisito fundamental para un sano entendimiento y desarrollo común. Disfrutar de una estabilidad emocional con tu pareja presupone haber establecido los cimientos sobre los que se basa la relación. En caso dado, ello les permite enfocar su energía hacia aspectos placenteros y constructivos para ambos.</p>
              </li>
              <li>
                <strong style={{color}}>Adoptar nuevos hábitos</strong>
                <p>Al vivir en pareja, ambos deben consensar y adaptarse a costumbres diferentes a las propias. Ello puede motivarlos a adoptar rasgos de personalidad y aspectos interesantes del uno y el otro. Por ejemplo, si tu pareja tiene hábitos de alimentación más saludables que los tuyos, puede motivarte a seguir esos pasos y mejorar tu estilo de vida. También puede ser a la inversa, aunque por lógica, el propósito de compartir es para avanzar y no para retroceder.</p>
              </li>
            </ul>
          </div>
          <img src='/static/img/projects/pareja3.jpg' />

        </div>
        <h2 style={{color}}>Las implicaciones de vivir en pareja</h2>
        <div className={style.textContainer}>
          <div>
            <ul>
              <li>
                <strong style={{color}}>Compromiso y responsabilidad</strong>
                <p>Una de las principales características de vivir en pareja radica en asumir compromisos y responsabilidades. A medida de que la relación progresa, los proyectos en común van aumentando, como apoyarse en la formación personal o profesional, adquirir ciertos bienes como una vivienda, colaborar en un negocio o tener hijos, es decir, lo más probable es que busques formar una familia y crear un patrimonio para ella, lo cual te compromete a varias responsabilidades.</p>
              </li>
              <li>
                <strong style={{color}}>Presupuesto compartido</strong>
                <p>Al cohabitar con otra persona algunos gastos tienden a reducirse, principalmente los relacionados a servicios del hogar, como agua, gas, energía eléctrica, la renta, o quizá también los gastos de transporte. Por otro lado, los costos relacionados con el entretenimiento tienden a aumentar.</p>
                <p>Cada pareja estable sus prioridades y gustos. Para compartir actividades necesitan organizar sus recursos. Un presupuesto del hogar puede ser de gran ayuda para lograr acuerdos, distribuir y vigilar los gastos, independientemente de que los ingresos provengan de uno o de ambos. Este aspecto es particularmente sensible porque el dinero, o mejor dicho, la carencia o limitación de dinero, suele ser una de las principales presiones y motivo de conflictos al vivir en pareja. Bien dice el dicho, “cuentas claras, amistades –y relaciones- largas”.</p>
                <p>Cuando hay ingresos suficientes, el presupuesto del hogar puede ser holgado, despreocupado y sin demasiada supervisión, y sí, ello procura tranquilidad y placer, pero cuando por algún motivo el dinero empieza a escasear, o viene una mala racha o de plano nunca alcanza, es entonces cuando se valora la habilidad para planear un buen presupuesto en pareja. De hecho, aunque los ingresos sean abundantes, todos sabemos que gastar a rienda suelta es un peligro y que la sensatez, adquirida por experiencia, nos recuerda constantemente que hay que buscar entradas económicas, ahorrar y sobre todo invertir, para disminuir y superar las carencias.</p>
                <p>El que ambos generen ingresos suele ser la mejor forma para equilibrar la responsabilidad y la participación económica dentro de la vida en pareja. En acuerdo mutuo, cuando uno deja de percibir ingresos el otro puede reforzar el desbalance hasta reorganizar y reestablecer los ingresos acostumbrados o deseados. Este acuerdo es particularmente importante al perder un empleo o por un mal negocio, al estar en transición entre un trabajo y otro, por maternidad o por enfermedad. Nuevamente, lo más recomendable es desarrollar la capacidad en pareja para prever estas situaciones y realizar acciones oportunas para cubrirse.</p>
              </li>
              <br/>
              <li>
                <strong style={{color}}>Metas en común y personales</strong>
                <p>Establecer metas comunes es parte esencial de la vida en pareja y de ahí que el trabajo “en equipo” sea una valiosa cualidad. Como se dijo antes, si ambos “se van a invertir” en ello, más vale que sea con toda la intención de que juntos disfruten el hacer y construir, crecer y ganar.</p>
                <p>En ocasiones, la vida en pareja se estaciona en una rutina que la puede sofocar. Así como las relaciones nacen al compartir sueños y proyectos, Antoine de Saint-Exupery (autor de El Principito)  también alertó en alguna ocasión que “Amar no significa verse el uno al otro, sino ver juntos hacia la misma dirección”. En el caso de que sientas cierto desaire al vivir en pareja, sirva esta frase para que te cuestiones cuáles son tus límites de autonomía o de dependencia, de dónde y cómo te alimentas de ideas, intereses y experiencias para después nutrir a tu relación, y de paso, en qué medida tu pareja también se cuestiona y se contesta estas preguntas.</p>
                <p>En efecto, vivir en pareja también es cuidar tu autonomía y la de tu pareja. Hay muchas vivencias donde hay que atender este tema y una particularmente ilustrativa es sobre el retiro o jubilación.</p>
                <p>Cuando se tienen ingresos y se deben administrar existe un ejercicio natural sobre el manejo del dinero. Así como “la práctica hace al maestro”, la práctica financiera respaldada por adecuada información también hace al buen usuario financiero; aquel que saca el mayor provecho de los servicios financieros y del manejo de su propio dinero.</p>
                <p>Una persona que cuida y valora sus ingresos suele prever, y por lo mismo es más propensa a ahorrar e invertir para su retiro, ya sea por ahorro obligatorio descontado de su nómina y/o por su ahorro voluntario. Cuando ambos obtienen ingresos, ahorrar para el retiro es un tema personal con mayor o menor dedicación, pero cuando solo uno de los dos trabaja, el otro puede quedar desprotegido. Estadísticamente, las mujeres están en desventaja por muchas razones, siendo la maternidad y el cuidado de los hijos las actividades no remuneradas que mayormente las alejan de construirse su propio fondo de dinero para el retiro.</p>
                <p>Pensando en pareja y por el bienestar futuro de ambos, a pesar de que solo uno tenga ingresos, ambos deberán ahorrar, invertir y hacer crecer sus recursos individuales. Ello también protege a ambas partes en el caso de la separación.</p>
              </li>
              <li>
                <strong style={{color}}>El hogar para vivir</strong>
                <p>Vivir en pareja significa convivir bajo un mismo techo. Generalmente, el primer paso es definir la ubicación y las características básicas de la vivienda (tipo y tamaño), de preferencia lo más cerca posible de los puntos de trabajo, de actividad o de contacto familiar, y ello directamente relacionado con el costo y la capacidad financiera de la pareja. </p>
                <p>Muchas inician rentando un inmueble y, según sus posibilidades, después eligen un lugar para comprarlo y establecerse de manera permanente. Por supuesto, adquirirlo es un paso importante para fortalecer su patrimonio.</p>
                <p>Quizá obtengan su vivienda de una herencia, o la renten, la compren o la construyan de golpe o poco a poco. Lo importante es conocer lo mejor posible todos los requisitos para estar administrativa y fiscalmente tranquilos.</p>
              </li>
              <li>
                <strong style={{color}}>¿Casarse o no casarse?</strong>
                <p>Un acuerdo a tomar antes o después de vivir en pareja es si piensan formalizar legalmente su unión. Si deciden hacerlo, deberán tomar una decisión financiera relevante para el futuro de la pareja (o familia): si la unión se realizará por bienes mancomunados o por bienes separados.</p>
                <p>En el primer caso, ambos se comprometen a compartir la propiedad de todos los bienes que posean. En el segundo caso, cada quien conserva la propiedad de sus pertenencias hasta antes del matrimonio y solo compartirán lo que adquieran ya estando casados.</p>
                <p>Esta es una decisión financiera importante que repercute en todo lo que ocurra a futuro, así que valórenlo bien y tomen juntos la mejor decisión.</p>
              </li>
              <li>
                <strong style={{color}}>En caso de separación o divorcio</strong>
                <p>A pesar de que lo dicho hasta ahora en este proyecto de vida va dirigido a construir la relación en pareja, es de sabios estar informados y conocer ambos lados de la moneda.</p>
                <p>Si la pareja decide separarse, las decisiones que hayan tomado en el pasado cobrarán relevancia. Por ejemplo, si hubo matrimonio, lo más probable es que lleguen a un divorcio cuando decidan que la separación es definitiva. Derivado de ello, hay que considerar el monto de la pensión alimenticia que se deberá aportar, en su caso, a quien conserve la tutela o patria potestad de los hijos cuando sean menores de edad o sigan estudiando.</p>
                <p>Asimismo, tendrán que decidir cómo distribuir los bienes materiales y económicos que hayan adquirido durante el matrimonio por bienes separados, o del total de los bienes en caso de establecerlos como mancomunados.</p>
              </li>
            </ul>
          </div>
        </div>
        <div className={style.links}>
          <h3 style={{color}}>Ligas de interés</h3>
          <ul>
            <li><a target='_blank' style={{color}} href='http://portal.infonavit.org.mx/wps/wcm/connect/infonavit/inicio'>INFONAVIT. Instituto del Fondo Nacional de la Vivienda para los Trabajadores. Portal</a></li>
            <li><a target='_blank' style={{color}} href='https://www.gob.mx/fovissste'>FOVISSSTE. Fondo de la Vivienda del Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado. Portal</a></li>
            <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_vidapareja.html'>CONDUSEF – Eventos en tu vida: Vida en pareja</a></li>
            <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvpresupuesto.html'>CONDUSEF – Cuadernos y videos: Presupuesto</a></li>
          </ul>
        </div>
      </div>
    </Layout>
  )
}

Pareja.seo = {
  title: 'Vivir en pareja | CONSAR ',
  description: 'Cuando hay buena química se disfrutan todos los experimentos. ¿Y si  compartimos fórmulas?',
  ogtitle: 'Vivir en pareja | CONSAR',
  ogdescription: 'Cuando hay buena química se disfrutan todos los experimentos. ¿Y si  compartimos fórmulas?',
  keywords: [
    'consar',
    'Vivir en pareja'
  ]
}

export default Pareja
