import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Estudios = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/realizar estudios superiores.png' />
        <h1 style={{color}}>Realizar estudios superiores</h1>
        <div className={style.textContainer}>
          <h4 style={{color}}>Pasos a seguir</h4>
          <div>
            <ol>
            <strong><li><p>Investiga e infórmate con el mayor grado de detalle sobre todas las opciones en torno a la carrera o especialización que elijas: busca los planes y programas de estudio, los requisitos de admisión, los costos y planes de financiamiento.</p></li></strong>

            <strong><li><p>Arma un expediente con por lo menos tres opciones, por orden de preferencia. Elije de manera congruente a tu potencial, piensa en posibilidades nacionales o internacionales y que los costos no sean una barrera anticipada mientras no hayas investigado las posibles fuentes de financiamiento.</p></li></strong>

            <strong><li><p>Planea y diseña en una posible mezcla de fuentes de financiamiento. Infórmate bien y sé prudente con los créditos educativos.</p></li></strong>

            <strong><li><p>Proyecta el impacto de tus estudios en tu presupuesto personal o familiar. En su momento, realiza ajustes  frecuentes hasta lograr la distribución más equilibrada.</p></li></strong>
            </ol>
            <img src='/static/img/projects/estudios1.jpg' />

            <p>Realizar estudios superiores es una inversión que rendirá de acuerdo a tu propio potencial. Puede culminar con un título, o puede trascender si la aprovechas en las oportunidades que se presenten y en las que generes.</p>
            <p>Sin duda, el interés de realizar una especialización, maestría o doctorado, también puede responder a otros motivos, pero es ante todo una inversión en tu formación personal que debes aprovechar, practicar y enriquecer con tu desempeño y desarrollo profesional. Adquirirás un mayor nivel de especialización de carácter académico sobre una disciplina o un sector, lo que a su vez te permitirá cotizarte mejor y aspirar, por ejemplo, a una mayor remuneración económica.</p>
            <p>Lo interesante es que, efectivamente, podrás aspirar a más: más relaciones, círculos especializados de investigación y trabajo, información reservada, oportunidades más exclusivas y recursos, pero obtener todo esto no es una consecuencia automática solo por haber cursado estudios superiores, intervienen además varios otros factores relacionados, como tu desempeño académico, dónde y con quién estudiaste, la relevancia de tu personalidad, perfil y proyecto profesional, entre otros.</p>
            <p>Como sea, pensar en estudios superiores remite a costos que pueden llegar a ser muy altos, y por tanto, te conviene elaborar un plan financiero integral y oportuno para llevar a cabo esta inversión en la aventura de tu vida.</p>
          </div>
          <img src='/static/img/projects/estudios2.jpg' />

        </div>
        <h2 style={{color}}>Los beneficios de realizar estudios superiores</h2>
        <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
          <div>
            <ul>
              <li>
                <strong style={{color}}>Oportunidades de empleo</strong>
                  <p>La educación superior por lo general representa un mayor nivel de capacitación y conocimientos sobre una materia o campo específico. Debido a que algunas empresas buscan personas más especializadas o con un espectro de acción más robusto, un título incrementa tus posibilidades de ocupar empleos o de realizar actividades en condiciones más interesantes. Un título siempre es una ventaja que te hace más competitivo.</p>
              </li>
              <li>
                <strong style={{color}}>Mayor ingreso</strong>
                <p>Un mayor grado de capacitación, de especialización y de conocimientos es una llamativa carta de presentación que te puede favorecer para acceder a trabajos mejor remunerados.</p>
              </li>
              <li>
                <strong style={{color}}>Trabajo deseado</strong>
                <p>Con estudios superiores existe una mayor probabilidad de que puedas desempeñar tu carrera profesional en el ámbito que deseas.</p>
              </li>
              <li>
                <strong style={{color}}>Visión de emprendimiento</strong>
                <p>Al tener una formación de educación superior es más fácil que amplíes tu panorama laboral buscando emprender proyectos o negocios propios. Si aprovechas el gran nivel de interacción (networking) que por lo general ofrece el ámbito académico, podrías fortalecer tus vínculos con las personas con quienes tengas objetivos afines o que puedan abrirte puertas hacia nuevas oportunidades.</p>
              </li>
            </ul>
          </div>
          <img src='/static/img/projects/estudios3.jpg' />

        </div>
        <h2 style={{color}}>Las implicaciones de realizar estudios superiores</h2>
        <div className={style.textContainer}>
          <div>
            <ul>
              <li>
                <strong style={{color}}>Elecciones en torno a los estudios</strong>
                <p>Tener la certeza de lo que se desea estudiar no siempre resulta sencillo. Por un lado, la oferta de planes y programas de estudios es muy amplia, pero más importante aún es que tengas claro lo que tú pretendes con estos estudios. Más vale que te tomes el tiempo necesario para informarte e investigar a fondo las ventajas, las desventajas, las alternativas a tu alcance y las implicaciones generales. Se dice fácil, pero la seguridad que tengas de tu elección dependerá de haberle dado la vuelta al mayor número de opciones.</p>
                <p>Tan es así que, por ejemplo, resulta astuto candidatearse en las diferentes instituciones educativas que más te hayan interesado, por su conjunto de características, como lo son el plan de estudios, costos, programas de financiamiento, ubicación, reputación o referentes, entre otras. Factores como los exámenes de admisión y los criterios de selección propios a cada institución jugarán a tu favor o en tu contra, según tu nivel y tus motivos expresados en una carta de postulación.</p>
                <p>Otro ejemplo interesante a tener en cuenta al momento de elegir el rumbo de tus estudios, es la gran velocidad con la que se mueve el mercado profesional. Cerciórate que tu visión profesional a futuro, precisamente, tenga futuro, y no termines en un mercado saturado u obsoleto. </p>
              </li>
              <li>
                <strong style={{color}}>Inversión</strong>
                <p>Puedes acceder a estudios superiores por medio de instituciones del Estado o privadas. Los costos de las primeras son más bajos que las segundas, y por lo mismo hay una fuerte demanda y competencia para ingresar. Aquí vale la pena señalar que la calidad de los estudios no depende forzosamente de su costo; hay instituciones del Estado así como privadas, de gran prestigio educativo. Nuevamente, infórmate y decide.</p>
                <p>Con respecto al financiamiento de tus estudios superiores, existen varias posibilidades:</p>
                <ul>
                  <li>Financiamiento por parte de una empresa o institución
                  <p>Muchas organizaciones valoran la capacitación permanente de sus empleados e implementan este tipo de financiamientos a manera de prestaciones, precisamente como un tipo de inversión en la calidad de la formación y la actualización de su planta laboral.</p>
                  </li>
                  <li>Méritos propios para obtener becas
                  <p>Diversas instituciones y fundaciones valoran el esfuerzo personal sobresaliente y lo fomentan proporcionando becas en diversos porcentajes y proporciones. Una vez más, es necesario ser candidato y competir por ellas. Las becas pueden ser a fondo perdido o bien, a cambio, te pueden proponer que cumplas con condiciones específicas.</p>
                  </li>
                  <li>Planes previsionales de financiamiento
                  <p>Estos planes pueden ser muy variables, pero en términos generales, se trata de que ahorres e inviertas tus recursos a través de instituciones financieras especializadas que te darán rendimientos, para que al cabo de cierto tiempo estés en posibilidad de pagar tus estudios superiores o una parte de ellos. Por ejemplo, la Cuenta AFORE para menores puede servir como cuenta de ahorro e inversión con miras a reunir un fondo económico para solventar sus estudios.</p> </li>
                  <li>Pago directo
                  <p>Si trabajas y tus ingresos te lo permiten, puedes pagar tus cuotas sin recurrir a ahorros o a créditos.</p>
                  </li>
                  <li>
                  Financiamiento por créditos educativos
                  <p>Podría pensarse que esta no es una buena opción y sin duda es complicada, si no sabes o no logras utilizar el crédito a tu favor. Los casos de créditos educativos que a la larga resultan impagables, pueden asustar a más de uno, pero no tiene por qué ser regla. Por ejemplo, supongamos que te falte un semestre para terminar tus estudios superiores pero te quedas sin fondos para ello. Recurrir a un crédito, en el entendido de tener un plan de pagos acorde a tus posibilidades, puede ser una excelente opción para no quedarte a tan poco de tu meta educativa.</p>
                  </li>
                </ul>
                <p>La mezcla o combinación de algunas de estas formas de financiamiento puede resultar muy conveniente. Por lo mismo, anticipa tus necesidades y las gestiones correspondientes.</p>
                <p>Otros gastos de los estudios superiores, que no hay que menospreciar, son los que corresponden al transporte y a los diferentes materiales como libros, útiles, equipo de cómputo y herramientas que sean parte de lo requerido para realizar y cumplir con tus programas.</p>
              </li>
              <br/>
              <li>
                <strong style={{color}}>Duración / Tiempo</strong>
                <p>En promedio, el tiempo requerido para realizar estudios superiores oscila entre 2 y 5 años si te apegas a los programas de estudio de la mayoría de las instituciones educativas (hasta 8 años para Medicina), aunque ello también puede ser muy variable. Por ejemplo, puedes llevar más materias que las sugeridas por periodo escolar y con ello terminar antes, o bien puedes llevar menos materias, lo que extenderá la duración de tus estudios pero te permitirá administrar mejor tu tiempo para quizá atender tu trabajo, otras actividades o tu familia.</p>
                <p>Sin duda, lo importante es que logres la mejor dosificación posible de tu tiempo para que puedas dedicarte con gusto y calidad a tus diferentes actividades, incluyendo dormir y descansar.</p>
              </li>
            </ul>
          </div>
          </div>
        <div className={style.links}>
          <h3 style={{color}}>Ligas de interés</h3>
          <ul>
            <li><a target='_blank' style={{color}} href='http://www.enlacejudio.com/2017/07/13/no-compren-coches-ni-estudien-derecho/'>Artículo. No compren coches ni estudien derecho</a></li>
            <li><a target='_blank' style={{color}} href='http://www.becas.sep.gob.mx/'>SEP. Secretaría de Educación Pública. Sección Becas</a></li>
            <li><a target='_blank' style={{color}} href='http://www.conacyt.gob.mx/'>Consejo Nacional de ciencia y tecnología. Portal</a></li>
            <li><a target='_blank' style={{color}} href='http://www.anuies.mx/html/diries/index.php'>ANUIES. Asociación Nacional de Universidades e Instituciones de Educación Superior. Directorio Nacional de Instituciones de Educación Superior (México)</a></li>
            <li><a target='_blank' style={{color}} href='http://www.anuies.mx/sitios-de-interes'>Sección de sitios de interés.</a></li>
            <li><a target='_blank' style={{color}} href='http://www.funedmx.org/quienes-somos'>La Fundación Mexicana para la Educación, la Tecnología y la Ciencia (FUNED)</a></li>
          </ul>
        </div>
      </div>
    </Layout>
  )
}

Estudios.seo = {
  title: 'Realizar estudios superiores | CONSAR ',
  description: 'Así como la mejor forma de ganar es preparándose, la mayor técnica del aprendizaje es compartir el conocimiento.',
  ogtitle: 'Realizar estudios superiores | CONSAR',
  ogdescription: 'Así como la mejor forma de ganar es preparándose, la mayor técnica del aprendizaje es compartir el conocimiento.',
  keywords: [
    'consar',
    'Realizar estudios superiores'
  ]
}

export default Estudios
