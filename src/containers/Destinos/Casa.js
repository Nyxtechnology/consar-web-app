import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Casa = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/adquirir una vivienda.png' />
        <h1 style={{color}}>Adquirir mi vivienda </h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Elige la vivienda que quieres con el costo adecuado a tus posibilidades. No olvides gastos colaterales como la escrituración, remodelación y mudanza, entre otros.</p></li></strong>
                <strong><li><p>Explora las alternativas de créditos o préstamos a los que puedas tener acceso. Si vas a adquirir un crédito hipotecario considera el pago del enganche y los plazos y montos a pagar de acuerdo a tus posibilidades.</p></li></strong>
                <strong><li><p>Aprovecha el ahorro que tienes acumulado en la Subcuenta de Vivienda de tu cuenta AFORE.</p></li></strong>
              </ol>
              <img src='/static/img/projects/casa1.jpg' />

              <p>En algún momento existe la necesidad de ser independientes, ya sea por crecimiento personal o compromiso familiar, llega la hora de buscar un lugar propio, que tenga nuestro sello y a la vez nos transmita seguridad y tranquilidad.</p>

              <p>Construir, adecuar y cuidar ese espacio es parte esencial de la aventura de vivir.
                ¿Eres de los que cree que seguir rentando es desperdiciar su dinero? ¿O más bien prefieres no aferrarte a un espacio y seguir mudándote para cambiar de aires libremente?</p>

              <p>De acuerdo con un estudio publicado por la Condusef, los millennials gastan 65% de su sueldo adquiriendo deudas en renta de una casa o departamento. Contrariamente, generaciones mayores valoran más el hacerse de un patrimonio inmobiliario.</p>

              <p>Si tú has elegido adquirir tu propia vivienda, entonces has decidido tener sentido de pertenencia sobre tu hábitat.</p>

            </div>
            <img src='/static/img/projects/vivienda2.jpg' />
          </div>
          <h2 style={{color}}>Los beneficios de adquirir tu vivienda:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                <p>  <strong style={{color}}>Protección y seguridad: </strong>
                  Si eres propietario de una vivienda resuelves quizá el tema patrimonial más importante de tu vida. A la par, obtienes un gran sentido de pertenencia y cobijo.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Independencia: </strong>
                    Puedes tomar decisiones sobre modificaciones y construcción con total libertad. También mayor confianza para expandir el tamaño de tu familia, hijos o mascotas, sin restricciones de terceros. No existen gastos de arrendamiento o inspecciones por parte del arrendador, que pueden resultar incómodas.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Genera valor adicional: </strong>
                  Generalmente, cuando se hacen mejoras en las viviendas o en sus alrededores, con el paso del tiempo se genera un valor adicional conocido como plusvalía, incrementando el valor de tu hogar año con año.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Legado familiar: </strong>
                  Si estás pensando en tener una familia o ya tienes una, tu vivienda es un patrimonio importante que podrás heredarles.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Patrimonio futuro para tu vejez: </strong>
                  Todo el patrimonio que logres acumular cuenta y es valioso como una fuente de financiamiento para la etapa de la vejez, por eso es importante cuidarlo. Tal vez en ese momento no necesites un espacio tan grande y decidas cambiar de casa o tal vez decidas rentar parte de tu hogar y con ello generar un ingreso extra.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/vivienda3.jpg' />

          </div>
          <h2 style={{color}}>Las consideraciones de adquirir tu vivienda:</h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <p><strong style={{color}}>Tamaño: </strong>
                  Debes considerar si el tamaño de la vivienda es adecuado con respecto a la cantidad de personas que vivirán en ella.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Ubicación: </strong>
                    Analiza qué tan cerca está de tu lugar de trabajo, escuelas de tus hijos, supermercados, hospitales, vías primarias y secundarias, así como medios de transporte disponibles. Recuerda que si vives en una ciudad transitada puedes recorrer distancias cortas en tiempos excesivamente largos.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Servicios: </strong>
                    Investiga qué tan buena es la calidad de los servicios básicos requeridos, es decir, abasto de agua, luz, gas, así como servicios públicos: drenaje, recolección de basura, áreas verdes y de recreación, seguridad, entre otros.</p>
                  </li>
                <li>
                  <p><strong style={{color}}>Costos de mudanza: </strong>
                  Al adquirir la vivienda incurrirás en un costo de mudanza que es la forma por medio de la cual se transportarán tus bienes.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Gastos de remodelación: </strong>
                  Independientemente de si la casa/departamento que adquieras es nueva o usada, se pueden generar modificaciones y remodelaciones a nivel estructural o con fines decorativos. Evalúa cuál es tu capacidad para realizar este tipo de cambios, ya que, por lo regular, resultan costosos.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Ingreso suficiente: </strong>
                  Debido a que la compra de una casa/departamento representa una fuerte inversión, debes determinar cuál es la mejor manera de adquirirla. Explora las alternativas que ofrecen distintas instituciones financieras como créditos o préstamos, cuál será el monto que te comprometes a pagar y cómo impactará esto en tus egresos.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Financiamiento: </strong>
                    Generalmente se busca adquirir un crédito hipotecario, que puede ser otorgado por instituciones financieras, privadas o públicas. Este tipo de crédito se compone de dos partes, en primera instancia el depósito o enganche que normalmente representa entre el 20% y el 30% del valor de la vivienda y posteriormente se realizan pagos en plazos para la liquidación total de la deuda.
                    Si ya tienes tu cuenta AFORE, dispones de una Subcuenta de Vivienda que, dependiendo de tu ahorro acumulado, te permite financiar parte o la totalidad del inmueble, a través del FOVISSSTE o el INFONAVIT. Recuerda que si cotizas para el IMSS o para el ISSSTE el ahorro de tu Subcuenta de Vivienda aparece en tu estado de cuenta AFORE.
                    Además, debes contar con un buen historial crediticio, de lo contrario, tendrás dificultades para la adquisición de un financiamiento.
                    NOTA: Recuerda que si no ocupas el ahorro de tu Subcuenta de Vivienda, éste se te devolverá al momento de tu retiro.</p>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='http://www.condusef.gob.mx/Revista/index.php/credito/hipotecario'>CONDUSEF - Crédito Hipotecario</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_comprarcasa.html'>CONDUSEF – Eventos en tu vida: Comprar una vivienda</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvburo.html'>CONDUSEF – Cuadernos y eventos: Buró de Entidades Financieras</a></li>
              <li><a target='_blank' style={{color}} href='http://www.burodecredito.com.mx/reporte-info.html'>Buró de crédito – Reporte de crédito especial</a></li>
              <li><a target='_blank' style={{color}} href='http://portal.infonavit.org.mx/wps/wcm/connect/infonavit/inicio'>INFONAVIT - Portal</a></li>
              <li><a target='_blank' style={{color}} href='https://www.gob.mx/consar/articulos/subcuenta-de-vivienda'>CONSAR - Subcuenta de vivienda</a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Casa.seo = {
  title: 'Adquirir mi vivienda | CONSAR ',
  description: 'Busco un espacio de fuertes cimientos, para que  en él construya mi hogar con sueños y logros.',
  ogtitle: 'Adquirir mi vivienda | CONSAR',
  ogdescription: 'Busco un espacio de fuertes cimientos, para que  en él construya mi hogar con sueños y logros.',
  keywords: [
    'consar',
    'Adquirir mi vivienda'
  ]
}

export default Casa
