import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Personal = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/alcanzar desarrollo personal.png' />
        <h1 style={{color}}>Alcanzar mi meta profesional </h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Imagina y visualiza lo esencial de tu trayectoria profesional a largo plazo. Cuál es el tema, área de trabajo o sector. Qué te mueve y qué esperas de esa trayectoria en particular.</p></li></strong>

                <strong><li><p>Los estudios que necesitas: cuáles y cuántos. Investiga dónde, cuánto cuestan y las posibles fuentes de financiamiento mediante qué.</p></li></strong>

                <strong><li><p>Los empleos o trabajos que necesitas haber experimentado. Investígalos y prepárate. Si lo has hecho bien, ellos te buscarán a ti.</p></li></strong>

                <strong><li><p>Construye tu personalidad: Inviértete, con tu tiempo, dinero y esfuerzo en todo lo que te gusta y que terminará por describirte. Diversifica tu atención y tus acciones para enriquecer tu desarrollo personal. </p></li></strong>
              </ol>
              <img src='/static/img/projects/profesional1.jpg'/>

              <p>Este proyecto es sobre tu desarrollo personal y profesional a lo largo de la vida. Es un tema tan amplio que, para fines prácticos, aquí lo enmarcaremos principalmente en el ámbito profesional, y ello no significa pensar únicamente en los estudios que tengas en mente. “Alcanzar mi meta profesional” se nutre de aspectos muy diversos que hablan de tu formación integral, de quien eres y de tu sentimiento de realización. Este proyecto te invita a reflexionar y a contestar la pregunta: ¿Qué contiene y, sobre todo, qué me gustaría que incluyera mi biografía, mi historia de vida?</p>
              <p>Otro ejemplo práctico puede ser tu currículum vitae, que si bien presenta tus estudios profesionales, también indica los idiomas que hablas, tus fortalezas y áreas de interés, tus logros y referencias, así como toda la información que consideres conveniente para describirte de manera interesante y proactiva.</p>
              <p>Al igual que para varios de los proyectos de La Aventura de mi Vida, la pregunta central es saber qué tanto eres tú quien planea y decide el rumbo de tus acciones, o si básicamente prevalece el azar y por tanto estás más expuesto a la incertidumbre.</p>
              <p>En este sentido, decisiones de vida como formar tu propia familia, obtener el o los empleos que quieras, emprender negocios, realizar estudios superiores, escribir un libro o filmar un documental, comprometerte con una causa humanitaria u otra, lograr la estabilidad financiera que te permita gozar tu jubilación, generar y dar abundancia, son todos capítulos que pueden escribirse en la aventura de tu vida.</p>
              <p>Entonces, ¿qué incluirá tu biografía y a quién más inspirará?</p>
            </div>
            <img src='/static/img/projects/profesional2.jpg' />
          </div>
          <h2 style={{color}}>Los beneficios de alcanzar mi meta profesional:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Satisfacción personal, realización y plenitud</strong>
                    <p>Como en todos los proyectos de la vida, el primer beneficio inicia con el interés y gusto personal. Una persona que se siente realizada encuentra un sentido profundo en su día a día y valora el aprendizaje que ha adquirido a lo largo de la vida. Donde hay interés, hay empuje, acciones, creatividad y memoria. Alcanzar un sentimiento de plenitud sobre tu bagaje profesional y de desarrollo personal es una forma de describir la actitud con la que recorres el camino.</p>
                    <p>Lo más deseable es que tu recuento sea positivo y gustoso, que tu capacidad de abrirte camino por donde te interesa supere las vivencias involuntarias y desagradables, porque dejar lo que te sucede solo en manos del destino te coloca como espectador pasivo de tu andar. Decidir y actuar puede ser tu estandarte. </p>
                </li>
                <li>
                  <strong style={{color}}>Reconocimiento</strong>
                    <p>El reconocimiento sincero y honesto es un poderoso alimento para el alma; sacia la necesidad de saberte identificado, integrado y relacionado con otros. Más aún cuando nace de hechos concretos, de las aportaciones que ofreciste para beneficiar a otras personas y que por lo tanto te expresan su gratitud, su disposición para compartir contigo y hasta su admiración. </p>
                    <p>El reconocimiento puede ser en el ámbito que quieras, familiar, comunitario o hasta masivo gracias a los medios digitales, y puede ser producto de lo que te propongas: quizá de manera fortuita, o como culminación de un largo proceso de esfuerzo y trabajo, o en respuesta a tu liderazgo de opinión, etc. </p>
                </li>
                <li>
                  <strong style={{color}}>Aprendizaje integral</strong>
                  <p>Cada persona tiene sus temas y proyectos de interés. Algunos son por gusto mientras que otros, por ejemplo, son principalmente para obtener recursos. Lograr que tus proyectos satisfagan simultáneamente varias de tus necesidades personales los hace más atractivos y te permiten aprendizajes multidisciplinarios. Dicho de otra manera, date la oportunidad de ver lo que sucede en tu entorno mientras persigues cada meta. Con la mirada fija al frente, quizá no percibas los atajos y los beneficios que yacen latentes a tu alrededor. Amplía tu atención.</p>
                </li>
                <li>
                  <strong style={{color}}>Trascendencia</strong>
                  <p>Atender tu desarrollo personal y profesional siempre irá acompañado de propósitos que rebasen tu satisfacción inmediata o a corto plazo. Son procesos que llevan tiempo. A la par, hay retos donde las limitaciones, los problemas y la adversidad, propios y ajenos, se convierten en tus mejores aliados para seguir avanzando a pesar de “los imposibles”. Sin duda existen causas que te inspiran y te mueven al grado de que rebases tus límites una y otra vez.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/profesional3.jpg' />
          </div>
          <h2 style={{color}}>Las implicaciones de alcanzar mi meta profesional:</h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Tener visión</strong>
                  <p>Visualizar tu objetivo sirve para dimensionarlo y llevarlo a cabo. Visualizarlo, en todos los sentidos, es en sí el mejor inicio de acción. Por supuesto, puede ser tan vasto o específico como te lo propongas. De ahí la idea de pensar un gran objetivo, idealmente inspirador y que te apasione, y que se construya con diversas metas y etapas que podrás controlar, modificar y culminar más fácilmente.</p>
                  <p>Poner en la mira un gran objetivo profesional necesita que seas capaz de pensar en el largo plazo, puesto que se trata de armar una historia; la historia de tu vida profesional que sin duda se construirá con las metas que sumes a lo largo de varios años.</p>
                  <p>Hablar de un proyecto de largo plazo significa paciencia, perseverancia, disciplina y sobretodo convicción, para que literalmente no pierdas de vista tu gran objetivo y no permitas que tus metas te rebasen; de lo contrario, podrías sentirte abrumado o abrumada.</p>
                  <p>Para ello, procura desarrollar tus habilidades, empezando por aprovechar tus conocimientos; amplía tu capacidad de observación y de escuchar a los demás, aprende de la experiencia, reta tu creatividad y atiende tu intuición, mide y administra tus recursos. Inviértete en todo lo que emprendas, con tu tiempo, tu dinero y tu esfuerzo, a cambio de resultados mejores y mayores.</p>
                  <p>Tiempo atrás, tener éxito en la vida podía resumirse con el “cliché” de cubrir o realizar una serie de experiencias como: estudiar, tener un buen trabajo, casarse, hacerse de una vivienda, tener hijos, adoptar un perro, escribir un libro y plantar un árbol. ¿Así visualizas tu vida? Piénsalo dos veces y no te limites.</p>
                  <p>¿Cuáles serían tus vivencias clave? ¿Cuáles son los proyectos -en la aventura de tu vida- que conformarán la biografía que te gustaría tener y contar? Imagínalo por un momento y haz tu lista.</p>
                  <p>¿Cuál es tu proyecto inmediato? ¿Cómo lo visualizas y qué tanto estás haciendo para alcanzarlo?</p>
                </li>
                <li>
                  <strong style={{color}}>Diseñar la trayectoria profesional</strong>
                  <p>Más allá de un “cliché” sobre la vida exitosa, en el marco de los proyectos profesionales hay una infinidad de temas de dónde escoger para planear tu trayectoria y meta profesional.</p>
                  <p>De igual manera, existe una gran variedad de circunstancias que pueden influir en tu elección del o de los rumbos a seguir. Por ejemplo:</p>
                  <ul>
                    <li><p>La educación básica es multidisciplinaria; es un gran tronco común con información y códigos que sirven de cimiento para el desarrollo del conocimiento. Representa una vasta exposición a temas para activar el interés. </p></li>
                    <li><p>Experiencias de entretenimiento, ocupación y trabajo durante la adolescencia y la juventud pueden descubrir o reafirmar tus preferencias.</p> </li>
                    <li><p>El ejemplo que obtengas de familiares, amigos u otros, puede ser inspirador y un modelo a seguir.</p></li>
                    <li><p>Tus propias aptitudes y habilidades.</p></li>
                    <li><p>Compararte con otros y aspirar a algo diferente o mejor.</p></li>
                    <li><p>Tus sentidos y tu capacidad de explorarlos y usarlos.</p></li>
                    <li><p>Tu personalidad y formación, entendida como esa compleja mezcla de talentos y estímulos que te permiten ser más atento, interesado, intuitivo, deductivo, preciso, creativo, etc.</p></li>
                    <li><p>Los estudios de preparatoria y bachillerato pueden encausarte hacia una determinada disciplina, área o vocación.</p></li>
                    <li><p>La información y formación vocacional en sentido amplio, no solo por comprender qué tipo de actividades e intereses se persiguen en los diferentes sectores laborales, sino también por tomar en cuenta factores más amplios, como la oferta y la demanda laboral presente y sobretodo futura, la evolución de la tecnología con la subsecuente creación o cancelación de fuentes de trabajo, etc.</p></li>
                    <li><p>Los estudios universitarios y de postgrado, que si bien ya responden a determinadas áreas, también pueden abrir tu interés por la especialización, la investigación y la docencia, entre otras.</p></li>
                    <li><p>Los estándares de remuneración y de reconocimiento por tipo de profesión o actividad.</p></li>
                    <li><p>Y sí, también el azar, que un día te pone en una circunstancia imprevista y ésta te sorprende, revelándote un sugestivo mundo por recorrer.</p></li>
                    <li><p>Entre muchas más.</p></li>
                  </ul>
                    <p>Planear el rumbo de tu trayectoria profesional es como aprovechar una brújula para dirigirte hacia una dirección. Te ayudará a saber si estás caminando en línea recta o vagando un poco hacia otros lados, o caer en la cuenta de que has estado caminando en círculo o quizá también, que has vuelto por donde venías.</p>
                    <p>Todo suma. Solo tú sabrás qué esperas encontrar cuando alcances tu meta, o que no hay tal y que el mayor beneficio ha sido no perderte de lo que viviste en el camino.</p>
                </li>
                <li>
                  <strong style={{color}}>Capacitarse y cultivarse</strong>
                    <p>Este proyecto es de desarrollo personal y por tanto solo puede ir acompañado por el gusto de saber y aprender. Más que nunca pareciera que la información y las oportunidades se expanden, no quedando pretexto para quedarse atrás.</p>
                    <p>El auténtico interés por las cosas te lleva automáticamente a encontrarte con lo que buscas.</p>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
          <h3 style={{color}}>Ligas de interés</h3>
          <ul>
            <li><a style={{color}} target='_blank' href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvcapitalemprendedor.html'>CONDUSEF – Capital emprendedor</a></li>
            <li><a style={{color}} target='_blank' href='https://www.inadem.gob.mx/'>INADEM - Instituto Nacional del Emprendedor</a></li>
          </ul>
        </div>
      </div>
    </Layout>
  )
}
Personal.seo = {
  title: 'Alcanzar mi meta profesional | CONSAR ',
  description: 'Mi vida se define por las oportunidades, incluyendo las que he perdido. ¿De qué está hecho mi currículum y qué le falta?',
  ogtitle: 'Alcanzar mi meta profesional | CONSAR',
  ogdescription: 'Mi vida se define por las oportunidades, incluyendo las que he perdido. ¿De qué está hecho mi currículum y qué le falta?',
  keywords: [
    'consar',
    'Alcanzar mi meta profesional'
  ]
}

export default Personal
