import React from 'react'
import style from './style.css'

const Laboral = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <div className={style.item}>
      <img src='/static/img/projects/banners/obtener el empleo que quiero.png' />
      <h1 style={{color}}>Obtener el empleo que quiero </h1>
        <div className={style.textContainer}>
          <h4 style={{color}}>Pasos a seguir</h4>
          <div>
            <ol>
              <strong><li><p>Define qué te gusta o apasiona como para dedicarte a ello de manera profesional.</p></li></strong>
              <strong><li><p>Analiza a detalle todas las implicaciones del puesto que te resulta atractivo: actividad, horarios, cargas de trabajo, nivel de responsabilidad, prestaciones, y valora si realmente es lo que deseas.</p></li></strong>
              <strong><li><p>Asegúrate de que la remuneración que te ofrecen sea la justa de acuerdo a tu perfil.</p></li></strong>
              <strong><li><p>Busca capacitación para conocer bien la materia y mantente actualizado sobre las nuevas tendencias en tu campo. Existe una gran oferta de información y cursos vía internet que te pueden ayudar a hacer más eficiente este proceso. Algunos tiene costo pero hay muchas opciones gratuitas.</p></li></strong>
              <strong><li><p>Infórmate bien de las responsabilidades y obligaciones fiscales que adquieres al ser contratado. Busca que tu empleador te contrate en un esquema formal y con ello te permita acceder a todas las prestaciones de Ley.</p></li></strong>
            </ol>
            <img src='/static/img/projects/empleo1.jpg' />

            <p>
              Para tu primer empleo, al igual que para el quinto o el décimo, requieres de las mismas capacidades: adaptación y cambio, simultáneamente.
            </p>
            <p>
              Al inicio de tu vida laboral, probablemente las entiendas por separado, como capacidad de adaptación y como capacidad de cambio, pero más adelante, habiendo acumulado experiencia, lograrás aplicarlas como “adaptación al cambio”. La pregunta es… ¿Te puedes preparar para afrontar los cambios, no solo porque sucedan, sino porque participes en provocarlos?
            </p>
            <p>
              Dedicarle una gran parte de tu tiempo a tu trabajo merece honestidad contigo mismo sobre el valor que le otorgas a realizar las actividades que te ocupan, que te interesan y que te motivan a proyectarte, es decir, a visualizar tu crecimiento profesional y tu desarrollo personal en el futuro.
            </p>
            <p>
              Por ejemplo, revalorar de vez en cuando tu trabajo, y todo lo que hay alrededor de él, te puede sacar de las llamadas “zonas de confort” en las que comúnmente se deterioran nuestras capacidades de adaptación, de reacción y de cambio.
            </p>
            <p>
              ¿Has oído hablar de la parábola de la rana hervida? Si pones a una rana en una olla con agua caliente buscará saltar y escaparse, mientras que si la pones en una olla con agua a temperatura ambiente, sin asustarla, se quedará tranquila. Si aumentas poco a poco la intensidad del fuego bajo la olla, la rana quedará cada vez más aturdida y finalmente no estará en condiciones de salir de la olla. Los seres humanos reaccionamos igual que la rana. Saltamos si el cambio es repentino y nos adaptamos si el cambio es paulatino.
            </p>
            <p>
              El proceso de visualizarte en lo que te gustaría hacer y buscar ese empleo o actividad en particular, funciona igual. Por naturaleza buscamos los senderos ya recorridos y marcados; es más cómodo que abrir nuevas brechas, pero cuidado, depende para qué: pasar por un sendero trazado quizá te permita circular más rápido y seguro, mientras que abrirte paso a campo traviesa quizá te sirva para descubrir nuevos enfoques y sobre todo oportunidades.
            </p>
            <p>
              La forma como te muevas depende de los objetivos que persigas. Tu preparación y convicción hacen la diferencia sobre lo que quieres y obtienes. A fin de cuentas, desarrolla una reputación laboral por la que siempre recibas ofertas de trabajo en vez de que seas tú quien las esté buscando.
            </p>
          </div>
          <img src='/static/img/projects/empleo2.jpg' />

        </div>
        <h2 style={{color}}>Los beneficios obtener el empleo que quiero:</h2>
        <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
          <div>
            <ul>
              <li>
                <strong style={{color}}>Crecimiento y desarrollo personal: </strong>
                <p>Por medio del aprendizaje constante desarrollas competencias y habilidades que pones en práctica para seguir obteniendo resultados.</p>
              </li>
              <li>
                <strong style={{color}}>Mejorar tu calidad de vida: </strong>
                <p>Obtener el empleo que quieres responde a tus necesidades profesionales, económicas y vivenciales, entre otras. Por ejemplo, tu salario te permite obtener cierto poder adquisitivo. Lo ideal es aspirar  a un sueldo que no solo te permita cubrir tus gastos regulares, sino que te otorgue la posibilidad de ahorrar e invertir para emprender y crecer tus proyectos.</p>
              </li>
              <li>
                <strong style={{color}}>Oportunidades de crecimiento profesional: </strong>
                <p>En la medida en que adquieres experiencia, se incrementan tus oportunidades de realizar actividades más interesantes y mejor remuneradas.</p>
              </li>
              <li>
                <strong style={{color}}>Relaciones interpersonales: </strong>
                <p>Aprovecha el gran nivel de interacción que por lo general ofrece el ámbito laboral. Fortalece vínculos con las personas que en el futuro podrían abrirte puertas hacia oportunidades que buscas en lo profesional.</p>
              </li>
              <li>
                <strong style={{color}}>Satisfacción y confianza: </strong>
                <p>Es producto de tu decisión. Quizá te resulte fácil porque las necesidades del puesto y tus habilidades sean congruentes, o quizá sea difícil por un sinfín de razones, lo importante es que asimiles lo que hayas hecho para adaptarte al reto y superarlo.</p>
              </li>
            </ul>
          </div>
          <img src='/static/img/projects/empleo3.jpg' />
        </div>
        <h2 style={{color}}>Las implicaciones de obtener el empleo que quiero: </h2>
        <div className={style.textContainer}>
          <div>
            <ul>
              <li>
                <strong style={{color}}>Definición de tus objetivos: </strong>
                <p>Siempre busca dedicarte a lo que te apasiona. Lo importante es que te visualices en lo que te quieras desempeñar. Ello te dará la pauta de lo que necesitas para ocupar ese puesto. De igual forma, analiza a detalle todas las implicaciones del puesto que te resulta atractivo: actividad, horarios, cargas de trabajo, nivel de responsabilidad, prestaciones, y valora si realmente es lo que deseas. Por ejemplo, hay trabajos que en apariencia son muy divertidos, atractivos o sumamente bien remunerados, pero quizá sean más demandantes o complicados, por eso asegúrate de tener el nivel de compromiso requerido.</p>
              </li>
              <li>
                <strong style={{color}}>Capacitación y actualización: </strong>
                  <p>Un aspecto determinante es que elijas tu vocación: un oficio, una carrera técnica o una a nivel profesional, porque de ello depende qué tipo de perfil laboral tendrás y a qué tipo de trabajo podrás aspirar.
                  Una vez definidos tus objetivos y tu vocación, te encuentras en un nivel de “tronco común”, es decir, conocimientos generales enfocados a un área determinada. Aspirar a ser el mejor siempre te demandará dedicar tiempo y recursos a especializarte. Es importante que sepas dónde las estarás obteniendo la capacitación y actualización adecuadas.
                  Una herramienta útil para desempeñarte mejor contigo mismo y con los demás es que cuentes, o por lo menos comprendas, lo que son las habilidades gerenciales: liderazgo, trabajo en equipo, orientación a resultados, organización, comunicación efectiva, entre otras. Hoy día existe mucha información que te puede ayudar a estudiarlas.</p>
              </li>
              <li>
                <strong style={{color}}>Preparativos: </strong>
                <p>
                  Es fundamental que te prepares para la solicitud de empleo. Considera que el proceso de reclutamiento puede incluir varias fases: entrevista de trabajo (capacidades profesionales como por ejemplo conocimiento del puesto y habilidades gerenciales), pruebas psicométricas (evaluación de tu perfil psicológico), estudio socioeconómico (evaluación de tu entorno familiar, social y económico), entre otros.
                </p>
                <p>
                  Tip: Sé tú mismo, honesto y seguro. Sé ambicioso pero, cuidado, querer “comerse el mundo a rebanadas” puede ser un arma de doble filo. Todo lleva un proceso de crecimiento. De igual forma, valora tu trabajo, no lo malvendas, establece por convicción la remuneración que te parece justa por lo que tú ofreces y, en la medida de las posibilidades, asegúrate de que se cumpla.
                </p>
                <ul>
                  <li>
                    <h3 style={{color}}>Cómo redactar tu currículum vitae (CV)</h3>
                    <p>
                      Investiga los rubros más estratégicos a incluir en tu CV de acuerdo al puesto que pretendes. Considera los siguientes aspectos mínimos:
                    </p>
                    <ul>
                      <li><p>Sé conciso y directo.</p></li>
                      <li><p>Incluye una descripción breve de tu perfil profesional en tres o cuatro líneas.</p></li>
                      <li><p>Cita únicamente tu último grado de estudios.</p></li>
                      <li><p>Menciona si hablas otros idiomas pero no con porcentajes.</p></li>
                      <li><p>Señala los cursos que respaldan tu especialización/actualización.</p></li>
                      <li><p>Haz énfasis en logros en vez de responsabilidades.</p></li>
                      <li><p>Utiliza una sola tipografía y colores atractivos.</p></li>
                      <li><p>Omite por completo errores ortográficos.</p></li>
                    </ul>
                  </li>
                  <li>
                    <h3 style={{color}}>Investigación del puesto</h3>
                    <p>
                      Debes tener un conocimiento previo suficiente del empleo, empresa o industria a la que aspiras para estar en mejores posibilidades de competir.
                    </p>
                    <p>
                    Mientras más sepas del puesto que pretendes, mayores posibilidades tendrás de destacar aportando ideas desde un principio. Esto será muy valorado en la entrevista.
                    </p>
                    <p>A partir de la investigación que hagas del puesto, podrás saber si estás al nivel de lo requerido o en determinado momento sub o sobre-calificado para esa posición.</p>
                  </li>
                  <li>
                    <h3 style={{color}}>Algunas preguntas y respuestas clave durante la entrevista:</h3>
                    <ul>
                      <li><p>A qué aspiras en el puesto</p></li>
                      <li><p>Qué le puedes aportar tú a la empresa</p></li>
                      <li><p>Por qué dejaste el trabajo anterior o por qué quieres cambiar de trabajo</p></li>
                      <li><p>Cuáles son tus fortalezas y debilidades</p></li>
                    </ul>
                  </li>
                  <li>
                    <h3 style={{color}}>Tips:</h3>
                    <ul>
                      <li><p>No temas preguntar cualquier cosa que necesites para tú también evaluar lo que implica el trabajo. De hecho, preguntas inteligentes le darán un mayor valor a tu entrevista.</p></li>
                      <li><p>Busca dejar una buena impresión de ti tanto en el aspecto físico como de actitud: sé puntual, serio y respetuoso.</p></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <strong style={{color}}>Condiciones fiscales y administrativas de contratación (empleado de confianza/nómina, honorarios, free lance) </strong>
                <p>
                  Infórmate bien, sobre todo de las responsabilidades y obligaciones fiscales que adquieres al ser contratado. Busca que tu empleador te contrate en un esquema formal y con ello te permita acceder a todas las prestaciones de Ley.
                </p>
                <table>
                  <caption><p><strong>PRESTACIONES DE LEY - IMSS</strong></p></caption>
                  <thead>
                    <tr>
                      <th>CONCEPTO</th>
                      <th>FUNDAMENTO</th>
                      <th>DEFINICIÓN</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Salario base de cotización</td>
                      <td>Art. 27 de la LSS y 84 de la Ley Federal del Trabajo (LFT)</td>
                      <td>
                        Se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, alimentación, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador por su trabajo.
                      </td>
                    </tr>
                    <tr>
                      <td>Horas  extra de trabajo</td>
                      <td>Art. 67 LFT</td>
                      <td>
                        Se pagaran al 100% más del salario que corresponda a las horas de la jornada.
                      </td>
                    </tr>
                    <tr>
                      <td>Días de descanso</td>
                      <td>Art. 71 LFT</td>
                      <td>
                        Los trabajadores que presten servicio en día domingo tendrán derecho a una prima adicional de un veinticinco por ciento, por lo menos, sobre el salario de los días ordinarios de trabajo.
                      </td>
                    </tr>
                    <tr>
                      <td>Vacaciones</td>
                      <td>Art. 76 LFT</td>
                      <td>
                        Por cada año laborado se otorgarán no menos de 6 días de descanso remunerado y aumentará en dos días laborables, hasta llegar a doce, por cada año subsecuente de servicios.
                        Después del cuarto año, el período de vacaciones aumentará en dos días por cada cinco años de servicios.
                      </td>
                    </tr>
                    <tr>
                      <td>Prima vacacional</td>
                      <td>Art. 80 LFT</td>
                      <td>
                        Tendrán derecho a una prima no menor de veinticinco por ciento sobre los salarios que les correspondan durante el período de vacaciones.
                      </td>
                    </tr>
                    <tr>
                      <td>Aguinaldo</td>
                      <td>Art. 87. LFT</td>
                      <td>
                        Tendrán derecho a un aguinaldo anual que deberá pagarse antes del día veinte de diciembre, equivalente a quince días de salario, por lo menos. O la parte proporcional que se haya laborado, en su caso.
                      </td>
                    </tr>
                    <tr>
                      <td>Utilidades de la Empresa</td>
                      <td>Art. 117 y 122 LFT</td>
                      <td>
                        Los trabajadores participarán en las utilidades de las empresas, de conformidad con el porcentaje que determine la Comisión Nacional para la Participación de los Trabajadores en las Utilidades de las Empresas.

                        El reparto de utilidades entre los trabajadores deberá efectuarse dentro de los sesenta días siguientes a la fecha en que deba pagarse el impuesto anual.
                      </td>
                    </tr>
                    <tr>
                      <td>Prima de antigüedad</td>
                      <td>Art. 162 LFT</td>
                      <td>
                        Consistirá en el importe de doce días de salario, por cada año de servicio.
                      </td>
                    </tr>
                    <tr>
                      <td>Indemnización para riesgos de trabajo.</td>
                      <td>Art. 484 LFT</td>
                      <td>
                        Se tomará como base el salario diario que perciba el trabajador al ocurrir el riesgo y los aumentos posteriores que correspondan al empleo que desempeñaba, hasta que se determine el grado de la incapacidad, el de la fecha en que se produzca la muerte o el que percibía al momento de su separación de la empresa.
                      </td>
                    </tr>
                    <tr>
                      <td>AFORE</td>
                      <td>Art. 168 de la Ley del Seguro Social (LSS)</td>
                      <td>
                        Retiro 2% <br />
                        Cesantía y Vejez: 4.5% <br />
                        Vivienda: 5% <br />
                        Cuota Social: Aportación mensual para trabajadores que ganen hasta 15 veces el salario mínimo. <br />
                        Porcentajes con respecto al salario Base de Cotización. <br />
                      </td>
                    </tr>
                    <tr>
                      <td>Indemnización</td>
                      <td>Art. 50 LFT</td>
                      <td>
                        I. Si la relación de trabajo fuese por tiempo menor a un año, el pago será de la mitad del tiempo de servicios prestados; si excediera de un año, se pagará el importe de los salarios de seis meses por el primer año y de veinte días por cada uno de los años siguientes en los que hubiera prestado sus servicios;
                        <br /> II. Si la relación de trabajo fuese por tiempo indeterminado, la indemnización consistirá en veinte días de salario por cada uno de los años de servicios prestados; y
                        <br /> III. Además de las indemnizaciones a que se refieren las fracciones anteriores, se pagará el importe de tres meses de salario y los salarios vencidos e intereses, en caso, de ser determinado por la Junta de Conciliación y Arbitraje.
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table>
                  <caption><p><strong>PRESTACIONES DE LEY - ISSSTE</strong></p></caption>
                  <thead>
                    <tr>
                      <th>CONCEPTO</th>
                      <th>FUNDAMENTO</th>
                      <th>DEFINICIÓN</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Sueldo Básico</td>
                      <td>
                        Art. 32 de la Ley Federal de los Trabajadores al Servicio del Estado, Reglamentaria del apartado b) del artículo 123 constitucional. (LFTSE)
                        <br />  Art. 17 de la Ley del ISSSTE
                      </td>
                      <td>
                        El sueldo o salario que se asigna en los tabuladores regionales para cada puesto, constituye el sueldo total que debe pagarse al trabajador a cambio de los servicios prestados, sin perjuicio de otras prestaciones ya establecidas.
                        <br />El Sueldo Básico que se tomará en cuenta será el sueldo del tabulador regional que para cada puesto se haya señalado.
                      </td>
                    </tr>
                    <tr>
                      <td>Horas extra de trabajo</td>
                      <td>
                        Art. 39 LFTSE
                      </td>
                      <td>
                        Las horas extraordinarias de trabajo se pagarán con un ciento por ciento más del salario asignado a las horas de jornada ordinaria.
                      </td>
                    </tr>
                    <tr>
                      <td>Días de descanso</td>
                      <td>
                        Art. 40 LFTSE
                      </td>
                      <td>
                        En los días de descanso obligatorio y en las vacaciones, los trabajadores recibirán salario íntegro; cuando el salario se pague por unidad de obra, se promediará el salario del último mes.
                        <br /> Los trabajadores que presten sus servicios durante el día domingo, tendrán derecho a un pago adicional de un veinticinco por ciento sobre el monto de su sueldo o salario de los días ordinarios de trabajo.
                      </td>
                    </tr>
                    <tr>
                      <td>Vacaciones</td>
                      <td>
                        Art. 30 LFTSE
                      </td>
                      <td>
                        Los trabajadores que tengan más de seis meses consecutivos de servicios, disfrutarán de dos períodos anuales de vacaciones, de diez días laborables cada uno, en las fechas que se señalen al efecto.
                        Cuando un trabajador no pudiere hacer uso de las vacaciones en los períodos señalados, por necesidades del servicio, disfrutará de ellas durante los diez días siguientes a la fecha en que haya desaparecido la causa que impidiere el disfrute de ese descanso, pero en ningún caso los trabajadores que laboren en períodos de vacaciones tendrán derecho a doble pago de sueldo.
                      </td>
                    </tr>
                    <tr>
                      <td>Prima vacacional</td>
                      <td>
                        Art. 40 LFTSE
                      </td>
                      <td>
                        Los trabajadores percibirán una prima adicional de un treinta por ciento, sobre el sueldo o salario que les corresponda durante dichos períodos.
                      </td>
                    </tr>
                    <tr>
                      <td>Aguinaldo</td>
                      <td>
                        Art. 42 bis LFTSE
                      </td>
                      <td>
                        Los trabajadores tendrán derecho a un aguinaldo anual que deberá pagarse en un 50% antes del 15 de diciembre y el otro 50% a más tardar el 15 de enero, y que será equivalente a 40 días del salario, cuando menos, sin deducción alguna
                      </td>
                    </tr>
                    <tr>
                      <td>Prima de antigüedad</td>
                      <td>
                        Art. 34 LFTSE
                      </td>
                      <td>
                        Por cada cinco años de servicios efectivos prestados hasta llegar a veinticinco, los trabajadores tendrán derecho al pago de una prima como complemento del salario.
                      </td>
                    </tr>
                    <tr>
                      <td>AFORE</td>
                      <td>
                        Art. 102 Ley del ISSSTE
                        <br /> Art. 194 Ley del ISSSTE
                        <br /> Art. 100 Ley del ISSSTE
                      </td>
                      <td>
                        <ul>
                          <li>
                            Cesantía y Vejez:
                            <ul>
                              <li>3.175% (Dependencias o Entidades)</li>
                              <li>6.125% (trabajador)</li>
                            </ul>
                          </li>
                          <li>Retiro 2% (Dependencias o Entidades)</li>
                          <li>Cuota Social 5.5% del Salario mínimo general (Gobierno Federal)</li>
                          <li>Vivienda: 5% (Dependencias o Entidades) </li>
                        </ul>
                        <p>(Porcentajes con respecto al sueldo básico)</p>
                        <p>Ahorro Solidario: Los trabajadores podrán optar por que se les descuente hasta el dos por ciento de su Sueldo Básico, para ser acreditado en la subcuenta de ahorro solidario que se abra al efecto en su cuenta individual (Cuenta AFORE). </p>
                        <p>
                          Las Dependencias y Entidades en la que presten sus servicios los trabajadores que opten por dicho descuento, estarán obligadas a depositar en la referida subcuenta, tres pesos con veinticinco centavos por cada peso que ahorren los trabajadores con un tope máximo del seis punto cinco por ciento del Sueldo Básico.
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p>
                  Seguridad, convicción y certeza son tus aliados. Asegúrate de incluirlos en la aventura de tu vida, porque recuerda que “son muchos los llamados, pero pocos los elegidos”.
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div className={style.links}>
          <h3 style={{color}}>Ligas de interés</h3>
          <ul>
            <li><a target='_blank' style={{color}} href='https://www.gob.mx/stps/acciones-y-programas/servicio-nacional-de-empleo-99031'>SNE - Servicio Nacional de Empleo</a></li>
            <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_tenerempleo.html'>CONDUSEF – Eventos en tu vida: Tener un empleo</a></li>
          </ul>
        </div>
    </div>
  )
}

Laboral.seo = {
  title: 'Obtener el empleo que quiero | CONSAR ',
  description: 'Es llegar a donde siempre quise, convencido de que es lo que merezco y por lo que he trabajado.',
  ogtitle: 'Obtener el empleo que quiero | CONSAR',
  ogdescription: 'Es llegar a donde siempre quise, convencido de que es lo que merezco y por lo que he trabajado.',
  keywords: [
    'consar',
    'Obtener el empleo que quiero'
  ]
}

export default Laboral
