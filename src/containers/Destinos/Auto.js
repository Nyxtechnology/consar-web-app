import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Auto = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/comprar un auto.png' />
        <h1 style={{color}}>Comprarme un auto </h1>
        <div className={style.textContainer}>
          <h4 style={{color}}>Pasos a seguir</h4>
          <div>
            <ol>
              <strong><li>
                <p>
                  Determina las características del coche que deseas comprar de acuerdo tus necesidades y posibilidades.
                </p>
              </li></strong>
            <strong><li>
                <p>
                  Decide si tu capacidad de pago te permite comprar tu auto con recursos propios o deberás contratar una fuente de financiamiento (crédito bancario o de alguna automotriz).
                </p>
              </li></strong>
            <strong><li>
                <p>
                  Analiza las opciones en el mercado en términos de marcas y precios. No olvides costos colaterales como tenencia, seguro, placas, verificación vehicular, cambio de propietario, licencia para conducir, tarjeta de circulación.
                </p>
              </li></strong>
            </ol>
            <img src='/static/img/projects/auto1.jpg' />

          <p>
            ¿Has pensado en un automóvil como una oportunidad? Tu coche puede ser también parte de tu patrimonio, llevarte a conocer nuevos caminos, a recorrer distancias más largas e incluso puedes usarlo como instrumento de trabajo.
          </p>
          <p>
            Afortunadamente, comprar un auto podría resultar sencillo gracias a los distintos mecanismos de crédito y financiamiento existentes, pero es importante ser muy responsable y tomar en cuenta ciertos factores que te permitirán administrar los gastos derivados. Con ello evitarás que comprar tu auto se convierta en un problema.
          </p>
        </div>
        <img src='/static/img/projects/auto2.jpg' />

        </div>
        <h2 style={{color}}>Los beneficios de comprarme un auto</h2>
        <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
          <div>
            <ul>
              <li>
                <p>
                  <strong style={{color}}>Mayor autonomía: </strong>
                    Te permite movilidad… ¡siempre y cuando el tráfico te lo permita! Podrás transportarte sin horarios ni depender del transporte público.
                </p>
              </li>
              <li>
                <p>
                  <strong style={{color}}>Seguridad y comodidad: </strong>
                  Además de contar con un espacio cómodo y privado para transportarse, les brindará a ti y a los tuyos mayor protección.
                </p>
              </li>
              <li>
                <p>
                  <strong style={{color}}>Fuente de ingreso adicional: </strong>
                  Si requieres otro medio para generar ingresos, un automóvil siempre te brinda la posibilidad de transportar personas o mercancía para diversos tipos de negocio.
                </p>
              </li>
            </ul>
          </div>
          <img src='/static/img/projects/auto3.jpg' />
        </div>
        <h2 style={{color}}>Las implicaciones de comprarme un auto</h2>
        <div className={style.textContainer}>
          <div>
            <ul>
              <li>
                <strong style={{color}}>Marca y Modelo: </strong>
                <p>
                  Define para qué lo quieres e infórmate. Dependiendo de tus necesidades deberás realizar un análisis sobre las distintas marcas, modelos y opciones que existen en el mercado para que tengas toda la información disponible en cuanto a calidad, durabilidad y rendimiento del auto. Revisa, compara y resuelve todas tus dudas antes de comprar. Sé congruente con el tamaño de tus necesidades y lo que vas a comprar. Sé ambicioso, gustosamente, pero dimensiona lo que vas a invertir y lo que te costará más adelante.
                </p>
              </li>
              <li>
                <strong style={{color}}>¿Nuevo o Seminuevo? </strong>
                <table style={{color}}>
                  <thead>
                    <tr>
                      <th>Nuevo</th>
                      <th>Seminuevo</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th colSpan={2}>GARANTÍA</th>
                    </tr>
                    <tr>
                      <td>
                        Contará con garantías de agencia o marca que te permiten hacer frente a desperfectos. Si no sigues las recomendaciones del fabricante, es posible que el auto deje de estar cubierto por las garantías.
                      </td>
                      <td>
                        No las hay. Verifica con un mecánico que estés comprando un vehículo en buen estado.
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={2}>MANTENIMIENTO</th>
                    </tr>
                    <tr>
                      <td>
                        Solo será necesario realizar los servicios de acuerdo a lo establecido por el fabricante. Para conservar las garantías del fabricante, deberás respetar los servicios establecidos en el manual.
                      </td>
                      <td>
                        Una vez que las garantías han vencido, no te verás obligado a realizar el mantenimiento en la agencia, por lo que puedes acceder a servicios de otros talleres mecánicos, probablemente con mejores precios.
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={2}>DEPRECIACIÓN</th>
                    </tr>
                    <tr>
                      <td colSpan={2}>
                        Todos los autos están sujetos a un porcentaje de depreciación. Si decides venderlo, será a un precio menor del que originalmente lo adquiriste. ¿Sabías que cuando lo sacas a la calle un auto nuevo pierde, en ese momento, aproximadamente un 30% de su valor?
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={2}>AVANCES TECNOLÓGICOS</th>
                    </tr>
                    <tr>
                      <td colSpan={2}>
                        Dependiendo la versión y/ o antigüedad del vehículo que adquieras, éste podría tener los más recientes avances en materia de equipo de seguridad, consumo de combustible y confort.
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={2}>PRECIO Y COSTOS</th>
                    </tr>
                    <tr>
                      <td>
                        Siempre un auto de agencia será más caro que uno similar usado.
                      </td>
                      <td>
                        Para evaluar un auto usado en las mejores condiciones, lo importante es disponer del tiempo y buenos tips, como por ejemplo: que solo haya tenido un dueño, el menor kilometraje posible y poco desgaste en general.
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={2}>SEGUROS E IMPUESTOS</th>
                    </tr>
                    <tr>
                      <td colSpan={2}>
                        El único seguro obligatorio, a nivel nacional, es el de responsabilidad civil. Adicionalmente, es tu decisión contratar un seguro de cobertura amplia o limitada.
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Los costos de seguros e impuestos, como tenencia y/o refrendo, son más altos.
                      </td>
                      <td>
                        Por lo general los costos de seguros y tenencia son más bajos dependiendo de la depreciación y antigüedad del vehículo.
                      </td>
                    </tr>
                  </tbody>
                </table>
              </li>
              <li>
                <strong style={{color}}>Método de compra: </strong>
                <p>Hay tres principales modalidades para adquirir un auto:</p>
                <ul>
                  <li><p><strong style={{color}}>Ahorro. </strong> Pagar en una exhibición con dinero ahorrado. Esto te puede hacer acreedor a descuentos específicos.</p></li>
                  <li><p><strong style={{color}}>Financiamiento. </strong> Un crédito automotriz el cual te permite cubrir el costo del vehículo en parcialidades, de acuerdo a tus necesidades.</p></li>
                  <li><p><strong style={{color}}>O una combinación: </strong> Puedes pagar una determinada cantidad que supere el monto del enganche, lo cual será de gran ayuda ya que el crédito requerido resultará menor, así como los pagos parciales.</p></li>
                </ul>
              </li>
              <li>
                <strong style={{color}}>Precio: </strong>
                <p>
                  Se debe tomar en cuenta cuál es el precio del auto y la cantidad exacta a pagar en plazos, en caso de un crédito automotriz o autofinanciamiento, por lo que es necesario:
                </p>
                <ul>
                  <li><p>Conocer el monto del enganche.</p></li>
                  <li><p>Saber con exactitud el monto de los pagos, así como el plazo en el que se realizarán (generalmente son mensuales) y evaluar si tus ingresos te lo permiten sin desproteger tu presupuesto personal o familiar.</p></li>
                  <li><p>Tener en cuenta cuál es la tasa de interés del crédito. Ésta representa el costo que pagamos por contratar el crédito y pagar el auto a plazos. Al saber el porcentaje de interés que pagarás, puedes calcular de manera sencilla cuál es el monto final del vehículo en un determinado tiempo. Hay muchos planes a mensualidades sin intereses.</p></li>
                </ul>
                <p style={{color, textAlign: 'center'}}>
                  <strong>La fórmula es sencilla: </strong>
                  <br />
                  <br />
                  Monto de enganche + monto de pagos periódicos x número de plazos = costo final del auto
                  <br />
                </p>
              </li>
              <li>
                <strong style={{color}}>Trámites: </strong>
                <p>
                  Lo más importante es conocer el reglamento de tránsito y los documentos que debes portar para poder conducir.
                </p>
                <ul>
                  <strong style={{color}}>Si el auto es nuevo debes tener o tramitar lo siguiente:</strong>
                  <li><p>Licencia para conducir</p></li>
                  <li><p>Placas</p></li>
                  <li><p>Tarjeta de circulación</p></li>
                  <li><p>La primera verificación para obtener el engomado 00 (donde aplique)</p></li>
                  <li><p>Pago de tenencia</p></li>
                  <li><p>Contratación de un seguro de responsabilidad civil y otras coberturas</p></li>
                </ul>
                <ul>
                  <strong style={{color}}>Si el auto es usado o seminuevo debes tener o tramitar:</strong>
                  <li><p>Licencia para conducir</p></li>
                  <li><p>Cambio de propietario</p></li>
                  <li><p>Tarjeta de circulación con tus datos</p></li>
                  <li><p>Cambio de placas (si el auto tiene placas de otro estado)</p></li>
                  <li><p>Pago de tenencia/refrendo del vehículo</p></li>
                  <li><p>Verificación vehicular (donde aplique)</p></li>
                  <li><p>Contratación de un seguro de responsabilidad civil y otras coberturas</p></li>
                </ul>
              </li>
              <li>
                <strong style={{color}}>Gastos recurrentes:</strong>
                <ul>
                  <li>
                    <p>
                      <strong style={{color}}>Combustible: </strong> Para calcular el costo mensual del combustible, estima la cantidad con base en el rendimiento de combustible (km/l) del auto (puedes pedir este dato en la agencia), y suma las distancias que recorrerás de manera habitual.
                    </p>
                  </li>
                  <li>
                    <p>
                      <strong style={{color}}>Mantenimiento: </strong> Limpieza, revisión de rutina, cambio de aceite, afinación y verificaciones semestrales.
                    </p>
                  </li>
                  <li>
                    <p>
                      <strong style={{color}}>Otros: </strong> Estacionamientos, peajes, multas, etc.
                    </p>
                  </li>
                </ul>
              </li>
              <li>
                <strong style={{color}}>Contratación de seguro: </strong>
                <p>
                  Uno de los aspectos fundamentales a considerar cuando eres propietario de un coche es asegurar la cobertura de riesgo ante posibles accidentes. Esto se logra mediante la contratación de un seguro, el cual puede cubrir desde el aspecto económico ante daños a terceros, robo total o parcial del auto, daños y reparaciones, entre otros. El seguro no debe ser considerado como una obligación sino como un requerimiento para conducir. Inclusive, en algunos estados de la República es obligatorio contar por lo menos con un seguro de daños a terceros.
                </p>
                <ul>
                  <li>
                    <strong style={{color}}>¿Cómo funcionan?</strong>
                    <p>
                      El seguro de autos principalmente te ampara desde el punto de vista económico y financiero frente a distintos riesgos dependiendo de la cobertura que contrates.
                    </p>
                    <p>
                      Usualmente existe un monto conocido como deducible. Es un porcentaje sobre el valor comercial del auto. El deducible es tu participación en la pérdida ocasionada por el siniestro y tiene como finalidad que hagas todo lo que esté a tu alcance para evitar que acontezca el accidente.
                    </p>
                    <p>
                      El deducible es determinado con base en la propensión que cada cliente tiene a sufrir un accidente y el tipo de cobertura que se requiere. Por ejemplo, las personas más jóvenes pagan un monto mayor por ser más arriesgados.
                    </p>
                  </li>
                  <li>
                    <strong style={{color}}>Tipos de cobertura:</strong>
                    <p>
                      En algunos estados de la República, es obligatorio contar al menos con el tipo de cobertura más básica, de lo contrario te harás acreedor a una multa.
                    </p>
                    <ul>
                      <li>
                        <p><strong style={{color}}>Responsabilidad civil (RC): </strong> Esta cobertura es obligatoria ya que cubre daños causados a terceros en sus bienes, lesiones físicas o muerte, representación legal para el asegurado y la indemnización que se determine por dichas causas. En ciertos casos incluye gastos médicos de los ocupantes, asistencia vial y de viajes.
                        </p>
                      </li>
                      <li>
                        <p><strong style={{color}}>Limitada (RC y Robo total): </strong>Incluye la cobertura de responsabilidad civil más el robo total de tu vehículo. En algunos casos abarca protección legal, gastos médicos, asistencia vial y de viajes.
                        </p>
                      </li>
                      <li>
                        <p><strong style={{color}}>Amplia (RC, Robo total y Daños materiales): </strong>Es la cobertura más completa, ya que incluye la cobertura limitada y las posibles pérdidas que sufra tu auto a consecuencia de un siniestro o accidente. Además, se puede incluir: protección legal, gastos médicos de los ocupantes, asistencia vial y de viajes. En algunos casos las aseguradoras ofrecen un vehículo sustituto provisional mientras se repara tu auto.
                        </p>
                      </li>
                      <li>
                        <p><strong style={{color}}>Tip: </strong>
                          Existen distintas calculadoras que te permiten simular el costo de un seguro de auto, dependiendo de tus necesidades y presupuesto.
                        </p>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
            <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a style={{color}} target='_blank' href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_comprarauto.html'>CONDUSEF – Eventos en tu vida: Comprar un auto</a></li>
              <li><a style={{color}} target='_blank' href='https://phpapps.condusef.gob.mx/condusef_autoseguro/entradas.php'>CONDUSEF – Simulador de Seguro de Automóvil</a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Auto.seo = {
  title: 'Comprar un auto | CONSAR ',
  description: 'No hay nada como llevar las riendas de mi vida, aunque para fines prácticos, ¡mejor me pongo al volante!',
  ogtitle: 'Comprar un auto | CONSAR',
  ogdescription: 'No hay nada como llevar las riendas de mi vida, aunque para fines prácticos, ¡mejor me pongo al volante!',
  keywords: [
    'consar',
    'Comprar un auto'
  ]
}

export default Auto
