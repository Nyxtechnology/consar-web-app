import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Salud = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/protegerse de emergencias.png' />
        <h1 style={{color}}>Asegurar mi fondo para emergencias </h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Establece las situaciones de emergencia o imprevistos a las que puedes estar expuesto.</p></li></strong>

                <strong><li><p>Determina una meta de ahorro periódica y cúmplela cabalmente. Esto te permitirá tener un fondo del cual disponer ágilmente ante cualquier eventualidad. </p></li></strong>

                <strong><li><p>En la contratación de seguros para prevenir accidentes, enfermedades, fallecimiento o desastres naturales debes estar bien informado sobre las condiciones que ofrecen las muchas aseguradoras en el mercado. Hay simuladores que te pueden ayudar a valorar los costos de primas, deducibles y todas las características de los seguros.</p></li></strong>

                <strong><li><p>Designa a los beneficiarios de tus seguros y haz un testamento.</p></li></strong>

                <strong><li><p>Recuerda siempre tener a la mano un plan operativo para las emergencias: directorio de hospitales, bomberos, ambulancia, policía, botiquín de primeros auxilios, etc.</p></li></strong>

              </ol>
              <img src='/static/img/projects/emergencias1.jpg' />

              <p>Todos estamos expuestos a imprevistos. Curiosamente, también estamos expuestos y hasta predestinados a cierto número de “previstos”, pero que consideramos desagradables y que por lo mismo desatendemos, como riesgos específicos de trabajo o de nuestras actividades, propensión a determinados males y enfermedades, desastres naturales, o simplemente la muerte.</p>
              <p>Este proyecto de vida responde y atiende directamente al dicho “más vale prevenir que lamentar”, y es que prevenir oportunamente tiene la gran ventaja de minimizar tu vulnerabilidad, gastos excesivos y, quizás, sufrimiento. Analizar, planear y reunir lo necesario para que enfrentes las emergencias y los desafortunados acontecimientos de tu vida, puede hacerse poco a poco y con la ventaja de que obtengas tranquilidad y seguridad en el presente y futuro. </p>
              <p>Prever y hacerte de un fondo que te proteja de las emergencias implica que superes la sensación de perder tu dinero. Por ejemplo, pagar un seguro de auto desde hace cinco años y nunca haberlo necesitado, puede generarte cierta frustración y la sensación de estar tirando tu dinero, pero ¿qué tal cuando efectiva y lamentablemente te robaron el auto o tuviste un accidente de consideración y el seguro cumplió su cometido? O bien, ¿te ha pasado que lamentas no haber actuado con anticipación porque una emergencia te resultó más compleja, dolorosa y tres veces más cara? Recuerda que el “hubiera”, no existe.</p>
              <p>Probablemente, el punto de partida para incorporar y construir este proyecto en la aventura de tu vida, depende del grado de inseguridad que tengas sobre la protección de tus seres queridos, de ti mismo y de tus bienes. Hazlo a tu medida y establece tus prioridades para protegerte y fortalecerte.</p>
            </div>
            <img src='/static/img/projects/emergencias2.jpg' />

          </div>
          <h2 style={{color}}>Los beneficios de asegurar tu fondo para emergencias:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <p><strong style={{color}}>Disminuir la preocupación </strong>
                  Cuentas con respaldos que te dan tranquilidad. Estos recursos, a tu disposición en la medida de tu capacidad y de tus prioridades, pueden ser meramente económicos, ahorrando un fondo para enfrentar gastos por el monto que consideres prudente, o bien, pueden ser servicios que contrates en diversos rubros para obtener la atención y el respaldo integral de especialistas calificados.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Disminuir la vulnerabilidad financiera </strong>
                  Los recursos económicos que dispones para enfrentar las eventualidades representan un respaldo que puedes usar sin preocuparte de interrumpir, arriesgar o descuidar otros proyectos importantes. Tener tu fondo con recursos suficientes y/o servicios vigentes, te permite no verte en la necesidad de malvender urgentemente tus bienes, o empeñar o requerir préstamos en condiciones desfavorables.</p>
                </li>
                <li>
                  <p><strong style={{color}}>Estar informado </strong>
                  Prever un fondo para emergencias no solo tiene que ver con ahorrar o pagar a terceros para que se hagan cargo de tus eventos indeseables. También se trata de que obtengas conocimientos sobre ellos, del porqué suceden, cómo y cuándo, y sobre todo cómo reaccionar cuando acontecen. El beneficio de estar informado tiene que ver, primero, con incrementar tus posibilidades de evitar los riesgos y, segundo, no caer en fraudes con servicios o productos-fantasma, riesgosos y más caros por tratarse de urgencias, o caer en manos de oportunistas que se aprovechan de la desgracia y confusión ajena, entre otros.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/emergencias3.jpg' />

          </div>
          <h2 style={{color}}>Las implicaciones de asegurar tu fondo para emergencias: </h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Definir las emergencias y necesidades </strong>
                  <p>No existe un modelo estándar para planear y diseñar tu fondo para emergencias. Por lo mismo, lo primero es que hagas una lista personal/familiar de tus posibles contingencias y las priorices, es decir, cuáles serían los factores de riesgo que deseas cubrir, los más probables en suceder y, de igual forma, cuáles te implicarían consecuencias más graves en lo necesario para atenderlas o en lo económico.</p>
                  <p>He aquí algunos ejemplos:</p>
                  <ul>
                    <li><p>Atención médica (puede ser de gastos menores o mayores, según lo consideres más útil) </p></li>
                    <li><p>Enfermedad que implique una intervención/tratamiento urgente</p></li>
                    <li><p>Accidente (en el hogar u otro lugar que acostumbres, vehicular, de trabajo, etc.)</p></li>
                    <li><p>Desastre natural con afectaciones humanas y/o materiales</p></li>
                    <li><p>Fallecimiento tuyo o de un familiar</p></li>
                  </ul>
                  <p>Cada uno de estos temas puede tratarse con mayor o menor profundidad. Por ejemplo, accidentes hay muchos, supongamos un accidente físico, como romperse un brazo. Podrías tener cierta cantidad de dinero en efectivo para cubrir los costos correspondientes, pero ¿sería suficiente? ¿Tienes una idea aproximada de cuánto cuesta atender un accidente así, o cualquier otro? Dependiendo del accidente, ¿tienes claridad sobre a dónde acudir o a quién llamar? Y si tú fueras la víctima ¿saben tus familiares o amigos qué hacer? Por lo mismo, quizá te resultaría más sencillo contratar un seguro o un servicio que resolviera las muchas preguntas que surgen al momento de una emergencia.</p>
                  <p>No esperes a tener el problema encima y prevé qué harías si estuvieras en determinadas situaciones:</p>
                  <ul>
                    <li><p>A quién dar aviso: Ten a la mano los números de emergencia, familiares, doctores, clínica u hospital, aseguradora o proveedor correspondiente. Disponer de sus números telefónicos, datos de ubicación y contacto actualizados, referencias de pólizas, términos y condiciones, etc.</p></li>
                    <li><p>Transporte: Prevé qué necesitarías y cómo te desplazarías.</p></li>
                    <li><p>Información especial de salud: Tipo sanguíneo, alergias a medicamentos u otros, antecedentes de consideración, etc.</p></li>
                    <li><p>Recursos económicos: Garantizar el acceso a los recursos que necesites para la emergencia, ya sea en efectivo, con tarjetas o los documentos que avalen que estás al día para recibir servicios de atención y protección que hayas contratado, etc.</p></li>
                    <li><p>Acciones y consecuencias: Terceros a los que haya que informar sobre procesos colaterales. Por ejemplo, avisar en tu trabajo o a tus clientes, posponer asuntos diversos y dar aviso a quien corresponda, hacerse cargo de los pendientes de otros, etc.</p></li>
                    <li><p>No te confíes. Supongamos que te ocurrió un accidente y perdiste tu celular, ¿sabrías cómo comunicarte? Supongamos que una persona te presta su celular, ¿conoces los números telefónicos que necesitas, o solo estaban en la memoria de tu celular? Piénsalo bien para que por lo menos siempre tengas una salida al problema o un plan B.</p></li>
                  </ul>
                  <p>Detallar lo que implicaría enfrentar tus posibles emergencias, a su vez, te ayuda a dimensionar tu fondo para este tipo de imprevistos.</p>
                </li>
                <li>
                  <strong style={{color}}>Planeación financiera </strong>
                  <p>Debido a que se incurre en gastos, la protección contra emergencias requiere de una planeación minuciosa desde el punto de vista económico. Toma en cuenta cuál es la parte de tu ingreso que destinarás al pago de los distintos seguros adquiridos y/o el monto que vas a ahorrar para cubrir las situaciones de riesgo elegidas.</p>
                </li>
                <li>
                  <strong style={{color}}>Adquisición de seguros</strong>
                  <p>Adquirir un seguro es una de las maneras más eficientes para protegerse contra cierto tipo de emergencias. Un seguro o cobertura es un contrato entre dos partes; la primera es el cliente, quien se compromete a pagar una determinada cantidad de dinero que le permita liberarse de la mayor parte o del total del riesgo y los costos que representa verse involucrado en una situación, siniestro o evento específico. La segunda parte es la aseguradora que, al recibir el pago del cliente, se compromete a absorber el total, o lo pactado, sobre el riesgo y los costos que surgen de una situación, evento o siniestro en especial.</p>
                  <p>Entre los seguros más contratados se encuentran:</p>
                  <ul>
                  <li><p>Seguro de gastos médicos mayores. Es un seguro correctivo, generalmente funciona una vez que se verifica que el asegurado tiene un daño en su salud. Además, el asegurado decide en dónde desea ser atendido.</p></li>
                  <li><p>Seguro de gastos médicos menores. Es de carácter preventivo, pues tiene como principal propósito la conservación de la salud. Generalmente, las aseguradoras brindan opciones para elegir dónde ser atendido.</p></li>
                  <li><p>Seguro de vida. En éste se garantiza el pago de un determinado monto de dinero a los beneficiarios de la póliza, en caso de fallecimiento del asegurado.</p></li>
                  <li><p>Seguro de auto. Un seguro de auto cubre el riesgo derivado de la conducción de un automóvil en caso de verse involucrado en algún accidente.</p></li>
                  </ul>
                  <p>¿Qué debes tomar en cuenta antes y después de contratar un seguro?</p>
                  <ul>
                    <li><p>Determina cuál es la cobertura que necesitas. Por ejemplo, en un seguro de gastos médicos debes considerar cuál es la suma asegurada, el nivel hospitalario y la prima o deducible. De esta forma garantizas el que tú y tu familia reciban los mayores beneficios.</p> </li>
                    <li><p>Realiza un ejercicio comparativo sobre cada aseguradora, los beneficios que ofrece su póliza y los precios. Esto te ayudará a tomar una decisión informada sobre cuál es la aseguradora que más te conviene.</p></li>
                    <li><p>Una vez elegida la aseguradora y la cobertura que necesites vuelve a revisar aquellas situaciones sobre las cuales tu seguro es válido y cuáles no lo son. Por ejemplo, un seguro de auto, por lo general, no cubre accidentes donde se determine que el asegurado se encuentre bajo la influencia de ciertas sustancias tóxicas.</p></li>
                  </ul>
                </li>
                <br/>
                <li>
                  <strong style={{color}}>Beneficiarios</strong>
                  <p>En algunos seguros y otros productos financieros, es necesario que el cliente designe a uno o varios beneficiarios. El beneficiario es la persona o las personas que tendrán derecho a la suma asegurada o a recursos específicos, en caso de que el asegurado fallezca.</p>
                  <p>Entre los productos financieros que requieren asignar beneficiarios, se encuentran:</p>
                  <ul>
                    <li><p>Cuenta de cheques, débito</p></li>
                    <li><p>Cuenta de ahorros</p></li>
                    <li><p>Cuenta AFORE</p></li>
                    <li><p>Seguro de vida</p></li>
                    <li><p>Seguro de gastos médicos</p></li>
                    <li><p>Testamento</p></li>
                  </ul>
                  <p>Hay muchas maneras de determinar las cláusulas bajo las cuales se asigna a los beneficiarios y de qué forma. Para conocer más, acércate con las instituciones financieras a fin de que te brinden la información y los procedimientos para realizar la asignación de beneficiarios o el reclamo de beneficios.</p>
                </li>
              </ul>
            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_inesperados.html'>CONDUSEF – Eventos en tu vida: Eventos inesperados</a></li>
              <li><a target='_blank' style={{color}} href='http://www.condusef.gob.mx/comparativos/comparativos.php?idc=3&im=seguros.jpg&h=1'>CONDUSEF - Cuadros comparativos de aseguradoras, calculadoras y calificaciones entre aseguradoras</a></li>
              <li><a target='_blank' style={{color}} href='http://www.condusef.gob.mx/Revista/index.php/seguros'>CONDUSEF – Información sobre seguros</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvseguros.html'>CONDUSEF – Cuadernos y videos: Seguros</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvahorro.html'>CONDUSEF – Cuadernos y videos: Ahorro</a></li>
              <li><a target='_blank' style={{color}} href='
              http://www.bansefimas.mx/'>BANSEFI – Banco del Ahorro Nacional y Servicios Financieros. Paquete de beneficios Bansefi+</a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Salud.seo = {
  title: 'Asegurar mi fondo para emergencias | CONSAR ',
  description: '¿Qué atender primero: lo urgente o lo importante? En todos los casos, prever me salva del apuro.',
  ogtitle: 'Asegurar mi fondo para emergencias | CONSAR',
  ogdescription: '¿Qué atender primero: lo urgente o lo importante? En todos los casos, prever me salva del apuro.',
  keywords: [
    'consar',
    'Asegurar mi fondo para emergencias'
  ]
}

export default Salud
