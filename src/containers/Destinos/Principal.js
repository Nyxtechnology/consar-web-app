import React from 'react'
import style from './style.css'
import Layout from '../Layout/'
import options from './options'
import {Link} from 'react-router-dom'

export default class Principal extends React.Component {
  render = () => {
    const url = this.props.location.pathname.match(/jovenes|adultos|mayores/)
    const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
    const color = ['#925152', '#002453', '#47774C'][section]

    return (
      <Layout {...this.props}>
        <div className={style.destinos}>
          <div className={style.header}
            style={{color, backgroundImage: `url(/static/img/projects/banner.jpg)`}}>
            <div>
              <div style={{borderColor: color}}>
                <h1>Catálogo de proyectos</h1>
                <h2>Visualiza a detalle los grandes proyectos de tu vida y lo que necesitas para alcanzarlos.</h2>
              </div>
            </div>
          </div>
          <div className={style.cards}>
            {Object.values(options).map((o, i) => (
              <Link style={{color}} to={`/${url[0]}/proyectos/${o.name}`}>
                <div>
                  <img src={`/static/img/itinerario/${o.name}.svg`} />
                  <h3>{o.title}</h3>
                  <p>{o.descption}</p>
                </div>
              </Link>
            ))}
          </div>
          <img src={`/static/img/referencias/footer${section}.svg`} />
        </div>
      </Layout>
    )
  }
}
