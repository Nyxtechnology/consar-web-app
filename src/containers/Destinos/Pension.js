import React from 'react'
import Layout from '../Layout/'
import style from './style.css'

const Pension = props => {
  const url = props.location.pathname.match(/jovenes|adultos|mayores/)
  const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <Layout {...props}>
      <div className={style.item}>
        <img src='/static/img/projects/banners/pensionarse como lo deseas.png' />
        <h1 style={{color}}>Preparar mi jubilación</h1>
          <div className={style.textContainer}>
            <h4 style={{color}}>Pasos a seguir</h4>
            <div>
              <ol>
                <strong><li><p>Diseña un plan financiero para tu vida laboral, del que se desprenderá tu plan de retiro.</p></li></strong>
                <strong><li><p>Establece los objetivos y metas que desees conseguir al momento de jubilarte. Recuerda que deben ser específicas, medibles, alcanzables, realistas y delimitadas por tiempo.</p></li></strong>
                <strong><li><p>Toma las mejores decisiones a lo largo de tu vida laboral para que tu ahorro en la cuenta AFORE obtenga la mayor rentabilidad. Consulta las secciones en el sitio web de la CONSAR:
                  <ul>
                    <li><p>El manejo básico de la cuenta AFORE</p></li>
                    <li><p>Cómo elegir la mejor AFORE</p></li>
                    <li><p>Ahorro Voluntario</p></li>
                    <li><p>Calculadoras de ahorro y retiro</p></li>
                    <li><p>Todo sobre el retiro de recursos del SAR</p></li>
                  </ul>
                </p></li></strong>
                <strong><li><p>Prevé las fuentes de ingreso que tendrás en la etapa de retiro:
                  <ul>
                    <li><p>Ahorro personal, en cuentas bancarias, en efectivo y/o en tu Cuenta AFORE</p></li>
                    <li><p>Inversiones</p></li>
                    <li><p>Bienes raíces</p></li>
                    <li><p>Bienes propios</p></li>
                    <li><p>Seguir trabajando</p></li>
                    <li><p>Una combinación de los anteriores</p></li>
                  </ul>
                </p></li></strong>
              </ol>
              <img src='/static/img/projects/jubilacion1.jpg' />

              <p>
                Antes que dar por hecho que todos queremos retirarnos algún día de la vida laboral, ¿qué representa para ti la idea de “retirarte”? ¿Qué piensas de la posibilidad de llegar a una edad en la que decidas dejar de trabajar, desconectarte de la necesidad de obtener ingresos a cambio de tu trabajo diario, y pasar a otra dinámica de ocupación durante varios años más?
              </p>
              <p>
                Hoy, probablemente el día a día te funciona porque obtienes determinados ingresos para vivir, pero cuando ya no quieras o no puedas trabajar, ¿cómo imaginas que obtendrás los recursos para llevar tu vida? ¿De dónde saldrá el dinero para tus gastos diarios o semanales, para tus gustos y actividades?
              </p>
              <p>
                Si logras visualizarte en ese momento, en tu futuro, con toda la intención de verte bien, sin temores y viviendo lo que te gusta, donde y con quien quieras estar, entonces ya posees una capacidad fundamental y necesaria para fijarte objetivos hacia la jubilación que deseas. El principio básico es que verdaderamente creas en gozar tu jubilación.
              </p>
              <p>
                Y no hay truco ni magia: El nivel de estabilidad financiera que vivirás en esa etapa de tu vida depende de lo que hayas previsto y hecho para ello, desde ahora. Debes saber que preparar tu futuro de ninguna manera exige que desatiendas tu presente. El objetivo de este proyecto es que los desarrolles simultáneamente.
              </p>
            </div>
            <img src='/static/img/projects/jubilacion2.jpg' />

          </div>
          <h2 style={{color}}>Los beneficios de preparar tu jubilación:</h2>
          <div className={style.textContainer} style={{flexDirection: 'row-reverse'}}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Tranquilidad y bienestar </strong>
                  <p>Sin duda alguna, el mayor beneficio de preparar tu jubilación está en adquirir tranquilidad a medida que veas que tus planes toman forma, que avanzan y se van haciendo realidad. Cada paso hacia el objetivo de poder vivir tu jubilación en buenas condiciones es motivo de valiosa y merecida satisfacción. En cierta forma, saber que lo estás haciendo y logrando es ya una forma de predisponerte y anticiparte al gozo que vivirás.</p>
                </li>
                <li>
                  <strong style={{color}}>Reinventar tu estilo de vida </strong>
                    <p>Habrá para quienes la etapa de la jubilación sea para descansar, para por fin cerrarles el paso al estrés, a la prisa y a lo urgente, para bajar el ritmo y atender a los pequeños detalles, gestos y cariños del diario; para darse uno mismo el tiempo de lo que quiera.
                    Habrá para quienes la etapa de la jubilación de ninguna manera implique bajar la guardia y el empuje, y más bien sea motivo para liberar un acumulado de pendientes y proyectos.
                    Y como en todo, habrá para quienes la etapa de la jubilación simple y sencillamente venga sobre la marcha, para hacer y resolver al momento lo que se vaya dando y como se pueda.
                    Sea como sea, disfruta que seas tú el que decide el grado de planeación y/o de improvisación que le quieras imprimir a esta etapa de tu vida.</p>
                </li>
                <li>
                  <strong style={{color}}>Abrir y cerrar ciclos: </strong>
                  <p>El retiro representa un parteaguas, con la posibilidad de preguntarte qué has hecho y qué tienes pendiente. Planear tu jubilación es un ejercicio que te ayuda a prever los proyectos que desearías en tu vejez, pero al mismo tiempo te hace evaluar los que tienes encaminados actualmente. Aprovecha este proceso para revisar de vez en cuando qué ciclos habrás de cerrar antes de retirarte y cuáles otros mantendrás abiertos. Cerrar ciclos te puede dar tranquilidad y el espacio necesario para emprender nuevos retos, independientemente de tu edad.</p>
                </li>
              </ul>
            </div>
            <img src='/static/img/projects/jubilacion3.jpg' />

          </div>
          <h2 style={{color}}>Las implicaciones de preparar tu jubilación: </h2>
          <div className={style.textContainer}>
            <div>
              <ul>
                <li>
                  <strong style={{color}}>Visualizarse en la jubilación: el plan de retiro</strong>
                  <p>
                    Pensar en quién serás en esa etapa de tu vida es un ejercicio complicado de imaginación y, de alguna manera, una combinación entre lo que no deseas que te suceda, lo que te sabes capaz de alcanzar y los sueños e ilusiones a los que aspiras. Como sea, resulta importante mantener los pies sobre la tierra, es decir, procurar que seas realista sobre lo que podrías vivir en el futuro. Con dicho ejercicio deberías poder contestar preguntas que parecerían elementales como: ¿Dónde viviré? ¿Con quién o quiénes? ¿Cuáles serán mis actividades favoritas? Entre varias otras dudas que pueden ser más o menos precisas y detalladas, según sea tu visión del futuro.
                  </p>
                  <p>
                    Poder contestar a estas preguntas, con respuestas viables o posibles de realizar, es el primer paso para que prepares los objetivos y las metas hacia tu jubilación.
                  </p>
                  <p>
                    Por otra parte, preparar la jubilación tiene mucho que ver con alcanzar cierta estabilidad financiera desde antes del retiro. Básicamente, el concepto de estabilidad financiera se refiere a cuando organizas y dispones de tus recursos de manera eficaz y constante, permitiéndote cubrir tus necesidades, contar con un margen para atender y resistir imprevistos, e invertir para incrementar tu patrimonio.
                  </p>
                  <p>
                    Hablar de estabilidad financiera para el futuro significa perfeccionar tu propia destreza y tu buen desempeño financiero. Para ello, podrías diseñar y ejecutar un plan financiero a lo largo de tu vida laboral, del que se desprenderá tu plan de retiro.
                  </p>
                  <p>
                    Para crear dicho plan, primero debes establecer ciertos objetivos y metas que te representen algo que vale la pena conseguir al momento de jubilarte. De lo contrario, cualquier ahorro que no hayas “etiquetado” para algo concreto que te motive será únicamente una acumulación de recursos, mayormente expuestos a que los disperses o gastes.
                  </p>
                  <p>
                    Recuerda que los objetivos pueden ser el fin último y más general, como ir a vivir tu jubilación en otra región, mientras que las metas pueden ser más específicas y medibles, como investigar y determinar la vivienda que te gustaría habitar en esa región, determinar cuántos y cuáles recursos necesitarás para adquirirla, y cada una de las metas planteada en un calendario (cronograma) con su fecha de culminación.
                  </p>
                  <p>
                    Tanto los objetivos como las metas deben cumplir con las siguientes características:
                  </p>
                  <ul>
                    <li>
                       <p>Específico - Debe ser lo más concreto posible, identificando claramente qué es lo que deseas lograr, descartando supuestos, entredichos, ambigüedades, imprecisiones, etc.</p>
                    </li>
                    <li>
                      <p>Medible – Es fundamental para que determines tu progreso y, en caso dado, implementes acciones correctivas. Debes medir con indicadores concretos y claramente cuantificables.</p>
                    </li>
                    <li>
                      <p>Alcanzable - Implica conocer objetivamente tu potencial de realización, es decir, tu situación personal, tus habilidades y capacidades, el contexto que influye o impacta al proyecto, qué tanto puedes controlar todas esas variables de manera a que, a pesar de ser ambicioso, el conjunto se encuentre dentro de tus posibilidades de logro.</p>
                    </li>
                    <li>
                      <p>Realista o relevante - El objetivo debe representar un alto nivel de importancia para ti, de manera que ante cualquier obstáculo o contratiempo, no pierdas la motivación para lograr tu objetivo. La convicción y seguridad que tengas en la culminación de tu proyecto marca la diferencia entre que te sucedan problemas o que te enfrentes a nuevos retos. Podrías tropezar dos o tres veces y desistir, o bien ver en cada tropiezo la imperiosa necesidad de “darle la vuelta”, de ser más creativo y encontrar opciones para resolver.</p>
                    </li>
                    <li>
                      <p>Delimitado por el tiempo - Establece fechas y plazos específicos en los que cumplirás tus metas, son un indicador fundamental para medir el avance, y en la medida en que cumplas en los tiempos programados, funciona como un excelente proceso de motivación.</p>
                    </li>
                  </ul>
                  <p>
                    Para darle forma a tu plan de retiro, primero piensa qué es lo que más importa, lo que más te interesa y te haría feliz durante tu jubilación. Para no perder el hilo de tus ideas realiza lo siguiente:
                  </p>
                  <ul>
                    <li>
                      <p>Registra: Escribe cada una de tus objetivos y metas, así como las acciones que deberás tomar para conseguirlos, lo cual te permitirá diferenciar entre necesidades y deseos.</p>
                    </li>
                    <li>
                      <p>Cuantifica: Para cada meta, investiga y realiza el cálculo de cuánto dinero o qué recursos necesitarás para alcanzarla.</p>
                    </li>
                    <li>
                      <p>Prioriza: Ordena las metas de acuerdo a su importancia y al tiempo que requerirás para alcanzar cada una.</p>
                    </li>
                  </ul>
                  <p>
                    Una vez que sepas cuáles son las metas que deseas conseguir, podrás compararlas para identificar cuáles podrían ir de la mano, cuáles son independientes y cuáles compiten entre sí o se contraponen. Esto te permitirá reevaluar su importancia y priorizarlas mejor, ver cuántas y cuáles son alcanzables en las fechas que planees y con los recursos que dispongas.
                  </p>
                  <p>
                    Al finalizar este ejercicio, tendrás una idea mucho más clara y visual acerca de lo que deseas conseguir, y de lo que deberías hacer para obtenerlo.
                  </p>
                </li>
                <li>
                  <strong style={{color}}>Elegir la edad de retiro </strong>
                  <p>
                    La normatividad del Sistema de Ahorro para el Retiro, en México, establece que la edad de retiro es a los 65 años, aunque también puedes retirarte a partir de los 60 años con ciertas condiciones y restricciones. Si deseas conocer los detalles, este es un tema bastante amplio, así que te sugerimos de la página de la Comisión Nacional del Sistema de Ahorro para el Retiro (CONSAR), en la siguiente liga:
                    <a target='_blank' href='https://www.gob.mx/consar/articulos/todo-sobre-el-retiro-100587?idiom=es'>
                       https://www.gob.mx/consar/articulos/todo-sobre-el-retiro-100587?idiom=es
                    </a>
                  </p>
                  <p>
                    Hay dos aspectos relevantes que debes considerar sobre la edad en la que decidas retirarte, hablando en este caso específicamente del retiro de los recursos que tengas en tu Cuenta AFORE.
                  </p>
                  <p>
                    Por un lado, entre mayor sea la cantidad de años que trabajes cotizando al IMSS y/o ISSSTE, mayor será tu aportación por Ley a tu Cuenta AFORE. A esto se le llama densidad de cotización. Por ejemplo, una persona que trabajó la mayor parte de su vida cotizando y aportando a su Cuenta AFORE tendrá un mayor número de aportaciones que una persona que trabajó intermitentemente o solo unos cuantos años en el sector formal, suponiendo que ambos tuvieran el mismo sueldo. Para mayor información sobre la densidad de cotización, lee la nota: “La densidad de cotización: elemento fundamental de las pensiones”:
                    <a target='_blank' href='https://www.gob.mx/consar/articulos/la-densidad-de-cotizacion-elemento-fundamental-de-las-pensiones?idiom=es'>
                      https://www.gob.mx/consar/articulos/la-densidad-de-cotizacion-elemento-fundamental-de-las-pensiones?idiom=es
                    </a>
                  </p>
                  <p>
                    Por otro lado, la esperanza de vida va en aumento, es decir que viviremos más años, y eso a su vez significa que nuestra jubilación podría no ser de 10 o 15 años, sino de 20 o más. Si una persona se retira a los 65 años y vive hasta los 90, estará dedicando aproximadamente una cuarta parte de su vida a la etapa de la jubilación. Para mayor información sobre este tema, desde la perspectiva del envejecimiento poblacional y el impacto pensionario, lee la nota: “Envejecimiento: el ‘tsunami’ demográfico que se avecina”:
                    <a target='_blank' href='https://www.gob.mx/consar/articulos/envejecimiento-el-tsunami-demografico-que-se-avecina'>https://www.gob.mx/consar/articulos/envejecimiento-el-tsunami-demografico-que-se-avecina
                    </a>
                  </p>
                  <p>
                    Para elegir la edad de tu retiro, considera:
                  </p>
                  <ul>
                    <li>
                      <p>Tu salud y el historial de salud familiar. Eres el que mejor conoce tu historial de salud, tus puntos fuertes pero sobre todo tus puntos débiles, que son los que te conviene prevenir. Contar con antecedentes familiares claros y fidedignos (no siempre es fácil) es muy útil, pues sabrás a qué tipo de enfermedades o padecimientos eres susceptible. No por nada los estudios clínicos te preguntan si sabes de casos de cáncer, diabetes o problemas cardiacos en tus familiares cercanos.</p>
                    </li>
                    <li>
                      <p>Derivado del punto anterior, una estimación sobre la edad total que podrías alcanzar.</p>
                    </li>
                    <li>
                      <p>Tu opinión sobre el trabajo. Quizá desees dejar de trabajar a los 60 años y vivas de tu pensión, o te pensiones a los 65 años y además nunca dejes de trabajar. Es cosa de cada quien.</p>
                    </li>
                    <li>
                      <p>Tu opinión sobre la jubilación, en cuanto a retirar tu dinero del SAR tan pronto como te sea posible, o mantenerlo invertido un mayor número de años, por ejemplo hasta tus 70 años, para que te rinda más.</p>
                    </li>
                    <li>
                      <p>Con base en los puntos anteriores, una estimación del número de años que vivirás como jubilado y del fondo económico correspondiente, para garantizar tu manutención. Es probable que este punto te ayude a ver que hay una gran diferencia entre los recursos que necesitarás para vivir una jubilación de 10 años, o una de 20 años. Este es un ejercicio realista e importante para determinar tu edad de retiro.</p>
                    </li>
                  </ul>
                </li>
                <li>
                  <strong style={{color}}>Prever las fuentes de ingreso en la jubilación</strong>
                  <p>
                    Como se mencionó antes, prever cuáles serán tus fuentes de ingreso durante tu jubilación requiere prepararlas y habilitarlas desde tu presente. Debes hacerte del ahorro, de las inversiones y de los bienes que te darán estabilidad financiera en tu futuro, antes de tu retiro.
                  </p>
                  <p>
                    De antemano, ten por seguro que la clave para disponer de recursos durante tu vejez está en que ahorres para invertir, desde tu presente.
                  </p>
                  <ul>
                    <li>
                      <p>Ahorro personal</p>
                    </li>
                    <li>
                      <p>Cuentas de ahorro. Existe una gran variedad de cuentas de ahorro en bancos e instituciones financieras, mismas que ofrecen un vasto abanico de opciones y condiciones para ahorrar. Estas cuentas, en el entendido de que NO sean cuentas de inversión, o en muy pequeña escala, te darán bajos rendimientos, por lo que no resultan atractivas como posible fuente de ingresos para tu retiro. De hecho, si los intereses que te dan son inferiores a la inflación, posiblemente pierdas el poder adquisitivo de tus ahorros, o dicho de otra forma, pierdes dinero.
                      </p>
                      <p>
                        Las cuentas de ahorro que te dan bajos rendimientos son útiles para el ahorro de corto plazo, es decir, para propósitos en un futuro cercano. Básicamente se trata de tener un lugar donde resguardar y acumular cierta cantidad de dinero, para usarlo próximamente. Por otro lado, suelen ser cuentas seguras donde tus ahorros, hasta por poco más de $2,300,000 pesos, están protegidos por el Instituto para la Protección al Ahorro Bancario (IPAB).
                      </p>
                    </li>
                    <li>
                      <p>Ahorro en efectivo.

                        Al igual que con las cuentas que únicamente son de ahorro, guardar el dinero en efectivo “debajo del colchón” o escondido donde sea, lo convierte en dinero ocioso que no produce y que, por lo mismo, está expuesto a la depreciación causada por la inflación. El monto ahorrado no disminuye, pero sí su poder adquisitivo.
                      </p>
                      <p>
                        Quizá prefieras “tener pájaro en mano, que un ciento volando”; quizá desconfíes de las instituciones financieras y te sientas más seguro guardando tú mismo tus ahorros. Cuida bien que nadie encuentre tu tesoro antes de jubilarte, porque el dinero guardado así no tiene dueño, y por tanto es de quien lo encuentra.
                      </p>
                      <p>
                        A lo sumo, la mejor recomendación sería que por lo menos hicieras alguna vez una prueba, quizá con un monto pequeño en una cuenta bancaria que te dé ganancias iguales o superiores a la inflación, sin arriesgar. Quien quita y la experiencia te sorprenda, para bien.
                      </p>
                    </li>
                    <li>
                      <p>La Cuenta AFORE, del Sistema de Ahorro para el Retiro (SAR).

                        La Cuenta AFORE es una cuenta de ahorro personal, complementado por un sistema de inversión que te ayuda a crear un fondo de dinero destinado a tu pensión. Tiene su origen como una prestación laboral del Estado para los trabajadores del sector formal, aunque hoy en día todo mexicano puede abrir y aprovechar su cuenta individual o Cuenta AFORE.
                      </p>
                    </li>
                    <p>
                      Conoce cómo funciona este importante instrumento financiero, mediante una descripción clara y completa en la siguiente liga:  <a href={'/' + section + '/afore'} target='_blank' > “El manejo básico de la Cuenta AFORE”</a>
                    </p>
                  </ul>
                </li>
                <li>
                  <strong style={{color}}>¿Cuáles son los principales beneficios de ahorrar e invertir en tu Cuenta AFORE?</strong>
                    <ul>
                      <li>
                        <strong style={{color}}>Seguridad</strong>
                        <p>
                          La Comisión Nacional del Sistema de Ahorro para el Retiro (CONSAR), es la autoridad gubernamental que supervisa el desempeño del SAR y de las AFORES. Su labor fundamental es la de regular el SAR, que está constituido por las cuentas individuales (Cuentas AFORE) a nombre de los trabajadores y que son administradas por las AFORES. ¿Qué significa que CONSAR regula al SAR y a las AFORES?
                        </p>
                        <ul>
                          <li><p>La CONSAR establece las reglas para que el SAR funcione adecuadamente.</p></li>
                          <li><p>Vigila que se resguarden los recursos de los trabajadores.</p></li>
                          <li><p>Supervisa que los recursos de los trabajadores se inviertan de acuerdo a los parámetros y límites establecidos por la Comisión (Régimen de inversión).</p></li>
                          <li><p>Se asegura de que las AFORES brinden la información requerida para los trabajadores (por ejemplo, que te envíen tu estado de cuenta tres veces al año).</p></li>
                          <li><p>Está facultada para imponer multas a las AFORES y sanciones a los empleados de éstas en caso de algún incumplimiento.</p></li>
                        </ul>
                        <p>
                          Con todo lo anterior se protegen los ahorros y las inversiones de las Cuentas AFORE con los más altos estándares internacionales de seguridad y rentabilidad, para beneficio de los ahorradores.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Aportaciones tripartitas de Ley </strong>
                        <p>
                          Siendo un asalariado formal, tu empleador deposita en tu Cuenta AFORE las aportaciones obligatorias de Ley, mismas que se constituyen de un pequeño porcentaje de tu sueldo mensual más otros porcentajes complementarios por parte de tu empleador y del Gobierno Federal.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Altos rendimientos por inversiones de largo plazo </strong>
                        <p>
                          Además de resguardar y administrar tus ahorros, las AFORES se especializan en las inversiones a largo plazo y con ello te generan altos rendimientos.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Interés compuesto </strong>
                        <p>
                          Al invertir tus ahorros, los rendimientos generados a su vez se reinvierten, generando más y más rendimientos que se suman siempre al total de tus recursos.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Aportaciones voluntarias para el retiro </strong>
                        <p>
                          Además de las aportaciones obligatorias derivadas directamente de tu salario, puedes realizar Ahorro Voluntario en tu Cuenta AFORE. Cuando instruyes a tu AFORE que tu Ahorro Voluntario es de largo plazo o “complementario”, obtienes mejores rendimientos, beneficios fiscales y lo más importante es que incrementas tu ahorro para el retiro, con lo que lograrás una mejor pensión.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Retiro del Ahorro Voluntario </strong>
                        <p>
                          Cuando tus aportaciones voluntarias son de corto plazo, las puedes retirar al cabo de dos o seis meses del depósito, según lo establezca la AFORE que administra tus ahorros.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Subcuenta de vivienda </strong>
                        <p>
                          Tu Cuenta AFORE tiene una subcuenta de vivienda en la que, como su nombre lo indica, se depositan y acumulan recursos para financiar, parcial o hasta íntegramente, tu adquisición de una vivienda a través del INFONAVIT o del FOVISSSTE.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Accesibilidad</strong>
                        <p>
                          El control y manejo eficaz de tu Cuenta AFORE implica que puedas realizar tus movimientos donde y cuando lo desees, y por lo mismo, que tengas fácil acceso a la información de tu cuenta. Para ello existe una vasta gama de opciones proporcionadas directamente por las AFORES, medios electrónicos y digitales -como la aplicación AforeMóvil- y establecimientos físicos a nivel nacional para realizar aportaciones voluntarias.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Inversiones y fondos de inversión</strong>
                        <p>
                          Invertir significa utilizar tus recursos o los que tienes a tu alcance para obtener otros, o más. Inviertes tiempo y dedicación en algo, siempre con un propósito aunque sea por simple satisfacción. Inviertes al interesarte, al ocuparte, al participar y aportar. El principio de invertir para tu jubilación, o para cualquier otro objetivo, se nutre de tus recursos puestos a trabajar para obtener algo más.
                        </p>
                        <p>Lo interesante es que invertir siempre implica un riesgo entre ganar y perder. Como todo, mientras más informado y experimentado seas, menores serán tus probabilidades de cometer errores; y no se trata de invertir temerosamente, “pensando chiquito”, porque tu ganancia será proporcionalmente pequeña. Puedes invertir pensando en grande, con ambición, y para ello necesitas estar informado. Como dice el dicho, “la información es poder”, al igual que el conocimiento y la experiencia; utilízalos a tu favor, haz camino empezando desde abajo y ascendiendo al ritmo de tus capacidades.</p>
                        <p>Puedes invertir en un sinfín de proyectos pensando en generar recursos para tu retiro. Desde invertir en un negocio que puedas traspasar más adelante o que te dé ingresos durante tu jubilación, hasta invertir en instrumentos de inversión del sistema financiero, como pagarés, sociedades de inversión, depósitos a plazo, acciones, bonos, cetes, entre muchos más. Sea como sea, debes conocer las condiciones de inversión lo más detalladamente posible, destacando cuánto podría darte a ganar y en cuánto tiempo.</p>
                        <p>En términos generales, es importante destacar que a mayor plazo de inversión, menor liquidez, pero mayor rendimiento.</p>
                        <p>Por ejemplo, las AFORES pagan rendimientos atractivos gracias a que los recursos se invierten con un horizonte de inversión de mediano y largo plazo. En una cuenta de ahorro bancaria puedes depositar hoy y retirar tu dinero mañana, lo que hace necesario que el Banco conserve una buena parte de tu ahorro disponible en efectivo. En cambio, una AFORE recibe tu dinero y lo invierte de inmediato asumiendo que ese dinero no saldrá del Sistema de Ahorro para el Retiro hasta dentro de 10, 20 o 30 años.</p>
                        <p>Tip: Sé muy cauteloso con cualquier tipo de inversión que te ofrezca altas ganancias en poco tiempo. Esta suele ser la mejor señal –muy seductora- de que probablemente exista algo dudoso en la propuesta. El dinero fácil puede salir muy caro.</p>
                      </li>
                      <li>
                        <strong style={{color}}>Vivir de tus rentas</strong>
                        <p>
                          Sin duda, esta fuente de ingresos es el sueño dorado de la mayoría. ¡Qué mejor que poseer un patrimonio que te siga generando ingresos durante tu vejez!
                        </p>
                        <p>
                          Vivir de tus rentas será el resultado de haber invertido oportunamente en la construcción o adquisición de algún inmueble o negocio que te siga aportando ingresos periódicamente durante el retiro.
                        </p>
                        <p>
                          Esto tampoco surge espontáneamente justo al momento de retirarse, por lo que para ese entonces es probable que ya conozcas gran parte de lo que se necesita para operar, administrar y sobretodo mantener esa fuente de ingresos, o bien cómo supervisar a quien lo haga por ti.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Vender bienes propios</strong>
                        <p>
                          Vender bienes personales como un inmueble, un terreno, joyería, oro y plata, obras de arte o cualquier producto de valor, puede aportarte un valioso recurso económico. Lo que hay que saber es que al momento de convertir su valor a dinero, deberás administrarlo con cuidado para que te rinda más, durante el mayor tiempo posible. Adquirir oportunamente ciertos bienes como los aquí mencionados, y particularmente aprovechar cuando se te presenten circunstancias favorables de compra a lo largo de tu vida, es también una manera de invertir en un patrimonio que podrás aprovechar en tu futuro.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Depender de otros</strong>
                        <ul>
                          <li>
                            <p>Familiares. Sin duda, depender de familiares no debería estar en la lista de fuentes de ingreso para la jubilación, sin embargo y lamentablemente, sucede con frecuencia, precisamente por la falta de previsión y planeación para el retiro. Depender de los recursos de nuestros familiares es afectar su propia economía. Este punto debería verse como un apoyo esporádico más que como un ingreso periódico del jubilado.</p>
                          </li>
                          <li><p>Programas de Gobierno. Correctamente diseñados y ejecutados, estos programas deben responder a las carencias reales de la población desprotegida y necesitada. Los hay de muchos tipos y con propósitos muy diversos para apoyar a las personas mayores, según las regiones y gobiernos.</p></li>
                        </ul>
                      </li>
                      <li>
                        <strong style={{color}}>Seguir trabajando</strong>
                        <p>
                          Como dicho antes en el rubro “Elegir la edad de retiro”, el Sistema de Ahorro para el Retiro (SAR) establece que la edad de retiro es a los 65 años, aunque puede ser a partir de los 60 años con ciertas condiciones y restricciones.
                        </p>
                        <p>
                          Retirarse de la vida laboral remunerada es un tema mucho más flexible que lo que se establece en el SAR. Por ejemplo, tomarse un año sabático representa de alguna manera un retiro momentáneo del ámbito laboral remunerado, o bien, lo mismo podría decirse de un trabajador que deja el sector formal para trabajar de manera independiente, y después regresa o cotiza intermitentemente e interrumpe sus aportaciones de Ley a su Cuenta AFORE. Como sea, dejar de trabajar implica obligatoriamente disponer de alguna o algunas de las fuentes de ingreso aquí mencionadas. Por supuesto, y a reserva de que exista una causa de fuerza mayor, es una decisión personal.
                        </p>
                        <p>
                          Visto desde la perspectiva del Estado, el retiro laboral del trabajador aproximadamente a la edad de 65 años, es el momento a partir del cual puede acceder a sus recursos ahorrados dentro del SAR. Aquí hay que destacar y aclarar que el acceso a ese dinero está sujeto a la normatividad del SAR, diseñado con la prioridad de que el fondo ahorrado sirva para proveer una pensión periódica durante la jubilación de la persona. De hecho, si el pensionado decide volver a trabajar y por lo mismo volver a cotizar al SAR, su pensión será suspendida mientras aparezca nuevamente como “trabajador en activo”.
                        </p>
                        <p>
                          Recuerda consultar la sección “Todo sobre el retiro de recursos del SAR” de la página de la Comisión Nacional del Sistema de Ahorro para el Retiro (CONSAR), en la siguiente liga:
                          <a href="https://www.gob.mx/consar/articulos/todo-sobre-el-retiro-100587?idiom=es" target="_blank">https://www.gob.mx/consar/articulos/todo-sobre-el-retiro-100587?idiom=es</a>
                        </p>
                        <p>
                          Ahora bien, visto desde una perspectiva ajena al ahorro invertido en el SAR, cada quien es totalmente libre de trabajar y obtener una remuneración económica hasta la edad que desee, y probablemente en condiciones más convenientes, como trabajar en casa, medio tiempo o por encargos, etc.
                        </p>
                      </li>
                      <li>
                        <strong style={{color}}>Un poco de todo</strong>
                        <p>
                          Sería maravilloso vivir de tus rentas o de ahorros suficientes que te quitaran cualquier tipo de preocupación. Es un ideal al que sin duda puedes y debes aspirar. Además, independientemente de nuestro nivel socioeconómico, es común para todos que “el dinero se vaya como agua” y por lo mismo, sea mucho o poco, es indispensable saberlo administrar. Piensa y analiza cuáles serán tus fuentes de ingreso y prepáralas.
                        </p>
                        <p>
                          Así como se recomienda no arriesgar poniendo todos los huevos en la misma canasta, diversificar tus fuentes de ingreso durante la vejez puede resultar una alternativa viable y más efectiva:
                        </p>
                        <ul>
                          <li><p>Ahorros</p></li>
                          <li><p>Ahorros para invertir</p></li>
                          <li><p>Inversión en negocios</p></li>
                          <li><p>Utilidades</p></li>
                          <li><p>Inversión en bienes</p></li>
                          <li><p>Ventas y rentas</p></li>
                          <li><p>Trabajo remunerado</p></li>
                        </ul>
                      </li>
                      <li>
                        <strong style={{color}}>El plan de ahorro e inversión para el retiro</strong>
                        <p>
                          Al inicio de esta ficha se habló de “Visualizarse en la jubilación: el plan de retiro“. Describir tu objetivo consiste en imaginar el tipo de vida que llevarás en tu jubilación, con gusto, y cuánto te costará. Quizá te parezca un ejercicio impreciso, con un alto grado de incertidumbre, y sin duda lo es, pero el simple hecho de suponerlo y visualizarte en él, es la mejor manera de empezar a moverte hacia la acción.
                        </p>
                        <p>
                          Hay dos formas de pensar el ahorro y la inversión para tu retiro. La primera, efectivamente, es un plan y consiste en que describas tu objetivo por alcanzar y después planees con precisión y disciplina cómo lo harás posible con metas de ahorro e inversión. La segunda se desarrolla al revés, es decir, echas a andar la mayor cantidad posible de “procesos de ahorro e inversión” que te generen ingresos y con base en lo que hayas logrado reunir, llegado el momento, verás para qué te alcanza.
                        </p>
                        <p>
                          A pesar de que ambas pueden alcanzar resultados, la organización de un plan debería proporcionarte mayor claridad sobre el proceso, es decir, sobre lo que persigues y la forma como vas progresando, o no, para mantener o ajustar el rumbo. Además, establecer un gran objetivo hecho de metas específicas e irlas superando a medida que avanzas, se va convirtiendo en un importante incentivo motivacional.
                        </p>
                        <p>
                          Como sea, las herramientas para trabajar este plan son: 1) los proyectos en los que ves oportunidad de ganar dinero, 2) tu ahorro, 3) tu inversión. Inicia con asignar una parte de tu ingreso personal a la acumulación de ahorro, hasta que alcances el monto que ya sabes que necesitas para invertirlo en esos negocios o proyectos que ya has estudiado y que no te impliquen gran riesgo de pérdida. Puede ser uno, así como pueden ser varios. Recuerda que es más prudente al inicio y más ventajoso enfocarte en diferentes opciones, mismas que se convertirán en diferentes fuentes de ingreso.
                        </p>
                        <p>
                          Tip: No dudes en empezar desde abajo con inversiones modestas y sencillas. A medida que avances, adquirirás valiosa experiencia que te permitirá hacerlo cada vez mejor.
                        </p>
                      </li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
          <h2 style={{color}}></h2>
          <div className={style.textContainer}>
            <div>

            </div>
          </div>
          <div className={style.links}>
            <h3 style={{color}}>Ligas de interés</h3>
            <ul>
              <li><a target='_blank' style={{color}} href='https://www.gob.mx/consar'>CONSAR. Comisión Nacional del Sistema de Ahorro para el Retiro. Portal</a></li>
              <li><a target='_blank' style={{color}} href='https://www.gob.mx/consar/acciones-y-programas/como-entender-tu-ahorro-para-el-futuro?idiom=es'>Blog. Cómo entender tu ahorro para el Futuro</a></li>
              <li><a target='_blank' style={{color}} href='http://www.consar.gob.mx/gobmx/Aplicativo/WebDashboard/'>Tablero interactivo de inversiones de las AFORES</a></li>
              <li><a target='_blank' style={{color}} href='http://www.amafore.org'>Asociación Mexicana de Afores (Amafore). Portal</a></li>
              <li><a target='_blank' style={{color}} href='https://www.gob.mx/ipab'>Instituto para la Protección al Ahorro Bancario (IPAB). Portal</a></li>
              <li><a target='_blank' style={{color}} href='https://www.gob.mx/inapam'>Instituto Nacional de las Personas Adultas Mayores (INAPAM). Portal</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/eventos_retirovejez.html'>CONDUSEF – Eventos en tu vida: Retiro</a></li>
              <li><a target='_blank' style={{color}} href='https://eduweb.condusef.gob.mx/EducaTuCartera/cuadernos-videos/cvretiro.html'>CONDUSEF – Cuadernos y videos: Retiro</a></li>
            </ul>
          </div>
      </div>
    </Layout>
  )
}

Pension.seo = {
  title: 'Preparar mi jubilación | CONSAR ',
  description: 'La oportunidad de reinventarme, tomando lo mejor de mí y lo que falte por venir.',
  ogtitle: 'Preparar mi jubilación | CONSAR',
  ogdescription: 'La oportunidad de reinventarme, tomando lo mejor de mí y lo que falte por venir.',
  keywords: [
    'consar',
    'Preparar mi jubilación'
  ]
}

export default Pension
