export default [
  {
    slug: 'empleadores',
    title: 'Empleadores',
    icon: '/static/img/referencias/publics/empleadores',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/Empleadores_1.jpg',
        title: 'Save 10',
        text: 'Programa que incentiva a los patrones a involucrar a sus empleados en el ahorro para su retiro, destinando el 10 % de su sueldo a este rubro. Esto con atractivos beneficios para los empleadores.',
        url: 'http://save10.org/'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Empleadores_2.jpg',
        title: 'Employers Expanding Ways to Get Financial Education to Employees',
        text: 'Nota que expone los resultados de un estudio realizado a empleadores de Inglaterra sobre las acciones de educación financiera que estarían dispuestos a implementar en sus empresas para que sus empleados se retiren con seguridad.',
        url: 'https://www.plansponsor.com/employers-expanding-ways-to-get-financial-education-to-employees/'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Empleadores_3.jpg',
        title: 'The Real Deal. Retirement Income Adquacy al Large Companies',
        text: 'Sexta entrega de un estudio estadounidense que analiza la preparación financiera con la que cuentan 2.1 millones de empleados para su jubilación, así como las acciones que están implementando sus empleadores para apoyarlos en este tema.',
        url: 'http://www.aon.com/human-capital-consulting/thought-leadership/retirement/the-real-deal-2015.jsp'
      }
    ]
  },
  {
    slug: 'inversionistas',
    icon: '/static/img/referencias/publics/inversionistas',
    title: 'Inversionistas',
    text: '',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/inversionistas_1.jpg',
        title: 'Kubo financiero',
        text: 'Comunidad financiera en México especializada en Crowdfunding y Peer to Peer (P2P). Su página explica cómo funciona una inversión con ayuda de su blog y de un simulador que ayuda a probar cómo se desarrollaría ésta.',
        url: 'https://www.kubofinanciero.com'
      },
      {
        img: '/static/img/referencias/publicsThumbs/inversionistas_2.jpg',
        title: 'Tipos de Fondos o Sociedades de Inversión',
        text: 'Sección de página web que explica los tipos y los estilos de inversión para que el inversionista o futuro inversionista elija el/los que mejor se adapten a sus necesidades y estilo de vida financiera y pueda tomar una decisión informada.',
        url: 'http://www.franklintempleton.com.mx/es_MX/public/recursos-inversionista/fondos-mutuos.page?'
      },
      {
        img: '/static/img/referencias/publicsThumbs/inversionistas_3.jpg',
        title: 'Teach investors the basics, rest will follow',
        text: 'Documento sobre Educación Financiera enfocado en el análisis del ahorro y las inversiones, la evolución de su industria, su estructura, barreras conductuales y culturales; así como las políticas que se han implementado en diversos gobiernos para luchar contra ellas.',
        url: 'http://www.oecd-ilibrary.org/finance-and-investment/financial-education-savings-and-investments_5k94gxrw760v-en?crawler=true'
      }
    ]
  },
  {
    slug: 'milenials',
    title: 'Millennials',
    icon: '/static/img/referencias/publics/milenials',
    text: '',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/Milenials_1.jpg',
        title: 'KUZAMBA. Finanzas para millennials',
        text: 'Página creada por periodistas e investigadores que busca informar de forma sencilla y potenciar la cultura financiera. Ofrece noticias y análisis de importancia para la toma de decisiones de inversión tomando en cuenta al público Latinoamericano.',
        url: 'https://kuzamba.com/'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Milenials_2.jpg',
        title: '8 Tips financieros para Millennials',
        text: 'Nota que otorga 8 consejos sobre finanzas a la generación Millennial para que se desempeñen con éxito en su situación presente y futura.',
        url: 'http://expansion.mx/mi-dinero/2015/05/12/8-tips-financieros-para-millennials'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Milenials_3.jpg',
        title: 'Tomando el control de nuestro futuro financiero ',
        text: 'Video-conferencia que explica de forma entretenida y simple la importancia de contemplar y ocuparse de las finanzas personales desde joven para cumplir con las metas que se desean.',
        url: 'https://www.youtube.com/watch?v=agsTBnU8plU'
      }
    ]
  },
  {
    slug: 'maestros',
    title: 'Maestros',
    icon: '/static/img/referencias/publics/maestros',
    text: '',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/Maestros_1.jpg',
        title: 'My Classroom economy',
        text: 'Programa dirigido a profesores para enseñar a sus alumnos, de cualquier grado, responsabilidad financiera de forma divertida y didáctica, con un temario y materiales diseñados para diferentes edades.',
        url: 'http://myclassroomeconomy.org/?sf8320707=1'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Maestros_2.jpg',
        title: 'Cuaderno de estrategias para la formación económica y financiera',
        text: 'Cuaderno de apoyo con estrategias y actividades propuestas para docentes que buscan inculcar en sus alumnos el tema de educación financiera bajo los ejes temáticos de: Ahorro y Trabajo, Manejo de recursos, Ingreso y gasto y Consumo Inteligente.',
        url: 'https://es.scribd.com/document/237945037/Cuaderno-de-Estrategias-de-Educacion-Financiera'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Maestros_3.jpg',
        title: 'Análisis de las políticas para maestros de educación básica en México ',
        text: 'Estudio que analiza el contexto, las oportunidades y los desafíos que tiene el sistema educativo a nivel básico en México y su personal docente, en específico con la implementación del programa “Alianza por la Calidad de la Educación” establecido desde 2008.',
        url: 'https://www.oecd.org/mexico/44906091.pdf'
      }
    ]
  },
  {
    slug: 'empresas',
    title: 'Medianas, pequeñas y micro empresas',
    icon: '/static/img/referencias/publics/empresas',
    text: '',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/Pymes_1.jpg',
        title: 'Talleres de educación financiera para PyMES',
        text: 'Cursos especializados para Pequeñas y Medianas Empresas, completamente gratuitos para mejorar las habilidades y aumentar los conocimientos financieros de los emprendedores.',
        url: 'https://www.yosoypyme.net/nota.aspx?nota=8be678fc-34fe-43b4-9cc4-6d9438449259&tit=Talleres-de-educacion-financiera-gratis'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Pymes_2.jpg',
        title: 'Pymempresario',
        text: 'Revista digital dirigida a micro, pequeño y mediano empresarios con información sobre educación financiera, recomendaciones y novedades en este campo.',
        url: 'http://www.pymempresario.com/'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Pymes_3.jpg',
        title: 'Report Financial education for MSMEs and potential entrepreneurs',
        text: 'Reporte que tiene la intención de proporcionar orientación política o práctica en el área de Educación Financiera para micro, pequeñas y medianas empresas con base en información y ejemplos recabados mundialmente.',
        url: 'https://www.gpfi.org/sites/default/files/documents/08-%20OECD-INFE%20Progress%20Report%20on%20Financial%20Education%20for%20MSMEs.pdf'
      }
    ]
  },
  {
    slug: 'mujeres',
    title: 'Mujeres',
    icon: '/static/img/referencias/publics/mujeres',
    text: '',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/Mujeres_1.jpg',
        title: '5ta Encuesta de AMAFORE (2015) Ahorro y Futuro. Una perspectiva de género',
        text: 'Encuesta que aporta elementos para conocer las principales diferencias en los comportamientos económicos de hombres y mujeres, así como la forma en que las construcciones sexogenéricas interactúan con las decisiones respecto al ahorro para el retiro.',
        url: 'http://amafore.org/documentos/Encuesta2015.pdf'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Mujeres_2.jpg',
        title: 'La mujer en la legislación de los sistemas de pensiones reformados en América Latina',
        text: 'Se analiza el panorama general de la situación de la mujer en países que ya han aplicado reformas de seguridad social, las desigualdades en la cobertura de las pensiones entre géneros y los desafíos y problemas de las legislaciones para lograr condiciones más equitativas.',
        url: 'http://www.cepal.org/mujer/proyectos/pensiones/publicaciones/word_doc/daniel_jimenez.PDF'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Mujeres_3.jpg',
        title: 'Gender Performance Indicators Manual: How well are we serving women?',
        text: 'Sitio web de organización mundial sin fines de lucro dedicada a brindar a instituciones financieras indicadores para ampliar y fortalecer el acceso de las mujeres de bajos ingresos a herramientas y recursos financieros.',
        url: 'http://www.womensworldbanking.org/publications/gender-performance-indicators-how-well-are-we-serving-women/'
      }
    ]
  },
  {
    slug: 'menores',
    title: 'Menores de edad',
    icon: '/static/img/referencias/publics/menores',
    text: '',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/ninos_1.jpg',
        title: 'Aprender cuenta',
        text: 'Proyecto educativo diseñado especialmente para enseñar educación financiera y emprendimiento a los niños y niñas de 5 a 16 años de edad, de forma didáctica y divertida.',
        url: 'https://www.aprendercuenta.com/'
      },
      {
        img: '/static/img/referencias/publicsThumbs/ninos_2.jpg',
        title: 'Once Upon a Dime',
        text: 'Cómic estadounidense hecho por estudiantes cuya historia explica los conceptos básicos de economía y finanzas para un niño con la finalidad de estimular su curiosidad por conocer más sobre este tema.',
        url: 'https://www.newyorkfed.org/medialibrary/media/outreach-and-education/comic-books/NewYorkFed-OnceUponADime-WebFullColor.pdf'
      },
      {
        img: '/static/img/referencias/publicsThumbs/ninos_3.jpg',
        title: 'El Cotorro del Ahorro',
        text: 'Libro ilustrado que cuenta la  historia de una familia que descubre  la importancia de ahorrar para el futuro.',
        url: 'http://www.aiosfp.org/wp-content/uploads/2015/11/mexico_el-cotorro.pdf'
      }
    ]
  },
  {
    slug: 'padres',
    icon: '/static/img/referencias/publics/padres',
    title: 'Padres de familia',
    text: '',
    links: [
      {
        img: '/static/img/referencias/publicsThumbs/Padres_DeFamilia_1.jpg',
        title: 'Cómo fomentar el ahorro entre los más pequeños: decálogo',
        text: 'Nota sobre cómo 10 pautas a tener en cuenta para fomentar el ahorro y la educación financiera entre los más pequeños de la casa.',
        url: 'http://valenciaplaza.com/como-fomentar-el-ahorro-entre-los-mas-pequenos-ahi-va-un-decalogo'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Padres_DeFamilia_2.jpg',
        title: '¿Cómo enseño a mis hijos a valorar lo que tienen?',
        text: 'Nota que da una serie de consejos a los padres de familia para lograr inculcar en sus hijos la educación financiera, con acciones sencillas y cotidianas.',
        url: 'http://www.elfinanciero.com.mx/opinion/como-enseno-a-mis-hijos-a-valorar-lo-que-tienen.html'
      },
      {
        img: '/static/img/referencias/publicsThumbs/Padres_DeFamilia_3.jpg',
        title: 'Trucos para convertir a tus hijos en ahorradores',
        text: 'Nota que brinda algunos tips para que los padres de familia puedan fomentar en sus hijos poco a poco el hábito del ahorro sin que estos lo perciban.',
        url: 'http://www.dineroenimagen.com/2016-04-29/72260'
      }
    ]
  }
]
