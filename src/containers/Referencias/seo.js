
export default {
  title: 'Buenas referencias sobre finanzas y ahorro previsional | CONSAR',
  description: 'Recomendaciones de la CONSAR. Toma nota de documentos y casos de interés nacionales e internacionales sobre finanzas y educación previsional.',
  ogtitle: 'Buenas referencias sobre finanzas y ahorro previsional | CONSAR',
  ogdescription: 'Recomendaciones de la CONSAR. Toma nota de documentos y casos de interés nacionales e internacionales sobre finanzas y educación previsional.',
  keywords: [
    'Consar',
    'Presupuesto personal',
    'Presupuesto familiar',
    'Ahorro previsional',
    'finanzas personales'
  ]
}
