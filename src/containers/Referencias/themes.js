export default [
  {
    slug: 'ahorro-para-el-retiro',
    title: 'Ahorro para el retiro',
    icon: '/static/img/referencias/themes/ahorro-para-el-retiro',
    links: [
      {
        img: '/static/img/referencias/thumbnails/ahorroRetiro1.jpg',
        title: '‘Juvenescence’ and why we need to act younger to thrive in the coming era',
        href: 'http://www.scmp.com/business/article/1977376/juvenescence-and-why-we-need-act-younger-thrive-coming-era',
        text: 'Nota sobre prever los escenarios del retiro de las nuevas generaciones, ante la creciente esperanza de vida.',
        keys: ['ahorro para el retiro', 'esperanza de vida']
      },
      {
        img: '/static/img/referencias/thumbnails/ahorroRetiro2.jpg',
        title: 'MA-RC Introduces New Retirement Savings Calculator',
        href: 'http://www.icmarc.org/prebuilt/static/growyoursavings/',
        text: 'Calculadora para visualizar el interés compuesto. The new "Grow Your Savings" calculator shows the effect of increasing contributions to retirement plans and compound interest on savings.',
        keys: ['ahorro para el retiro', 'herramientas digitales financieras', 'calculadora', 'interes compuesto']
      },
      {
        img: '/static/img/referencias/thumbnails/ahorroRetiro3.jpg',
        title: 'Juego de mesa PlayPension. Fundación MAPFRE',
        href: 'https://segurosypensionesparatodos.fundacionmapfre.org/syp/es/talleres/taller-playpension/default.jsp',
        text: 'Juego de mesa descargable para sensibilizar a los estudiantes sobre la importancia del ahorro a largo plazo. Durante el juego deberán manejar un presupuesto  familiar y conocerán sencillos instrumentos de ahorro que les permitan afrontar imprevistos a lo largo de la vida.',
        keys: ['ahorro para el retiro', 'juego']
      },
      {
        img: '/static/img/referencias/thumbnails/ahorroRetiro4.jpg',
        title: 'Retirement Goal Planning System',
        href: 'http://befi.allianzgi.com/en/retirement-goal-planning-system/Pages/default.aspx',
        text: 'Herramienta digital para la planeación personal de objetivos con respecto al tipo de jubilación a la que se aspira.',
        keys: ['ahorro para el retiro', 'fintech', 'planeacion', 'previsional']
      }
    ]
  },
  {
    slug: 'capacidades-financieras',
    title: 'Capacidades financieras',
    icon: '/static/img/referencias/themes/capacidades-financieras',
    links: [
      {
        img: '/static/img/referencias/thumbnails/capacidades_finacieras_1.jpg',
        title: 'Building Millennials Financial Health Via Financial Capability',
        href: 'http://www.newamerica.org/asset-building/policy-papers/building-millennials-financial-health-via-financial-capability/',
        text: 'Nota sobre un estudio realizado en EUA. PDF descargable. This study investigates the state of Millennials financial health and provides a preliminary test of the effectiveness of financial capability—an intervention combining financial education and financial inclusion.',
        keys: ['capacidades financieras', 'millennials']
      },
      {
        img: '/static/img/referencias/thumbnails/capacidades_finacieras_2.jpg',
        title: 'National Financial Capability Study (NFCS) 2016',
        href: 'http://www.usfinancialcapability.org/',
        text: 'Estudio realizado en EUA. PDF descargable. Read the report for survey findings that underscore the need to ensure all Americans have access to the education, resources and tools they need to manage their money with confidence.',
        keys: ['capacidades financieras']
      },
      {
        img: '/static/img/referencias/thumbnails/capacidades_finacieras_3.jpg',
        title: 'Modern financial literacy begins at primary',
        href: 'http://www.nzherald.co.nz/personal-finance/news/article.cfm?c_id=12&objectid=11504527',
        text: 'Nota con ejemplos diversos y casos de educación financiera aplicada (financial capability) en escuelas de EUA.',
        keys: ['capacidades financieras']
      },
      {
        img: '/static/img/referencias/thumbnails/Adultos_2.jpg',
        title: 'International Survey of Adult Financial Literacy Competencies',
        text: 'Resultados de encuesta internacional organizada por la OCDE en 30 países de todos los continentes sobre alfabetización financiera, tomando en cuenta el comportamiento, actitudes e inclusión de la población adulta de cada uno de ellos.',
        href: 'http://www.condusef.gob.mx/Revista/index.php/ahorro/retiro',
        keys: ['capacidades financieras']
      },
      {
        img: '/static/img/referencias/thumbnails/Adultos_3.jpg',
        title: 'Adult Financial Capability Framework',
        text: 'Guía de apoyo para personas que se encargan de impartir educación financiera para adultos con secciones sobre: Conocimiento financiero y comprensión; Habilidades y competencia financiera; Responsabilidad financiera en tres niveles. ',
        href: 'http://shop.niace.org.uk/media/catalog/product/A/1/A1957_adult_financial_capability_framework_1.pdf',
        keys: ['capacidades financieras']
      }
    ]
  },
  {
    slug: 'cursos-de-educacion-financiera',
    title: 'Cursos de educación financiera',
    icon: '/static/img/referencias/themes/cursos-de-educacion-financiera',
    links: [
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_1.jpg',
        title: '“Aprende y Crece” - Grupo Elektra - Banco Azteca ',
        href: 'http://www.aprendeycrece.mx/Herramientas/AprendeLinea',
        text: 'Cursos en línea gratuitos de Finanzas Personales  y de Negocios',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_2.jpg',
        title: 'Grupo Financiero Inbursa',
        href: 'https://www.inbursa.com/portal/?page=Document/doc_view_section.asp&id_document=4962&id_category=94',
        text: 'Curso Finanzas Personales. “Aprende a manejar de una mejor manera tus recursos, planear tu economía doméstica y utilizar racionalmente los servicios financieros para alcanzar un mejor nivel de vida.”',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_3.jpg',
        title: 'Programa de educación financiera. Fundación Carlos Slim',
        href: 'https://capacitateparaelempleo.org/pages.php?r=.tema&tagID=5355',
        text: 'Curso en línea. Programa de educación financiera. “Crear la necesidad en las personas por hacerlas consientes sobre el conocimiento sobre aspectos financieros que le permitan cuidar y hacer crecer su  patrimonio de forma eficaz al tener una conciencia financiera.”',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_4.jpg',
        title: 'Portal de capacitación en línea de Educación Financiera Banamex',
        href: 'https://www.banamex.com/esp/grupo/saber_cuenta/cursos/',
        text: 'Curso en línea de Finanzas personales: tu trabajo, tu dinero y tu patrimonio. “Curso que te introducirá en el mundo de las finanzas personales y familiares a través de herramientas financieras como la planeación financiera personal, el ahorro, el presupuesto, el crédito, la inversión y los seguros.”',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_5.jpg',
        title: 'BBVA Bancomer',
        href: 'https://www.educacionfinancierabancomer.com/',
        text: 'Talleres del programa Educación Financiera Bancomer desarrollados con el conocimiento técnico de los especialistas de BBVA Bancomer y con el apoyo pedagógico del MIDE (Museo Interactivo de Economía), con un lenguaje accesible para todo mundo y en un formato de un taller interactivo y atractivo.',
        keys: ['cursos de educación financiera', 'finanzas personales', 'MIDE']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_6.jpg',
        title: 'TareasPlus. Para saber más',
        href: 'https://www.tareasplus.com/Negocios/Finanzas',
        text: 'Sitio con una amplia categoría de cursos, con costo.',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_7.jpg',
        title: 'Condusef',
        href: 'https://phpapps.condusef.gob.mx/diplomado/',
        text: '“El Diplomado en Educación Financiera está conformado por apoyos innovadores para desarrollar tu habilidad de tomar las decisiones financieras inteligentes que te permitan cumplir con tus metas en cada etapa de tu vida.”',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_8.jpg',
        title: 'Condusef',
        href: 'https://eduweb.condusef.gob.mx/EducaTuCartera/cursosEF.html',
        text: 'Cursos gratuitos presenciales de Educación Financiera. Siete sesiones didácticas sobre los ejes básicos de Educación Financiera: Presupuesto y ahorro, crédito, ahorro para el retiro, inversión, seguros, herramientas financieras, Buró de Entidades Financieras, y medidas de seguridad de los usuarios.',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_9.jpg',
        title: 'Condusef',
        href: 'https://eduweb.condusef.gob.mx/EducaTuCartera/FormadoresEF.html',
        text: '“Taller para formador de formadores en Educación Financiera. Para grupos comprometidos con sus comunidades, interesados en convertirse en “formadores” de temas de educación financiera al replicar los conocimientos adquiridos, y que buscan beneficiar al mayor número de personas.”',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_10.jpg',
        title: 'Condusef',
        href: 'https://eduweb.condusef.gob.mx/EducaTuCartera/EF_en_tu_institucion.html',
        text: '“Educación Financiera en tu institución. Programa que desarrolla y difunde contenidos prácticos en materia de finanzas personales, para que los empleados o colaboradores de las instituciones, empresas o negocios puedan tomar mejores decisiones respecto al manejo de su dinero.”',
        keys: ['cursos de educación financiera', 'finanzas personales']
      },
      {
        img: '/static/img/referencias/thumbnails/curso_educ_fin_11.jpg',
        title: 'Museo Interactivo de Economía (MIDE)',
        href: 'http://www.mide.org.mx/mide/talleres-gratuitos-de-finanzas/',
        text: 'La oferta de cursos del MIDE te ayudan a mejorar tus finanzas personales, a entender la economía en tu vida cotidiana, a aprender cómo ser más consciente con el medio ambiente y a obtener herramientas de comunicación y mercadotecnia para tu negocio o vida profesional.',
        keys: ['cursos de educación financiera', 'finanzas personales']
      }
    ]
  },
  {
    slug: 'economia-conductual',
    title: 'Economía conductual',
    icon: '/static/img/referencias/themes/economia-conductual',
    links: [
      {
        img: '/static/img/referencias/thumbnails/economia_conductual_1.jpg',
        title: 'The Behavioral Economics Guide 2017',
        href: 'https://www.behavioraleconomics.com/the-behavioral-economics-guide-2017/',
        text: 'Publicación anual. PDF descargable.',
        keys: ['economia conductual', 'educacion financiera']
      },
      {
        img: '/static/img/referencias/thumbnails/economia_conductual_2.jpg',
        title: 'Behavioural insights',
        href: 'http://www.oecd.org/gov/regulatory-policy/behavioural-insights.htm',
        text: 'Sección sobre temas conductuales del sitio Web, de la OECD.',
        keys: ['economia conductual']
      },
      {
        img: '/static/img/referencias/thumbnails/economia_conductual_3.jpg',
        title: 'La muerte y las decisiones económicas',
        href: 'http://eleconomista.com.mx/finanzas-personales/2016/11/01/muerte-las-decisiones-economicas',
        text: 'Nota de El Economista. La falta de una adecuada percepción de la mortalidad futura produce conductas inconsistentes en el presente.',
        keys: ['economia conductual']
      },
      {
        img: '/static/img/referencias/thumbnails/economia_conductual_4.jpg',
        title: 'El uso de las ciencias del comportamiento para aumentar los ahorros para el retiro. ',
        href: 'https://www.gob.mx/cms/uploads/attachment/file/67484/ideas42_MX-Pensiones_Esp.pdf',
        text: 'Estudio realizado por la firma estadounidense de diseño conductual ideas42 en colaboración con la CONSAR.',
        keys: ['economia conductual']
      }
    ]
  },
  {
    slug: 'educacion-financiera',
    title: 'Educación financiera',
    icon: '/static/img/referencias/themes/educacion-financiera',
    links: [
      {
        img: '/static/img/referencias/thumbnails/educ_financiera_1.jpg',
        title: '¿Aprobarías el examen de cultura financiera? Son solo cinco preguntas',
        href: 'http://economia.elpais.com/economia/2015/11/19/actualidad/1447957909_428797.html',
        text: 'Estas preguntas han servido para determinar el nivel de alfabetización financiera de 150.000 adultos en 140 países diferentes. Son sencillas preguntas sobre la tasa de interés simple y compuesto, la inflación y la diversificación de riesgos. Y tú, ¿aprobarás?',
        keys: ['Educación financiera', 'Test']
      },
      {
        img: '/static/img/referencias/thumbnails/educ_financiera_2.jpg',
        title: 'Educación e inclusión financieras en América Latina y el Caribe. Programas de los bancos centrales y las superintendencias financieras',
        href: 'http://www.cemla.org/PDF/otros/2014-10-Educacion-Inclusion-Financieras-ALC.pdf',
        text: 'Publicación que resume los hallazgos de tres estudios con información sobre los principales actores públicos y privados, las estrategias nacionales, el público objetivo, los contenidos y objetivos de los programas, los canales utilizados para su difusión y los servicios ofrecidos.',
        keys: ['Educación financiera', 'Test']
      },
      {
        img: '/static/img/referencias/thumbnails/educ_financiera_3.jpg',
        title: 'Centro para la Educación y Capacidades Financieras',
        href: 'https://www.bbvaedufin.com/',
        text: 'El Centro para la Educación y Capacidades Financieras es una plataforma virtual para promover la importancia de conocimientos y habilidades financieras, a través de la investigación, la divulgación y el aprendizaje orientado a la acción.',
        keys: ['Educación financiera', 'Sitios web de EF internacional', 'Blog']
      },
      {
        img: '/static/img/referencias/thumbnails/educ_financiera_4.jpg',
        title: 'El futuro en tus manos. Técnicas financieras para toda la vida. Wells Fargo',
        href: 'https://handsonbanking.org/?lang=es',
        text: '“Centro de aprendizaje por Internet que ofrece recursos para ayudar a su familia a hacerse cargo de su futuro financiero. Artículos financieros, recursos para el aula para educadores y cursos autodirigidos le ayudan mucho a mejorar sus conocimientos financieros en cada etapa de la vida.” ',
        keys: ['Educación financiera', 'Sitios web de EF internacional', 'Blog']
      },
      {
        img: '/static/img/referencias/thumbnails/educ_financiera_5.jpg',
        title: 'Smart About Money',
        href: 'https://www.smartaboutmoney.org/',
        text: 'Free online courses. At your own pace. On your own time. You try to be physically healthy. Watch what you eat. Exercise. But are you doing the right things to be financially healthy too? Consider the experts at Smart About Money your partners in financial fitness and long-term wealth health.',
        keys: ['Educación financiera', 'Sitios web de EF internacional', 'Blog']
      }
    ]
  },
  {
    slug: 'emprendimiento',
    title: 'Emprendimiento',
    icon: '/static/img/referencias/themes/emprendimiento',
    links: [
      {
        img: '/static/img/referencias/thumbnails/emprendimiento_1.jpg',
        title: 'Explore, learn and enjoy The Spirit of Adventure!',
        href: 'http://cfeespiritofadventure.com/',
        text: 'Sitio web enfocado a promover el espíritu del emprendimiento con video-testimoniales y documentos de apoyo. The Spirit of Adventure is designed for youth; teachers/Instructors of entrepreneurship and enterprise education; start-up entrepreneurs; and for those interested in learning more… ',
        keys: ['emprendimiento']
      },
      {
        img: '/static/img/referencias/thumbnails/emprendimiento_2.jpg',
        title: '5 consejos de una mexicana para que alcances la libertad financiera',
        href: 'http://www.forbes.com.mx/5-consejos-de-una-mexicana-para-que-alcances-la-libertad-financiera/',
        text: 'La educación financiera no es una fórmula, no es un Excel, no es un plan de negocio; es una transformación mental que logra hacernos superar mitos, prejuicios y concepciones erradas sobre cómo nos comportamos con el dinero. ',
        keys: ['emprendimiento']
      },
      {
        img: '/static/img/referencias/thumbnails/emprendimiento_3.jpg',
        title: 'El emprendimiento, competencia clave a desarrollar en las aulas',
        href: 'http://www.educaciontrespuntocero.com/formacion/emprendimiento-competencia-clave-desarrollar-las-aulas/36087.html',
        text: 'Nota en la revista Educación 3.0 (España) “…En primaria, el emprendimiento se relaciona con conceptos como la capacidad de trabajo individual y en equipo, cuestiones metodológicas relativas a la utilización de las TIC, o bien el uso de competencias como la comunicativa o la matemática.”',
        keys: ['emprendimiento']
      }
    ]
  },
  {
    slug: 'fintech-y-herramientas-digitales-financieras',
    title: 'Fintech y herramientas digitales',
    icon: '/static/img/referencias/themes/fintech-y-herramientas-digitales-financieras',
    links: [
      {
        img: '/static/img/referencias/thumbnails/Fintech_1.jpg',
        title: 'App Afore Móvil',
        href: 'https://www.gob.mx/aforemovil',
        text: '“Con la nueva APP AFORE Móvil ten el control de tu ahorro para el retiro en tu cuenta AFORE, desde tu celular. Así de fácil, rápido y seguro.”',
        keys: ['fintech', 'ahorro para el retiro', 'app']
      },
      {
        img: '/static/img/referencias/thumbnails/Fintech_2.jpg',
        title: 'Mimolido, incentivo al ahorro ',
        href: 'http://www.mifutu.ro/',
        text: 'Presentación en plataforma (Chile). La aplicación Mimolido permite que los usuarios ahorren el vuelto cada vez que realizan una transacción con tarjeta de débito. La App es una solución a dos problemas, "uno es que las personas deben decidir entre gastar y ahorrar, el otro es que hay que ahorrar una vez al mes, lo que se transforma en un costo fijo"',
        keys: ['fintech', 'app']
      },
      {
        img: '/static/img/referencias/thumbnails/Fintech_3.jpg',
        title: 'My BlackRock Adventure',
        href: 'http://myblackrockadventure.com/',
        text: 'Entretenida herramienta digital para el diagnóstico y la orientación correspondiente sobre cómo mejorar su situación personal con el 401 (K) en EUA.',
        keys: ['fintech', 'ahorro para el retiro', 'simulador']
      },
      {
        img: '/static/img/referencias/thumbnails/Fintech_4.jpg',
        title: 'El Instituto Aviva lanza la nueva App “Mi dinero y yo” para fomentar la educación financiera de los más pequeños',
        href: 'http://www.instituto-aviva-de-ahorro-y-pensiones.es/corporativa/prensa/notas-prensa/el-instituto-aviva-lanza-la-nueva-app-mi-dinero-y-yo-para-fomentar-la-educacion-financiera-de-los-mas-pequenos',
        text: 'Nota. Presentación de App. "Mi dinero y yo" es un cuento interactivo que enseña a los niños los principios fundamentales del ahorro, del consumo responsable y del valor del dinero',
        keys: ['fintech', 'educacion financiera', 'ef para niños']
      },
      {
        img: '/static/img/referencias/thumbnails/Fintech_5.jpg',
        title: 'México: mayor mercado Fintech de la región',
        href: 'http://www.cioal.com/2016/09/01/mexico-mayor-mercado-fintech-de-la-region/',
        text: 'Nota. “México se ha convertido en el mayor mercado de empresas dedicadas a las tecnologías financieras (Fintech) en América Latina, al registrar un total de 158 empresas dedicadas a este creciente sector.”',
        keys: ['fintech']
      },
      {
        img: '/static/img/referencias/thumbnails/Fintech_6.jpg',
        title: 'MassMutual turns to video games to boost retirement savings',
        href: 'http://www.investmentnews.com/article/20160223/FREE/160229983/massmutual-turns-to-video-games-to-boost-retirement-savings',
        text: 'Nota. "FUTUREJET," is a video game available via mobile app meant to instill the importance of saving now for what seems to younger workers to be a far-off life event. The launch uses the concept of gamification, which has been accepted by many behavioral economists as a way to promote better savings habits among participants. ',
        keys: ['fintech']
      }
    ]
  },
  {
    slug: 'sitios-web-de-ef-internacional',
    title: 'Sitios web de educación financiera internacional',
    icon: '/static/img/referencias/themes/sitios-web-de-ef-internacional',
    alternate: false,
    links: [
      {
        img: '/static/img/referencias/thumbnails/australia.png',
        title: 'ASIC’s Money Smart. Financial guidance you can trust',
        href: 'https://www.moneysmart.gov.au/'
      },
      {
        img: '/static/img/referencias/thumbnails/belgica.png',
        title: 'Wikifin.be',
        href: 'http://www.wikifin.be/fr'
      },
      {
        img: '/static/img/referencias/thumbnails/brazil.png',
        title: 'Estrategia Nacional de Educação Financiera (ENEF)',
        href: 'http://www.vidaedinheiro.gov.br/'
      },
      {
        img: '/static/img/referencias/thumbnails/canada.png',
        title: 'Financial Consumer Agency of Canada',
        href: 'http://www.fcac-acfc.gc.ca/Fra/Pages/home-accueil.aspx'
      },
      {
        img: '/static/img/referencias/thumbnails/canada.png',
        title: 'Canadian Foundation for Economic Education (CFEE)',
        href: 'http://www.cfee.org/'
      },
      {
        img: '/static/img/referencias/thumbnails/canada.png',
        title: 'Your Money',
        href: 'http://yourmoney.cba.ca/'
      },
      {
        img: '/static/img/referencias/thumbnails/chile.png',
        title: 'Previsión para todos',
        href: 'http://www.previsionparatodos.cl/'
      },
      {
        img: '/static/img/referencias/thumbnails/españa.png',
        title: 'Finanzas para todos',
        href: 'http://www.finanzasparatodos.es/es/secciones/sobre'
      },
      {
        img: '/static/img/referencias/thumbnails/españa.png',
        title: 'Educación Financiera',
        href: 'http://edufinanciera.com/las-7-webs-de-educacion-financiera-para-jovenes-mas-divertidas-de-la-red/'
      },
      {
        img: '/static/img/referencias/thumbnails/españa.png',
        title: 'Edufinet',
        href: 'http://www.edufinet.com/edufinext/'
      },
      {
        img: '/static/img/referencias/thumbnails/españa.png',
        title: 'Finanzas para mortales',
        href: 'http://www.finanzasparamortales.es'
      },
      {
        img: '/static/img/referencias/thumbnails/usa.png',
        title: 'MYMONEY.GOV Financial Literacy and Education Commission',
        href: 'http://www.mymoney.gov/Pages/default.aspx'
      },
      {
        img: '/static/img/referencias/thumbnails/usa.png',
        title: 'Money as you grow. Consumer Financial Protection Bureau',
        href: 'https://www.consumerfinance.gov/es/el-dinero-mientras-creces/'
      },
      {
        img: '/static/img/referencias/thumbnails/usa.png',
        title: 'Practical Money Skills for Life (Visa)',
        href: 'http://www.practicalmoneyskills.com/'
      },
      {
        img: '/static/img/referencias/thumbnails/usa.png',
        title: 'Better Money Habbits. Bank of America in partnership with Khan Academy',
        href: 'https://www.bettermoneyhabits.com/index.html'
      },
      {
        img: '/static/img/referencias/thumbnails/francia.png',
        title: 'La finance pour tous',
        href: 'http://www.lafinancepourtous.com/'
      },
      {
        img: '/static/img/referencias/thumbnails/india.png',
        title: 'National Centre for Financial Education',
        href: 'http://www.ncfeindia.org/'
      },
      {
        img: '/static/img/referencias/thumbnails/irlanda.png',
        title: 'MakingCents',
        href: 'http://www.makingcents.ie/'
      },
      {
        img: '/static/img/referencias/thumbnails/mexico.png',
        title: 'Comisión Nacional para la Protección y Defensa de los Usuarios de Servicios Financieros',
        href: 'https://www.gob.mx/condusef'
      },
      {
        img: '/static/img/referencias/thumbnails/newzeland.png',
        title: 'Sorted (The Kiwi guide to money)',
        href: 'https://www.sorted.org.nz/'
      },
      {
        img: '/static/img/referencias/thumbnails/peru.png',
        title: 'Portal de Educación Financiera',
        href: 'http://www.sbs.gob.pe/0/home_educacion.aspx'
      },
      {
        img: '/static/img/referencias/thumbnails/reinounido.png',
        title: 'The Money Advice Service',
        href: 'https://www.moneyadviceservice.org.uk/en'
      },
      {
        img: '/static/img/referencias/thumbnails/dominicana.png',
        title: 'Milkcash',
        href: 'http://milkcash.do/users/login'
      },
      {
        img: '/static/img/referencias/thumbnails/chile.png',
        title: 'Sano de Lucas',
        href: 'http://sanodelucas.cl/'
      }
    ]
  }
]
