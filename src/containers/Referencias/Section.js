import React from 'react'
import style from './style.css'
import orderBy from 'lodash/orderBy'

export class Section extends React.Component {
  componentDidMount = () => this.props.referenceLoad(this.props.match.params.name)
  render = () => {
    const {general, references, section} = this.props
    const color = ['#925152', '#002453', '#47774C'][section]
    const topic = references.topic
    console.log(references);
    if (!topic) return null
    return (
      <div className={style.referencias}>
        <div className={style.header}
          style={{color, backgroundImage: `url(${topic.img})`}}>
          <div>
            <div style={{borderColor: color}}>
              <h1>{topic.title}</h1>
              <h2>{topic.text}</h2>
            </div>
          </div>
        </div>
        <div className={style.linkContainer}>
          {orderBy(topic.ReferenceItems, 'order', 'asc').map((link, i) => (
            <a
              key={i}
              className={style.link}
              href={link.url || link.href} target='_blank'
              style={{
                color,
                flexDirection: general.w < 900 ? 'column' : 'row',
                alignItems: general.w < 900 ? 'center' : 'flex-start'
              }}>
              <div style={{width: '200px', height: '200px', background: '#FFF'}}><img src={link.img} /></div>
              <div>
                <h3>{link.title} {link.alternate}</h3>
                <p>{link.text}</p>
              </div>
            </a>
          ))}
        </div>
        <div className={style.avisoLigas}>
          <p>
            Las ligas y los documentos incluidos en esta sección se consideran de acceso público
            al encontrarse disponibles en internet para la exploración de cualquier persona.
          </p>
          <p>
            Tienen la finalidad de proveer información al lector, así como incentivar la discusión
            y el debate de temas de educación financiera, previsional y en particular sobre el ahorro
            para el retiro en México.
          </p>
          <p>
            Sus contenidos, así como las conclusiones que de ellos se deriven, son responsabilidad
            exclusiva de sus autores y no reflejan necesariamente la opinión de la CONSAR.
          </p>
        </div>
        <img src={`/static/img/referencias/footer${section}.svg`} />
      </div>
    )
  }
}

export default Section
