import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as referencesActions from '../../actions/references'
import List from './List'
import Section from './Section'
import Layout from '../Layout/'
import seo from './seo'

class Referencias extends React.Component {
  static seo = seo
  componentDidMount = () => {
    this.props.referencesLoad()
    window.scrollTo(0, 0)
  }
  componentWillReceiveProps = next => {
    if (next.match.url !== this.props.match.url) {
      return window.scrollTo(0, 0)
    }
  }
  render = () => {
    const url = this.props.location.pathname.match(/jovenes|adultos|mayores/)
    const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
    console.log(this.props.references);
    return (
      <Layout {...this.props}>
        {this.props.match.params.name
          ? <Section {...{...this.props, section}} />
          : <List {...{...this.props, data: this.props.references.list, section}} />
        }
      </Layout>
    )
  }
}

export default connect(({general, references}) => ({general, references}),
  (dispatch) => bindActionCreators(referencesActions, dispatch))(Referencias)
