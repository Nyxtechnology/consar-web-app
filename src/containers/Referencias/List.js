import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'

const List = ({data, section}) => {
  const color = ['#925152', '#002453', '#47774C'][section]
  return (
    <div className={style.referencias}>
      <div className={style.header}
        style={{color, backgroundImage: `url(/static/img/referencias/banner.png)`}}>
        <div>
          <div style={{borderColor: color}}>
            <h1>Las buenas referencias</h1>
            <h2>
              Toma nota de documentos y casos de interés nacionales e internacionales
              sobre finanzas personales y educación previsional.
            </h2>
          </div>
        </div>
      </div>
      <div>
        <div className={style.cards}>
          <h2 style={{color}}>Para:</h2>
          <div>
            {data.filter(r => r.type === 'public').map((e, i) => (
              <div key={i}>
                <Link to={`referencias/${e.slug}`}>
                  {e.title}
                  <img src={e['img' + section]} />
                </Link>
              </div>
            ))}
          </div>
        </div>
        <div className={style.links}>
          <h2 style={{color}}>Sobre:</h2>
          <div>
            {data.filter(r => r.type === 'theme').map((e, i) => (
              <Link key={i} to={`referencias/${e.slug}`} style={{color}}>
                <div><img src={e['img' + section]} /></div>
                <div>
                  <h2>{e.title}</h2>
                  <p>{e.text}</p>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
      <img src={`/static/img/referencias/footer${section}.svg`} />
    </div>
  )
}

export default List
