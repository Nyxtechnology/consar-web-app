import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/articulos/como-me-registro-en-una-afore',
  'https://www.gob.mx/consar/articulos/quien-debe-entregarme-mi-estado-de-cuenta',
  'https://www.gob.mx/consar/documentos/nuevo-estado-de-cuenta-de-afore-23119',
  'https://www.gob.mx/consar/articulos/conoce-los-formatos-para-registro-y-cambio-de-afore',
  'https://www.gob.mx/consar/articulos/correccion-y-actualizacion-de-datos',
  'https://www.gob.mx/consar/videos/registro'
]

const Step2 = ({color, deselect, view}) => (
  <div className={style.sectionContent} id='step3'>
    <div>
      <h3 style={{color}}>3. Registro en una AFORE, corrección y actualización de datos</h3>
      <p>
        La mejor manera de que controles y sepas cuánto dinero llevas ahorrado en tu Cuenta AFORE
        es “con papelito en mano” y ese documento llamado estado de cuenta te puede llegar cómodamente
        a tu domicilio, tres veces al año…  pero siempre y cuando la AFORE tenga tus datos vigentes, como
        tu dirección actual y completa; si no es así debes acudir a tu AFORE a actualizarlos.
      </p>
      <p>
        Igual que la dirección, todos los datos que registra la AFORE son importantes; algunos para
        identificación o seguridad y otros para la realización de trámites diversos. Por eso es necesario
        que registres tus datos en la AFORE que elijas para la administración de
        tu cuenta. Puedes acelerar el trámite como se explica
        en la sección <Link {...{color, href: links[0]}}>¿Cómo me registro en una AFORE?</Link>
      </p>
      <p>
        A semejanza de los trámites y documentos importantes para toda la vida, debes informar a tu
        AFORE cada vez que cambies algún dato de tu registro, para que te pueda contactar y atender
        adecuadamente a lo largo del tiempo.
      </p>
      <ul>
        <li><Link {...{color, href: links[1]}}>¿Quién debe entregarme mi estado de cuenta?</Link></li>
        <li><Link {...{color, href: links[2]}}>¿Cómo leer mi estado de cuenta?</Link></li>
        <li><Link {...{color, href: links[3]}}>Conoce los formatos para registro y cambio de AFORE</Link></li>
        <li><Link {...{color, href: links[4]}}>Corrección y actualización de datos</Link></li>
        <li><Link {...{color, href: links[5]}}>VIDEOINFOGRAFÍA: Registro</Link></li>
      </ul>
    </div>
  </div>
)

export default Step2
