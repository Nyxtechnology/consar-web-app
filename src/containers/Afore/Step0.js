import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/articulos/como-funciona-operativamente-el-sistema-de-ahorro-para-el-retiro',
  'https://www.gob.mx/consar/videos/entendiendo-a-las-afores'
]

const Step0 = props => {
  const {type} = props
  const color = ['#925152', '#002453', '#47774C'][type]
  const backgroundColor = ['#e88e82', '#60CFF1', '#87C400'][type]

  return (
    <div id='step0'>
      <div className={style.intro} style={{backgroundColor}}>
        Te damos la bienvenida a este espacio diseñado para conozcas información
        clave del Sistema de Ahorro para el Retiro (SAR), y sepas lo esencial sobre
        cómo manejar tu cuenta de ahorro para el retiro.
      </div>
      <div className={style.content}>
        <h2 style={{borderBottom: '3px solid ' + color}}>
          El Sistema de Ahorro para el Retiro y la Cuenta AFORE
        </h2>
        <p>
          Debido a que las cuotas que los trabajadores aportan a las instituciones
          de Seguridad Social ya no son suficientes para pagar las pensiones de un
          creciente número de pensionados que además tienen una mayor esperanza de
          vida, es decir, vivirán jubilaciones más largas, el sistema de pensiones
          mexicano, como el de muchos otros países, se reformó hacia esquemas donde
          cada trabajador debe financiar su propia pensión. El Sistema de Ahorro
          para el Retiro (SAR) es un sistema diseñado para fomentar el ahorro de los
          mexicanos, donde el trabajador lleva las riendas de su ahorro.
        </p>
        <p>
          La cuenta individual de ahorro para el retiro es administrada por una
          Administradora de Fondos para el Retiro (AFORE), por lo que también se
          le conoce como Cuenta AFORE. Es única, personal, da a ganar rendimientos
          superiores a prácticamente los de cualquier otro instrumento de ahorro
          disponible para los trabajadores, y es heredable, pues constituye un
          importante patrimonio de cada trabajador.
        </p>
        <ul>
          <li>
            <a target='_blank' href={links[0]} style={{color}}>
              BLOG CONSAR: ¿Cómo funciona operativamente el Sistema de Ahorro para el Retiro?
            </a>
          </li>
          <li>
            <a target='_blank' href={links[1]} style={{color}}>
              VIDEOINFOGRAFÍA: Entendiendo a las AFORE
            </a>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default Step0
