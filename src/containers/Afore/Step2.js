import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/acciones-y-programas/como-elegir-la-mejor-afore',
  'https://www.gob.mx/consar/articulos/como-me-cambio-de-afore',
  'https://www.gob.mx/consar/videos/traspasos'
]
const Step1 = ({color, deselect, view}) => (
  <div className={style.sectionContent} id='step2'>
    <div>
      <h3 style={{color}}>2. Comparar y elegir la mejor AFORE </h3>
      <p>
        Para poder registrar tu cuenta de ahorro en una AFORE, primero debes elegir
        la AFORE que a tu parecer administrará mejor tu dinero.
      </p>
      <p>
        Existen 11 Administradoras de Fondos para el Retiro (AFORE) de dónde escoger. Para
        poder cotejarlas, la CONSAR pone a tu disposición tablas comparativas sobre tres
        factores importantes de cada AFORE:
      </p>
      <ul>
        <li>Los <strong>rendimientos</strong> que da, porque tu dinero ahorrado en la cuenta AFORE te genera ganancias.</li>
        <li>Los <strong>servicios</strong> que ofrece, considerando la cercanía, rapidez, efectividad y calidad.</li>
        <li>La <strong>comisión</strong> que cobra, porque administrar las cuentas de ahorro implica costos de operación para la AFORE.</li>
      </ul>
      <p>
        Por supuesto, la AFORE que conviene es la que más hace crecer tu dinero. El Indicador de
        Rendimiento Neto (IRN) es la resta simple del rendimiento que te da la AFORE (las ganancias) menos
        la comisión que te cobra (por administrar, resguardar e invertir tu dinero). Por cierto, el
        rendimiento neto de tu cuenta AFORE es superior a la inflación, por lo que tu ahorro realmente
        crece con el tiempo. La información y las tablas comparativas del IRN están disponibles en la
        sección <Link {...{color, href: links[0]}}>¿Cómo elegir la mejor AFORE?</Link>
      </p>
      <ul>
        <li><Link {...{color, href: links[1]}}>¿Cómo me cambio de AFORE?</Link></li>
        <li><Link {...{color, href: links[2]}}>VIDEOINFOGRAFÍA: Traspasos</Link></li>
      </ul>
    </div>
  </div>
)

export default Step1
