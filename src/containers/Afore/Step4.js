import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/acciones-y-programas/calculadoras-de-ahorro-y-retiro?idiom=es',
  'https://www.gob.mx/ahorrade10en10/articulos/cuanto-dinero-debo-ahorrar-para-alcanzar-una-buena-pension-parte-i-54271',
  'https://www.gob.mx/consar/articulos/cuanto-dinero-debo-ahorrar-para-alcanzar-una-buena-pension-parte-ii',
  'https://www.gob.mx/consar/articulos/recibiran-en-su-domicilio____-millones-de-ahorradores-un-calculo-personalizado-de-pension-146033?idiom=es'
]

const Step3 = ({color, deselect, view}) => (
  <div className={style.sectionContent} id='step4'>
    <div>
      <h3 style={{color}}>4. Planear la pensión y calcular el ahorro</h3>
      <p>
        Este punto es para imaginar, calcular y aterrizar el objetivo: La pensión
        que queremos y por lo tanto el ahorro que necesitamos hacer.
      </p>
      <p>
        La pregunta ¿cuánto recibiré al mes cuando me retire? o dicho de otro modo ¿de cuánto
        será mi pensión? tiene una respuesta sencilla: Depende directamente de lo que juntemos
        ahorrando antes de pensionarnos. Y para eso vale la pena preguntarse ¿Qué porcentaje de
        lo que gano hoy se convertirá en lo que me pagaré a mí mismo(a) mañana, con mis ahorros
        y los rendimientos de la cuenta AFORE?
      </p>
      <p>
        Las <Link {...{color, href: links[0]}}>Calculadoras de ahorro para el retiro</Link> sirven para
        que tengas ejemplos con cantidades y resultados concretos. Hay que combinar tus datos laborales
        y algunas variables, como la edad de retiro, la administradora en la que estás, entre otras. El
        objetivo es saber cuánto deseamos obtener de pensión y comparar esa cantidad con lo que en realidad
        queremos ahorrar y podemos acumular. Eso nos dirá qué tan cerca o lejos estamos de ahorrar lo necesario, y a
        su vez, hacer los ajustes que más nos convengan, por ejemplo, con Ahorro Voluntario.
      </p>
      <ul>
        <li><Link {...{color, href: links[3]}}>BOLETÍN DE PRENSA: Recibirán en su domicilio 21 millones de ahorradores un cálculo personalizado de pensión</Link></li>
        <li><Link {...{color, href: links[1]}}>BLOG CONSAR: ¿Cuánto dinero debo ahorrar para alcanzar una buena pensión? (Parte I)</Link></li>
        <li><Link {...{color, href: links[2]}}>BLOG CONSAR: ¿Cuánto dinero debo ahorrar para alcanzar una buena pensión? (Parte II)</Link></li>
      </ul>
    </div>
  </div>
)

export default Step3
