import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/articulos/como-se-invierten-mis-recursos',
  'https://www.gob.mx/consar/articulos/entendiendo-los-ciclos-naturales-de-los-mercados-financieros-y-como-impactan-en-mi-cuenta-afore',
  'https://www.gob.mx/consar/articulos/los-beneficios-de-no-poner-todos-los-huevos-en-la-misma-canasta',
  'https://www.gob.mx/consar/videos/regimen-de-inversion',
  'https://www.gob.mx/consar/articulos/por-que-suben-y-bajan-los-rendimientos-en-el-sar?idiom=es'
]
const Step5 = ({color, deselect, view}) => (
  <div className={style.sectionContent} id='step6'>
    <div>
      <h3 style={{color}}>6. Invertir para ganar rendimientos</h3>
      <p>
        Ahorrar para el retiro implica acumular la mayor cantidad posible de dinero y es por eso que la
        cuenta AFORE es una cuenta de inversión. Para que tus recursos generen ganancias y crezcan más
        rápido, es necesario invertirlos. Tratándose de los ahorros para el futuro de los trabajadores, la
        CONSAR vigila el mejor equilibrio entre la seguridad de las inversiones y el mayor rendimiento posible.
      </p>
      <p>
        De hecho, a diferencia del ahorro en una cuenta de banco, los ahorros de la cuenta AFORE permanecerán
        invertidos por muchos años hasta que nos retiremos, en 15, 20, 30 o más años. Invertir a largo plazo genera
        los mayores rendimientos. Es por eso que mientras más pronto empieces a ahorrar para el retiro, mayores
        serán los rendimientos en tu ahorro acumulado, al momento de pensionarte.
      </p>
      <ul>
        <li><Link {...{color, href: links[0]}}>¿Cómo se invierten mis recursos?</Link></li>
        <li><Link {...{color, href: links[1]}}>BLOG CONSAR: Entendiendo los ciclos naturales de los mercados financieros y cómo impactan en mi cuenta de AFORE</Link></li>
        <li><Link {...{color, href: links[2]}}>BLOG CONSAR: Los beneficios de no poner todos los huevos en la misma canasta</Link></li>
        <li><Link {...{color, href: links[3]}}>VIDEOINFOGRAFÍA: Régimen de inversión</Link></li>
        <li><Link {...{color, href: links[4]}}>VIDEOINFOGRAFÍA: ¿Por qué suben y a veces bajan los rendimientos en mi AFORE?</Link></li>
      </ul>
    </div>
  </div>
)

export default Step5
