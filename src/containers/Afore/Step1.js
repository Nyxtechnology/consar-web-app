import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/acciones-y-programas/en-que-afore-estoy-56776',
  'https://www.gob.mx/consar/documentos/medios-para-contactar-a-tu-afore',
  'https://www.gob.mx/consar/articulos/unificacion-de-cuentas-sar-issste-afore',
  'https://www.gob.mx/consar/acciones-y-programas/trabajadores-independientes-30078?idiom=es'
]

const Step0 = ({color, deselect, view, type}) => (
  <div className={style.sectionContent} id='step1'>
    <div>
      <h3 style={{color}}>1. Apertura o localización de cuentas</h3>
      <p>
        Por ley, todo trabajador del sector formal tiene derecho a ciertas prestaciones
        de seguridad social. Para cumplir con ello, los patrones y las dependencias
        gubernamentales deben afiliar a sus empleados al Seguro Social (IMSS o ISSSTE). En
        ese momento se crea la cuenta individual del trabajador, que está ligada a su Número
        de Seguridad Social (NSS) y/o a su CURP, pero aún falta que el trabajador “le ponga
        nombre y apellido”, es decir,  la registre con sus datos personales en la AFORE de su elección.
      </p>
      <p>
        Si por empleos anteriores crees que ya existe tu cuenta AFORE, localízala siguiendo
        las indicaciones de la sección <Link {...{color, href: links[0]}}>¿En qué AFORE estoy?</Link> A
        los nuevos afiliados al ISSSTE sin registro previo, automáticamente se les apertura su
        cuenta en la AFORE PENSIONISSSTE.
      </p>
      <p>
        Durante tu vida laboral, en esa cuenta se acumulan las aportaciones del patrón, del Gobierno
        Federal y las tuyas como trabajador, destinadas a tu ahorro para el retiro. Si trabajas
        independiente y no cotizas al IMSS o ISSSTE, también puedes abrir tu cuenta AFORE y ahorrar
        voluntariamente en ella, obteniendo las mismas ganancias que cualquier ahorrador del SAR.
      </p>
      <ul>
        <li><Link {...{color, href: links[1]}}>Medios para contactar a tu AFORE</Link></li>
        <li><Link {...{color, href: links[2]}}>Unificación de cuentas del SAR: IMSS-ISSSTE</Link></li>
        <li><Link {...{color, href: links[3]}}>Trabajadores independientes</Link></li>
      </ul>
    </div>
  </div>
)

export default Step0
