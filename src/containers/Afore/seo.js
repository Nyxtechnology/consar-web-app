
export default {
  title: '¿Guía para el manejo básico de la cuenta AFORE | CONSAR',
  description: 'Todo sobre tu cuenta afore ¿En que Afore estoy? Cuánto dinero tengo en mi afore? Como funciona? Que afore seleccionar? Como sacar el mayor provecho? CONSAR ',
  ogtitle: '¿Guía para el manejo básico de la cuenta AFORE | CONSAR',
  ogdescription: 'Todo sobre tu cuenta afore ¿En que Afore estoy? Cuánto dinero tengo en mi afore? Como funciona? Que afore seleccionar? Como sacar el mayor provecho? CONSAR ',
  keywords: [
    'CONSAR',
    'consar afore',
    'Cuenta AFORE',
    'ahorro para el retiro',
    'ahorro voluntario',
    'en que afore estoy',
    'consultar afore',
    'rendimientos afores',
    'como checar mi afore',
    'cuanto tengo en mi afore',
    'como saber mi afore',
    'como retirar dinero de mi afore',
    'como saber en que afore estoy'
  ]
}
