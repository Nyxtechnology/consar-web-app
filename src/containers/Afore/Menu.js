import React from 'react'
import style from './style.css'

const links = [
  'https://www.gob.mx/consar/articulos/como-funciona-operativamente-el-sistema-de-ahorro-para-el-retiro',
  'https://www.gob.mx/consar/videos/entendiendo-a-las-afores'
]

const AforeMenu = (props) => {
  const {hover, type} = props
  const color = ['#925152', '#002453', '#47774C'][type]
  const backgroundColor = ['#e88e82', '#60CFF1', '#87C400'][type]
  const getPos = (ry, rx, n) => ({
    a: (360 / 8 * n) - (45 * 2),
    y: ry * Math.sin((Math.PI * 2 / 8) * n - (0.785 * 2)) + 42,
    x: rx * Math.cos((Math.PI * 2 / 8) * n - (0.785 * 2)) + 46.9
  })
  const getArrowStyle = (n) => {
    const {a, y, x} = getPos(31.5, 17.5, n)
    return {top: y + '%', left: x + '%', transform: `rotate(${a}deg)`}
  }
  const getTextStyle = (n) => {
    const {a, y, x} = getPos(48, 36, n)
    return {
      top: y + '%',
      left: x + '%',
      flexDirection: a === 90 ? 'column' : a < 90 ? 'row' : 'row-reverse'
    }
  }
  const items = [
    'Introducción',
    'Apertura o localización de cuentas del SAR',
    'Comparar y elegir la mejor AFORE',
    'Registro, correción y actualización de datos',
    'Planear la pensión y calcular el ahorro',
    'Aportaciones por ley y ahorro voluntario',
    'Invertir para ganar rendimientos',
    'Retiros parciales y retiro total de recursos'
  ]

  return (
    <div>
      <div className={style.header} style={{color}}>
        <h1>El manejo básico de la Cuenta AFORE</h1>
        <h2>
          Conoce cómo funciona la Cuenta AFORE y saca el mayor provecho de tu ahorro para el retiro.
        </h2>
      </div>
      <div id='menu' className={style.menuContainer}>
        <div className={style.menuCircle}><img src={`/static/img/afore/circle${type}.png`} /></div>
        {items.map((d, i) => [
          <div
            onMouseEnter={e => props.enter(i)}
            onMouseLeave={e => props.leave(i)}
            onClick={() => props.select('step' + i)}
            className={style.menuOptionText}
            style={getTextStyle(i)}>
            {i === 0 ? null : <i style={{color: ['#ED6C5C', '#3AC2ED', '#88C227'][type]}}>{i}.</i>}
            <div>{d}</div>
          </div>,
          <div
            onMouseEnter={e => props.enter(i)}
            onMouseLeave={e => props.leave(i)}
            onClick={() => props.select('step' + i)}
            className={style.menuOptionArrow}
            style={getArrowStyle(i)}>
            <img src={`/static/img/afore/arrow${type}${hover === i ? 'a' : ''}.svg`} />
          </div>
        ])}
      </div>
    </div>
  )
}

export default AforeMenu
