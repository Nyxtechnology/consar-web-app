import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/articulos/retiros-parciales-97089?state=draft',
  'https://www.gob.mx/consar/articulos/retiro-parcial-por-desempleo-issste?state=draft',
  'https://www.gob.mx/consar/articulos/donde-consulto-mi-historial-de-cotizaciones-y-aportaciones',
  'https://www.gob.mx/consar/articulos/todo-sobre-el-retiro-100587?idiom=es',
  'https://www.gob.mx/consar/articulos/retiro-por-desempleo-mas-informacion-para-una-mejor-decision',
  'https://www.gob.mx/consar/videos/retiros-por-desempleo-y-matrimonio',
  'https://www.gob.mx/consar/videos/beneficiarios'
]

const Step6 = ({color, deselect, view}) => (
  <div className={style.sectionContent} id='step7'>
    <div>
      <h3 style={{color}}>7. Retiros parciales y retiro total de recursos</h3>
      <p>
        El objetivo de la cuenta AFORE es el ahorro constante hasta que el trabajador
        se jubile, momento en que esos recursos servirán para proporcionarle una pensión. En
        el camino, hay dos retiros parciales del dinero ahorrado que pueden ayudar a los
        trabajadores del sector formal en aprietos económicos, porque quedaron desempleados, y
        sólo en el caso de los que cotizan al IMSS, como ayuda de gastos para contraer matrimonio.
      </p>
      <p>
        Consulta los requisitos y las condiciones para realizar retiros parciales por desempleo y
        el retiro parcial por matrimonio en la información específica
        para <Link {...{color, href: links[0]}}>Retiros parciales IMSS</Link> y
        <Link {...{color, href: links[1]}}> Retiros parciales ISSSTE.</Link>
      </p>
      <p>
        Al acercarse los 60 o 65 años, deberás informarte muy bien sobre las alternativas que
        tienes para la disposición de tus recursos:
      </p>
      <ul>
        <li>
          Por tus características como ahorrador del SAR: Décimo Transitorio, Generación de
          Transición, Mixto (IMSS-ISSSTE), Generación AFORE.
        </li>
        <li>
          Por los diversos regímenes, tipos y modalidades de pensión que existen.
        </li>
        <li>
          Por tu número de semanas cotizadas y el saldo acumulado al retiro, y lo que ello te permita obtener como pensión.
        </li>
      </ul>
      <ul>
        <li><Link {...{color, href: links[2]}}>¿Dónde consulto mi historial de cotizaciones y aportaciones?</Link></li>
        <li><Link {...{color, href: links[3]}}>Todo sobre el Retiro de recursos del SAR</Link></li>
        <li><Link {...{color, href: links[4]}}>BLOG CONSAR: Retiro por desempleo, más información para una mejor decisión</Link></li>
        <li><Link {...{color, href: links[5]}}>VIDEOINFOGRAFÍA: Retiros por desempleo y matrimonio</Link></li>
        <li><Link {...{color, href: links[6]}}>VIDEOINFOGRAFÍA: Beneficiarios</Link></li>
      </ul>
      <p>
        ¿Quieres contactar a tu AFORE? Consulta los medios por los que te puedes poner
        en contacto con ella <a target='_blank' href='https://www.gob.mx/consar/articulos/contacta-a-tu-afore'>aquí</a>.
      </p>
    </div>
  </div>
)

export default Step6
