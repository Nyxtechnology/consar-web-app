import React from 'react'
import style from './style.css'
import Menu from './Menu'
import Step0 from './Step0'
import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'
import Step4 from './Step4'
import Step5 from './Step5'
import Step6 from './Step6'
import Step7 from './Step7'
import Layout from '../Layout/'
import document from 'global/document'
import seo from './seo'

class Afore extends React.Component {
  static seo = seo
  state = {view: null, hover: null, scroll: 0}
  componentDidMount = () => {
    document.addEventListener('scroll', this.setScroll)
    window.scrollTo(0, 0)
  }
  componentWillUnmount = () => document.removeEventListener('scroll', this.setScroll)
  select = view => {
    console.log(view)
    document.body.scrollTo(0, document.getElementById(view).offsetTop + 100)
  }
  deselect = () => this.setState({view: null})
  enter = hover => this.setState({hover})
  leave = () => this.setState({hover: null})
  setScroll = e => this.setState({scroll: document.body.scrollTop > document.getElementById('step0').offsetTop})
  render = () => {
    const {select, deselect, state, enter, leave, props} = this
    const url = props.location.pathname.match(/jovenes|adultos|mayores/)
    const type = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
    const color = ['#ED6C5C', '#3AC2ED', '#88C227'][type]
    const pprops = {...props, ...state, type, select, deselect, color, enter, leave}
    return (
      <Layout {...props}>
        <div ref='afore' className={style.afore}>
          <div>
            <Menu {...pprops} />
            <Step0 {...pprops} />
            <Step1 {...pprops} />
            <Step2 {...pprops} />
            <Step3 {...pprops} />
            <Step4 {...pprops} />
            <Step5 {...pprops} />
            <Step6 {...pprops} />
            <Step7 {...pprops} />
            {state.scroll
              ? <div
                onClick={() => this.select('menu')}
                className={style.arrow}><i style={{background: color}} className='fa fa-arrow-up' /></div>
              : null}
          </div>
        </div>
      </Layout>
    )
  }
}

export default Afore
