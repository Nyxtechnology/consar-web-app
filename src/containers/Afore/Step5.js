import React from 'react'
import style from './style.css'
import Link from './Link'

const links = [
  'https://www.gob.mx/consar/acciones-y-programas/todo-sobre-el-ahorro-voluntario',
  'https://www.gob.mx/consar/articulos/mas-ahorro-mas-pension',
  'https://www.gob.mx/consar/articulos/que-factores-determinan-mi-pension-una-vision-sobre-las-tasas-de-reemplazo',
  'https://www.gob.mx/consar/videos/aportaciones-voluntarias',
  'https://www.gob.mx/consar/videos/aportaciones-voluntarias-2'
]

const Step4 = ({color, deselect, view}) => (
  <div className={style.sectionContent} id='step5'>
    <div>
      <h3 style={{color}}>5. Aportaciones por ley y ahorro voluntario</h3>
      <p>
        Hay dos tipos de ahorro en la cuenta AFORE:
      </p>
      <p>
        Las aportaciones de ley son obligatorias, se “descuentan” (transfieren) mensualmente
        de tu nómina y se depositan cada bimestre en tu cuenta AFORE. En el mejor de los casos
        y aportando durante toda la vida laboral, este ahorro por ley sumará lo necesario para
        que obtengas una pensión mensual aproximada del 30% de tu último salario base; eso es
        menos de la mitad de tu salario. ¿Te parece poco? Pues entonces es tu decisión y responsabilidad
        incrementar tu ahorro para el retiro y, por supuesto, te conviene.
      </p>
      <p>
        <Link {...{color, href: links[0]}}>Todo sobre el Ahorro Voluntario.</Link>  El Ahorro Voluntario es
        el que realizas por iniciativa e interés propio. No hay mínimos ni máximos y no se requieren aportaciones
        periódicas. En la modalidad de corto plazo puedes hacer retiros a los dos o seis meses, dependiendo de la
        AFORE que administra tu cuenta. Cuando tu Ahorro Voluntario es a largo plazo o “complementario”, obtienes
        mejores rendimientos, beneficios fiscales y lo más importante es que incrementas tu ahorro para el
        retiro, con lo que lograrás una mejor pensión.
      </p>
      <ul>
        <li><Link {...{color, href: links[1]}}>BLOG CONSAR: MÁS AHORRO = MÁS PENSIÓN. ¿De cuánto será mi pensión cuando me retire? Una estimación personalizada</Link></li>
        <li><Link {...{color, href: links[2]}}>BLOG CONSAR: ¿Qué factores determinan mi pensión? Una visión sobre las tasas de reemplazo</Link></li>
        <li><Link {...{color, href: links[3]}}>VIDEOINFOGRAFÍA: Aportaciones voluntarias</Link></li>
        <li><Link {...{color, href: links[4]}}>VIDEOINFOGRAFÍA: Aportaciones voluntarias II</Link></li>
      </ul>
    </div>
  </div>
)

export default Step4
