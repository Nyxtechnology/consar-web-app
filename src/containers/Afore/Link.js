import React from 'react'

const Link = (props) => <a {...{target: '_blank', style: {color: props.color}, ...props}} />
export default Link
