
export default {
  title: '¿Tienes entre 18 y 27 años? Planea la aventura de tu vida | CONSAR',
  description: 'La CONSAR te ayuda en planear tu futuro y alcanzar las metas que te mueven: Realizar estudios superiores, Obtener el empleo que quiero, Comprar un auto etc',
  ogtitle: '¿Tienes entre 18 y 27 años? Planea la aventura de tu vida | CONSAR',
  ogdescription: 'La CONSAR te ayuda en planear tu futuro y alcanzar las metas que te mueven: Realizar estudios superiores, Obtener el empleo que quiero, Comprar un auto etc',
  keywords: [
    'Consar',
    'Aventura de mi vida',
    'Afore',
    'metodos de ahorro',
    'Comprar auto',
    'Libertad financiera',
    'Primer empleo',
    'Estudiar en la universidad',
    'Vivir en pareja',
    'Planificar viaje',
    'Consolidar mi pasatiempo',
    'Iniciar mi negocio'
  ]
}
