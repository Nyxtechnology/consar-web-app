import React from 'react'
import style from './style.css'
import Post from '../../../components/Post/'

const posts = [
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=197',
    img: '/static/img/posts/rojo/1.png',
    title: 'La diferencia entre ahorrar e invertir',
    desc: ''
  },
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=206',
    img: '/static/img/posts/rojo/2.png',
    title: '¿Qué significa ser trabajador formal?',
    desc: ''
  },
  {
    link: 'http://www.mifuturoconsentido.gob.mx/wordpress2/?p=218',
    img: '/static/img/posts/rojo/3.png',
    title: 'Los millenials en el sistema de ahorro para el retiro',
    desc: ''
  }
]

const Blog = (props) => (
  <div className={style.blog}>
    <Post {...{type: 0, posts, more: 'http://www.mifuturoconsentido.gob.mx/wordpress2/'}} />
  </div>
)

export default Blog
