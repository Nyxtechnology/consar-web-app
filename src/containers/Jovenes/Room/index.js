import React from 'react'
import style from './style.css'
import {Link} from 'react-router-dom'
import Icon from '../../../components/Icon/'
import random from 'lodash/random'
import data from '../../../components/MenuList/data'

const kdata = data().reduce((a, b, i) => ({...a, [b.img]: b}), {})

const cloudCfg = () => ({
  className: style.cloud,
  icon: '/joven/cloud' + random(1, 2) + '.svg',
  speed: random(1, 5),
  style: {
    width: random(2, 4) + '%',
    top: random(0, 8) + '%',
    left: random(0, 100) + '%'
  }
})
const clouds = [...Array(parseInt(10)).keys()].map((i) => <Icon key={i} {...cloudCfg()} />)

const Room = (props) => {
  const isNotStep1 = props.step !== 1
  const isNotStep2 = props.step !== 2
  const isStep2 = props.step === 2
  const applyClass = (cond, class1, class2) => cond ? class1 + ' ' + class2 : class1
  return (
    <div className={style.room}>
      <Icon className={applyClass(isNotStep2, style.fondoRojo, style.fondoRojoHidden)} icon='/joven/fondoRojo.svg' />
      <div className={applyClass(isNotStep2, style.cloudsCont, style.cloudsContHidden)}>{clouds}</div>
      <Icon className={applyClass(!isStep2, style.icon, style.iconRightOut)} icon='/joven/ARBOL.svg' style={{width: '3.5%', top: '25.5%', right: '20%'}} />
      <Icon className={applyClass(!isStep2, style.icon, style.iconLeftOut)} icon='/joven/ARBOL.svg' style={{width: '4%', top: '22%', left: '26%'}} />
      <Icon className={applyClass(!isStep2, style.icon, style.iconLeftOut)} icon='/joven/ARBOL.svg' style={{width: '8%', top: '30%', left: '15%'}} />
      <Icon className={applyClass(!isStep2, style.icon, style.iconRightOut)} icon='/joven/ARBOL.svg' style={{width: '12%', top: '28%', right: '8%'}} />
      <div className={style.autoContainer}>
        {props.hover
          ? <div className={style.textContainer}>
            <h2>{props.hover.name}</h2>
            <p>{props.hover.desc}</p>
          </div>
        : null}
        <Icon className={applyClass(!isStep2, style.auto, style.autoHidden)} icon='/joven/AUTO.svg' />
      </div>
      <Link
        onMouseEnter={e => props.setHover(kdata['referencias'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.referencias, style.referenciasOut)}
        to='/jovenes/referencias'>
        <Icon icon='/joven/referencias.png' />
      </Link>
      <Link
        onMouseEnter={e => props.setHover(kdata['movil'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.movil, style.movilOut)}
        to='/app'>
        <Icon icon='/joven/movil.png' />
      </Link>
      <Link
        onMouseEnter={e => props.setHover(kdata['proyectos'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.destinos, style.destinosOut)}
        to='/jovenes/proyectos'>
        <Icon icon='/joven/destinos.png' />
      </Link>
      <Link
        onMouseEnter={e => props.setHover(kdata['guia'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.guia, style.guiaOut)}
        to='/jovenes/guia'>
        <Icon icon='/joven/guia.png' />
      </Link>
      <Link
        onMouseEnter={e => props.setHover(kdata['cursos'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.cursos, style.cursosOut)}
        to='/jovenes/referencias/Cursos-de-educacion-financiera-SyayMQYBf'>
        <Icon icon='/joven/cursos.png' />
      </Link>
      <Link
        onMouseEnter={e => props.setHover(kdata['afore'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.cuenta, style.cuentaOut)}
        to='/jovenes/afore'>
        <Icon icon='/joven/cuenta.png' />
      </Link>
      <Link
        onMouseEnter={e => props.setHover(kdata['calculadora'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.calculadora, style.calculadoraOut)}
        to='/jovenes/calculadora'>
        <Icon icon='/joven/calculadora.png' />
      </Link>
      <Link
        onMouseEnter={e => props.setHover(kdata['mapeo'])} onMouseLeave={e => props.setHover(null)}
        className={applyClass(isStep2, style.itinerario, style.itinerarioOut)}
        to='/jovenes/mapeo'>
        <Icon icon='/joven/mapeo.png' style={{position: 'static', stroke: 'red'}} />
      </Link>
      <svg className={style.circle1} style={{transform: isNotStep1 ? 'translateY(-200%)' : 'translateY(0)'}}>
        <circle cx='50%' cy='50%' r='50%' />
      </svg>
      <svg className={style.circle2} style={{transform: isNotStep1 ? 'translateY(-350%)' : 'translateY(0)'}}>
        <circle cx='50%' cy='50%' r='50%' />
      </svg>
      <svg className={style.circle3} style={{transform: isNotStep1 ? 'translateY(-400%)' : 'translateY(0)'}}>
        <circle cx='50%' cy='50%' r='50%' />
      </svg>
      <img
        className={style.lamp} src={'/static/img/joven/lamp.svg'}
        style={{transform: isNotStep1 ? 'translateY(-100%)' : 'translateY(0)'}} />
      <img
        className={style.desktop} src='/static/img/joven/desktop.svg'
        style={{transform: isNotStep1 ? 'translateX(-100vw)' : 'translateX(0)'}} />
      <img
        className={style.books} src='/static/img/joven/books.svg'
        style={{transform: isNotStep1 ? 'translateX(-250%)' : 'translateX(0)'}} />
      <img
        className={style.bed} src='/static/img/joven/bed.svg'
        style={{transform: isNotStep1 ? 'translateX(150%)' : 'translateX(0)'}} />
      <img
        className={style.dude} src='/static/img/c1827.png'
        style={{transform: isNotStep1 ? 'translateX(220%)' : 'translateX(0)'}} />
      <img
        onClick={props.next} className={style.poster} src='/static/img/joven/poster.svg'
        style={{transform: isNotStep1 ? 'translateX(-700%)' : 'translateX(0)'}} />
      <div
        className={style.text1}
        style={{transform: isNotStep1 ? 'translateY(200%)' : 'translateY(0)'}}>
        <h1>La Aventura de mi Vida</h1>
        <h2>
          Estás en un momento de decisiones y experiencias. ¿Qué es lo que te motiva
          ahora y le dará forma a tu futuro?  Para ponerte a prueba sólo hay un requisito: Hazlo.
        </h2>
      </div>
    </div>
  )
}

export default Room
