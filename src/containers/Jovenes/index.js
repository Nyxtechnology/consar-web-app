import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as generalActions from '../../actions/general'
import style from './style.css'
import Video from '../../components/Video/'
import Room from './Room/'
import Blog from './Blog'
import Menu from '../../components/MenuList/'
import Layout from '../Layout/'
import seo from './seo'

class Jovenes extends React.Component {
  static seo = seo
  state = {process: false, hover: null}
  next = () => {
    this.props.closeVideo(0)
    this.props.setStep({step: this.props.general.step + 1, startTime: new Date()})
  }
  setHover = hover =>
    this.props.general.startTime.getTime() + 12000 < (new Date()).getTime() ? this.setState({hover}) : null
  render = () => {
    const {step, startTime} = this.props.general
    return (
      <Layout {...this.props}>
        <div className={style.container}>
          <Video
            toggle={this.state.process ? null : this.next}
            step={step === 0}
            url='0m1Wj3djeV4' />
          <div className={style.acontainer}>
            <Room {...{next: this.next, setHover: this.setHover, ...this.state, ...this.props, step, startTime}} />
          </div>
          <Menu {...{type: 0, active: true, className: style.menulist}} />
          {step === 0 ? null : <Blog />}
        </div>
      </Layout>
    )
  }
}

export default connect(({general}) => ({general}), (dispatch) =>
  bindActionCreators(generalActions, dispatch))(Jovenes)
