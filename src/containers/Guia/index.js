import React from 'react'
import style from './style.css'
import Layout from '../Layout/'
import seo from './seo'

class Guia extends React.Component {
  static seo = seo
  state = {step: 0}
  componentDidMount = () => window.scrollTo(0, 0)
  next = () => this.setState({step: 1})
  render = () => {
    const url = this.props.location.pathname.replace('/guia', '').replace('/', '')
    const section = ['jovenes', 'adultos', 'mayores'].indexOf(url)
    const color = ['#925152', '#002453', '#47774C'][section]
    const backgroundColor = ['#ED6C5C', '#60CFF1', '#87C400'][section]
    const isStep1 = this.state.step === 1

    const step0 = (
      <div style={{display: isStep1 ? 'none' : 'block'}}>
        <div className={style.header} style={{color}}>
          <div>
            <div style={{borderColor: color}}>
              <h1>Guías Previsionales</h1>
              <h2>
                Prepárate para anticipar las etapas y los sucesos de tu vida que requerirán un soporte económico.
              </h2>
            </div>
          </div>
        </div>
        <div
          className={style.resumen}
          style={{color}}>
          <p>
            Las guías previsionales te sensibilizan sobre las ventajas de prever tus necesidades económicas
            a futuro y pasar oportunamente a la práctica. Te explican los temas financieros más comunes y
            algunas incluyen fichas de trabajo que facilitan la tarea de planear y dar seguimiento al presupuesto
            personal/familiar. Todo ello, para fortalecer tu capacidad de ahorro previsional y de inversión.
          </p>
          <div>
            <a
              style={{color}}
              target='_blank'
              href='https://www.gob.mx/cms/uploads/attachment/file/116874/guia_sar_2016.pdf'>
              <img src={`/static/img/guide/imss.png`} />
              <h3>Guía del Sistema de Ahorro para el Retiro IMSS</h3>
              <h4>De: Comisión Nacional del Sistema de Ahorro para el Retiro (CONSAR)</h4>
            </a>
            <a
              style={{color}}
              target='_blank'
              href='https://www.jubilaciondefuturo.es/es/blog/claves-para-hacer-realidad-tus-suenos.html'>
              <img src={`/static/img/guide/claves.png`} />
              <h3>En busca de la tranquilidad. Claves para hacer realidad tus sueños</h3>
              <h4>De: Instituto BBVA de PENSIONES</h4>
            </a>
            <a
              style={{color}}
              target='_blank'
              href='https://www.dol.gov/sites/default/files/ebsa/about-ebsa/our-activities/resource-center/publications/savings-fitness-spanish.pdf'>
              <img src={`/static/img/guide/ahorro.png`} />
              <h3>Su Dinero y Futuro Económico: Una Guía para Ahorrar</h3>
              <h4>De: Departamento del Trabajo de EE.UU., Administración de Seguridad de Beneficios del Empleado.</h4>
            </a>
            <a
              style={{color}}
              target='_blank'
              href='https://www.fundacionmapfre.org/documentacion/publico/es/catalogo_imagenes/grupo.cmd?path=1093896'>
              <img src={`/static/img/guide/guia.png`} />
              <h3>Guía para tu jubilación</h3>
              <h4>De: Fundación MAPFRE</h4>
            </a>
          </div>
        </div>
      </div>
    )
    const step1 = (
      <div style={{display: isStep1 ? 'block' : 'none'}}>
        <iframe
          style={{width: '100%', height: '100%'}}
          src='//e.issuu.com/embed.html#31261013/53792752'
          frameBorder='0'
          allowFullScreen />
      </div>
    )
    return (
      <Layout {...this.props}>
        <div className={style.guia}>
          {step0}
          {step1}
          <div className={style.avisoLigas}>
            <p>
              Las ligas y los documentos incluidos en esta sección se consideran de acceso público
              al encontrarse disponibles en internet para la exploración de cualquier persona.
            </p>
            <p>
              Tienen la finalidad de proveer información al lector, así como incentivar la discusión
              y el debate de temas de educación financiera, previsional y en particular sobre el ahorro
              para el retiro en México.
            </p>
            <p>
              Sus contenidos, así como las conclusiones que de ellos se deriven, son responsabilidad
              exclusiva de sus autores y no reflejan necesariamente la opinión de la CONSAR.
            </p>
          </div>
        </div>

      </Layout>
    )
  }
}

export default Guia
