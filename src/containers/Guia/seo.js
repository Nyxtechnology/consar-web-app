
export default {
  title: 'Guía para el ahorro previsional: “Mi dinero, mi futuro” | CONSAR',
  description: ' Como crear tu presupuesto familiar o personal y ahorrar dinero para anticipar las etapas y los sucesos de tu vida que requerirán un soporte económico.',
  ogtitle: 'Guía para el ahorro previsional: “Mi dinero, mi futuro” | CONSAR',
  ogdescription: ' Como crear tu presupuesto familiar o personal y ahorrar dinero para anticipar las etapas y los sucesos de tu vida que requerirán un soporte económico.',
  keywords: [
    'Consar',
    'ahorro voluntario',
    'como ahorrar dinero',
    'cuánto ahorrar',
    'métodos de ahorro',
    'como sacar un presupuesto',
    'Presupuesto personal',
    'Presupuesto familiar',
    'Ahorro previsional',
    'finanzas personales',
    'Fondo de emergencias',
    'Guía para ahorrar'
  ]
}
