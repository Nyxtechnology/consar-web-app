import React from 'react'
import style from './style.css'
import wildcards from './wildcards'

const Step3 = ({section, selections, step, changeBg, bg}) => {
  const items = [...Object.values(selections), ...Object.values(wildcards)].slice(0, 6)
  const color = [['#925152', '#002453', '#47774C'], ['#ff6768', '#02c4f4', '#72c702']][bg][section]
  const bgcolor = ['#ff6b00', '#00bfd5', '#9678d2', '#6da33b', '#00b288', '#e5bab3']
  const darkerbgcolor = ['#ff6b00', '#00bfd5', '#9678d2', '#5e7748', '#2b654d', '#927571']
  const print = () => window.print()
  return (
    <div
      className={style.step3}
      style={{color, display: step === 2 ? 'flex' : 'none'}}>
      <img className={style.step3bg} src={`/static/img/itinerario/final/bg${bg}.png`} />
      <div className={style.fixedBtns}>
        <button onClick={print} style={{color}}>Imprimir</button>
        <button onClick={changeBg} style={{color}}>Cambiar Fondo</button>
      </div>
      <div className={style.step3container}
        style={{marginBottom: '4.5vw', justifyContent: 'flex-start', paddingRight: 'calc(100% / 7)'}}>
        {items.map((s, i) => i % 2 ? null : (
          <div key={i} style={{justifyContent: 'flex-end'}}>
            <div>
              {s.title ? <strong>{s.title}</strong> : null}
              {s.date ? <strong>{s.date}</strong> : null}
              <p style={s.wild ? {fontWeight: 'bold', color: '#888', fontSize: 'calc(14px + .8vw)'} : {fontWeight: 'normal'}}>{s.text}</p>
            </div>
            <div className={style.imgContainer}>
              <img src={`/static/img/itinerario/final/back${s.wild ? 'w' : i}.svg`} />
              <img src={`/static/img/itinerario/${s.name}.svg`} />
            </div>
          </div>
        ))}
      </div>
      <div className={style.step3Numberscontainer}>
        <img src={`/static/img/itinerario/final/hline.svg`} />
        {items.map((s, i) => (
          <div key={i} className={style.step3Line}
            style={{
              left: `calc(((100% / 5) * ${i}) - .5vw)`,
              marginTop: i % 2 ? '0' : '-4vw'
            }}>
            <img src={`/static/img/itinerario/final/line${i}.svg`} />
          </div>
        ))}
        {items.map((s, i) => (
          <div
            key={i}
            className={style.step3number}
            style={{left: `calc(((100% / 5) * ${i}) - 2vw)`}}>
            <img src={`/static/img/itinerario/final/number${i}.svg`} />
          </div>
        ))}
      </div>
      <div className={style.step3container}
        style={{marginTop: '4.5vw', justifyContent: 'flex-end', paddingLeft: 'calc(100% / 7)'}}>
        {items.map((s, i) => i % 2 === 0 ? null : (
          <div key={i} style={{justifyContent: 'flex-start'}}>
            <div className={style.imgContainer}>
              <img src={`/static/img/itinerario/final/back${s.wild ? 'w' : i}.svg`} />
              <img src={`/static/img/itinerario/${s.name}.svg`} />
            </div>
            <div>
              {s.title ? <strong>{s.title}</strong> : null}
              {s.date ? <strong>{s.date}</strong> : null}
              <p style={s.wild ? {fontWeight: 'bold', color: '#888', fontSize: 'calc(14px + .8vw)'} : {fontWeight: 'normal'}}>{s.text}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Step3
