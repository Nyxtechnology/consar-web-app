export default {
  'casa': {name: 'casa', title: 'Adquirir mi vivienda', text: '', video: 'fR7wPP2U79Q'},
  'negocio': {name: 'negocio', title: 'Emprender mi negocio', text: '', video: 'O0zVnYCpcbA'},
  'auto': {name: 'auto', title: 'Comprar un auto', text: '', video: 'wjmRti-VJz8'},
  'independencia': {name: 'independencia', title: 'Independizarme financieramente', text: '', video: '4Fspm9SgZv0'},
  'laboral': {name: 'laboral', title: 'Obtener el empleo que quiero', text: '', video: 'KtC_3WfzNdY'},
  'pension': {name: 'pension', title: 'Preparar mi jubilación', text: '', video: '-hlhdMihNOU'},
  'salud': {name: 'salud', title: 'Asegurar mi fondo para emergencias', text: '', video: 'yVBe--W9CHw'},
  'personal': {name: 'personal', title: 'Alcanzar mi meta profesional', text: '', video: 'S-prJ_hdEeg'},
  'estudios': {name: 'estudios', title: 'Realizar estudios superiores', text: '', video: 'Lt14Oo79k2I'},
  'pareja': {name: 'pareja', title: 'Vivir en pareja', text: '', video: 'DOiTNX-8CAQ'},
  'familia': {name: 'familia', title: 'Formar mi propia familia', text: '', video: 'qW4KF3_FMtw'},
  'viaje': {name: 'viaje', title: 'Lograr un viaje especial', text: '', video: 'FxtI7qfg-Y4'},
  'pasatiempo': {name: 'pasatiempo', title: 'Consolidar mi pasatiempo', text: '', video: '1g-BbA9iBK0'},
  'legado': {name: 'legado', title: 'Construir una herencia', text: '', video: 'YFDV7PVYngg'},
  'impulsar': {name: 'impulsar', title: 'Crecer mi negocio', text: '', video: 'q69i4YlhEiU'},
  'otro': {name: 'otro', title: 'Otro: Mi gran objetivo es…', text: '', video: 'PWzlZ5Gi9Ew'}
}
