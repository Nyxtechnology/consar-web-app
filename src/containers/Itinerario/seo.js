
export default {
  title: 'Diseña el Mapa de proyectos de la Aventura de tu vida | CONSAR ',
  description: 'Descubre y selecciona los proyectos clave que vivirás en la aventura de tu vida para que te generemos un plan para alcanzar tu metas. Rápido y Sencillo.',
  ogtitle: 'Diseña el Mapa de proyectos de la Aventura de tu vida | CONSAR',
  ogdescription: 'Descubre y selecciona los proyectos clave que vivirás en la aventura de tu vida para que te generemos un plan para alcanzar tu metas. Rápido y Sencillo.',
  keywords: [
    'Pensión IMSS',
    'Aventura de mi vida'
  ]
}
