import React from 'react'
import style from './style.css'
import {connect} from 'react-redux'
import omit from 'lodash/fp/omit'
import Step0 from './Step0'
import Step2 from './Step2'
import Step3 from './Step3b'
import moment from 'moment'
import Video from '../../components/Video/'
import Layout from '../Layout/'
import seo from './seo'
import options from '../Destinos/options'

class Itinerario extends React.Component {
  static seo = seo
  state = {step: 0, selections: {}, adding: null, calendar: null, video: null, info: null, bg: 0}
  componentDidMount = () => window.scrollTo(0, 0)
  add = data =>
    this.setState({adding: {userText: '', date: moment(new Date()).format('YYYY-MM-DD'), ...data}})
  select = data =>
    this.setState({selections: {...this.state.selections, [this.state.adding.name]: this.state.adding}, adding: null})
  toggleCalendar = data =>
    this.setState({calendar: this.state.calendar ? null : data})
  selectDate = data =>
    this.setState({adding: {...this.state.adding, date: data.value}, calendar: null})
  remove = (name, e) => {
    e.stopPropagation()
    this.setState({selections: omit(name, this.state.selections)})
  }
  next = () => {
    if (this.state.step === 0) return  this.setState({step: this.state.step + 1})
    if (this.state.step === 1 || Object.values(this.state.selections).length > 3) {
      this.setState({step: this.state.step + 1})
      this.setState({selections: {...this.state.selections, pension: options['pension']}})
    }
  }
  changeText = userText =>
    userText.length < 140
    ? this.setState({adding: {...this.state.adding, userText}})
    : null
  closeAddModal = () => this.setState({adding: null})
  setVideo = (video, e) => {
    if (e) e.stopPropagation()
    this.setState({video})
  }
  setInfo = (info, e) => {
    if (e) e.stopPropagation()
    this.setState({info})
  }
  changeBg = () => this.setState({bg: this.state.bg === 0 ? 1 : 0})
  render = () => {
    const {select, remove, next, state, toggleCalendar, selectDate, add, changeText, closeAddModal, setVideo, setInfo, changeBg} = this
    const {step} = state
    const url = this.props.location.pathname.match(/jovenes|adultos|mayores/)
    const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
    const color = ['#925152', '#002453', '#47774C'][section]
    const borderColor = ['#EF6A56', '#60CFF1', '#87C400'][section]
    const backgroundColor = ['#e88e82', '#60CFF1', '#87C400'][section]

    if (options[this.props.match.params.name]) {
      return (
        <div>
          {options[this.props.match.params.name].text({color, toggle: this.select, section: url[0]})}
        </div>
      )
    }

    return (
      <Layout {...this.props}>
        <Video
          toggle={() => setVideo(null)}
          step
          url={this.state.video} />
        <div className={style.container}>
          <Step0 {...{section, next, color, backgroundColor, step}} />
          <Step2 {...{closeAddModal, setVideo, setInfo, url: url[0], options,
            section, next, color, backgroundColor, borderColor, select, remove, toggleCalendar, selectDate, add, changeText, ...state}} />
          <Step3 {...{section, next, color, backgroundColor, changeBg, ...this.state}} />
        </div>
      </Layout>
    )
  }
}

export default connect(({general}) => ({general}), (dispatch) => ({}))(Itinerario)
