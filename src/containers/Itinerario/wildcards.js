export default {
  'aventura': {name: 'aventura', title: 'Tú eres el arquitecto de tu aventura.', wild: true},
  'crear': {name: 'crear', title: '"La única forma de predecir el futuro es crearlo" Peter Druker', wild: true},
  'arriesga': {name: 'arriesga', title: '¿Cómo puedes saber si es la decisión correcta si nunca la tomas? ¡Arriesga!', wild: true}
}
