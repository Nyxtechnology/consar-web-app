import React from 'react'
import style from './style.css'
import wildcards from './wildcards'
import window from 'global/window'

const loadImg = (url) => new Promise((resolve, reject) => {
  const img = new Image()
  img.src = url
  img.onload = () => resolve(img)
})

function measure (context, text, maxWidth) {
  if (!text) return 0
  let words = text.split(' ')
  let line = ''
  let lines = 0

  for (let n = 0; n < words.length; n++) {
    let testLine = line + words[n] + ' '
    let metrics = context.measureText(testLine)
    let testWidth = metrics.width
    if (testWidth > maxWidth && n > 0) {
      lines++
      line = words[n] + ' '
    } else {
      line = testLine
    }
  }
  lines++
  return lines
}
function wrapText (context, text, x, y, maxWidth, lineHeight) {
  if (!text) return 0
  let words = text.split(' ')
  let line = ''
  let lines = 0

  for (let n = 0; n < words.length; n++) {
    let testLine = line + words[n] + ' '
    let metrics = context.measureText(testLine)
    let testWidth = metrics.width
    if (testWidth > maxWidth && n > 0) {
      lines++
      context.fillText(line, x, y)
      line = words[n] + ' '
      y += lineHeight
    } else {
      line = testLine
    }
  }
  lines++
  context.fillText(line, x, y)
  return lines
}

class Step3 extends React.Component {
  componentDidMount = () => {
    this.refs.canvas.width = window.innerWidth
    this.refs.canvas.height = window.innerHeight
    this.ctx = this.refs.canvas.getContext('2d')
    this.drawIcons(this.props)
  }
  componentWillReceiveProps = (next) => {
    this.drawIcons(next)
  }
  drawIcons = ({selections, section, step, changeBg, bg}) => {
    const ctx = this.ctx
    const w = 1440
    const h = 800
    const rangeStep = ((w - (w * 0.2)) - (w * 0.2)) / 5
    const items = [...Object.values(selections), ...Object.values(wildcards)].slice(0, 6)
    const color = [['#925152', '#002453', '#47774C'], ['#ff6768', '#02c4f4', '#72c702']][bg][section]
    const bgcolor = ['#ff6b00', '#00bfd5', '#9678d2', '#6da33b', '#00b288', '#e5bab3']
    const darkerbgcolor = ['#ff6b00', '#00bfd5', '#9678d2', '#5e7748', '#2b654d', '#927571']
    const ciclesColors = ['#fe6a00', '#00bfd6', '#9778d4', '#6ca439', '#01b288', '#e5bab3']
    const wide = (w - (w * 0.4)) / 5

    this.refs.canvas.width = w
    this.refs.canvas.height = h

    ctx.textBaseline = 'middle'
    ctx.clearRect(0, 0, w, h)
    ctx.fillStyle = '#FFF'
    ctx.fillRect(0, 0, w, h)

    loadImg(`/static/img/itinerario/final/bg${bg}.png`)
    .then(bg => {
      this.ctx.drawImage(bg, 0, 0, w, h)

      ctx.lineWidth = 3
      ctx.beginPath()
      ctx.moveTo(w * 0.2, h / 2)
      ctx.lineTo(w * 0.8, h / 2)
      ctx.strokeStyle = color
      ctx.stroke()

      items.forEach((s, i) => {
        const d = w * 0.065
        const r = d / 2
        const t = r + d / 2
        ctx.strokeStyle = s.wild ? '#6E6E6E' : '#925052'
        ctx.fillStyle = s.wild ? '#E6E6E6' : ciclesColors[i]
        ctx.lineWidth = 3
        ctx.beginPath()
        ctx.arc((w / 5) + (rangeStep * i), (h / 2) + (i % 2 ? t : t * -1), r, 0, Math.PI*2, true)
        ctx.closePath()
        ctx.fill()
        ctx.stroke()

        ctx.beginPath()
        ctx.moveTo((w * 0.2) + (rangeStep * i), (h / 2) + (i % 2 ? 30 : 30 * -1))
        ctx.lineTo((w * 0.2) + (rangeStep * i), h / 2)
        ctx.lineWidth = 3
        ctx.strokeStyle = color
        ctx.stroke()

        ctx.strokeStyle = s.wild ? '#6E6E6E' : '#925052'
        ctx.fillStyle = s.wild ? '#E6E6E6' : ciclesColors[i]
        ctx.lineWidth = 2
        ctx.beginPath()
        ctx.arc((w * 0.2) + (rangeStep * i), (h / 2), w * 0.01, 0, Math.PI*2, true)
        ctx.closePath()
        ctx.fill()
        ctx.stroke()

        ctx.strokeStyle = s.wild ? '#6E6E6E' : '#925052'
        ctx.fillStyle = s.wild ? '#E6E6E6' : ciclesColors[i]
        ctx.lineWidth = 3
        ctx.beginPath()
        ctx.arc((w * 0.2) + (rangeStep * i), (h / 2) + (i % 2 ? 30 : 30 * -1), w * 0.002, 0, Math.PI*2, true)
        ctx.closePath()
        ctx.fill()
        ctx.stroke()

        loadImg(`/static/img/itinerario/${s.name}.png`)
        .then(icon => {
          const imgSize = w * 0.065
          const largo = icon.height > icon.width ? imgSize : icon.height / icon.width * imgSize
          const ancho = icon.width > icon.height ? imgSize : icon.width / icon.height * imgSize
          ctx.drawImage(icon, (w / 5) + (rangeStep * i) - (ancho / 2), ((h / 2) - (largo / 2)) + (i % 2 ? largo : -largo), ancho, largo)
        })

        const titleSize = (w * 1.5) / 100
        const titleLineHeight = titleSize
        const textSize = (w * 1.1) / 100
        const textLineHeight = textSize + (textSize * 0.35)
        const margin = d * 1.7
        ctx.fillStyle = s.wild ? '#888' : color
        ctx.textAlign = 'center'
        if (i % 2 === 0) {
          ctx.font = titleSize + 'px Arial'
          const titleLines = measure(ctx, s.title, wide * 2)
          ctx.font = textSize + 'px Arial'
          const textLines = measure(ctx, s.userText, wide * 2) - 1
          const textY = (h / 2) - margin - (textLines * textLineHeight)
          const dateY = textY - textLineHeight
          const titleY = dateY - (titleLines * titleLineHeight)
          wrapText(ctx, s.userText, ((w * 0.2) + (rangeStep * i)), textY, wide * 2, textLineHeight)
          wrapText(ctx, s.date, ((w * 0.2) + (rangeStep * i)), dateY, wide * 2, textLineHeight)
          ctx.font = titleSize + 'px Arial'
          wrapText(ctx, s.title, ((w * 0.2) + (rangeStep * i)), titleY, wide * 2, titleLineHeight)
        } else {
          ctx.font = titleSize + 'px Arial'
          const titleLines = measure(ctx, s.title, wide * 2) - 1
          const titleY = h / 2 + margin
          const dateY = titleY + textLineHeight + (titleLines * titleLineHeight)
          const textY = dateY + textLineHeight
          wrapText(ctx, s.title, ((w * 0.2) + (rangeStep * i)), titleY, wide * 2, titleLineHeight)
          ctx.font = textSize + 'px Arial'
          wrapText(ctx, s.date, ((w * 0.2) + (rangeStep * i)), dateY, wide * 2, textLineHeight)
          wrapText(ctx, s.userText, ((w * 0.2) + (rangeStep * i)), textY, wide * 2, textLineHeight)
        }
      })
      ctx.fillStyle = color
      ctx.textAlign = 'center'
      ctx.font = '40px Arial'
      ctx.fillText('Mapa de mis proyectos de vida', 700, 70)
      ctx.font = '28px Arial'
      ctx.fillText('Un proyecto sin acciones nunca dejará de ser proyecto.', 700, 710)
      ctx.fillText('¿Cuáles son mis siguientes pasos para avanzar hacia mis objetivos?', 700, 740)
    })
  }
  download = e => {
    e.target.href = this.refs.canvas.toDataURL()
    e.target.download = 'Consar'
  }
  render = () => {
    const {section, step, changeBg, bg} = this.props
    const color = [['#925152', '#002453', '#47774C'], ['#ff6768', '#02c4f4', '#72c702']][bg][section]
    const print = () => window.print()
    return (
      <div className={style.step3b} style={{color, display: step === 2 ? 'flex' : 'none'}} onClick={() => this.drawIcons(this.props)}>
        <div className={style.fixedBtns}>
          <a ref='download' onClick={this.download} style={{color}}>Descargar</a>
          <button onClick={print} style={{color}} className={style.printBtn}>Imprimir</button>
          <button onClick={changeBg} style={{color}}>Cambiar Fondo</button>
        </div>
        <canvas ref='canvas' style={{width: '100vw', height: '55vw'}} />
      </div>
    )
  }
}

export default Step3
