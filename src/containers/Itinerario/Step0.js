import React from 'react'
import style from './style.css'

const Step0 = ({color, backgroundColor, section, next, step}) => (
  <div
    className={style.step0} onClick={next}
    style={{color, display: step === 0 ? 'flex' : 'none'}}>
    <div className={style.text}>
      <h1>Mapa de proyectos</h1>
      <p>
        Tu mapa de proyectos te permite seleccionar y encarrilar
        las principales metas que deseas alcanzar
        en la aventura de tu vida.
      </p>
      <p>
        Conoce los 16 proyectos que te presentamos a continuación
        y selecciona el ícono de hasta 5 de ellos, del más al menos importante.
      </p>
      <button onClick={next} style={{backgroundColor}}>¡Listos! Iniciemos</button>
    </div>
    <div><img src={`/static/img/itinerario/cloudsbags${section}.svg`} /></div>
  </div>
)

export default Step0
