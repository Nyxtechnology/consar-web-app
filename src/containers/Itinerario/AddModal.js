import React from 'react'
import style from './style.css'

const addModal = (props) => {
  const {
    select,
    adding,
    toggleCalendar,
    changeText,
    section,
    closeAddModal,
    calendar,
    backgroundColor,
    setVideo,
    setInfo,
    url
  } = props

  return (
    <div className={style.addModal} style={{display: calendar ? 'none' : 'flex'}}>
      <div>
        <div>
          <h2>{adding.title}</h2>
          <img src={`/static/img/itinerario/${adding.name}.svg`} />
        </div>
        <div className={style.addBody}>
          <div>
            <label>Paso 1: Conoce el proyecto</label>
            <button
              style={{background: backgroundColor}}
              onClick={e => setVideo(adding.video, e)}>Ver Video</button>
            <a href={`/${url}/proyectos/${adding.name}`} style={{background: backgroundColor}} target='_blank'>Pasos a seguir</a>
          </div>
          <div>
            <label>Paso 2: ¿Cuál es tu primera acción para lograrlo?</label>
            <textarea
              placeholder='Ejemplo: Investigar, comparar, elegir y destinar el 5% de mis ingresos para este proyecto…'
              value={adding.userText} onChange={e => changeText(e.target.value)} />
          </div>
          <div>
            <label>Paso 3: ¿En qué fecha cumplirás esa primera acción?</label>
            <input
              value={adding.date}
              onClick={() => toggleCalendar({id: adding.name, value: adding.date})} />
          </div>
        </div>
        <div className={style.addBtns}>
          <button onClick={select}>Agregar</button>
          <button onClick={closeAddModal}>Cancelar</button>
        </div>
      </div>
    </div>
  )
}

export default addModal
