import React from 'react'
import style from './style.css'
import options from '../Destinos/options'
import Calendar from '../../components/Calendar/'
import AddModal from './AddModal'
import {Link} from 'react-router-dom'

const Step2 = (props) => {
  const {
    color,
    backgroundColor,
    borderColor,
    step,
    section,
    next,
    selections,
    select,
    remove,
    calendar,
    selectDate,
    toggleCalendar,
    adding,
    closeAddModal,
    changeText,
    url
  } = props

  return (
    <div className={style.step2} style={{color, display: step === 1 ? 'block' : 'none'}}>
      {calendar
        ? <div className={style.calendar}><Calendar date={calendar} onChange={selectDate} toggle={toggleCalendar} /></div>
        : null
      }
      {adding
        ? AddModal(props)
        : null
      }
      <h1>Mapa de proyectos</h1>
      <div className={style.optionsContainer}>
        <div className={style.instructions}>
          <h3>Instrucciones:</h3>
          <ul>
            <li><strong>1.</strong> Conoce los siguientes proyectos de vida.</li>
            <li><strong>2.</strong> Selecciona hasta 5, del más al menos importante.</li>
          </ul>
        </div>
        <div className={style.icons}>
          {Object.values(options).map((o, i) => (
            <div
              onClick={() => Object.values(props.selections).length < 5 ? selections[o.name] ? props.add(selections[o.name]) : props.add(o) : null}
              className={style.icon} key={i} style={selections[o.name] ? {borderColor} : {}}>
              <div>
                <img src={`/static/img/itinerario/${o.name}.svg`} />
                <h2>{o.title}</h2>
              </div>
              <div>
                <button onClick={e => props.setVideo(o.video, e)}>Ver Video</button>
                <a href={`/${url}/proyectos/${o.name}`} target='_blank' style={{color}}>Pasos a seguir</a>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className={style.selections}>
        {[...Array(parseInt(5)).keys()].map((s, i) => {
          const value = Object.values(selections)[i] || null
          return (
            <div
              key={i} style={{borderColor}}
              onClick={() => value && props.selections[value.name] ? props.add(value) : null}>
              <div>
                <h2>{i + 1}</h2>
                {value ? <i onClick={e => remove(value.name, e)} className='fa fa-times' /> : null}
              </div>
              <img
                style={{width: value ? '80%' : 0, transition: value ? 'all .2s ease' : ''}}
                src={`/static/img/itinerario/final/${value ? value.name : 'casa'}${section}.svg`} />
              {value ? <h3>{value.title}</h3> : null}
            </div>
          )
        })}
        <div style={{borderColor}}>
          <div>
            <h2>6</h2>
          </div>
          <img style={{width: '80%'}} src={`/static/img/itinerario/final/pension${section}.svg`} />
          <h3>{options.pension.title}</h3>
        </div>
      </div>
      <div className={style.next}>
        <h4>
          Selecciona por lo menos 3 proyectos de vida para continuar. Si puedes establecerte metas
          para alcanzar tus proyectos, aplica la misma técnica para alcanzar la calidad de vida que
          algún día TE AGRADECERÁS infinitamente. Prepara tu jubilación, desde ahora.
        </h4>
        <button onClick={Object.values(props.selections).length < 3 ? null : next} style={{backgroundColor}}>
          Siguiente
        </button>
      </div>
    </div>
  )
}

export default Step2
