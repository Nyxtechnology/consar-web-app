import React from 'react'
import style from './style.css'

export default function Others (props) {
  const {section, color} = props
  return (
    <div className={style.others}>
      <div>
        <a target='_blank' href='http://www.consar.gob.mx/gobmx/Aplicativo/calculadora/imss/CalculadoraIMSS.aspx'>
          <img src={`/static/img/calculator/imss${section}.svg`} />
          <h3 style={{color}}>Calculadora para trabajadores que cotizan al IMSS</h3>
        </a>
        <a target='_blank' href='http://www.consar.gob.mx/gobmx/Aplicativo/calculadora/issste/CalculadoraISSSTE.aspx'>
          <img src={`/static/img/calculator/issste${section}.svg`} />
          <h3 style={{color}}>Calculadora para trabajadores que cotizan al ISSSTE (Régimen de cuentas individuales)</h3>
        </a>
        <a target='_blank' href='http://www.consar.gob.mx/gobmx/Aplicativo/calculadora/independientes/CalculadoraIndi.aspx'>
          <img src={`/static/img/calculator/ti${section}.svg`} />
          <h3 style={{color}}>Calculadora para trabajadores independientes</h3>
        </a>
      </div>
    </div>
  )
}
