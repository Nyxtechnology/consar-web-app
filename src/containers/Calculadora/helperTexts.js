import React from 'react'

export default [
  {title: '', text: 'Elije la visualización que más te agrade para que veas con el tiempo, cuánto rendimiento le suma el interés compuesto a tus ahorros.'},
  {title: '', text: 'Selecciona el monto de tu primera aportación. También puedes escribirlo directamente en el recuadro. Puede ser un dinero que ahorraste antes y ahora quieres que te dé ganancias.'},
  {title: '', text: 'Selecciona el monto del depósito que deseas hacer periódicamente a tu cuenta de ahorro.'},
  {title: '', text: 'Selecciona la periodicidad, es decir, cada cuándo harás los depósitos a tu cuenta de ahorro.'},
  {title: '', text: <div>Selecciona un porcentaje de ganancia que obtendrías con tus ahorros. Por ejemplo, averigua <a href='https://www.gob.mx/consar/articulos/indicador-de-rendimiento-neto' target='_blank'>aquí</a> la tasa de rendimiento que te da tu Afore de acuerdo a tu edad.</div>},
  {title: '', text: 'Selecciona el número de años que tus ahorros se invertirán y reinvertirán una y otra vez. A mayor plazo, mayor es el crecimiento del interés compuesto.'}
]
