import React from 'react'
import chunk from 'lodash/chunk'
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Label} from 'recharts'

const downsample = (data, period) =>
  chunk(data, Math.ceil(data.length / period))
  .map((c, i) => ({...c.pop(), year: i + 1}))

export default class Graph extends React.Component {
  state = {data: []}
  componentDidMount = () => {
    this.processData(this.props.data, this.props.period)
  }
  componentWillReceiveProps = next => {
    this.processData(next.data, next.period)
  }
  processData = (rawdata, period) => {
    const data = downsample(rawdata, period)
      .map(i => ({
        ...i,
        totalInterest: parseFloat(i.totalInterest),
        periodicalInput: parseFloat(i.periodicalInput),
        totalCapital: parseFloat(i.totalCapital),
        initialInput: parseFloat(i.initialInput)
      }))
    this.setState({data})
  }
  render = () => {
    const width = this.props.general.w < 900 ? this.props.general.w : this.props.general.w / 2
    return (
      <div>
        <BarChart width={width} height={width * 0.8} data={this.state.data}
          margin={{top: 5, right: 30, left: 20, bottom: 5}}>
          <XAxis dataKey='year'>
          </XAxis>
          <YAxis datakey='value' tickFormatter={e => '$' + e}>
          </YAxis>
          <CartesianGrid strokeDasharray='3 3' />
          <Tooltip labelFormatter={l => 'Año ' + l} formatter={v => '$' + v.toMoney()} />
          <Legend verticalAlign='top' />
          <Bar name='Depósito inicial' stackId='a' dataKey='initialInput' stroke='rgba(75, 192, 192, 1)' fill='rgba(75, 192, 192, 1)' />
          <Bar name='Aportación voluntaria' stackId='a' dataKey='periodicalInput' stroke='rgba(54, 162, 235, 1)' fill='rgba(54, 162, 235, 1)' />
          <Bar name='Rendimiento' stackId='a' dataKey='totalInterest' stroke='rgba(255, 206, 86, 1)' fill='rgba(255,99,132,1)' />
        </BarChart>
        <div style={{textAlign: 'center'}}>Años</div>
      </div>
    )
  }
}
