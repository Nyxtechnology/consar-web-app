import React from 'react'
import style from './style.css'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as calculatorActions from '../../actions/calculator'
import * as generalActions from '../../actions/general'
import seo from './seo'
import Layout from '../Layout/'
import Header from './Header'
import Calculator from './Calculator'
import Animation from './Animation'
import Graph from './Graph'
import Others from './Others'
import Help from './Help'

Number.prototype.toMoney = function(n, x) { // eslint-disable-line
  const decimals = n || 2
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (decimals > 0 ? '\\.' : '$') + ')'
  return this.toFixed(Math.max(0, ~~decimals)).replace(new RegExp(re, 'g'), '$&,')
}
String.prototype.toMoney = function(n, x) { // eslint-disable-line
  const decimals = n || 2
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (decimals > 0 ? '\\.' : '$') + ')'
  return parseFloat(this || 0).toFixed(Math.max(0, ~~decimals)).replace(new RegExp(re, 'g'), '$&,')
}

class CalculatorContainer extends React.Component {
  componentDidMount = () => window.scrollTo(0, 0)
  render = () => {
    const url = this.props.location.pathname.match(/jovenes|adultos|mayores/)
    const section = ['jovenes', 'adultos', 'mayores'].indexOf(url[0])
    const color = ['#ed6c5c', '#3ac2ed', '#96cd2d'][section]
    const background = ['#de716b', '#3ac2ed', '#88c227'][section]
    const inputBg = ['#e4aaa7', '#44afd0', '#6db04a'][section]
    return (
      <Layout {...this.props}>
        <Help {...this.props} />
        <div className={style.calculadora}>
          <Header {...{...this.props, section, background, color}} />
          <div className={style.video}>
            <h3 style={{color}}>El siguiente video te explica qué es el interés compuesto.</h3>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/yR1NOFyj0AI' frameBorder='0' allowFullScreen />
          </div>
          <div className={style.calculatorContainer}>
            <Calculator {...{...this.props, background, inputBg}} />
            <div style={{position: 'relative', width: '100%'}}>
              <div style={{display: this.props.calculator.view === 1 ? 'block' : 'none'}}>
                <Animation {...{...this.props, section}} />
              </div>
              {this.props.calculator.view === 0
                ? <Graph data={this.props.calculator.history} period={this.props.calculator.period} general={this.props.general} />
                : null
              }
            </div>
          </div>
          <h3>
            <a style={{color, margin: '0 1em'}} href='https://www.gob.mx/consar/articulos/indicador-de-rendimiento-neto'>
              Consulta aquí el rendimiento neto que te ofrece tu AFORE
            </a>
          </h3>
          <Others {...{...this.props, section, color}} />
          <img className={style.rocks} src={`/static/img/calculator/rocks${section}.png`} />
        </div>
      </Layout>
    )
  }
}

CalculatorContainer.seo = seo

export default connect(({general, calculator}) => ({general, calculator}),
  (dispatch) => bindActionCreators({...calculatorActions, ...generalActions}, dispatch))(CalculatorContainer)
