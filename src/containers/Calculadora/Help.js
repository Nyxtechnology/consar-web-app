import React from 'react'
import style from './style.css'

export default function Help (props) {
  const {modal, value} = props.general
  if (modal !== 'help') return null
  return (
    <div className={style.helpModal}>
      <div>
        <div>
          <div>{value.title}</div>
          <i className='fa fa-times' />
        </div>
        <div>
          {value.text}
        </div>
      </div>
    </div>
  )
}
