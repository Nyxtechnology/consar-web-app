
export default {
  title: 'Calculadoras de ahorro para el retiro | CONSAR ',
  description: '¿Ya sabes cuánto dinero ahorrar para el retiro? Calculadoras de ahorro para evaluar pensión al jubilarse. Trabajadores IMSS, ISSSTE e Independientes.',
  ogtitle: '¿Calculadoras de ahorro para el retiro | CONSAR',
  ogdescription: '¿Ya sabes cuánto dinero ahorrar para el retiro? Calculadoras de ahorro para evaluar pensión al jubilarse. Trabajadores IMSS, ISSSTE e Independientes.',
  keywords: [
    'Pensión IMSS',
    'Pensión ISSSTE',
    'Pensión Trabajadores Independientes',
    'consar afore',
    'ahorro para el retiro',
    'ahorro voluntario',
    'como ahorrar dinero',
    'como sacar un presupuesto',
    'finanzas personales',
    'cuánto ahorrar',
    'métodos de ahorro',
    'rendimientos afores',
    'Jubilación ',
    'Retiro'
  ]
}
