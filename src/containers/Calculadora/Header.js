import React from 'react'
import style from './style.css'

export default function (props) {
  const {section, background, color} = props
  return (
    <div className={style.header}>
      <img className={style.hcloud1} src={`/static/img/calculator/darkc${section}.svg`} />
      <img className={style.hcloud2} src={`/static/img/calculator/darkc${section}.svg`} />
      <img className={style.hcloud3} src={`/static/img/calculator/darkc${section}.svg`} />
      <img className={style.hcloud4} src={`/static/img/calculator/darkc${section}.svg`} />
      <img className={style.hcloud5} src={`/static/img/calculator/darkc${section}.svg`} />
      <div style={{background}}>
        <div>
          <h1>
            Calculadoras de ahorro para el retiro
          </h1>
          <h2>
            Haz cuentas y obtén los resultados que más le sumen a tu ahorro para el retiro.
          </h2>
        </div>
      </div>
    </div>
  )
}
