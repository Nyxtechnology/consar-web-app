import React from 'react'
import style from './style.css'
import {coins, silver, cash} from './data.js'

const Icon = props =>
  <img
    src={`/static/img/calculator/${props.icon}.svg`}
    style={{
      position: 'absolute',
      zIndex: 2,
      ...props.data
    }} />

const nrange = (n, min, max) => parseInt(Math.min(max, Math.max(min, n)))

export default function Animation (props) {
  const {section, calculator} = props
  const initialInput = calculator.initialInput
  const periodicalInput = calculator.periodicalInput
  const capital = calculator.totalCapital
  return (
    <div className={style.pigContainer} style={{backgroundImage: `url(/static/img/calculator/waves${section}.svg)`}}>
      <img className={style.cloud1} src={`/static/img/calculator/lightc.svg`} />
      <img className={style.cloud2} src={`/static/img/calculator/lightc.svg`} />
      <img className={style.cloud3} src={`/static/img/calculator/lightc.svg`} />
      <img className={style.cloud4} src={`/static/img/calculator/lightc.svg`} />
      <img className={style.cloud5} src={`/static/img/calculator/lightc.svg`} />
      <img className={style.umbrella} src={`/static/img/calculator/umbrella${section}.svg`} />
      <img className={style.tree} src={`/static/img/calculator/tree${section}.svg`} />
      <img className={style.water} src={`/static/img/calculator/water${section}.svg`} />
      <img className={style.island} src={`/static/img/calculator/island${section}.svg`} />
      <img className={style.pig} src={`/static/img/calculator/pig${section}.svg`} />
      {[...Array(nrange(initialInput / 10000, 0, 27)).keys()].map((key) => <Icon {...{data: coins[key], key, icon: 'coin2'}} />)}
      {[...Array(nrange(periodicalInput / 1000, 0, 27)).keys()].map((key) => <Icon {...{data: silver[key], key, icon: 'gcoin'}} />)}
      {[...Array(nrange(capital / 10000, 0, 60)).keys()].map((key) => <Icon {...{data: cash[key], key, icon: 'cash'}} />)}
      <img className={style.coco} src={`/static/img/calculator/coco${section}.svg`} />
      <img className={style.bag} src={`/static/img/calculator/bag${section}.svg`} />
    </div>
  )
}
