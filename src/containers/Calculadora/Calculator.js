import React from 'react'
import style from './style.css'
import helperTexts from './helperTexts'
const isNumeric = n => !isNaN(parseFloat(n)) && isFinite(n)

export default function Calculator (props) {
  const {calculator, background, calculatorChange, inputBg} = props
  const view = props.calculator.view
  const border = '3px solid #FFF'
  const change = (e) => {
    const {value, name} = e.target
    const nvalue = value.replace(/$|%/g, '').replace(/[^\d.]/g, '')
    const clean = /^0+?[^.]/.test(nvalue) ? nvalue.replace('0', '') : nvalue
    if (name === 'frequency') calculatorChange({[name]: parseFloat(value)})
    if (name === 'period') calculatorChange({[name]: parseFloat(value) < 1 ? 1 : parseFloat(value) > 80 ? 80 : value})
    else calculatorChange({[name]: clean || 0})
  }
  const {
    frequency,
    initialInput,
    periodicalInput,
    interest,
    period,
    totalSaving,
    totalInterest,
    totalCapital
  } = calculator

  return (
    <div className={style.calculator} style={{background}}>
      <h2>Calculadora de interés compuesto</h2>
      <div className={style.graphSelector}>
        <button
          className={style.help}
          onClick={() => props.setModal({modal: 'help', value: helperTexts[0]})}>
          <i className='fa fa-question' />
        </button>
        <button
          onClick={() => props.setView(0)} style={{background: inputBg, border: view === 0 ? border : 'none'}}>Gráfica</button>
        <button
          onClick={() => props.setView(1)} style={{background: inputBg, border: view === 1 ? border : 'none'}}>Animación</button>
      </div>
      <div>
        <label>1. Depósito inicial</label>
        <button
          className={style.help}
          onClick={() => props.setModal({modal: 'help', value: helperTexts[1]})}>
          <i className='fa fa-question' />
        </button>
        <div>
          <input
            style={{background: inputBg}}
            name='initialInput' value={'$' + initialInput} onChange={change} />
          <input
            type='range'
            max={1000000}
            min={0}
            style={{background: inputBg, height: '15px'}}
            name='initialInput' value={initialInput} onChange={change} />
        </div>
      </div>
      <div>
        <label>2. Aportación voluntaria</label>
        <button
          className={style.help}
          onClick={() => props.setModal({modal: 'help', value: helperTexts[2]})}>
          <i className='fa fa-question' />
        </button>
        <div>
          <input
            style={{background: inputBg}}
            name='periodicalInput' value={'$' + periodicalInput} onChange={change} />
          <input
            type='range'
            max={50000}
            min={0}
            style={{background: inputBg, height: '15px'}}
            name='periodicalInput' value={periodicalInput} onChange={change} />
        </div>
      </div>
      <div>
        <label>3. Frecuencia de aportación</label>
        <button
          className={style.help}
          onClick={() => props.setModal({modal: 'help', value: helperTexts[3]})}>
          <i className='fa fa-question' />
        </button>
        <div>
          <select
            style={{background: inputBg}}
            name='frequency' value={frequency} onChange={change}>
            <option value={52}>Semanal</option>
            <option value={24}>Quincenal</option>
            <option value={12}>Mensual</option>
            <option value={4}>Trimestral</option>
            <option value={2}>Semestral</option>
            <option value={1}>Anual</option>
          </select>
        </div>
      </div>
      <div>
        <label>4. Tasa de rendimiento anual</label>
        <button
          className={style.help}
          onClick={() => props.setModal({modal: 'help', value: helperTexts[4]})}>
          <i className='fa fa-question' />
        </button>
        <div>
          <input
            style={{background: inputBg}}
            name='interest' value={interest + '%'} onChange={change} />
          <input
            type='range'
            max={5}
            min={0}
            style={{background: inputBg, height: '15px'}}
            name='interest' value={interest} onChange={change} />
        </div>
      </div>
      <div>
        <label>5. Periodo de ahorro (años)</label>
        <button
          className={style.help}
          onClick={() => props.setModal({modal: 'help', value: helperTexts[5]})}>
          <i className='fa fa-question' />
        </button>
        <div>
          <input
            style={{background: inputBg}}
            type='number' name='period' min={20} value={period} onChange={change} />
          <input
            type='range'
            max={70}
            min={20}
            style={{background: inputBg, height: '15px'}}
            name='period' value={period} onChange={change} />
        </div>
      </div>
      <div>
        <label><strong>6. Total depositado</strong></label>
        <div><input value={'$' + totalSaving.toMoney()} disabled /></div>
      </div>
      <div>
        <label><strong>7. Total de rendimientos por interés compuesto</strong></label>
        <div><input value={'$' + totalInterest.toMoney()} disabled /></div>
      </div>
      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
        <label><strong>8. Monto total</strong></label>
        <div style={{fontWeight: 'bolder', fontSize: '1.1em'}}>
          {'$' + totalCapital.toMoney()}
        </div>
      </div>
    </div>
  )
}
