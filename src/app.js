import React from 'react'
import {render} from 'react-dom'
import {Router, Switch, StaticRouter} from 'react-router-dom'
import injectTapEventPlugin from 'react-tap-event-plugin'
import debounce from 'lodash/debounce'
import configureStore from './store/configureStore'
import {Provider} from 'react-redux'
import history from './helpers/history'
import routes from './routes'
import {ensureReady} from './helpers/rrv4Helpers'
import {renderRoutes} from 'react-router-config';
import {AppContainer} from 'react-hot-loader'
import {I18nextProvider} from 'react-i18next'
import i18n from './i18n'

injectTapEventPlugin()
if (process.env.NODE_ENV === 'development') console.log(process.env.api)
const store = configureStore(window.initialState)
const resize = debounce(() => store.dispatch({type: 'setDimensions', h: window.innerHeight, w: window.innerWidth}), 100)
window.addEventListener('resize', resize)
resize()

const renderApp = (routes) => ensureReady(routes).then(() =>
  render((
    <I18nextProvider i18n={i18n}>
      <AppContainer>
        <Provider store={store}>
          <Router history={history}>
            {renderRoutes(routes, {})}
          </Router>
        </Provider>
      </AppContainer>
    </I18nextProvider>
  ), document.getElementById('root')))

renderApp(routes)

if (module.hot) {
  module.hot.accept('./routes', () => renderApp(require('./routes').default))
}
