var path = require('path')
var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var nodeExternals = require('webpack-node-externals')
var rootPath = process.cwd()
const api = process.env.api || 'http://189.202.226.159:3009/'
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

var client = {
  mode: 'production',
  context: path.join(rootPath, 'src'),
  name: 'client',
  entry: './app.js',
  output: {
    filename: '[name].js',
    path: path.join(rootPath, 'static/production'),
    publicPath: '/static/production/'
  },
  optimization: {
    minimize: true,
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[name].css"
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'target': JSON.stringify('production'),
        'NODE_ENV': JSON.stringify('production'),
        'targetenv': JSON.stringify('browser'),
        'api': JSON.stringify(api)
      }
    }),
    //new BundleAnalyzerPlugin()
  ],
  module: {
    rules: [
      { test: /\.js$/, use: ['babel-loader'], include: path.join(__dirname, 'src') },
      { test: /\.(css)$/,
          loader: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              query: {minimize: true, modules: true, importLoaders: 1, localIdentName: '[name]__[local]___[hash:base64:5]'}
            },
            'postcss-loader'
          ]
      }
    ]
  }
}

var server = {
  mode: 'production',
  name: 'server',
  target: 'node',
  externals: [nodeExternals()],
  context: path.join(rootPath, 'src'),
  entry: './routes.js',
  output: {
    filename: 'server.routes.js',
    path: path.join(rootPath, 'build/production'),
    libraryTarget: 'commonjs2',
    publicPath: '/build/production/'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'target': JSON.stringify('production'),
        'NODE_ENV': JSON.stringify('production'),
        'targetenv': JSON.stringify('server'),
        'api': JSON.stringify(api)
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/, loader: 'babel-loader', exclude: /node_modules/,
        options: {
          'presets': ['env', 'react'],
          'plugins': [
              'transform-runtime',
              'dynamic-import-node',
              'transform-object-rest-spread',
              'babel-plugin-transform-class-properties',
            ],
        }
      },
      { test: /\.(css)$/,
        use: [
          {
            loader: 'css-loader/locals',
            query: {modules: true, importLoaders: 1, localIdentName: '[name]__[local]___[hash:base64:5]'}
          },
          'postcss-loader'
        ]
      }
    ]
  }
}

module.exports = [client, server]
