import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../src/actions/unlockables'

jest.mock('../src/helpers/services')

const mockStore = configureMockStore([thunk])

describe('unlockables actions', () => {
  it('should create an action to set an unlockable as used', () => {
    expect(actions.unlockablesSetUsed()).toEqual({ type: 'unlockablesSetUsed' })
  })
  it('should create an action to set an unlockable as saved', () => {
    expect(actions.unlockablesSetSaved()).toEqual({ type: 'unlockablesSetSaved' })
  })
  it('should create an action to get the next unlockable', () => {
    expect(actions.unlockablesNext()).toEqual({ type: 'unlockablesNext' })
  })
  it('should push an array of valid unlockables', () => {
    const store = mockStore({unlockables: {process: null, queue: []}, user: {id: 1}})
    const data = [{id: 1}]
    return store.dispatch(actions.unlockablesPush(data))
    .then(() => expect(store.getActions()).toEqual([
      actions.unlockablesPushAttempt(),
      actions.unlockablesPushSuccess(data)
    ]))
  })
})
