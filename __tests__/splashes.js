import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../src/actions/splash'

jest.mock('../src/helpers/services')

const mockStore = configureMockStore([thunk])

describe('splashes actions', () => {
  it('should push valid events', () => {
    const store = mockStore({splash: {process: null, queue: []}, user: {id: 1}})
    const data = [{id: 1}]
    return store.dispatch(actions.splashPushEvents(data))
    .then(() => expect(store.getActions()).toEqual([
      actions.splashPushAttempt(),
      actions.splashPushSuccess(data)
    ]))
  })
})
