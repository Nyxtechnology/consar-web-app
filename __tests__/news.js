import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../src/actions/news'

jest.mock('../src/helpers/services')

const mockStore = configureMockStore([thunk])

describe('news actions', () => {
  it('should create an action to attempt load', () => {
    expect(actions.newsLoadAttempt()).toEqual({ type: 'newsLoadAttempt' })
  })
  it('should create an action to success load', () => {
    const data = 'data'
    expect(actions.newsLoadSuccess(data)).toEqual({ type: 'newsLoadSuccess', data })
  })
  it('should create an action to error load', () => {
    const data = 'data'
    expect(actions.newsLoadError(data)).toEqual({ type: 'newsLoadError', data })
  })
  it('it fetch the news', () => {
    const store = mockStore({news: {process: null, news: []}})
    return store.dispatch(actions.newsLoad()).then(() =>
      expect(store.getActions()).toEqual([
        actions.newsLoadAttempt(),
        actions.newsLoadSuccess([{id: 1}])
      ]))
  })
})
