require('babel-register', {
  "presets": ["env", "react"],
  "plugins": [
      "transform-runtime",
      "dynamic-import-node",
      "transform-object-rest-spread",
      "babel-plugin-transform-class-properties",
    ],
})
require('./src/server')
