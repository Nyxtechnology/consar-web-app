var path = require('path')
var webpack = require('webpack')
const api = process.env.api || 'http://localhost:3009/' // 'http://189.202.226.159:3009/'

module.exports = {
  mode: 'development',
  entry: [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client',
    path.join(__dirname, './src/app.js')
  ],
  output: {
    path: path.resolve('static'),
    filename: '[name].js',
    publicPath: '/static/development/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development'),
        'target': JSON.stringify('development'),
        'targetenv': JSON.stringify('browser'),
        'api': JSON.stringify(api)
      }
    })
  ],
  module: {
    rules: [
      { test: /\.js$/, use: ['babel-loader'], include: path.join(__dirname, 'src') },
      { test: /\.(css)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { modules: true, importLoaders: 1, localIdentName: '[name]__[local]___[hash:base64:5]' }
          },
          'postcss-loader'
        ]
      }
    ]
  }
}
